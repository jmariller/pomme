<?php

// Private channel for a game
Broadcast::channel('games.{gameId}', function ($user, $gameId) {
    return (int) $user->player->game->id === (int) $gameId;
});

// Private channel for a player
Broadcast::channel('player.{playerId}', function ($user, $playerId) {
    return (int) $user->id === (int) $playerId;
});

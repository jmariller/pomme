<?php

// Main routes
Route::get('/', function () {
    return view('pomme');
});

Route::post('/games', 'JoinGameController@store');
Route::post('/games/{game}/rounds/0', 'SelectionRoundController@store');
Route::post('/games/{game}/rounds/{roundNb}', 'NormalRoundController@store');
Route::post('/players/{player}/backup', 'TakeBackupController@store');
Route::post('/players/{player}/trump', 'ExchangeTrumpController@store');


//************ !! <TESTS ONLY> !! ************
Route::get('/tests/pomme', function () {
    return view('tests.pomme');
});

Route::get('/tests/animation', function () {
    return view('tests.animation');
});
//************ !! </TESTS ONLY> !! ************

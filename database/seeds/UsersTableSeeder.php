<?php

use Faker\Factory as Faker;
use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'pseudo' => 'Julien',
            'name' => 'Julien',
            'email' => 'julien@mariller.ch',
            'password' => bcrypt('secret'),
        ]);

        DB::table('users')->insert([
            'pseudo' => 'Rossella',
            'name' => 'Rossella',
            'email' => 'rossella.giustolisi01@gmail.com',
            'password' => bcrypt('secret'),
        ]);

        DB::table('users')->insert([
            'pseudo' => 'Guest 1',
            'name' => 'Guest 1',
            'password' => bcrypt((Faker::create())->password),
            'guest' => true,
        ]);

        DB::table('users')->insert([
            'pseudo' => 'Guest 2',
            'name' => 'Guest 2',
            'password' => bcrypt((Faker::create())->password),
            'guest' => true,
        ]);
    }
}

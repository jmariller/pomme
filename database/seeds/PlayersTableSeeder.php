<?php

use Illuminate\Database\Seeder;

class PlayersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('players')->insert([
            'user_id' => 1,
            'game_id' => null,
            'pseudo' => 'Julien',
        ]);

        DB::table('players')->insert([
            'user_id' => 2,
            'game_id' => null,
            'pseudo' => 'Rossella',
        ]);

        DB::table('players')->insert([
            'user_id' => 3,
            'game_id' => null,
            'pseudo' => 'Guest 1',
        ]);

        DB::table('players')->insert([
            'user_id' => 4,
            'game_id' => null,
            'pseudo' => 'Guest 2',
        ]);
    }
}

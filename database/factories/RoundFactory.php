<?php

use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(App\Jass\Entities\Round::class, function (Faker $faker) {
    return [
        'game_id' => function () {
            return factory(App\Jass\Entities\Game::class)->create()->id;
        },
        'game_type' => 'pomme',
        'nb_players' => 2,
        'round' => 0,
        'cards_deck' => '',
        'cards_drawn' => '',
        'trump' => null,
        'completed' => 0,
    ];
});

<?php

use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(App\Jass\Melds\PlayerMelds::class, function (Faker $faker) {
    return [
        'player_id' => 1,
        'round_id' => 1,
        'four_jacks' => false,
        'four_nines' => false,
        'sequence_9' => false,
        'sequence_8' => false,
        'sequence_7' => false,
        'sequence_6' => false,
        'sequence_5' => false,
        'four_aces' => false,
        'four_kings' => false,
        'four_queens' => false,
        'four_tens' => false,
        'sequence_4' => false,
        'sequence_3' => false,
        'royal_couple' => false,
        'royal_couple_alone' => false,    ];
});
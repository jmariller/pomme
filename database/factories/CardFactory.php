<?php

use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(App\Jass\Entities\Card::class, function (Faker $faker) {
    return [
        'trick_id' => function () {
            return factory(App\Jass\Entities\Trick::class)->create()->id;
        },
        'card_id' => 'SA',
    ];
});

<?php

use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(App\Jass\Entities\Game::class, function (Faker $faker) {
    return [
        'type' => 'pomme',
        'nb_players' => 2,
        'players_count' => 0,
        'rounds_count' => 0,
        'completed' => false,
        'winner_id' => null,
        'created_at' => now(),
        'updated_at' => now()
    ];
});

$factory->state(App\Jass\Entities\Game::class, 'pomme', [
    'type' => 'pomme',
    'nb_players' => 2,
]);

$factory->state(App\Jass\Entities\Game::class, 'jass', [
    'type' => 'jass',
    'nb_players' => 4,
]);

$factory->state(App\Jass\Entities\Game::class, 'complete', ['completed' => true]);

$factory->state(App\Jass\Entities\Game::class, 'one_player', ['players_count' => 1]);
$factory->state(App\Jass\Entities\Game::class, 'two_players', ['players_count' => 2]);
$factory->state(App\Jass\Entities\Game::class, 'three_players', ['players_count' => 3]);
$factory->state(App\Jass\Entities\Game::class, 'four_players', ['players_count' => 4]);

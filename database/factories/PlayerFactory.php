<?php

use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(App\Jass\Entities\Player::class, function (Faker $faker) {
    return [
        'game_id' => 1,
        'sort_order' => 1,
        'pseudo' => $faker->unique()->userName,
    ];
});

$factory->state(App\Jass\Entities\Player::class, 'with_hand', [
    'hand' => 'D8;C9;D6;HA;D9;H10;DK;CJ;C8',
]);

$factory->state(App\Jass\Entities\Player::class, 'with_card', [
    'card' => 'HA',
]);
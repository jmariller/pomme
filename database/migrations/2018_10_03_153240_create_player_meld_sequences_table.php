<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePlayerMeldSequencesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('player_meld_sequences', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('player_meld_id');
            $table->string('sequence', 40)->nullable();
            $table->unsignedTinyInteger('length')->nullable();
            $table->unsignedTinyInteger('highest_rank_index')->nullable();
            $table->unsignedTinyInteger('points')->nullable();
            $table->string('trump', 3)->nullable();
            $table->boolean('has_royal_couple')->default(false);
            $table->timestamps();

            $table->unique(['player_meld_id', 'sequence']);

            $table->foreign('player_meld_id')->references('id')->on('player_melds')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('player_meld_sequences');
    }
}

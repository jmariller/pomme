<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCardsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cards', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('trick_id');
            $table->unsignedInteger('player_id')->nullable();

            // The card ID is stored as <Suit code><Rank symbol> (e.g. SA is Ace of Spades)
            $table->string('card_id', 3);

            $table->timestamps();

            $table->foreign('trick_id')->references('id')->on('tricks')->onDelete('cascade');
            $table->foreign('player_id')->references('user_id')->on('players')->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cards');
    }
}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGamesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('games', function (Blueprint $table) {
            $table->increments('id');
            $table->string('type', 10);
            $table->unsignedTinyInteger('nb_players');
            $table->unsignedTinyInteger('players_count')->default(0);
            $table->unsignedTinyInteger('rounds_count')->default(0);
            $table->boolean('completed')->default(false);
            $table->unsignedInteger('winner_id')->nullable();
            $table->unsignedInteger('who_is_next')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('games');
    }
}

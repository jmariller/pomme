<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTricksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tricks', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('round_id');

            // Added to improve performance
            $table->unsignedInteger('round')->nullable();
            $table->string('trump', 3)->nullable();

            $table->unsignedTinyInteger('trick')->index();
            $table->boolean('completed')->default(false);
            $table->unsignedInteger('first_player_id')->nullable();
            $table->unsignedInteger('winner_id')->nullable();
            $table->unsignedSmallInteger('points')->default(0);
            $table->unsignedTinyInteger('cards_count')->default(0);
            $table->timestamps();

            $table->foreign('round_id')->references('id')->on('rounds')->onDelete('cascade');
            $table->foreign('first_player_id')->references('user_id')->on('players')->onDelete('set null');
            $table->foreign('winner_id')->references('user_id')->on('players')->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tricks');
    }
}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRoundsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rounds', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('game_id');
            $table->string('game_type', 10);
            $table->unsignedTinyInteger('nb_players');
            $table->unsignedTinyInteger('round')->index();

            // This stores the card deck with the cards id and separators like so:
            // D6;DQ;H7;HJ;CK;SQ;C7 ...
            $table->string('cards_deck', 150)->nullable();

            // This stores the card drawn from the deck. Specific to the selection round
            $table->string('cards_drawn', 150)->nullable();

            // This stores the od of the trump for that round (e.g. 'SA' for the Ace of Spades)
            $table->string('trump', 3)->nullable();

            $table->boolean('completed')->default(false);

            $table->unsignedInteger('first_player_id')->nullable();
            $table->unsignedInteger('winner_id')->nullable();
            $table->unsignedInteger('melds_winner_id')->nullable();
            $table->unsignedTinyInteger('tricks_count')->default(0);
            $table->timestamps();

            $table->foreign('game_id')->references('id')->on('games')->onDelete('cascade');
            $table->foreign('first_player_id')->references('user_id')->on('players')->onDelete('set null');
            $table->foreign('winner_id')->references('user_id')->on('players')->onDelete('set null');
            $table->foreign('melds_winner_id')->references('user_id')->on('players')->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('rounds');
    }
}

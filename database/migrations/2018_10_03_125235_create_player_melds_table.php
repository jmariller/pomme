<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePlayerMeldsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('player_melds', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('player_id');
            $table->unsignedInteger('round_id');

            $table->boolean('four_jacks')->default(false);
            $table->boolean('four_nines')->default(false);
            $table->unsignedTinyInteger('sequence_9')->default(false);
            $table->unsignedTinyInteger('sequence_8')->default(false);
            $table->unsignedTinyInteger('sequence_7')->default(false);
            $table->unsignedTinyInteger('sequence_6')->default(false);
            $table->unsignedTinyInteger('sequence_5')->default(false);
            $table->boolean('four_aces')->default(false);
            $table->boolean('four_kings')->default(false);
            $table->boolean('four_queens')->default(false);
            $table->boolean('four_tens')->default(false);
            $table->unsignedTinyInteger('sequence_4')->default(false);
            $table->unsignedTinyInteger('sequence_3')->default(false);
            $table->boolean('royal_couple')->default(false);
            $table->boolean('royal_couple_alone')->default(false);

            $table->unsignedTinyInteger('best_index')->nullable();
            $table->unsignedSmallInteger('points')->nullable();

            $table->timestamps();

            $table->unique(['player_id', 'round_id']);

            $table->foreign('player_id')->references('user_id')->on('players')->onDelete('cascade');
            $table->foreign('round_id')->references('id')->on('rounds')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('player_melds');
    }
}

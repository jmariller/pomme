<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRoundScoresTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('round_scores', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('round_id');
            $table->unsignedInteger('player_id');
            $table->unsignedSmallInteger('points')->default(0);
            $table->unsignedSmallInteger('melds')->default(0);
            $table->unsignedSmallInteger('total')->default(0);
            $table->timestamps();

            $table->unique(['round_id', 'player_id']);

            $table->foreign('round_id')->references('id')->on('rounds')->onDelete('cascade');
            $table->foreign('player_id')->references('user_id')->on('players')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('round_scores');
    }
}

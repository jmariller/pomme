<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePlayersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('players', function (Blueprint $table) {
            $table->unsignedInteger('user_id');
            $table->unsignedInteger('game_id')->nullable();
            $table->unsignedInteger('round_id')->nullable();
            $table->unsignedTinyInteger('sort_order')->nullable();
            $table->string('pseudo')->unique();
            $table->string('card', 3)->nullable();
            $table->string('hand', 50)->nullable();
            $table->string('backup', 20)->nullable();
            $table->string('discarded', 20)->nullable();
            $table->string('played', 50)->nullable();
            $table->string('trump', 3)->nullable();
            $table->timestamps();

            $table->primary('user_id');
            $table->unique(['user_id', 'game_id']);

            $table->foreign('game_id')->references('id')->on('games')->onDelete('set null');
            $table->foreign('user_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('players');
    }
}

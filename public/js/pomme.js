(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["/js/pomme"],{

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/Card.vue?vue&type=script&lang=js&":
/*!***************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/Card.vue?vue&type=script&lang=js& ***!
  \***************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
/* harmony default export */ __webpack_exports__["default"] = ({
  props: ['card']
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/CardsDeck.vue?vue&type=script&lang=js&":
/*!********************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/CardsDeck.vue?vue&type=script&lang=js& ***!
  \********************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _Card__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./Card */ "./resources/js/components/Card.vue");
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
  components: {
    Card: _Card__WEBPACK_IMPORTED_MODULE_0__["default"]
  },
  methods: {
    /**
     * Select a card
     *
     * @param card
     */
    selectCard: function selectCard(card) {
      if (this.isSelectable(card)) {
        this.$store.dispatch('drawFromDeck', card.pos);
      }
    },

    /**
     * Prepare classes for a card in the deck
     *
     * @param card
     * @returns {*}
     */
    classes: function classes(card) {
      return {
        'card-game-selectable': this.isSelectable(card),
        'invisible': card.disabled && !card.animated
      };
    },

    /**
     * Determine if a card is selectable
     *
     * @returns {boolean}
     */
    isSelectable: function isSelectable(card) {
      return !card.disabled && this.$store.getters.canPlay;
    }
  },
  computed: {
    /**
     * Determine if we are in the selection round
     *
     * @returns {boolean}
     */
    selectionRound: function selectionRound() {
      return this.$store.getters.isSelectionRound;
    },

    /**
     * Determine if the game can start
     *
     * @returns {boolean}
     */
    canStart: function canStart() {
      return this.$store.getters.canStart;
    },

    /**
     * Determine if the deck can be shown
     *
     * @returns {boolean}
     */
    canShowDeck: function canShowDeck() {
      return this.canStart && this.selectionRound;
    }
  }
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/MeldsList.vue?vue&type=script&lang=js&":
/*!********************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/MeldsList.vue?vue&type=script&lang=js& ***!
  \********************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _Card__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./Card */ "./resources/js/components/Card.vue");
/* harmony import */ var _store_constants__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../store/constants */ "./resources/js/store/constants.js");
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//


/* harmony default export */ __webpack_exports__["default"] = ({
  components: {
    Card: _Card__WEBPACK_IMPORTED_MODULE_0__["default"]
  },
  props: ['modalId', 'melds', 'animationStart'],
  methods: {
    /**
     * Get the meld title based on its code
     *
     * @param code
     * @param sequence
     * @returns {*}
     */
    title: function title(code) {
      var sequence = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : null;
      var title = this.melds.titles[code]; // TODO: Manage translation (e.g. move the hardcoded string away from here)

      if (sequence != null && this._hasRoyalCouple(sequence)) title += ' and Stöck';
      return title;
    },

    /**
     * Get the points for a given meld code
     *
     * @param code
     * @returns {int}
     */
    meldPoints: function meldPoints(code) {
      var meld_quartet = this._meld(code);

      if (!meld_quartet) return 0;
      return meld_quartet.points;
    },

    /**
     * Get the points for a given sequence
     *
     * @param sequence
     * @returns {int}
     */
    sequencePoints: function sequencePoints(sequence) {
      var points = sequence.points;
      if (this._hasRoyalCouple(sequence.sequence)) points += _store_constants__WEBPACK_IMPORTED_MODULE_1__["default"].MELDS.filter(function (meld) {
        return meld.code === 'royal_couple';
      }).pop().points;
      return points;
    },

    /**
     * Get only quartet
     *
     * @param meld
     * @param index
     * @returns {boolean}
     */
    onlyQuartets: function onlyQuartets(meld, index) {
      return meld && index.includes('four_');
    },

    /**
     * Get a quartet from the player hand
     *
     * @param code
     * @returns {*[]}
     */
    quartet: function quartet(code) {
      var _this = this;

      var meld_quartet = this._meld(code);

      if (!meld_quartet) return [];
      var quartets = [];
      _store_constants__WEBPACK_IMPORTED_MODULE_1__["default"].SUITS.forEach(function (suit) {
        quartets.push(_this._fakeCard(suit.code, suit.name, meld_quartet.rank));
      });
      return quartets;
    },

    /**
     * Determine if there is a sequence containing the royal couple
     *
     * @return {boolean}
     * @private
     */
    sequenceWithRoyalCouple: function sequenceWithRoyalCouple() {
      var _this2 = this;

      return this.melds.sequences.some(function (sequence) {
        return _this2._hasRoyalCouple(sequence.sequence);
      });
    },

    /**
     * Get a meld from constants list
     *
     * @param code
     * @returns {T}
     * @private
     */
    _meld: function _meld(code) {
      return _store_constants__WEBPACK_IMPORTED_MODULE_1__["default"].MELDS.filter(function (meld) {
        return meld.code === code;
      }).pop();
    },

    /**
     * Determine if a sequence contains the royal couple
     *
     * @param sequence
     * @return {boolean}
     * @private
     */
    _hasRoyalCouple: function _hasRoyalCouple(sequence) {
      if (!sequence[0].suit.trump) return false;
      return sequence.filter(function (card) {
        return card.rank.symbol === 'K' || card.rank.symbol === 'Q';
      }).length === 2;
    },

    /**
     * Generate a fake trump
     *
     * @param suitCode
     * @param suitName
     * @param rankSymbol
     * @param isTrump
     * @return {*}
     * @private
     */
    _fakeCard: function _fakeCard(suitCode, suitName, rankSymbol) {
      var isTrump = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : false;
      return {
        id: suitCode + rankSymbol,
        suit: {
          name: suitName,
          trump: isTrump
        },
        rank: {
          symbol: rankSymbol
        }
      };
    }
  },
  computed: {
    /**
     * Generate header based on the melds owner
     */
    header: function header() {
      // TODO: Replace hard-written text by dynamic & translated ones
      if (this.melds.player_id === this.$store.state.player.id) return 'You have melds!';
      return "".concat(this.$store.getters.opponent.pseudo, " has melds!");
    },

    /**
     * Get the royal couple
     *
     * @returns {*[]}
     */
    royalCouple: function royalCouple() {
      var trump = this.$store.state.round.trump;
      return [this._fakeCard(trump.suit.code, trump.suit.name, 'Q', true), this._fakeCard(trump.suit.code, trump.suit.name, 'K', true)];
    },

    /**
     * Find the proper AnimateCSS classes based on the start position
     *
     * @returns {*}
     * @private
     */
    _animationClasses: function _animationClasses() {
      return {
        'bottom': {
          show: 'bounceInUp',
          hide: 'bounceOutDown'
        },
        'top': {
          show: 'bounceInDown',
          hide: 'bounceOutUp'
        },
        'left': {
          show: 'bounceInRight',
          hide: 'bounceOutLeft'
        },
        'right': {
          show: 'bounceInLeft',
          hide: 'bounceOutRight'
        }
      }[this.animationStart];
    }
  },
  mounted: function mounted() {
    var _this3 = this;

    var $dialog = $("#".concat(this.modalId, " .modal-dialog"));
    $("#".concat(this.modalId)).on('show.bs.modal', function (e) {
      return $dialog.attr('class', "modal-dialog modal-dialog-centered ".concat(_this3._animationClasses.show, " animated"));
    }).on('hide.bs.modal', function (e) {
      return $dialog.attr('class', "modal-dialog modal-dialog-centered ".concat(_this3._animationClasses.hide, " animated"));
    });
  }
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/OtherHand.vue?vue&type=script&lang=js&":
/*!********************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/OtherHand.vue?vue&type=script&lang=js& ***!
  \********************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _Card__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./Card */ "./resources/js/components/Card.vue");
/* harmony import */ var _MeldsList__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./MeldsList */ "./resources/js/components/MeldsList.vue");
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//


/* harmony default export */ __webpack_exports__["default"] = ({
  components: {
    Card: _Card__WEBPACK_IMPORTED_MODULE_0__["default"],
    MeldsList: _MeldsList__WEBPACK_IMPORTED_MODULE_1__["default"]
  },
  props: ['other_player'],
  methods: {
    /**
     * Determine if a given player is next to play
     *
     * @param player
     * @returns {boolean}
     */
    otherTurn: function otherTurn(player) {
      return player.id === this.$store.state.game.who_is_next;
    },

    /**
     * Determine if the melds can be shown
     *
     * @important must be in "methods", not in "computed" as melds cannot remain cached
     *
     * @returns {boolean}
     */
    canShowMelds: function canShowMelds() {
      return this.$store.getters.hasMelds(this.other_player);
    }
  }
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/PlayerHand.vue?vue&type=script&lang=js&":
/*!*********************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/PlayerHand.vue?vue&type=script&lang=js& ***!
  \*********************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _api_player__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../api/player */ "./resources/js/api/player.js");
/* harmony import */ var _Card__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./Card */ "./resources/js/components/Card.vue");
/* harmony import */ var _MeldsList__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./MeldsList */ "./resources/js/components/MeldsList.vue");
/* harmony import */ var _pomme_PlayerHandDiscarded__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./pomme/PlayerHandDiscarded */ "./resources/js/components/pomme/PlayerHandDiscarded.vue");
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//




/* harmony default export */ __webpack_exports__["default"] = ({
  components: {
    PlayerHandDiscarded: _pomme_PlayerHandDiscarded__WEBPACK_IMPORTED_MODULE_3__["default"],
    Card: _Card__WEBPACK_IMPORTED_MODULE_1__["default"],
    MeldsList: _MeldsList__WEBPACK_IMPORTED_MODULE_2__["default"]
  },
  data: function data() {
    return {
      discarding: false
    };
  },
  methods: {
    /**
     * Select a card to play
     * [Specific for Pomme] => select a card to discard
     *
     * @param card
     */
    selectCard: function selectCard(card) {
      if (this.canDraw(card)) {
        this.$store.dispatch('drawFromHand', card.id);
      }
    },

    /**
     * Take backup cards
     */
    takeBackup: function takeBackup() {
      if (this.$store.getters.discardedEnoughCards) {
        this.$store.dispatch('takeBackup');
      }
    },

    /**
     * Acknowledge the backup cards
     */
    acknowledgeBackup: function acknowledgeBackup() {
      this.discarding = false;
      this.$store.dispatch('emptyDiscarded');
      this.$store.dispatch('emptyBackup');
    },

    /**
     * Prepare classes for a card in the hand
     *
     * @param card
     * @returns {*}
     */
    classes: function classes(card) {
      return {
        'card-game-selectable': this.canDraw(card),
        'card-game-disabled': !this.canDraw(card),
        'card-game-trump': card.suit.trump
      };
    },

    /**
     * Show the list of melds
     */
    showMelds: function showMelds() {
      _api_player__WEBPACK_IMPORTED_MODULE_0__["default"].showMelds(this.$store.state.player.id);
    },

    /**
     * Is it possible to draw a card?
     *
     * @param card
     * @returns {boolean}
     */
    canDraw: function canDraw(card) {
      return this.$store.getters.canPlay && !this.$store.state.player.card && !this.$store.state.animation && this.$store.getters.canDrawCard(card, this.previousPlayerCard);
    },

    /**
     * Stop discarding of cards
     */
    stopDiscarding: function stopDiscarding() {
      this.discarding = false;
      this.$store.dispatch('emptyDiscarded');
    }
  },
  computed: {
    /**
     * Find previous player's card
     *
     * @returns {null|*}
     */
    previousPlayerCard: function previousPlayerCard() {
      var previous_player = this.$store.state.game.other_players[0];
      if (!previous_player) return;
      return previous_player.card;
    },

    /**
     * Determine if the round started
     * 
     * @returns {boolean}
     */
    roundStarted: function roundStarted() {
      return this.$store.state.round.tricks_count > 0;
    },

    /**
     * Determine if I won the trick
     *
     * @returns {boolean}
     */
    wonTrick: function wonTrick() {
      return this.$store.state.trick.winner_id === this.$store.state.player.id;
    },

    /**
     * Determine if the backup should be shown
     *
     * @returns {boolean}
     */
    canShowBackup: function canShowBackup() {
      return !this.roundStarted && !this.$store.state.player.discarded.length;
    },

    /**
     * Determine if the melds can be shown
     *
     * @returns {boolean}
     */
    canShowMelds: function canShowMelds() {
      return this.$store.getters.hasMelds(this.$store.state.player) && this.$store.state.player.hand.length;
    },

    /**
     * Determine if enough cards were discarded
     *
     * @returns {boolean}
     */
    discardedEnoughCards: function discardedEnoughCards() {
      return this.$store.getters.discardedEnoughCards;
    }
  }
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/pomme/Carpet.vue?vue&type=script&lang=js&":
/*!***********************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/pomme/Carpet.vue?vue&type=script&lang=js& ***!
  \***********************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _Card__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../Card */ "./resources/js/components/Card.vue");
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
  components: {
    Card: _Card__WEBPACK_IMPORTED_MODULE_0__["default"]
  },
  methods: {
    /**
     * Exchange the six of trump with the current trump
     */
    exchangeSixOfTrump: function exchangeSixOfTrump() {
      if (this.canExchangeTrump) {
        this.$store.dispatch('drawSixOfTrump');
        this.$store.dispatch('takeTrump');
      }
    },

    /**
     * Determine if another player drew a card
     *
     * @param otherPlayer
     * @returns {boolean}
     */
    otherPlayerDrewCard: function otherPlayerDrewCard(otherPlayer) {
      return otherPlayer != null && otherPlayer.card != null;
    }
  },
  computed: {
    /**
     * Get the player in front
     *
     * @return {*}
     */
    playerInFront: function playerInFront() {
      return this.$store.getters.opponent;
    },

    /**
     * Determine if we are in the current round
     *
     * @returns {boolean}
     */
    selectionRound: function selectionRound() {
      return this.$store.getters.isSelectionRound;
    },

    /**
     * Specifically for the Pomme: build the list of remaining cards under the trump
     *
     * @returns {array}
     */
    remainingCards: function remainingCards() {
      return Array.from(Array(11).keys()).map(function (key) {
        return {
          'id': key + 1
        };
      });
    },

    /**
     * Determine if the player can exchange the trump
     *
     * @returns {boolean}
     */
    canExchangeTrump: function canExchangeTrump() {
      return this.$store.state.round.tricks_count <= 1 && !this.$store.state.player.card && this.hasSixOfTrump;
    },

    /**
     * Specific to the Pomme: Does the player have the six of trump?
     *
     * @returns {boolean}
     */
    hasSixOfTrump: function hasSixOfTrump() {
      return this.$store.getters.sixOfTrump !== undefined;
    }
  }
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/pomme/PlayerHandDiscarded.vue?vue&type=script&lang=js&":
/*!************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/pomme/PlayerHandDiscarded.vue?vue&type=script&lang=js& ***!
  \************************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _Card__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../Card */ "./resources/js/components/Card.vue");
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
  components: {
    Card: _Card__WEBPACK_IMPORTED_MODULE_0__["default"]
  },
  methods: {
    /**
     * Select a card to discard
     *
     * @param card
     */
    selectCard: function selectCard(card) {
      if (!this.$store.getters.gotBackupCards) {
        var index = this.$store.getters.discardedIndex(card);

        if (index !== -1) {
          this.$store.dispatch('rollbackDiscarded', index);
        } else if (!this.discardedEnoughCards) {
          this.$store.dispatch('discardCard', card);
        }
      }
    },

    /**
     * Prepare CSS classes for a card in the hand
     *
     * @param card
     * @returns {*}
     */
    classes: function classes(card) {
      return {
        'card-game-selectable': this.isDiscarding(card),
        'card-game-selected': this.$store.getters.discardedIndex(card) !== -1 || this.isBackup(card),
        'card-game-dismissible': !this.$store.getters.gotBackupCards,
        'card-game-trump': card.suit.trump
      };
    },

    /**
     * Determine if a given card is within the backup cards
     *
     * @param card
     * @returns {boolean}
     */
    isBackup: function isBackup(card) {
      return this.$store.getters.gotBackupCards && this.$store.state.backup.find(function (backupCard) {
        return backupCard.id === card.id;
      }) !== undefined;
    },

    /**
     * Is discarding in progress?
     *
     * @param card
     * @returns {boolean}
     */
    isDiscarding: function isDiscarding(card) {
      return this.$store.getters.discardedIndex(card) !== -1 || !this.discardedEnoughCards;
    }
  },
  computed: {
    /**
     * Determine if enough cards were discarded
     *
     * @returns {boolean}
     */
    discardedEnoughCards: function discardedEnoughCards() {
      return this.$store.getters.discardedEnoughCards;
    }
  }
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/pomme/RoundResults.vue?vue&type=script&lang=js&":
/*!*****************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/pomme/RoundResults.vue?vue&type=script&lang=js& ***!
  \*****************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
/* harmony default export */ __webpack_exports__["default"] = ({
  methods: {
    /**
     * Get the player name based on their identifier
     *
     * @param playerId
     * @returns {string}
     */
    playerName: function playerName(playerId) {
      if (playerId === this.$store.state.player.id) return 'Me';
      return this.opponent ? this.opponent.pseudo : '';
    },

    /**
     * Get total points for a given player
     *
     * @param playerId
     * @returns {*}
     */
    total: function total(playerId) {
      return this.$store.getters.total(this.previousRound.scores, playerId);
    }
  },
  computed: {
    /**
     * Determine the title if player won, lost or if there is a tie
     *
     * @returns {state.previous_round|{}}
     */
    sortedScores: function sortedScores() {
      if (!this.previousRound || !this.previousRound.scores) return [];
      var scores = this.previousRound.scores.slice();
      return scores.sort(function (score1, score2) {
        return score2.total - score1.total;
      });
    },

    /**
     * Determine the title if player won, lost or if there is a tie
     *
     * @returns {string}
     */
    wonOrLost: function wonOrLost() {
      if (!this.opponent) return '';
      if (this.total(this.$store.state.player.id) > this.total(this.opponent.id)) return 'You won the round!';
      if (this.total(this.$store.state.player.id) < this.total(this.opponent.id)) return 'You lost the round...';
      return 'Incredible, it is a tie!';
    },

    /**
     * Get the current opponent
     *
     * @returns {Object}
     */
    opponent: function opponent() {
      return this.$store.getters.opponent;
    },

    /**
     * Get the previous round
     *
     * @returns {state.previous_round|{}}
     */
    previousRound: function previousRound() {
      return this.$store.state.previous_round;
    }
  }
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/pomme/Scores.vue?vue&type=script&lang=js&":
/*!***********************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/pomme/Scores.vue?vue&type=script&lang=js& ***!
  \***********************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _store_constants__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../../store/constants */ "./resources/js/store/constants.js");
/* harmony import */ var _api_player__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../api/player */ "./resources/js/api/player.js");
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//


/* harmony default export */ __webpack_exports__["default"] = ({
  data: function data() {
    return {
      CONSTANTS: _store_constants__WEBPACK_IMPORTED_MODULE_0__["default"]
    };
  },
  methods: {
    /**
     * Show melds for a player
     *
     * @param playerId
     */
    showMelds: function showMelds(playerId) {
      if (this.meldsPoints(playerId) > 0) _api_player__WEBPACK_IMPORTED_MODULE_1__["default"].showMelds(playerId);
    },

    /**
     * Get melds points for a player
     *
     * @param playerId
     * @return {*}
     */
    meldsPoints: function meldsPoints(playerId) {
      if (this.$store.getters.isFirstTrick) return 0;
      return this.$store.getters.melds(this.$store.state.round.scores, playerId);
    }
  }
});

/***/ }),

/***/ "./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/Card.vue?vue&type=style&index=0&id=b9bc2c0a&scoped=true&lang=css&":
/*!**********************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/css-loader??ref--6-1!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--6-2!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/Card.vue?vue&type=style&index=0&id=b9bc2c0a&scoped=true&lang=css& ***!
  \**********************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(/*! ../../../node_modules/css-loader/lib/css-base.js */ "./node_modules/css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "\n.card-game[data-v-b9bc2c0a] {\n    font-size: 0.8rem;\n}\n.card-suit[data-v-b9bc2c0a] {\n    margin: -15px 0 0 -15px;\n    padding: 0;\n}\n.card-rank[data-v-b9bc2c0a] {\n    font-size: 1.2em;\n    margin: 0 0 0 -11px;\n    padding: 0;\n}\n", ""]);

// exports


/***/ }),

/***/ "./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/MeldsList.vue?vue&type=style&index=0&lang=css&":
/*!***************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/css-loader??ref--6-1!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--6-2!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/MeldsList.vue?vue&type=style&index=0&lang=css& ***!
  \***************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(/*! ../../../node_modules/css-loader/lib/css-base.js */ "./node_modules/css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "\n.melds .card-game {\n    margin: 0 -80px 0 0;\n}\n.melds-points {\n    right: 10px;\n    top: 10px;\n    z-index: 1;\n}\n", ""]);

// exports


/***/ }),

/***/ "./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/pomme/Carpet.vue?vue&type=style&index=0&lang=css&":
/*!******************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/css-loader??ref--6-1!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--6-2!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/pomme/Carpet.vue?vue&type=style&index=0&lang=css& ***!
  \******************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(/*! ../../../../node_modules/css-loader/lib/css-base.js */ "./node_modules/css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "\n.pomme-trump .card-game {\n    margin: 0 -61px;\n}\n", ""]);

// exports


/***/ }),

/***/ "./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/pomme/PlayerHandDiscarded.vue?vue&type=style&index=0&id=55e963a9&scoped=true&lang=css&":
/*!*******************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/css-loader??ref--6-1!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--6-2!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/pomme/PlayerHandDiscarded.vue?vue&type=style&index=0&id=55e963a9&scoped=true&lang=css& ***!
  \*******************************************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(/*! ../../../../node_modules/css-loader/lib/css-base.js */ "./node_modules/css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "\n.card-game-dismissible[data-v-55e963a9] {\n    border: 1px dashed #ff5140;\n    transition: all 0.4s;\n}\n.card-game-dismissible[data-v-55e963a9]:hover {\n    border: 1px solid #ff5140 !important;\n}\n", ""]);

// exports


/***/ }),

/***/ "./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/pomme/RoundResults.vue?vue&type=style&index=0&lang=css&":
/*!************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/css-loader??ref--6-1!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--6-2!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/pomme/RoundResults.vue?vue&type=style&index=0&lang=css& ***!
  \************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(/*! ../../../../node_modules/css-loader/lib/css-base.js */ "./node_modules/css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "\ntr.winner {\n    font-size: 1.2rem;\n    font-weight: bold;\n}\n", ""]);

// exports


/***/ }),

/***/ "./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/pomme/Scores.vue?vue&type=style&index=0&id=318c78d5&scoped=true&lang=css&":
/*!******************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/css-loader??ref--6-1!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--6-2!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/pomme/Scores.vue?vue&type=style&index=0&id=318c78d5&scoped=true&lang=css& ***!
  \******************************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(/*! ../../../../node_modules/css-loader/lib/css-base.js */ "./node_modules/css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "\n.clickable[data-v-318c78d5] {\n    color: #3b9ce3;\n    cursor: pointer;\n    text-decoration: underline;\n}\n.clickable[data-v-318c78d5]:hover {\n    text-decoration: none;\n}\n", ""]);

// exports


/***/ }),

/***/ "./node_modules/style-loader/index.js!./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/Card.vue?vue&type=style&index=0&id=b9bc2c0a&scoped=true&lang=css&":
/*!**************************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/style-loader!./node_modules/css-loader??ref--6-1!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--6-2!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/Card.vue?vue&type=style&index=0&id=b9bc2c0a&scoped=true&lang=css& ***!
  \**************************************************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {


var content = __webpack_require__(/*! !../../../node_modules/css-loader??ref--6-1!../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../node_modules/postcss-loader/src??ref--6-2!../../../node_modules/vue-loader/lib??vue-loader-options!./Card.vue?vue&type=style&index=0&id=b9bc2c0a&scoped=true&lang=css& */ "./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/Card.vue?vue&type=style&index=0&id=b9bc2c0a&scoped=true&lang=css&");

if(typeof content === 'string') content = [[module.i, content, '']];

var transform;
var insertInto;



var options = {"hmr":true}

options.transform = transform
options.insertInto = undefined;

var update = __webpack_require__(/*! ../../../node_modules/style-loader/lib/addStyles.js */ "./node_modules/style-loader/lib/addStyles.js")(content, options);

if(content.locals) module.exports = content.locals;

if(false) {}

/***/ }),

/***/ "./node_modules/style-loader/index.js!./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/MeldsList.vue?vue&type=style&index=0&lang=css&":
/*!*******************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/style-loader!./node_modules/css-loader??ref--6-1!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--6-2!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/MeldsList.vue?vue&type=style&index=0&lang=css& ***!
  \*******************************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {


var content = __webpack_require__(/*! !../../../node_modules/css-loader??ref--6-1!../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../node_modules/postcss-loader/src??ref--6-2!../../../node_modules/vue-loader/lib??vue-loader-options!./MeldsList.vue?vue&type=style&index=0&lang=css& */ "./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/MeldsList.vue?vue&type=style&index=0&lang=css&");

if(typeof content === 'string') content = [[module.i, content, '']];

var transform;
var insertInto;



var options = {"hmr":true}

options.transform = transform
options.insertInto = undefined;

var update = __webpack_require__(/*! ../../../node_modules/style-loader/lib/addStyles.js */ "./node_modules/style-loader/lib/addStyles.js")(content, options);

if(content.locals) module.exports = content.locals;

if(false) {}

/***/ }),

/***/ "./node_modules/style-loader/index.js!./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/pomme/Carpet.vue?vue&type=style&index=0&lang=css&":
/*!**********************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/style-loader!./node_modules/css-loader??ref--6-1!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--6-2!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/pomme/Carpet.vue?vue&type=style&index=0&lang=css& ***!
  \**********************************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {


var content = __webpack_require__(/*! !../../../../node_modules/css-loader??ref--6-1!../../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../../node_modules/postcss-loader/src??ref--6-2!../../../../node_modules/vue-loader/lib??vue-loader-options!./Carpet.vue?vue&type=style&index=0&lang=css& */ "./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/pomme/Carpet.vue?vue&type=style&index=0&lang=css&");

if(typeof content === 'string') content = [[module.i, content, '']];

var transform;
var insertInto;



var options = {"hmr":true}

options.transform = transform
options.insertInto = undefined;

var update = __webpack_require__(/*! ../../../../node_modules/style-loader/lib/addStyles.js */ "./node_modules/style-loader/lib/addStyles.js")(content, options);

if(content.locals) module.exports = content.locals;

if(false) {}

/***/ }),

/***/ "./node_modules/style-loader/index.js!./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/pomme/PlayerHandDiscarded.vue?vue&type=style&index=0&id=55e963a9&scoped=true&lang=css&":
/*!***********************************************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/style-loader!./node_modules/css-loader??ref--6-1!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--6-2!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/pomme/PlayerHandDiscarded.vue?vue&type=style&index=0&id=55e963a9&scoped=true&lang=css& ***!
  \***********************************************************************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {


var content = __webpack_require__(/*! !../../../../node_modules/css-loader??ref--6-1!../../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../../node_modules/postcss-loader/src??ref--6-2!../../../../node_modules/vue-loader/lib??vue-loader-options!./PlayerHandDiscarded.vue?vue&type=style&index=0&id=55e963a9&scoped=true&lang=css& */ "./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/pomme/PlayerHandDiscarded.vue?vue&type=style&index=0&id=55e963a9&scoped=true&lang=css&");

if(typeof content === 'string') content = [[module.i, content, '']];

var transform;
var insertInto;



var options = {"hmr":true}

options.transform = transform
options.insertInto = undefined;

var update = __webpack_require__(/*! ../../../../node_modules/style-loader/lib/addStyles.js */ "./node_modules/style-loader/lib/addStyles.js")(content, options);

if(content.locals) module.exports = content.locals;

if(false) {}

/***/ }),

/***/ "./node_modules/style-loader/index.js!./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/pomme/RoundResults.vue?vue&type=style&index=0&lang=css&":
/*!****************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/style-loader!./node_modules/css-loader??ref--6-1!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--6-2!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/pomme/RoundResults.vue?vue&type=style&index=0&lang=css& ***!
  \****************************************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {


var content = __webpack_require__(/*! !../../../../node_modules/css-loader??ref--6-1!../../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../../node_modules/postcss-loader/src??ref--6-2!../../../../node_modules/vue-loader/lib??vue-loader-options!./RoundResults.vue?vue&type=style&index=0&lang=css& */ "./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/pomme/RoundResults.vue?vue&type=style&index=0&lang=css&");

if(typeof content === 'string') content = [[module.i, content, '']];

var transform;
var insertInto;



var options = {"hmr":true}

options.transform = transform
options.insertInto = undefined;

var update = __webpack_require__(/*! ../../../../node_modules/style-loader/lib/addStyles.js */ "./node_modules/style-loader/lib/addStyles.js")(content, options);

if(content.locals) module.exports = content.locals;

if(false) {}

/***/ }),

/***/ "./node_modules/style-loader/index.js!./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/pomme/Scores.vue?vue&type=style&index=0&id=318c78d5&scoped=true&lang=css&":
/*!**********************************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/style-loader!./node_modules/css-loader??ref--6-1!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--6-2!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/pomme/Scores.vue?vue&type=style&index=0&id=318c78d5&scoped=true&lang=css& ***!
  \**********************************************************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {


var content = __webpack_require__(/*! !../../../../node_modules/css-loader??ref--6-1!../../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../../node_modules/postcss-loader/src??ref--6-2!../../../../node_modules/vue-loader/lib??vue-loader-options!./Scores.vue?vue&type=style&index=0&id=318c78d5&scoped=true&lang=css& */ "./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/pomme/Scores.vue?vue&type=style&index=0&id=318c78d5&scoped=true&lang=css&");

if(typeof content === 'string') content = [[module.i, content, '']];

var transform;
var insertInto;



var options = {"hmr":true}

options.transform = transform
options.insertInto = undefined;

var update = __webpack_require__(/*! ../../../../node_modules/style-loader/lib/addStyles.js */ "./node_modules/style-loader/lib/addStyles.js")(content, options);

if(content.locals) module.exports = content.locals;

if(false) {}

/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/Card.vue?vue&type=template&id=b9bc2c0a&scoped=true&":
/*!*******************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/Card.vue?vue&type=template&id=b9bc2c0a&scoped=true& ***!
  \*******************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "div",
    {
      staticClass: "card card-game shadow-sm",
      on: {
        click: function($event) {
          _vm.$emit("select")
        }
      }
    },
    [
      _c("div", { staticClass: "card-body" }, [
        _vm.card
          ? _c("div", [
              _c("p", { staticClass: "card-suit" }, [
                _c("img", {
                  attrs: {
                    src:
                      "images/suits/" +
                      _vm.card.suit.name.toLowerCase() +
                      "-small.png",
                    alt: ""
                  }
                })
              ]),
              _vm._v(" "),
              _c("p", { staticClass: "card-rank" }, [
                _vm._v(
                  "\n                " +
                    _vm._s(_vm.card.rank.symbol) +
                    "\n            "
                )
              ])
            ])
          : _vm._e()
      ])
    ]
  )
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/CardsDeck.vue?vue&type=template&id=60c73a5e&":
/*!************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/CardsDeck.vue?vue&type=template&id=60c73a5e& ***!
  \************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _vm.canShowDeck
    ? _c(
        "div",
        { staticClass: "cards-deck" },
        _vm._l(_vm.$store.state.deck.cards, function(card) {
          return _c("card", {
            key: card.pos,
            staticClass: "card-game-face-down position-absolute",
            class: _vm.classes(card),
            attrs: { id: "card-deck-" + card.pos },
            on: {
              select: function($event) {
                _vm.selectCard(card)
              }
            }
          })
        }),
        1
      )
    : _vm._e()
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/MeldsList.vue?vue&type=template&id=1ed88b74&":
/*!************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/MeldsList.vue?vue&type=template&id=1ed88b74& ***!
  \************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "div",
    {
      staticClass: "modal fade",
      attrs: {
        id: _vm.modalId,
        tabindex: "-1",
        role: "dialog",
        "aria-labelledby": "melds-modal-label",
        "aria-hidden": "true"
      }
    },
    [
      _c("div", { staticClass: "modal-dialog", attrs: { role: "document" } }, [
        _c("div", { staticClass: "modal-content" }, [
          _c("div", { staticClass: "modal-header" }, [
            _c(
              "h5",
              {
                staticClass: "modal-title",
                attrs: { id: "melds-modal-label" }
              },
              [
                _vm._v(
                  "\n                    " +
                    _vm._s(_vm.header) +
                    "\n                "
                )
              ]
            ),
            _vm._v(" "),
            _vm._m(0)
          ]),
          _vm._v(" "),
          _c("div", { staticClass: "modal-body melds" }, [
            _vm.melds && _vm.melds.sequences
              ? _c(
                  "ul",
                  {
                    staticClass: "list-group list-group-flush melds-sequences"
                  },
                  [
                    _vm._l(_vm.melds, function(meld, code) {
                      return _vm.onlyQuartets(meld, code)
                        ? _c(
                            "li",
                            { staticClass: "list-group-item d-flex" },
                            [
                              _vm._l(_vm.quartet(code), function(card) {
                                return _c("card", {
                                  key: card.id,
                                  staticClass: "card-game card-game-face-up",
                                  attrs: { card: card }
                                })
                              }),
                              _vm._v(" "),
                              _c(
                                "span",
                                {
                                  staticClass:
                                    "badge badge-pill badge-success shadow-sm melds-points position-absolute"
                                },
                                [
                                  _vm._v(
                                    "\n                            " +
                                      _vm._s(_vm.title(code)) +
                                      " - " +
                                      _vm._s(_vm.meldPoints(code)) +
                                      " points\n                        "
                                  )
                                ]
                              )
                            ],
                            2
                          )
                        : _vm._e()
                    }),
                    _vm._v(" "),
                    _vm.melds["royal_couple"] && !_vm.sequenceWithRoyalCouple()
                      ? _c(
                          "li",
                          { staticClass: "list-group-item d-flex" },
                          [
                            _vm._l(_vm.royalCouple, function(card) {
                              return _c("card", {
                                key: card.id,
                                staticClass:
                                  "card-game card-game-face-up card-game-trump",
                                attrs: { card: card }
                              })
                            }),
                            _vm._v(" "),
                            _c(
                              "span",
                              {
                                staticClass:
                                  "badge badge-pill badge-success shadow-sm melds-points position-absolute"
                              },
                              [
                                _vm._v(
                                  "\n                            " +
                                    _vm._s(_vm.title("royal_couple")) +
                                    " - " +
                                    _vm._s(_vm.meldPoints("royal_couple")) +
                                    " points\n                        "
                                )
                              ]
                            )
                          ],
                          2
                        )
                      : _vm._e(),
                    _vm._v(" "),
                    _vm._l(_vm.melds.sequences, function(sequence) {
                      return _c(
                        "li",
                        { staticClass: "list-group-item d-flex" },
                        [
                          _vm._l(sequence.sequence, function(card) {
                            return _c("card", {
                              key: card.id,
                              staticClass: "card-game card-game-face-up",
                              class: { "card-game-trump": card.suit.trump },
                              attrs: { card: card }
                            })
                          }),
                          _vm._v(" "),
                          _c(
                            "span",
                            {
                              staticClass:
                                "badge badge-pill badge-success shadow-sm melds-points position-absolute"
                            },
                            [
                              _vm._v(
                                "\n                            " +
                                  _vm._s(
                                    _vm.title(sequence.code, sequence.sequence)
                                  ) +
                                  " - " +
                                  _vm._s(_vm.sequencePoints(sequence)) +
                                  " points\n                        "
                              )
                            ]
                          )
                        ],
                        2
                      )
                    })
                  ],
                  2
                )
              : _vm._e()
          ]),
          _vm._v(" "),
          _vm._m(1)
        ])
      ])
    ]
  )
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c(
      "button",
      {
        staticClass: "close",
        attrs: {
          type: "button",
          "data-dismiss": "modal",
          "aria-label": "Close"
        }
      },
      [_c("span", { attrs: { "aria-hidden": "true" } }, [_vm._v("×")])]
    )
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "modal-footer" }, [
      _c(
        "button",
        {
          staticClass: "btn btn-primary",
          attrs: { type: "button", "data-dismiss": "modal" }
        },
        [_vm._v("OK")]
      )
    ])
  }
]
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/OtherHand.vue?vue&type=template&id=05572378&":
/*!************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/OtherHand.vue?vue&type=template&id=05572378& ***!
  \************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "div",
    { staticClass: "row d-flex justify-content-center" },
    [
      _vm.other_player
        ? _c("div", [
            _c("h5", { staticClass: "my-2" }, [
              _vm.otherTurn(_vm.other_player) && !_vm.other_player.card
                ? _c("i", { staticClass: "far fa-arrow-alt-circle-right" })
                : _vm._e(),
              _vm._v(" " + _vm._s(_vm.other_player.pseudo) + "\n        ")
            ]),
            _vm._v(" "),
            _c("span", {
              attrs: { id: "hidden-pocket-" + _vm.other_player.id }
            }),
            _vm._v(" "),
            !_vm.$store.getters.isSelectionRound
              ? _c(
                  "div",
                  {
                    staticClass:
                      "container-fluid player-hand d-flex justify-content-center",
                    attrs: { id: "player-hand-" + _vm.other_player.id }
                  },
                  _vm._l(_vm.other_player.hand, function(card) {
                    return _c("card", {
                      key: card.id,
                      staticClass: "card-game-small card-game-face-down",
                      attrs: {
                        id: _vm.$store.getters.cardCssId(
                          _vm.other_player.id,
                          card.id
                        )
                      }
                    })
                  }),
                  1
                )
              : _vm._e()
          ])
        : _c("div", [
            _c("h5", { staticClass: "my-2" }, [
              _vm._v("Waiting for a player to join...")
            ])
          ]),
      _vm._v(" "),
      _vm.canShowMelds()
        ? _c("melds-list", {
            attrs: {
              "modal-id": "melds-modal-" + _vm.other_player.id,
              melds: _vm.other_player.melds[0],
              "animation-start": "top"
            }
          })
        : _vm._e()
    ],
    1
  )
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/PlayerHand.vue?vue&type=template&id=9c89a8ca&":
/*!*************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/PlayerHand.vue?vue&type=template&id=9c89a8ca& ***!
  \*************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "div",
    [
      _c("span", {
        attrs: { id: "hidden-pocket-" + _vm.$store.state.player.id }
      }),
      _vm._v(" "),
      _c("div", { staticClass: "row d-flex justify-content-center" }, [
        _c("h5", { staticClass: "my-2" }, [
          _vm.$store.getters.canPlay
            ? _c("i", { staticClass: "far fa-arrow-alt-circle-right" })
            : _vm._e(),
          _vm._v(" Me\n        ")
        ])
      ]),
      _vm._v(" "),
      !_vm.$store.getters.isSelectionRound
        ? _c("div", { staticClass: "d-flex" }, [
            _c(
              "div",
              {
                directives: [
                  {
                    name: "show",
                    rawName: "v-show",
                    value: !_vm.$store.state.player.card,
                    expression: "!$store.state.player.card"
                  }
                ],
                staticClass: "col"
              },
              [_vm._v(" ")]
            ),
            _vm._v(" "),
            _c(
              "div",
              {
                staticClass: "col player-hand my-hand",
                attrs: { id: "player-hand-" + _vm.$store.state.player.id }
              },
              [
                !_vm.discarding
                  ? _c(
                      "div",
                      {
                        staticClass: "row d-flex justify-content-center",
                        attrs: {
                          id: "player-hand-" + _vm.$store.state.player.id
                        }
                      },
                      _vm._l(_vm.$store.state.player.hand, function(card, key) {
                        return _c("card", {
                          key: card.id,
                          staticClass: "card-game-hand card-game-face-up",
                          class: _vm.classes(card),
                          style: "z-index: " + (key + 1),
                          attrs: {
                            card: card,
                            id: _vm.$store.getters.cardCssId(
                              _vm.$store.state.player.id,
                              card.id
                            )
                          },
                          on: {
                            select: function($event) {
                              _vm.selectCard(card)
                            }
                          }
                        })
                      }),
                      1
                    )
                  : _vm._e(),
                _vm._v(" "),
                _vm.discarding ? _c("player-hand-discarded") : _vm._e()
              ],
              1
            ),
            _vm._v(" "),
            _c(
              "div",
              {
                directives: [
                  {
                    name: "show",
                    rawName: "v-show",
                    value: !_vm.$store.state.player.card,
                    expression: "!$store.state.player.card"
                  }
                ],
                staticClass: "col"
              },
              [
                _c("div", { staticClass: "d-flex ml-3" }, [
                  _vm.$store.getters.gotBackupCards
                    ? _c(
                        "button",
                        {
                          staticClass: "btn btn-sm btn-primary",
                          on: {
                            click: function($event) {
                              _vm.acknowledgeBackup()
                            }
                          }
                        },
                        [_vm._v("Ok!")]
                      )
                    : _vm._e(),
                  _vm._v(" "),
                  _c(
                    "div",
                    {
                      directives: [
                        {
                          name: "show",
                          rawName: "v-show",
                          value: _vm.canShowBackup,
                          expression: "canShowBackup"
                        }
                      ],
                      staticClass: "take-backup"
                    },
                    [
                      _c("div", {
                        staticClass: "player-options backup-cards mr-2",
                        class: { selected: _vm.discarding },
                        on: {
                          click: function($event) {
                            _vm.discarding = !_vm.discarding
                          }
                        }
                      }),
                      _vm._v(" "),
                      _c(
                        "div",
                        {
                          directives: [
                            {
                              name: "show",
                              rawName: "v-show",
                              value: _vm.discarding,
                              expression: "discarding"
                            }
                          ],
                          staticClass: "actions"
                        },
                        [
                          _c("i", {
                            staticClass: "fas fa-check action confirm",
                            class: { disabled: !_vm.discardedEnoughCards },
                            on: {
                              click: function($event) {
                                _vm.takeBackup()
                              }
                            }
                          }),
                          _vm._v(" "),
                          _c("i", {
                            staticClass: "fas fa-ban action cancel",
                            on: {
                              click: function($event) {
                                _vm.stopDiscarding()
                              }
                            }
                          })
                        ]
                      )
                    ]
                  ),
                  _vm._v(" "),
                  _c("div", {
                    directives: [
                      {
                        name: "show",
                        rawName: "v-show",
                        value: !_vm.roundStarted && _vm.canShowMelds,
                        expression: "!roundStarted && canShowMelds"
                      }
                    ],
                    staticClass: "player-options has-melds",
                    on: {
                      click: function($event) {
                        _vm.showMelds()
                      }
                    }
                  })
                ])
              ]
            )
          ])
        : _vm._e(),
      _vm._v(" "),
      _vm.canShowMelds
        ? _c("melds-list", {
            attrs: {
              "modal-id": "melds-modal-" + _vm.$store.state.player.id,
              melds: _vm.$store.state.player.melds[0],
              "animation-start": "bottom"
            }
          })
        : _vm._e()
    ],
    1
  )
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/pomme/Carpet.vue?vue&type=template&id=db5c9f02&":
/*!***************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/pomme/Carpet.vue?vue&type=template&id=db5c9f02& ***!
  \***************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", { staticClass: "container-fluid carpet" }, [
    _c("div", { staticClass: "row carpet-row" }, [
      _c(
        "div",
        { staticClass: "col d-flex justify-content-center" },
        [
          _c("card", {
            staticClass: "card-game-played",
            class: {
              invisible: !_vm.otherPlayerDrewCard(_vm.playerInFront),
              "card-game-trump":
                _vm.otherPlayerDrewCard(_vm.playerInFront) &&
                _vm.playerInFront.card.suit.trump
            },
            attrs: {
              id:
                "player-card-" + (_vm.playerInFront ? _vm.playerInFront.id : 0),
              card: _vm.otherPlayerDrewCard(_vm.playerInFront)
                ? _vm.playerInFront.card
                : ""
            }
          })
        ],
        1
      )
    ]),
    _vm._v(" "),
    _c(
      "div",
      {
        staticClass: "row my-5",
        class: {
          "carpet-row": _vm.selectionRound,
          "carpet-row-collapsed": !_vm.selectionRound
        }
      },
      [
        _vm.selectionRound
          ? _c("div", { staticClass: "col" }, [_vm._t("default")], 2)
          : _vm._e(),
        _vm._v(" "),
        _vm.$store.state.round.trump && _vm.$store.state.player.hand.length > 0
          ? _c(
              "div",
              { staticClass: "col d-flex justify-content-end pomme-trump" },
              [
                _vm._l(_vm.remainingCards, function(card) {
                  return _c("card", {
                    key: "FF" + card.id,
                    staticClass: "card-game-face-down"
                  })
                }),
                _vm._v(" "),
                _c("card", {
                  staticClass: "card-game-face-up",
                  class: {
                    "card-game-selectable":
                      _vm.canExchangeTrump && _vm.hasSixOfTrump,
                    "card-game-trump": _vm.$store.state.round.trump.suit.trump,
                    "card-game-disabled":
                      !_vm.canExchangeTrump || !_vm.hasSixOfTrump
                  },
                  attrs: {
                    id: "card-game-current-trump",
                    card: _vm.$store.state.round.trump
                  },
                  on: {
                    select: function($event) {
                      _vm.exchangeSixOfTrump()
                    }
                  }
                })
              ],
              2
            )
          : _vm._e()
      ]
    ),
    _vm._v(" "),
    _c("div", { staticClass: "row carpet-row" }, [
      _c(
        "div",
        { staticClass: "col d-flex justify-content-center " },
        [
          _c("card", {
            staticClass: "card-game-played",
            class: {
              invisible: !_vm.$store.state.player.card,
              "card-game-trump":
                _vm.$store.state.player.card &&
                _vm.$store.state.player.card.suit.trump
            },
            attrs: {
              id: "player-card-" + _vm.$store.state.player.id,
              card: _vm.$store.state.player.card
            }
          })
        ],
        1
      )
    ])
  ])
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/pomme/PlayerHandDiscarded.vue?vue&type=template&id=55e963a9&scoped=true&":
/*!****************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/pomme/PlayerHandDiscarded.vue?vue&type=template&id=55e963a9&scoped=true& ***!
  \****************************************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", [
    _c(
      "div",
      { staticClass: "row d-flex justify-content-center" },
      _vm._l(_vm.$store.state.player.hand, function(card) {
        return _c("card", {
          key: card.id,
          staticClass: "card-game-hand card-game-face-up",
          class: _vm.classes(card),
          attrs: {
            card: card,
            id: _vm.$store.getters.cardCssId(
              _vm.$store.state.player.id,
              card.id
            )
          },
          on: {
            select: function($event) {
              _vm.selectCard(card)
            }
          }
        })
      }),
      1
    )
  ])
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/pomme/RoundResults.vue?vue&type=template&id=7235dabc&":
/*!*********************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/pomme/RoundResults.vue?vue&type=template&id=7235dabc& ***!
  \*********************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "div",
    {
      staticClass: "modal fade",
      attrs: {
        id: "round-results-modal",
        tabindex: "-1",
        role: "dialog",
        "aria-labelledby": "round-results-modal-label",
        "aria-hidden": "true"
      }
    },
    [
      _c("div", { staticClass: "modal-dialog", attrs: { role: "document" } }, [
        _c("div", { staticClass: "modal-content" }, [
          _c("div", { staticClass: "modal-header" }, [
            _c(
              "h5",
              {
                staticClass: "modal-title",
                attrs: { id: "round-results-modal-label" }
              },
              [
                _vm._v(
                  "\n                    " +
                    _vm._s(_vm.wonOrLost) +
                    "\n                "
                )
              ]
            ),
            _vm._v(" "),
            _vm._m(0)
          ]),
          _vm._v(" "),
          _c("div", { staticClass: "modal-body" }, [
            _c("table", { staticClass: "table table-sm" }, [
              _vm._m(1),
              _vm._v(" "),
              _c(
                "tbody",
                _vm._l(_vm.sortedScores, function(score, index) {
                  return _c(
                    "tr",
                    {
                      key: score.player_id,
                      class: {
                        winner: index === 0,
                        "table-success": index === 0,
                        "table-warning": index > 0
                      }
                    },
                    [
                      _c("td", [
                        _vm._v(_vm._s(_vm.playerName(score.player_id)))
                      ]),
                      _vm._v(" "),
                      _c("td", { staticClass: "text-right" }, [
                        _vm._v(_vm._s(score.points))
                      ]),
                      _vm._v(" "),
                      _c("td", { staticClass: "text-right" }, [
                        _vm._v(_vm._s(score.melds))
                      ]),
                      _vm._v(" "),
                      _c("td", { staticClass: "text-right" }, [
                        _vm._v(_vm._s(score.total))
                      ])
                    ]
                  )
                }),
                0
              )
            ])
          ]),
          _vm._v(" "),
          _vm._m(2)
        ])
      ])
    ]
  )
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c(
      "button",
      {
        staticClass: "close",
        attrs: {
          type: "button",
          "data-dismiss": "modal",
          "aria-label": "Close"
        }
      },
      [_c("span", { attrs: { "aria-hidden": "true" } }, [_vm._v("×")])]
    )
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("thead", { staticClass: "thead-light" }, [
      _c("tr", [
        _c("th", [_vm._v("Player")]),
        _vm._v(" "),
        _c("th", { staticClass: "text-right" }, [_vm._v("Points")]),
        _vm._v(" "),
        _c("th", { staticClass: "text-right" }, [_vm._v("Melds")]),
        _vm._v(" "),
        _c("th", { staticClass: "text-right" }, [_vm._v("Total")])
      ])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "modal-footer" }, [
      _c(
        "button",
        {
          staticClass: "btn btn-primary",
          attrs: { type: "button", "data-dismiss": "modal" }
        },
        [_vm._v("OK")]
      )
    ])
  }
]
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/pomme/Scores.vue?vue&type=template&id=318c78d5&scoped=true&":
/*!***************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/pomme/Scores.vue?vue&type=template&id=318c78d5&scoped=true& ***!
  \***************************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _vm.$store.state.game.scores && _vm.$store.state.game.scores.length > 0
    ? _c(
        "div",
        { staticClass: "game-board position-absolute rounded px-4 py-3" },
        [
          _c("h5", [_vm._v("Game scores")]),
          _vm._v(" "),
          _c("p", { staticClass: "my-0" }, [_vm._v("Me:")]),
          _vm._v(" "),
          _c(
            "div",
            {
              staticClass:
                "my-0 d-flex justify-content-between align-items-center"
            },
            [
              _c(
                "p",
                _vm._l(Array(_vm.CONSTANTS.ROUNDS_TO_WIN), function(
                  value,
                  index
                ) {
                  return _c(
                    "span",
                    {
                      key: index,
                      staticClass: "round-won",
                      class: {
                        "round-won-highlighted":
                          index + 1 <=
                          _vm.$store.getters.score(_vm.$store.state.player.id)
                            .rounds_won
                      }
                    },
                    [_vm._v("I")]
                  )
                }),
                0
              ),
              _vm._v(" "),
              _c(
                "p",
                _vm._l(Array(_vm.CONSTANTS.POMMES_TO_LOSE), function(
                  value,
                  index
                ) {
                  return _c("i", {
                    key:
                      "player-" +
                      _vm.$store.state.player.id +
                      "-pomme-count-" +
                      index,
                    staticClass: "fas fa-apple-alt ml-1 pommes-count",
                    class: {
                      "pommes-count-highlighted":
                        index + 1 <=
                        _vm.$store.getters.score(_vm.$store.state.player.id)
                          .pommes_count
                    }
                  })
                }),
                0
              )
            ]
          ),
          _vm._v(" "),
          _vm.$store.getters.opponent
            ? _c("div", { staticClass: "my-0" }, [
                _c("p", { staticClass: "my-0" }, [
                  _vm._v(_vm._s(_vm.$store.getters.opponent.pseudo) + ":")
                ]),
                _vm._v(" "),
                _c(
                  "div",
                  {
                    staticClass:
                      "my-0 d-flex justify-content-between align-items-center"
                  },
                  [
                    _c(
                      "p",
                      _vm._l(Array(_vm.CONSTANTS.ROUNDS_TO_WIN), function(
                        round,
                        index
                      ) {
                        return _c(
                          "span",
                          {
                            key: index,
                            staticClass: "round-won",
                            class: {
                              "round-won-highlighted":
                                index + 1 <=
                                _vm.$store.getters.score(
                                  _vm.$store.getters.opponent.id
                                ).rounds_won
                            }
                          },
                          [_vm._v("I")]
                        )
                      }),
                      0
                    ),
                    _vm._v(" "),
                    _c(
                      "p",
                      _vm._l(Array(_vm.CONSTANTS.POMMES_TO_LOSE), function(
                        pomme,
                        index
                      ) {
                        return _c("i", {
                          key:
                            "player-" +
                            _vm.$store.getters.opponent.id +
                            "-pomme-count-" +
                            index,
                          staticClass: "fas fa-apple-alt ml-1 pommes-count",
                          class: {
                            "pommes-count-highlighted":
                              index + 1 <=
                              _vm.$store.getters.score(
                                _vm.$store.getters.opponent.id
                              ).pommes_count
                          }
                        })
                      }),
                      0
                    )
                  ]
                )
              ])
            : _vm._e(),
          _vm._v(" "),
          _c("hr"),
          _vm._v(" "),
          _c("h5", [
            _vm._v(
              "\n        Round #" +
                _vm._s(
                  _vm.$store.state.round ? _vm.$store.state.round.round : 0
                ) +
                "\n    "
            )
          ]),
          _vm._v(" "),
          _c("table", { staticClass: "table table-sm table-borderless" }, [
            _vm._m(0),
            _vm._v(" "),
            _c("tbody", [
              _c("tr", [
                _c("td", [_vm._v("Me")]),
                _vm._v(" "),
                _c("td", { staticClass: "text-right" }, [
                  _vm._v(
                    "\n                    " +
                      _vm._s(
                        _vm.$store.getters.points(
                          _vm.$store.state.round.scores,
                          _vm.$store.state.player.id
                        )
                      ) +
                      "\n                "
                  )
                ]),
                _vm._v(" "),
                _c("td", { staticClass: "text-right" }, [
                  _c(
                    "span",
                    {
                      class: {
                        clickable:
                          _vm.meldsPoints(_vm.$store.state.player.id) > 0
                      },
                      attrs: {
                        id: "melds-points-" + _vm.$store.state.player.id
                      },
                      on: {
                        click: function($event) {
                          _vm.showMelds(_vm.$store.state.player.id)
                        }
                      }
                    },
                    [
                      _vm._v(
                        "\n                        " +
                          _vm._s(_vm.meldsPoints(_vm.$store.state.player.id)) +
                          "\n                    "
                      )
                    ]
                  )
                ]),
                _vm._v(" "),
                _c("td", { staticClass: "text-right" }, [
                  _vm._v(
                    "\n                    " +
                      _vm._s(
                        _vm.$store.getters.total(
                          _vm.$store.state.round.scores,
                          _vm.$store.state.player.id
                        )
                      ) +
                      "\n                "
                  )
                ])
              ]),
              _vm._v(" "),
              _c("tr", [
                _c("td", [_vm._v(_vm._s(_vm.$store.getters.opponent.pseudo))]),
                _vm._v(" "),
                _c("td", { staticClass: "text-right" }, [
                  _vm._v(
                    "\n                    " +
                      _vm._s(
                        _vm.$store.getters.points(
                          _vm.$store.state.round.scores,
                          _vm.$store.getters.opponent.id
                        )
                      ) +
                      "\n                "
                  )
                ]),
                _vm._v(" "),
                _c("td", { staticClass: "text-right" }, [
                  _c(
                    "span",
                    {
                      class: {
                        clickable:
                          _vm.meldsPoints(_vm.$store.getters.opponent.id) > 0
                      },
                      attrs: {
                        id: "melds-points-" + _vm.$store.getters.opponent.id
                      },
                      on: {
                        click: function($event) {
                          _vm.showMelds(_vm.$store.getters.opponent.id)
                        }
                      }
                    },
                    [
                      _vm._v(
                        "\n                        " +
                          _vm._s(
                            _vm.meldsPoints(_vm.$store.getters.opponent.id)
                          ) +
                          "\n                    "
                      )
                    ]
                  )
                ]),
                _vm._v(" "),
                _c("td", { staticClass: "text-right" }, [
                  _vm._v(
                    "\n                    " +
                      _vm._s(
                        _vm.$store.getters.total(
                          _vm.$store.state.round.scores,
                          _vm.$store.getters.opponent.id
                        )
                      ) +
                      "\n                "
                  )
                ])
              ])
            ])
          ])
        ]
      )
    : _vm._e()
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("thead", [
      _c("tr", [
        _c("th", [_vm._v("Who")]),
        _vm._v(" "),
        _c("th", [_vm._v("Pts")]),
        _vm._v(" "),
        _c("th", [_vm._v("Melds")]),
        _vm._v(" "),
        _c("th", [_vm._v("Tot")])
      ])
    ])
  }
]
render._withStripped = true



/***/ }),

/***/ "./resources/js/api/animation.js":
/*!***************************************!*\
  !*** ./resources/js/api/animation.js ***!
  \***************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @babel/runtime/regenerator */ "./node_modules/@babel/runtime/regenerator/index.js");
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var animejs__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! animejs */ "./node_modules/animejs/lib/anime.es.js");


function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }


/* harmony default export */ __webpack_exports__["default"] = ({
  animation: null,

  /**
   * Animate the giving of cards to trick winner
   *
   * @param winnerId
   * @returns {*}
   */
  giveCardsToWinner: function () {
    var _giveCardsToWinner = _asyncToGenerator(
    /*#__PURE__*/
    _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee(winnerId) {
      var $target;
      return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee$(_context) {
        while (1) {
          switch (_context.prev = _context.next) {
            case 0:
              $target = $("#hidden-pocket-".concat(winnerId));
              this.animation = Object(animejs__WEBPACK_IMPORTED_MODULE_1__["default"])({
                targets: ".card-game-played",
                translateY: function translateY(card) {
                  return $target.offset().top - $(card).offset().top;
                },
                scaleX: 0,
                scaleY: 0,
                easing: 'easeOutCubic',
                duration: 750
              });
              return _context.abrupt("return", this.animation.finished);

            case 3:
            case "end":
              return _context.stop();
          }
        }
      }, _callee, this);
    }));

    function giveCardsToWinner(_x) {
      return _giveCardsToWinner.apply(this, arguments);
    }

    return giveCardsToWinner;
  }(),

  /**
   * Reset the current animation
   */
  reset: function reset() {
    this.animation.seek(0);
  },

  /**
   * Animate the drawing of a card from a player hand
   *
   * @param cardId
   * @param playerId
   * @return {*}
   */
  drawFromHand: function () {
    var _drawFromHand = _asyncToGenerator(
    /*#__PURE__*/
    _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee2(cardId, playerId) {
      return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee2$(_context2) {
        while (1) {
          switch (_context2.prev = _context2.next) {
            case 0:
              return _context2.abrupt("return", this._drawCard($("#player-".concat(playerId, "-hand-card-").concat(cardId)), this._buildTargetFromCard($("#player-card-".concat(playerId)))));

            case 1:
            case "end":
              return _context2.stop();
          }
        }
      }, _callee2, this);
    }));

    function drawFromHand(_x2, _x3) {
      return _drawFromHand.apply(this, arguments);
    }

    return drawFromHand;
  }(),

  /**
   * Animate the drawing of a card from the deck to a player
   *
   * @param cardPos
   * @param playerId
   * @return {*}
   */
  drawFromDeck: function () {
    var _drawFromDeck = _asyncToGenerator(
    /*#__PURE__*/
    _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee3(cardPos, playerId) {
      return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee3$(_context3) {
        while (1) {
          switch (_context3.prev = _context3.next) {
            case 0:
              return _context3.abrupt("return", this._drawCard($("#card-deck-".concat(cardPos)), this._buildTargetFromCard($("#player-card-".concat(playerId)))));

            case 1:
            case "end":
              return _context3.stop();
          }
        }
      }, _callee3, this);
    }));

    function drawFromDeck(_x4, _x5) {
      return _drawFromDeck.apply(this, arguments);
    }

    return drawFromDeck;
  }(),

  /**
   * Build a target based on a given card
   *
   * @param $card
   * @returns {{top: *, left: *, width: (*|never), height: (*|never)}}
   * @private
   */
  _buildTargetFromCard: function _buildTargetFromCard($card) {
    return {
      left: $card.offset().left,
      top: $card.offset().top,
      width: $card.outerWidth(),
      height: $card.outerHeight()
    };
  },

  /**
   * Animate the drawing of a card
   *
   * @param $card
   * @param target
   * @returns {*}
   * @private
   */
  _drawCard: function _drawCard($card, target) {
    var scale = {
      x: target.width / $card.outerWidth(),
      y: target.height / $card.outerHeight()
    };
    var translate = {
      x: target.left - $card.offset().left + (scale.x === 1 ? 0 : $card.outerWidth() / scale.x),
      y: target.top - $card.offset().top + (scale.y === 1 ? 0 : $card.outerHeight() / scale.y)
    };
    this.animation = Object(animejs__WEBPACK_IMPORTED_MODULE_1__["default"])({
      targets: "#".concat($card.attr('id')),
      translateX: translate.x,
      translateY: translate.y,
      scaleX: scale.x,
      scaleY: scale.y,
      easing: 'easeOutCubic',
      duration: 750
    });
    return this.animation.finished;
  }
});

/***/ }),

/***/ "./resources/js/api/deck.js":
/*!**********************************!*\
  !*** ./resources/js/api/deck.js ***!
  \**********************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _store_constants__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../store/constants */ "./resources/js/store/constants.js");

/* harmony default export */ __webpack_exports__["default"] = ({
  /**
   * Initialize the deck
   *
   * @return {Array}
   */
  init: function init(cardsDrawn) {
    var _this = this;

    return Array.from(Array(_store_constants__WEBPACK_IMPORTED_MODULE_0__["default"].CARDS_IN_DECK).keys()).map(function (key) {
      return _this._emptyCard(key, cardsDrawn.includes(key));
    });
  },

  /**
   * Build an empty card
   *
   * @param pos
   * @param disabled
   * @returns {{id: null, pos: *, animated: boolean, disabled: boolean}}
   * @private
   */
  _emptyCard: function _emptyCard(pos, disabled) {
    return {
      id: null,
      pos: pos,
      animated: false,
      disabled: disabled
    };
  }
});

/***/ }),

/***/ "./resources/js/api/game.js":
/*!**********************************!*\
  !*** ./resources/js/api/game.js ***!
  \**********************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @babel/runtime/regenerator */ "./node_modules/@babel/runtime/regenerator/index.js");
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__);


function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

/* harmony default export */ __webpack_exports__["default"] = ({
  /**
   * Join a game
   *
   * @param type
   * @param callback
   * @return {*}
   */
  join: function () {
    var _join = _asyncToGenerator(
    /*#__PURE__*/
    _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee(type, callback) {
      return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee$(_context) {
        while (1) {
          switch (_context.prev = _context.next) {
            case 0:
              return _context.abrupt("return", axios.post('/games', {
                type: type
              }));

            case 1:
            case "end":
              return _context.stop();
          }
        }
      }, _callee, this);
    }));

    function join(_x, _x2) {
      return _join.apply(this, arguments);
    }

    return join;
  }(),

  /**
   * Wait for other players to join
   *
   * @param channel
   * @param callback
   */
  waitForOthers: function waitForOthers(channel, callback) {
    window.Echo.private(channel).listen('PlayerJoinedGame', function (_ref) {
      var player = _ref.player;
      return callback(player);
    });
  },

  /**
   * Wait for a card drawn from deck
   *
   * @param channel
   * @param callback
   */
  waitForCardDrawnFromDeck: function waitForCardDrawnFromDeck(channel, callback) {
    window.Echo.private(channel).listen('PlayerDrewCardFromDeck', function (data) {
      return callback(data);
    });
  },

  /**
   * Wait for same rank drawn from deck
   *
   * @param channel
   * @param callback
   */
  waitForSameRankDrawnFromDeck: function waitForSameRankDrawnFromDeck(channel, callback) {
    window.Echo.private(channel).listen('PlayersDrewSameRankFromDeck', function (data) {
      return callback(data);
    });
  },

  /**
   * Wait for the selection round to be completed
   *
   * @param channel
   * @param callback
   */
  waitForSelectionRoundCompleted: function waitForSelectionRoundCompleted(channel, callback) {
    window.Echo.private(channel).listen('SelectionRoundCompleted', function (data) {
      return callback(data);
    });
  },

  /**
   * Wait for a card drawn by others
   *
   * @param channel
   * @param callback
   */
  waitForCardDrawnFromHand: function waitForCardDrawnFromHand(channel, callback) {
    window.Echo.private(channel).listen('PlayerDrewCardFromHand', function (data) {
      return callback(data);
    });
  },

  /**
   * Wait for a player who won melds
   *
   * @param channel
   * @param callback
   */
  waitForPlayerWonMelds: function waitForPlayerWonMelds(channel, callback) {
    window.Echo.private(channel).listen('PlayerWonMelds', function (data) {
      return callback(data);
    });
  },

  /**
   * Wait to know who is next
   *
   * @param channel
   * @param callback
   */
  waitForWhoIsNext: function waitForWhoIsNext(channel, callback) {
    window.Echo.private(channel).listen('WhoIsNext', function (data) {
      return callback(data);
    });
  },

  /**
   * Wait for a normal round to be completed
   *
   * @param channel
   * @param callback
   */
  waitForNormalRoundCompleted: function waitForNormalRoundCompleted(channel, callback) {
    window.Echo.private(channel).listen('NormalRoundCompleted', function (data) {
      return callback(data);
    });
  },

  /**
   * Wait for a new round to be kicked off
   *
   * @param channel
   * @param callback
   */
  waitForNewRound: function waitForNewRound(channel, callback) {
    window.Echo.private(channel).listen('NewRoundStarted', function (data) {
      return callback(data);
    });
  },

  /**
   * Wait for backup cards
   *
   * @param channel
   * @param callback
   */
  waitForBackupTaken: function waitForBackupTaken(channel, callback) {
    window.Echo.private(channel).listen('BackupWasTakenByPlayer', function (data) {
      return callback(data);
    });
  },

  /**
   * Wait for trump taken by other player
   *
   * @param channel
   * @param callback
   */
  waitForTookTrump: function waitForTookTrump(channel, callback) {
    window.Echo.private(channel).listen('PlayerTookTrump', function (data) {
      return callback(data);
    });
  },

  /**
   * Wait for trump taken by me
   *
   * @param channel
   * @param callback
   */
  waitForTrumpTaken: function waitForTrumpTaken(channel, callback) {
    window.Echo.private(channel).listen('TrumpWasTakenByPlayer', function (data) {
      return callback(data);
    });
  }
});

/***/ }),

/***/ "./resources/js/api/player.js":
/*!************************************!*\
  !*** ./resources/js/api/player.js ***!
  \************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @babel/runtime/regenerator */ "./node_modules/@babel/runtime/regenerator/index.js");
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__);


function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

/* harmony default export */ __webpack_exports__["default"] = ({
  /**
   * Draw a card from the deck
   *
   * @param gameId
   * @param cardPos
   */
  drawCardFromDeck: function () {
    var _drawCardFromDeck = _asyncToGenerator(
    /*#__PURE__*/
    _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee(gameId, cardPos) {
      return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee$(_context) {
        while (1) {
          switch (_context.prev = _context.next) {
            case 0:
              return _context.abrupt("return", axios.post("/games/".concat(gameId, "/rounds/0"), {
                card_pos: cardPos
              }));

            case 1:
            case "end":
              return _context.stop();
          }
        }
      }, _callee, this);
    }));

    function drawCardFromDeck(_x, _x2) {
      return _drawCardFromDeck.apply(this, arguments);
    }

    return drawCardFromDeck;
  }(),

  /**
   * Draw a card from the hand
   *
   * @param gameId
   * @param roundNb
   * @param cardId
   * @return {*}
   */
  drawCardFromHand: function () {
    var _drawCardFromHand = _asyncToGenerator(
    /*#__PURE__*/
    _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee2(gameId, roundNb, cardId) {
      return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee2$(_context2) {
        while (1) {
          switch (_context2.prev = _context2.next) {
            case 0:
              return _context2.abrupt("return", axios.post("/games/".concat(gameId, "/rounds/").concat(roundNb), {
                card_id: cardId
              }));

            case 1:
            case "end":
              return _context2.stop();
          }
        }
      }, _callee2, this);
    }));

    function drawCardFromHand(_x3, _x4, _x5) {
      return _drawCardFromHand.apply(this, arguments);
    }

    return drawCardFromHand;
  }(),

  /**
   * Take the backup card
   *
   * @param playerId
   * @param discarded
   * @param callback
   */
  takeBackup: function takeBackup(playerId, discarded, callback) {
    axios.post("/players/".concat(playerId, "/backup"), {
      discarded: discarded
    });
  },

  /**
   * Exchange the six of trump with the trump card
   *
   * @returns {*}
   */
  exchangeSixOfTrump: function exchangeSixOfTrump(playerId) {
    return axios.post("/players/".concat(playerId, "/trump"), {
      'dummy': 'dummy'
    });
  },

  /**
   * Find the Six of Trump in a hand
   *
   * @param hand
   * @param trump
   * @returns {*}
   */
  sixOfTrump: function sixOfTrump(hand, trump) {
    if (!trump) return null;
    return hand.find(function (card) {
      return card.suit.code === trump.suit.code && card.rank.symbol === '6';
    });
  },

  /**
   * Get a card by its identifier from the given hand
   *
   * @param hand
   * @param cardId
   * @returns {*}
   */
  cardFromHandById: function cardFromHandById(hand, cardId) {
    return hand.find(function (card) {
      return card.id === cardId;
    });
  },

  /**
   * Get the hand without a given card
   *
   * @param hand
   * @param cardId
   * @returns {*}
   */
  handWithoutCard: function handWithoutCard(hand, cardId) {
    var new_hand = hand.slice();
    new_hand.splice(hand.findIndex(function (card) {
      return card.id === cardId;
    }), 1);
    return new_hand;
  },

  /**
   * Get a fake hand for other players
   *
   * @param nbCards
   * @returns {*}
   */
  fakeHand: function fakeHand(nbCards) {
    return Array.from(Array(nbCards).keys()).map(function (key) {
      return {
        'id': key + 1
      };
    });
  },

  /**
   * Determine the CSS identifier for a player's card
   *
   * @param playerId
   * @param cardId
   * @returns {string}
   */
  cardCssId: function cardCssId(playerId, cardId) {
    return "player-".concat(playerId, "-hand-card-").concat(cardId);
  },

  /**
   * Show the best meld title for a given player
   *
   * @param playerId
   * @param title
   */
  showBestMeldTitle: function showBestMeldTitle(playerId, title) {
    $("#player-hand-".concat(playerId)).popover('dispose').popover({
      content: "<strong class=\"px-2\">".concat(title, "</strong>"),
      html: true,
      trigger: 'manual',
      placement: 'bottom'
    }).popover('show');
  },

  /**
   * Hide the best meld title for a given player
   *
   * @param playerId
   */
  hideBestMeldTitle: function hideBestMeldTitle(playerId) {
    $("#player-hand-".concat(playerId)).popover('hide').popover('dispose');
  },

  /**
   * Show the list of melds for a given player
   *
   * @param playerId
   */
  showMelds: function showMelds(playerId) {
    $("#melds-modal-".concat(playerId)).modal('show');
  },

  /**
   * Check for a given hand if a card can be played against another one
   *
   * @param hand
   * @param myCard
   * @param otherCard
   * @returns {boolean}
   */
  canDrawCard: function canDrawCard(hand, myCard, otherCard) {
    // The trump is almighty and can be drawn anytime
    if (myCard.suit.trump) {
      return true;
    } // If the player doesn't have any card in that suit, they can play any card


    if (!this._hasSuit(hand, otherCard)) {
      return true;
    } // Special case when the Jass (Jack of trump) is the last trump in hand:
    // - Can be held till the end, so any other card can be drawn


    if (otherCard.suit.trump && this._nbTrumps(hand) === 1 && this._hasJass(hand)) {
      return true;
    } // The player must draw cards of same color


    return this._cardInSuit(myCard, otherCard.suit);
  },

  /**
   * Check if a card is in a suit
   *
   * @param card
   * @param suit
   * @returns {boolean}
   * @private
   */
  _cardInSuit: function _cardInSuit(card, suit) {
    return card.suit.code === suit.code;
  },

  /**
   * Does the player have the Jass (Jack of Trump)?
   *
   * @param hand
   * @returns {boolean}
   * @private
   */
  _hasJass: function _hasJass(hand) {
    return hand.find(function (card) {
      return card.suit.trump && card.rank.symbol === 'J';
    }) !== undefined;
  },

  /**
   * Does the player have a given suit?
   *
   * @param hand
   * @param someCard
   * @returns {boolean}
   * @private
   */
  _hasSuit: function _hasSuit(hand, someCard) {
    if (!someCard) return false;
    return hand.find(function (card) {
      return card.suit.code === someCard.suit.code;
    }) !== undefined;
  },

  /**
   * How many trumps does the player have?
   *
   * @param hand
   * @returns {number}
   * @private
   */
  _nbTrumps: function _nbTrumps(hand) {
    return hand.filter(function (card) {
      return card.suit.trump;
    }).length;
  }
});

/***/ }),

/***/ "./resources/js/api/score.js":
/*!***********************************!*\
  !*** ./resources/js/api/score.js ***!
  \***********************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ({
  /**
   * Show the results of last round
   */
  showLastRoundResults: function showLastRoundResults() {
    $('#round-results-modal').modal('show');
  },

  /**
   * Get game scores for a player
   *
   * @param scores
   * @param playerId
   * @returns {*}
   */
  score: function score(scores, playerId) {
    return scores.find(function (score) {
      return score.player_id === playerId;
    });
  },

  /**
   * Find round points for a player
   *
   * @param scores
   * @param playerId
   * @returns {*}
   */
  points: function points(scores, playerId) {
    return this._roundScore(scores, playerId, 'points');
  },

  /**
   * Find round melds for a player
   *
   * @param scores
   * @param playerId
   * @returns {*}
   */
  melds: function melds(scores, playerId) {
    return this._roundScore(scores, playerId, 'melds');
  },

  /**
   * Find round total for a player
   *
   * @param scores
   * @param playerId
   * @returns {*}
   */
  total: function total(scores, playerId) {
    return this._roundScore(scores, playerId, 'total');
  },

  /**
   * Get a round score by type for a given scores and player
   *
   * @param scores
   * @param playerId
   * @param scoreType
   * @returns {number}
   * @private
   */
  _roundScore: function _roundScore(scores, playerId, scoreType) {
    if (!scores || !playerId) return 0;
    var player_score = this.score(scores, playerId);
    return player_score ? player_score[scoreType] : 0;
  }
});

/***/ }),

/***/ "./resources/js/bootstrap.js":
/*!***********************************!*\
  !*** ./resources/js/bootstrap.js ***!
  \***********************************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var laravel_echo__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! laravel-echo */ "./node_modules/laravel-echo/dist/echo.js");
window._ = __webpack_require__(/*! lodash */ "./node_modules/lodash/lodash.js");
window.Popper = __webpack_require__(/*! popper.js */ "./node_modules/popper.js/dist/esm/popper.js").default;
/**
 * We'll load jQuery and the Bootstrap jQuery plugin which provides support
 * for JavaScript based Bootstrap features such as modals and tabs. This
 * code may be modified to fit the specific needs of your application.
 */

try {
  window.$ = window.jQuery = __webpack_require__(/*! jquery */ "./node_modules/jquery/dist/jquery.js");

  __webpack_require__(/*! bootstrap */ "./node_modules/bootstrap/dist/js/bootstrap.js");
} catch (e) {}
/**
 * We'll load the axios HTTP library which allows us to easily issue requests
 * to our Laravel back-end. This library automatically handles sending the
 * CSRF token as a header based on the value of the "XSRF" token cookie.
 */


window.axios = __webpack_require__(/*! axios */ "./node_modules/axios/index.js");
window.axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';
/**
 * Next we will register the CSRF Token as a common header with Axios so that
 * all outgoing HTTP requests automatically have it attached. This is just
 * a simple convenience so we don't have to attach every token manually.
 */

var token = document.head.querySelector('meta[name="csrf-token"]');

if (token) {
  window.axios.defaults.headers.common['X-CSRF-TOKEN'] = token.content;
} else {
  console.error('CSRF token not found: https://laravel.com/docs/csrf#csrf-x-csrf-token');
}
/**
 * Echo exposes an expressive API for subscribing to channels and listening
 * for events that are broadcast by Laravel. Echo and event broadcasting
 * allows your team to easily build robust real-time web applications.
 */



window.Pusher = __webpack_require__(/*! pusher-js */ "./node_modules/pusher-js/dist/web/pusher.js");
window.Echo = new laravel_echo__WEBPACK_IMPORTED_MODULE_0__["default"]({
  broadcaster: 'pusher',
  key: "f4c41d75d6d039ecbbca",
  cluster: "eu",
  encrypted: true
});

/***/ }),

/***/ "./resources/js/components/Card.vue":
/*!******************************************!*\
  !*** ./resources/js/components/Card.vue ***!
  \******************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _Card_vue_vue_type_template_id_b9bc2c0a_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./Card.vue?vue&type=template&id=b9bc2c0a&scoped=true& */ "./resources/js/components/Card.vue?vue&type=template&id=b9bc2c0a&scoped=true&");
/* harmony import */ var _Card_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./Card.vue?vue&type=script&lang=js& */ "./resources/js/components/Card.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _Card_vue_vue_type_style_index_0_id_b9bc2c0a_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./Card.vue?vue&type=style&index=0&id=b9bc2c0a&scoped=true&lang=css& */ "./resources/js/components/Card.vue?vue&type=style&index=0&id=b9bc2c0a&scoped=true&lang=css&");
/* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");






/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__["default"])(
  _Card_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _Card_vue_vue_type_template_id_b9bc2c0a_scoped_true___WEBPACK_IMPORTED_MODULE_0__["render"],
  _Card_vue_vue_type_template_id_b9bc2c0a_scoped_true___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  "b9bc2c0a",
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/components/Card.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/components/Card.vue?vue&type=script&lang=js&":
/*!*******************************************************************!*\
  !*** ./resources/js/components/Card.vue?vue&type=script&lang=js& ***!
  \*******************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Card_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../node_modules/babel-loader/lib??ref--4-0!../../../node_modules/vue-loader/lib??vue-loader-options!./Card.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/Card.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Card_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/components/Card.vue?vue&type=style&index=0&id=b9bc2c0a&scoped=true&lang=css&":
/*!***************************************************************************************************!*\
  !*** ./resources/js/components/Card.vue?vue&type=style&index=0&id=b9bc2c0a&scoped=true&lang=css& ***!
  \***************************************************************************************************/
/*! no static exports found */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_Card_vue_vue_type_style_index_0_id_b9bc2c0a_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../node_modules/style-loader!../../../node_modules/css-loader??ref--6-1!../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../node_modules/postcss-loader/src??ref--6-2!../../../node_modules/vue-loader/lib??vue-loader-options!./Card.vue?vue&type=style&index=0&id=b9bc2c0a&scoped=true&lang=css& */ "./node_modules/style-loader/index.js!./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/Card.vue?vue&type=style&index=0&id=b9bc2c0a&scoped=true&lang=css&");
/* harmony import */ var _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_Card_vue_vue_type_style_index_0_id_b9bc2c0a_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_Card_vue_vue_type_style_index_0_id_b9bc2c0a_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_Card_vue_vue_type_style_index_0_id_b9bc2c0a_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0__) if(__WEBPACK_IMPORT_KEY__ !== 'default') (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_Card_vue_vue_type_style_index_0_id_b9bc2c0a_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0__[key]; }) }(__WEBPACK_IMPORT_KEY__));
 /* harmony default export */ __webpack_exports__["default"] = (_node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_Card_vue_vue_type_style_index_0_id_b9bc2c0a_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0___default.a); 

/***/ }),

/***/ "./resources/js/components/Card.vue?vue&type=template&id=b9bc2c0a&scoped=true&":
/*!*************************************************************************************!*\
  !*** ./resources/js/components/Card.vue?vue&type=template&id=b9bc2c0a&scoped=true& ***!
  \*************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Card_vue_vue_type_template_id_b9bc2c0a_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../node_modules/vue-loader/lib??vue-loader-options!./Card.vue?vue&type=template&id=b9bc2c0a&scoped=true& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/Card.vue?vue&type=template&id=b9bc2c0a&scoped=true&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Card_vue_vue_type_template_id_b9bc2c0a_scoped_true___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Card_vue_vue_type_template_id_b9bc2c0a_scoped_true___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./resources/js/components/CardsDeck.vue":
/*!***********************************************!*\
  !*** ./resources/js/components/CardsDeck.vue ***!
  \***********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _CardsDeck_vue_vue_type_template_id_60c73a5e___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./CardsDeck.vue?vue&type=template&id=60c73a5e& */ "./resources/js/components/CardsDeck.vue?vue&type=template&id=60c73a5e&");
/* harmony import */ var _CardsDeck_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./CardsDeck.vue?vue&type=script&lang=js& */ "./resources/js/components/CardsDeck.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _CardsDeck_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _CardsDeck_vue_vue_type_template_id_60c73a5e___WEBPACK_IMPORTED_MODULE_0__["render"],
  _CardsDeck_vue_vue_type_template_id_60c73a5e___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/components/CardsDeck.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/components/CardsDeck.vue?vue&type=script&lang=js&":
/*!************************************************************************!*\
  !*** ./resources/js/components/CardsDeck.vue?vue&type=script&lang=js& ***!
  \************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_CardsDeck_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../node_modules/babel-loader/lib??ref--4-0!../../../node_modules/vue-loader/lib??vue-loader-options!./CardsDeck.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/CardsDeck.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_CardsDeck_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/components/CardsDeck.vue?vue&type=template&id=60c73a5e&":
/*!******************************************************************************!*\
  !*** ./resources/js/components/CardsDeck.vue?vue&type=template&id=60c73a5e& ***!
  \******************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_CardsDeck_vue_vue_type_template_id_60c73a5e___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../node_modules/vue-loader/lib??vue-loader-options!./CardsDeck.vue?vue&type=template&id=60c73a5e& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/CardsDeck.vue?vue&type=template&id=60c73a5e&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_CardsDeck_vue_vue_type_template_id_60c73a5e___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_CardsDeck_vue_vue_type_template_id_60c73a5e___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./resources/js/components/MeldsList.vue":
/*!***********************************************!*\
  !*** ./resources/js/components/MeldsList.vue ***!
  \***********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _MeldsList_vue_vue_type_template_id_1ed88b74___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./MeldsList.vue?vue&type=template&id=1ed88b74& */ "./resources/js/components/MeldsList.vue?vue&type=template&id=1ed88b74&");
/* harmony import */ var _MeldsList_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./MeldsList.vue?vue&type=script&lang=js& */ "./resources/js/components/MeldsList.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _MeldsList_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./MeldsList.vue?vue&type=style&index=0&lang=css& */ "./resources/js/components/MeldsList.vue?vue&type=style&index=0&lang=css&");
/* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");






/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__["default"])(
  _MeldsList_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _MeldsList_vue_vue_type_template_id_1ed88b74___WEBPACK_IMPORTED_MODULE_0__["render"],
  _MeldsList_vue_vue_type_template_id_1ed88b74___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/components/MeldsList.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/components/MeldsList.vue?vue&type=script&lang=js&":
/*!************************************************************************!*\
  !*** ./resources/js/components/MeldsList.vue?vue&type=script&lang=js& ***!
  \************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_MeldsList_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../node_modules/babel-loader/lib??ref--4-0!../../../node_modules/vue-loader/lib??vue-loader-options!./MeldsList.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/MeldsList.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_MeldsList_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/components/MeldsList.vue?vue&type=style&index=0&lang=css&":
/*!********************************************************************************!*\
  !*** ./resources/js/components/MeldsList.vue?vue&type=style&index=0&lang=css& ***!
  \********************************************************************************/
/*! no static exports found */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_MeldsList_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../node_modules/style-loader!../../../node_modules/css-loader??ref--6-1!../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../node_modules/postcss-loader/src??ref--6-2!../../../node_modules/vue-loader/lib??vue-loader-options!./MeldsList.vue?vue&type=style&index=0&lang=css& */ "./node_modules/style-loader/index.js!./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/MeldsList.vue?vue&type=style&index=0&lang=css&");
/* harmony import */ var _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_MeldsList_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_MeldsList_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_MeldsList_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0__) if(__WEBPACK_IMPORT_KEY__ !== 'default') (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_MeldsList_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0__[key]; }) }(__WEBPACK_IMPORT_KEY__));
 /* harmony default export */ __webpack_exports__["default"] = (_node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_MeldsList_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0___default.a); 

/***/ }),

/***/ "./resources/js/components/MeldsList.vue?vue&type=template&id=1ed88b74&":
/*!******************************************************************************!*\
  !*** ./resources/js/components/MeldsList.vue?vue&type=template&id=1ed88b74& ***!
  \******************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_MeldsList_vue_vue_type_template_id_1ed88b74___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../node_modules/vue-loader/lib??vue-loader-options!./MeldsList.vue?vue&type=template&id=1ed88b74& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/MeldsList.vue?vue&type=template&id=1ed88b74&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_MeldsList_vue_vue_type_template_id_1ed88b74___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_MeldsList_vue_vue_type_template_id_1ed88b74___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./resources/js/components/OtherHand.vue":
/*!***********************************************!*\
  !*** ./resources/js/components/OtherHand.vue ***!
  \***********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _OtherHand_vue_vue_type_template_id_05572378___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./OtherHand.vue?vue&type=template&id=05572378& */ "./resources/js/components/OtherHand.vue?vue&type=template&id=05572378&");
/* harmony import */ var _OtherHand_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./OtherHand.vue?vue&type=script&lang=js& */ "./resources/js/components/OtherHand.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _OtherHand_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _OtherHand_vue_vue_type_template_id_05572378___WEBPACK_IMPORTED_MODULE_0__["render"],
  _OtherHand_vue_vue_type_template_id_05572378___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/components/OtherHand.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/components/OtherHand.vue?vue&type=script&lang=js&":
/*!************************************************************************!*\
  !*** ./resources/js/components/OtherHand.vue?vue&type=script&lang=js& ***!
  \************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_OtherHand_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../node_modules/babel-loader/lib??ref--4-0!../../../node_modules/vue-loader/lib??vue-loader-options!./OtherHand.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/OtherHand.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_OtherHand_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/components/OtherHand.vue?vue&type=template&id=05572378&":
/*!******************************************************************************!*\
  !*** ./resources/js/components/OtherHand.vue?vue&type=template&id=05572378& ***!
  \******************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_OtherHand_vue_vue_type_template_id_05572378___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../node_modules/vue-loader/lib??vue-loader-options!./OtherHand.vue?vue&type=template&id=05572378& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/OtherHand.vue?vue&type=template&id=05572378&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_OtherHand_vue_vue_type_template_id_05572378___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_OtherHand_vue_vue_type_template_id_05572378___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./resources/js/components/PlayerHand.vue":
/*!************************************************!*\
  !*** ./resources/js/components/PlayerHand.vue ***!
  \************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _PlayerHand_vue_vue_type_template_id_9c89a8ca___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./PlayerHand.vue?vue&type=template&id=9c89a8ca& */ "./resources/js/components/PlayerHand.vue?vue&type=template&id=9c89a8ca&");
/* harmony import */ var _PlayerHand_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./PlayerHand.vue?vue&type=script&lang=js& */ "./resources/js/components/PlayerHand.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _PlayerHand_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _PlayerHand_vue_vue_type_template_id_9c89a8ca___WEBPACK_IMPORTED_MODULE_0__["render"],
  _PlayerHand_vue_vue_type_template_id_9c89a8ca___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/components/PlayerHand.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/components/PlayerHand.vue?vue&type=script&lang=js&":
/*!*************************************************************************!*\
  !*** ./resources/js/components/PlayerHand.vue?vue&type=script&lang=js& ***!
  \*************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_PlayerHand_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../node_modules/babel-loader/lib??ref--4-0!../../../node_modules/vue-loader/lib??vue-loader-options!./PlayerHand.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/PlayerHand.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_PlayerHand_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/components/PlayerHand.vue?vue&type=template&id=9c89a8ca&":
/*!*******************************************************************************!*\
  !*** ./resources/js/components/PlayerHand.vue?vue&type=template&id=9c89a8ca& ***!
  \*******************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_PlayerHand_vue_vue_type_template_id_9c89a8ca___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../node_modules/vue-loader/lib??vue-loader-options!./PlayerHand.vue?vue&type=template&id=9c89a8ca& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/PlayerHand.vue?vue&type=template&id=9c89a8ca&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_PlayerHand_vue_vue_type_template_id_9c89a8ca___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_PlayerHand_vue_vue_type_template_id_9c89a8ca___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./resources/js/components/pomme/Carpet.vue":
/*!**************************************************!*\
  !*** ./resources/js/components/pomme/Carpet.vue ***!
  \**************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _Carpet_vue_vue_type_template_id_db5c9f02___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./Carpet.vue?vue&type=template&id=db5c9f02& */ "./resources/js/components/pomme/Carpet.vue?vue&type=template&id=db5c9f02&");
/* harmony import */ var _Carpet_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./Carpet.vue?vue&type=script&lang=js& */ "./resources/js/components/pomme/Carpet.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _Carpet_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./Carpet.vue?vue&type=style&index=0&lang=css& */ "./resources/js/components/pomme/Carpet.vue?vue&type=style&index=0&lang=css&");
/* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");






/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__["default"])(
  _Carpet_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _Carpet_vue_vue_type_template_id_db5c9f02___WEBPACK_IMPORTED_MODULE_0__["render"],
  _Carpet_vue_vue_type_template_id_db5c9f02___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/components/pomme/Carpet.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/components/pomme/Carpet.vue?vue&type=script&lang=js&":
/*!***************************************************************************!*\
  !*** ./resources/js/components/pomme/Carpet.vue?vue&type=script&lang=js& ***!
  \***************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Carpet_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/babel-loader/lib??ref--4-0!../../../../node_modules/vue-loader/lib??vue-loader-options!./Carpet.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/pomme/Carpet.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Carpet_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/components/pomme/Carpet.vue?vue&type=style&index=0&lang=css&":
/*!***********************************************************************************!*\
  !*** ./resources/js/components/pomme/Carpet.vue?vue&type=style&index=0&lang=css& ***!
  \***********************************************************************************/
/*! no static exports found */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_Carpet_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/style-loader!../../../../node_modules/css-loader??ref--6-1!../../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../../node_modules/postcss-loader/src??ref--6-2!../../../../node_modules/vue-loader/lib??vue-loader-options!./Carpet.vue?vue&type=style&index=0&lang=css& */ "./node_modules/style-loader/index.js!./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/pomme/Carpet.vue?vue&type=style&index=0&lang=css&");
/* harmony import */ var _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_Carpet_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_Carpet_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_Carpet_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0__) if(__WEBPACK_IMPORT_KEY__ !== 'default') (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_Carpet_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0__[key]; }) }(__WEBPACK_IMPORT_KEY__));
 /* harmony default export */ __webpack_exports__["default"] = (_node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_Carpet_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0___default.a); 

/***/ }),

/***/ "./resources/js/components/pomme/Carpet.vue?vue&type=template&id=db5c9f02&":
/*!*********************************************************************************!*\
  !*** ./resources/js/components/pomme/Carpet.vue?vue&type=template&id=db5c9f02& ***!
  \*********************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Carpet_vue_vue_type_template_id_db5c9f02___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../node_modules/vue-loader/lib??vue-loader-options!./Carpet.vue?vue&type=template&id=db5c9f02& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/pomme/Carpet.vue?vue&type=template&id=db5c9f02&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Carpet_vue_vue_type_template_id_db5c9f02___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Carpet_vue_vue_type_template_id_db5c9f02___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./resources/js/components/pomme/PlayerHandDiscarded.vue":
/*!***************************************************************!*\
  !*** ./resources/js/components/pomme/PlayerHandDiscarded.vue ***!
  \***************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _PlayerHandDiscarded_vue_vue_type_template_id_55e963a9_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./PlayerHandDiscarded.vue?vue&type=template&id=55e963a9&scoped=true& */ "./resources/js/components/pomme/PlayerHandDiscarded.vue?vue&type=template&id=55e963a9&scoped=true&");
/* harmony import */ var _PlayerHandDiscarded_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./PlayerHandDiscarded.vue?vue&type=script&lang=js& */ "./resources/js/components/pomme/PlayerHandDiscarded.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _PlayerHandDiscarded_vue_vue_type_style_index_0_id_55e963a9_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./PlayerHandDiscarded.vue?vue&type=style&index=0&id=55e963a9&scoped=true&lang=css& */ "./resources/js/components/pomme/PlayerHandDiscarded.vue?vue&type=style&index=0&id=55e963a9&scoped=true&lang=css&");
/* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");






/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__["default"])(
  _PlayerHandDiscarded_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _PlayerHandDiscarded_vue_vue_type_template_id_55e963a9_scoped_true___WEBPACK_IMPORTED_MODULE_0__["render"],
  _PlayerHandDiscarded_vue_vue_type_template_id_55e963a9_scoped_true___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  "55e963a9",
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/components/pomme/PlayerHandDiscarded.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/components/pomme/PlayerHandDiscarded.vue?vue&type=script&lang=js&":
/*!****************************************************************************************!*\
  !*** ./resources/js/components/pomme/PlayerHandDiscarded.vue?vue&type=script&lang=js& ***!
  \****************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_PlayerHandDiscarded_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/babel-loader/lib??ref--4-0!../../../../node_modules/vue-loader/lib??vue-loader-options!./PlayerHandDiscarded.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/pomme/PlayerHandDiscarded.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_PlayerHandDiscarded_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/components/pomme/PlayerHandDiscarded.vue?vue&type=style&index=0&id=55e963a9&scoped=true&lang=css&":
/*!************************************************************************************************************************!*\
  !*** ./resources/js/components/pomme/PlayerHandDiscarded.vue?vue&type=style&index=0&id=55e963a9&scoped=true&lang=css& ***!
  \************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_PlayerHandDiscarded_vue_vue_type_style_index_0_id_55e963a9_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/style-loader!../../../../node_modules/css-loader??ref--6-1!../../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../../node_modules/postcss-loader/src??ref--6-2!../../../../node_modules/vue-loader/lib??vue-loader-options!./PlayerHandDiscarded.vue?vue&type=style&index=0&id=55e963a9&scoped=true&lang=css& */ "./node_modules/style-loader/index.js!./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/pomme/PlayerHandDiscarded.vue?vue&type=style&index=0&id=55e963a9&scoped=true&lang=css&");
/* harmony import */ var _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_PlayerHandDiscarded_vue_vue_type_style_index_0_id_55e963a9_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_PlayerHandDiscarded_vue_vue_type_style_index_0_id_55e963a9_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_PlayerHandDiscarded_vue_vue_type_style_index_0_id_55e963a9_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0__) if(__WEBPACK_IMPORT_KEY__ !== 'default') (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_PlayerHandDiscarded_vue_vue_type_style_index_0_id_55e963a9_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0__[key]; }) }(__WEBPACK_IMPORT_KEY__));
 /* harmony default export */ __webpack_exports__["default"] = (_node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_PlayerHandDiscarded_vue_vue_type_style_index_0_id_55e963a9_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0___default.a); 

/***/ }),

/***/ "./resources/js/components/pomme/PlayerHandDiscarded.vue?vue&type=template&id=55e963a9&scoped=true&":
/*!**********************************************************************************************************!*\
  !*** ./resources/js/components/pomme/PlayerHandDiscarded.vue?vue&type=template&id=55e963a9&scoped=true& ***!
  \**********************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_PlayerHandDiscarded_vue_vue_type_template_id_55e963a9_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../node_modules/vue-loader/lib??vue-loader-options!./PlayerHandDiscarded.vue?vue&type=template&id=55e963a9&scoped=true& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/pomme/PlayerHandDiscarded.vue?vue&type=template&id=55e963a9&scoped=true&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_PlayerHandDiscarded_vue_vue_type_template_id_55e963a9_scoped_true___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_PlayerHandDiscarded_vue_vue_type_template_id_55e963a9_scoped_true___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./resources/js/components/pomme/RoundResults.vue":
/*!********************************************************!*\
  !*** ./resources/js/components/pomme/RoundResults.vue ***!
  \********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _RoundResults_vue_vue_type_template_id_7235dabc___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./RoundResults.vue?vue&type=template&id=7235dabc& */ "./resources/js/components/pomme/RoundResults.vue?vue&type=template&id=7235dabc&");
/* harmony import */ var _RoundResults_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./RoundResults.vue?vue&type=script&lang=js& */ "./resources/js/components/pomme/RoundResults.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _RoundResults_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./RoundResults.vue?vue&type=style&index=0&lang=css& */ "./resources/js/components/pomme/RoundResults.vue?vue&type=style&index=0&lang=css&");
/* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");






/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__["default"])(
  _RoundResults_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _RoundResults_vue_vue_type_template_id_7235dabc___WEBPACK_IMPORTED_MODULE_0__["render"],
  _RoundResults_vue_vue_type_template_id_7235dabc___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/components/pomme/RoundResults.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/components/pomme/RoundResults.vue?vue&type=script&lang=js&":
/*!*********************************************************************************!*\
  !*** ./resources/js/components/pomme/RoundResults.vue?vue&type=script&lang=js& ***!
  \*********************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_RoundResults_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/babel-loader/lib??ref--4-0!../../../../node_modules/vue-loader/lib??vue-loader-options!./RoundResults.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/pomme/RoundResults.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_RoundResults_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/components/pomme/RoundResults.vue?vue&type=style&index=0&lang=css&":
/*!*****************************************************************************************!*\
  !*** ./resources/js/components/pomme/RoundResults.vue?vue&type=style&index=0&lang=css& ***!
  \*****************************************************************************************/
/*! no static exports found */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_RoundResults_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/style-loader!../../../../node_modules/css-loader??ref--6-1!../../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../../node_modules/postcss-loader/src??ref--6-2!../../../../node_modules/vue-loader/lib??vue-loader-options!./RoundResults.vue?vue&type=style&index=0&lang=css& */ "./node_modules/style-loader/index.js!./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/pomme/RoundResults.vue?vue&type=style&index=0&lang=css&");
/* harmony import */ var _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_RoundResults_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_RoundResults_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_RoundResults_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0__) if(__WEBPACK_IMPORT_KEY__ !== 'default') (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_RoundResults_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0__[key]; }) }(__WEBPACK_IMPORT_KEY__));
 /* harmony default export */ __webpack_exports__["default"] = (_node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_RoundResults_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0___default.a); 

/***/ }),

/***/ "./resources/js/components/pomme/RoundResults.vue?vue&type=template&id=7235dabc&":
/*!***************************************************************************************!*\
  !*** ./resources/js/components/pomme/RoundResults.vue?vue&type=template&id=7235dabc& ***!
  \***************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_RoundResults_vue_vue_type_template_id_7235dabc___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../node_modules/vue-loader/lib??vue-loader-options!./RoundResults.vue?vue&type=template&id=7235dabc& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/pomme/RoundResults.vue?vue&type=template&id=7235dabc&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_RoundResults_vue_vue_type_template_id_7235dabc___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_RoundResults_vue_vue_type_template_id_7235dabc___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./resources/js/components/pomme/Scores.vue":
/*!**************************************************!*\
  !*** ./resources/js/components/pomme/Scores.vue ***!
  \**************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _Scores_vue_vue_type_template_id_318c78d5_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./Scores.vue?vue&type=template&id=318c78d5&scoped=true& */ "./resources/js/components/pomme/Scores.vue?vue&type=template&id=318c78d5&scoped=true&");
/* harmony import */ var _Scores_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./Scores.vue?vue&type=script&lang=js& */ "./resources/js/components/pomme/Scores.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _Scores_vue_vue_type_style_index_0_id_318c78d5_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./Scores.vue?vue&type=style&index=0&id=318c78d5&scoped=true&lang=css& */ "./resources/js/components/pomme/Scores.vue?vue&type=style&index=0&id=318c78d5&scoped=true&lang=css&");
/* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");






/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__["default"])(
  _Scores_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _Scores_vue_vue_type_template_id_318c78d5_scoped_true___WEBPACK_IMPORTED_MODULE_0__["render"],
  _Scores_vue_vue_type_template_id_318c78d5_scoped_true___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  "318c78d5",
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/components/pomme/Scores.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/components/pomme/Scores.vue?vue&type=script&lang=js&":
/*!***************************************************************************!*\
  !*** ./resources/js/components/pomme/Scores.vue?vue&type=script&lang=js& ***!
  \***************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Scores_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/babel-loader/lib??ref--4-0!../../../../node_modules/vue-loader/lib??vue-loader-options!./Scores.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/pomme/Scores.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Scores_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/components/pomme/Scores.vue?vue&type=style&index=0&id=318c78d5&scoped=true&lang=css&":
/*!***********************************************************************************************************!*\
  !*** ./resources/js/components/pomme/Scores.vue?vue&type=style&index=0&id=318c78d5&scoped=true&lang=css& ***!
  \***********************************************************************************************************/
/*! no static exports found */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_Scores_vue_vue_type_style_index_0_id_318c78d5_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/style-loader!../../../../node_modules/css-loader??ref--6-1!../../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../../node_modules/postcss-loader/src??ref--6-2!../../../../node_modules/vue-loader/lib??vue-loader-options!./Scores.vue?vue&type=style&index=0&id=318c78d5&scoped=true&lang=css& */ "./node_modules/style-loader/index.js!./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/pomme/Scores.vue?vue&type=style&index=0&id=318c78d5&scoped=true&lang=css&");
/* harmony import */ var _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_Scores_vue_vue_type_style_index_0_id_318c78d5_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_Scores_vue_vue_type_style_index_0_id_318c78d5_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_Scores_vue_vue_type_style_index_0_id_318c78d5_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0__) if(__WEBPACK_IMPORT_KEY__ !== 'default') (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_Scores_vue_vue_type_style_index_0_id_318c78d5_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0__[key]; }) }(__WEBPACK_IMPORT_KEY__));
 /* harmony default export */ __webpack_exports__["default"] = (_node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_Scores_vue_vue_type_style_index_0_id_318c78d5_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0___default.a); 

/***/ }),

/***/ "./resources/js/components/pomme/Scores.vue?vue&type=template&id=318c78d5&scoped=true&":
/*!*********************************************************************************************!*\
  !*** ./resources/js/components/pomme/Scores.vue?vue&type=template&id=318c78d5&scoped=true& ***!
  \*********************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Scores_vue_vue_type_template_id_318c78d5_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../node_modules/vue-loader/lib??vue-loader-options!./Scores.vue?vue&type=template&id=318c78d5&scoped=true& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/pomme/Scores.vue?vue&type=template&id=318c78d5&scoped=true&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Scores_vue_vue_type_template_id_318c78d5_scoped_true___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Scores_vue_vue_type_template_id_318c78d5_scoped_true___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./resources/js/pomme.js":
/*!*******************************!*\
  !*** ./resources/js/pomme.js ***!
  \*******************************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var vue__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! vue */ "./node_modules/vue/dist/vue.common.js");
/* harmony import */ var vue__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(vue__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _store__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./store */ "./resources/js/store/index.js");
/* harmony import */ var _components_pomme_Scores__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./components/pomme/Scores */ "./resources/js/components/pomme/Scores.vue");
/* harmony import */ var _components_pomme_RoundResults__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./components/pomme/RoundResults */ "./resources/js/components/pomme/RoundResults.vue");
/* harmony import */ var _components_pomme_Carpet__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./components/pomme/Carpet */ "./resources/js/components/pomme/Carpet.vue");
/* harmony import */ var _components_CardsDeck__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./components/CardsDeck */ "./resources/js/components/CardsDeck.vue");
/* harmony import */ var _components_OtherHand__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./components/OtherHand */ "./resources/js/components/OtherHand.vue");
/* harmony import */ var _components_PlayerHand__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./components/PlayerHand */ "./resources/js/components/PlayerHand.vue");
/* harmony import */ var _api_animation__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./api/animation */ "./resources/js/api/animation.js");
__webpack_require__(/*! ./bootstrap */ "./resources/js/bootstrap.js");









new vue__WEBPACK_IMPORTED_MODULE_0___default.a({
  el: '#root',
  components: {
    Scores: _components_pomme_Scores__WEBPACK_IMPORTED_MODULE_2__["default"],
    RoundResults: _components_pomme_RoundResults__WEBPACK_IMPORTED_MODULE_3__["default"],
    Carpet: _components_pomme_Carpet__WEBPACK_IMPORTED_MODULE_4__["default"],
    CardsDeck: _components_CardsDeck__WEBPACK_IMPORTED_MODULE_5__["default"],
    PlayerHand: _components_PlayerHand__WEBPACK_IMPORTED_MODULE_7__["default"],
    OtherHand: _components_OtherHand__WEBPACK_IMPORTED_MODULE_6__["default"]
  },
  store: _store__WEBPACK_IMPORTED_MODULE_1__["default"],
  created: function created() {
    this.$store.dispatch('join', 'pomme');
  }
});

window.animation = _api_animation__WEBPACK_IMPORTED_MODULE_8__["default"];

/***/ }),

/***/ "./resources/js/store/actions.js":
/*!***************************************!*\
  !*** ./resources/js/store/actions.js ***!
  \***************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @babel/runtime/regenerator */ "./node_modules/@babel/runtime/regenerator/index.js");
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _api_animation__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../api/animation */ "./resources/js/api/animation.js");
/* harmony import */ var _api_deck__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../api/deck */ "./resources/js/api/deck.js");
/* harmony import */ var _api_game__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../api/game */ "./resources/js/api/game.js");
/* harmony import */ var _api_player__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../api/player */ "./resources/js/api/player.js");
/* harmony import */ var _api_score__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../api/score */ "./resources/js/api/score.js");


function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }






/* harmony default export */ __webpack_exports__["default"] = ({
  /**
   * Join a game
   *
   * @param commit
   * @param dispatch
   * @param getters
   * @param type
   */
  join: function () {
    var _join = _asyncToGenerator(
    /*#__PURE__*/
    _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee(_ref, type) {
      var commit, dispatch, getters, _ref2, data;

      return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee$(_context) {
        while (1) {
          switch (_context.prev = _context.next) {
            case 0:
              commit = _ref.commit, dispatch = _ref.dispatch, getters = _ref.getters;
              _context.next = 3;
              return _api_game__WEBPACK_IMPORTED_MODULE_3__["default"].join(type);

            case 3:
              _ref2 = _context.sent;
              data = _ref2.data;
              commit('updateAll', data);

              if (!getters.isSelectionRound) {
                commit('updateOtherPlayerHand', _api_player__WEBPACK_IMPORTED_MODULE_4__["default"].fakeHand(data.game.other_players[0].nb_cards));
              }

              if (getters.isSelectionRound) commit('updateDeck', _api_deck__WEBPACK_IMPORTED_MODULE_2__["default"].init(data.round.cards_drawn_indexes));
              dispatch('_initListeners', data);

            case 9:
            case "end":
              return _context.stop();
          }
        }
      }, _callee, this);
    }));

    function join(_x, _x2) {
      return _join.apply(this, arguments);
    }

    return join;
  }(),

  /**
   * Update the deck of cards
   *
   * @param commit
   * @param deck
   */
  updateDeck: function updateDeck(_ref3, deck) {
    var commit = _ref3.commit;
    commit('updateDeck', deck);
  },

  /**
   * Draw a card from the deck
   *
   * @param state
   * @param commit
   * @param cardPos
   */
  drawFromDeck: function () {
    var _drawFromDeck = _asyncToGenerator(
    /*#__PURE__*/
    _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee2(_ref4, cardPos) {
      var state, commit, _ref5, data;

      return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee2$(_context2) {
        while (1) {
          switch (_context2.prev = _context2.next) {
            case 0:
              state = _ref4.state, commit = _ref4.commit;
              commit('updateWhoIsNext', -1);
              commit('startDrawFromDeck', cardPos);
              _context2.next = 5;
              return _api_animation__WEBPACK_IMPORTED_MODULE_1__["default"].drawFromDeck(cardPos, state.player.id);

            case 5:
              _context2.next = 7;
              return _api_player__WEBPACK_IMPORTED_MODULE_4__["default"].drawCardFromDeck(state.game.id, cardPos);

            case 7:
              _ref5 = _context2.sent;
              data = _ref5.data;
              if (!state.player.card) commit('updatePlayerCard', data.player.card);
              commit('stopDrawFromDeck', cardPos);
              commit('updateWhoIsNext', data.game.who_is_next);

            case 12:
            case "end":
              return _context2.stop();
          }
        }
      }, _callee2, this);
    }));

    function drawFromDeck(_x3, _x4) {
      return _drawFromDeck.apply(this, arguments);
    }

    return drawFromDeck;
  }(),

  /**
   * Another player drew a card from the deck
   *
   * @param commit
   * @param data
   */
  otherDrewFromDeck: function () {
    var _otherDrewFromDeck = _asyncToGenerator(
    /*#__PURE__*/
    _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee3(_ref6, data) {
      var commit;
      return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee3$(_context3) {
        while (1) {
          switch (_context3.prev = _context3.next) {
            case 0:
              commit = _ref6.commit;
              commit('startDrawFromDeck', data.card_pos);
              commit('startAnimation');
              _context3.next = 5;
              return _api_animation__WEBPACK_IMPORTED_MODULE_1__["default"].drawFromDeck(data.card_pos, data.player.id);

            case 5:
              commit('stopDrawFromDeck', data.card_pos);
              commit('updateOtherPlayerCard', data.player.card);
              commit('updateRound', data.round);
              commit('updateTrick', data.trick);
              commit('stopAnimation');

            case 10:
            case "end":
              return _context3.stop();
          }
        }
      }, _callee3, this);
    }));

    function otherDrewFromDeck(_x5, _x6) {
      return _otherDrewFromDeck.apply(this, arguments);
    }

    return otherDrewFromDeck;
  }(),

  /**
   * Draw the Six of Trump
   *
   * @param state
   * @param getters
   * @param commit
   */
  drawSixOfTrump: function drawSixOfTrump(_ref7) {
    var state = _ref7.state,
        getters = _ref7.getters,
        commit = _ref7.commit;
    commit('drawSixOfTrump', getters.sixOfTrump);
  },

  /**
   * Exchange the six of trump with the current trump
   *
   * @param state
   * @param commit
   */
  takeTrump: function () {
    var _takeTrump = _asyncToGenerator(
    /*#__PURE__*/
    _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee4(_ref8) {
      var state, commit, _ref9, data;

      return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee4$(_context4) {
        while (1) {
          switch (_context4.prev = _context4.next) {
            case 0:
              state = _ref8.state, commit = _ref8.commit;
              _context4.next = 3;
              return _api_player__WEBPACK_IMPORTED_MODULE_4__["default"].exchangeSixOfTrump(state.player.id);

            case 3:
              _ref9 = _context4.sent;
              data = _ref9.data;
              commit('updatePlayerHand', data.player.hand);

            case 6:
            case "end":
              return _context4.stop();
          }
        }
      }, _callee4, this);
    }));

    function takeTrump(_x7) {
      return _takeTrump.apply(this, arguments);
    }

    return takeTrump;
  }(),

  /**
   * Add card to discarded list
   *
   * @param commit
   * @param card
   */
  discardCard: function discardCard(_ref10, card) {
    var commit = _ref10.commit;
    commit('discardCard', card);
  },

  /**
   * Add card to discarded list
   *
   * @param commit
   * @param cardIndex
   */
  rollbackDiscarded: function rollbackDiscarded(_ref11, cardIndex) {
    var commit = _ref11.commit;
    commit('rollbackDiscarded', cardIndex);
  },

  /**
   * Empty discarded list
   *
   * @param commit
   */
  emptyDiscarded: function emptyDiscarded(_ref12) {
    var commit = _ref12.commit;
    commit('emptyDiscarded');
  },

  /**
   * Exchange the six of trump with the current trump
   *
   * @param state
   * @param commit
   */
  takeBackup: function takeBackup(_ref13) {
    var state = _ref13.state,
        commit = _ref13.commit;
    _api_player__WEBPACK_IMPORTED_MODULE_4__["default"].takeBackup(state.player.id, state.discarded.map(function (card) {
      return card.id;
    }).join(';'));
  },

  /**
   * Empty backup cards list
   *
   * @param commit
   */
  emptyBackup: function emptyBackup(_ref14) {
    var commit = _ref14.commit;
    commit('emptyBackup');
  },

  /**
   * Draw a card from hand
   *
   * @param state
   * @param commit
   * @param dispatch
   * @param cardId
   */
  drawFromHand: function () {
    var _drawFromHand = _asyncToGenerator(
    /*#__PURE__*/
    _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee5(_ref15, cardId) {
      var state, commit, dispatch, _ref16, data;

      return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee5$(_context5) {
        while (1) {
          switch (_context5.prev = _context5.next) {
            case 0:
              state = _ref15.state, commit = _ref15.commit, dispatch = _ref15.dispatch;
              commit('updateWhoIsNext', -1);
              _context5.next = 4;
              return _api_animation__WEBPACK_IMPORTED_MODULE_1__["default"].drawFromHand(cardId, state.player.id);

            case 4:
              commit('updatePlayerCard', _api_player__WEBPACK_IMPORTED_MODULE_4__["default"].cardFromHandById(state.player.hand, cardId));
              commit('updatePlayerHand', _api_player__WEBPACK_IMPORTED_MODULE_4__["default"].handWithoutCard(state.player.hand, cardId));
              _context5.next = 8;
              return _api_player__WEBPACK_IMPORTED_MODULE_4__["default"].drawCardFromHand(state.game.id, state.round.round, cardId);

            case 8:
              _ref16 = _context5.sent;
              data = _ref16.data;
              commit('updateWhoIsNext', data.game.who_is_next);
              commit('updateRound', data.round);
              commit('updateTrick', data.trick);
              commit('updateScores', data);
              dispatch('clearCarpet', data);

            case 15:
            case "end":
              return _context5.stop();
          }
        }
      }, _callee5, this);
    }));

    function drawFromHand(_x8, _x9) {
      return _drawFromHand.apply(this, arguments);
    }

    return drawFromHand;
  }(),

  /**
   * Another player drew a card from their hand
   *
   * @param state
   * @param commit
   * @param getters
   * @param data
   */
  otherDrewFromHand: function () {
    var _otherDrewFromHand = _asyncToGenerator(
    /*#__PURE__*/
    _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee6(_ref17, data) {
      var state, commit, getters;
      return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee6$(_context6) {
        while (1) {
          switch (_context6.prev = _context6.next) {
            case 0:
              state = _ref17.state, commit = _ref17.commit, getters = _ref17.getters;

              if ((!state.trick || state.trick.trick === 1) && data.response.player.best_melds_title) {
                _api_player__WEBPACK_IMPORTED_MODULE_4__["default"].showBestMeldTitle(getters.opponent.id, data.response.player.best_melds_title);
              }

              commit('startAnimation');
              _context6.next = 5;
              return _api_animation__WEBPACK_IMPORTED_MODULE_1__["default"].drawFromHand(data.card_id, data.response.player.id);

            case 5:
              commit('updateOtherPlayerCard', data.response.card);
              commit('updateOtherPlayerHand', _api_player__WEBPACK_IMPORTED_MODULE_4__["default"].handWithoutCard(getters.opponent.hand, data.card_id));
              commit('stopAnimation');

            case 8:
            case "end":
              return _context6.stop();
          }
        }
      }, _callee6, this);
    }));

    function otherDrewFromHand(_x10, _x11) {
      return _otherDrewFromHand.apply(this, arguments);
    }

    return otherDrewFromHand;
  }(),

  /**
   * Clear the carpet
   *
   * @param state
   * @param commit
   * @param dispatch
   * @param data
   */
  clearCarpet: function clearCarpet(_ref18, data) {
    var state = _ref18.state,
        commit = _ref18.commit,
        dispatch = _ref18.dispatch;
    var trick_completed = data.trick && data.trick.completed;

    if (trick_completed && state.animation) {
      setTimeout(function () {
        return dispatch('clearCarpet', data);
      }, 500);
    } else if (trick_completed) {
      if (!data.trick.winner_id) return setTimeout(function () {
        return commit('clearCarpet');
      }, 500);
      if (data.round.round === 0) commit('resetDeck');
      dispatch('giveCardsToWinner', data);
    }
  },

  /**
   * Give cards to winner
   *
   * @param commit
   * @param getters
   * @param data
   */
  giveCardsToWinner: function giveCardsToWinner(_ref19, data) {
    var commit = _ref19.commit,
        getters = _ref19.getters;
    setTimeout(
    /*#__PURE__*/
    _asyncToGenerator(
    /*#__PURE__*/
    _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee7() {
      return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee7$(_context7) {
        while (1) {
          switch (_context7.prev = _context7.next) {
            case 0:
              _context7.next = 2;
              return _api_animation__WEBPACK_IMPORTED_MODULE_1__["default"].giveCardsToWinner(data.trick.winner_id);

            case 2:
              setTimeout(function () {
                if (data.round.round > 0) _api_player__WEBPACK_IMPORTED_MODULE_4__["default"].hideBestMeldTitle(getters.opponent.id);
                commit('clearCarpet');
                _api_animation__WEBPACK_IMPORTED_MODULE_1__["default"].reset();
              }, 500);

            case 3:
            case "end":
              return _context7.stop();
          }
        }
      }, _callee7, this);
    })), 500);
  },

  /**
   * Start a new round after any on-going animation is completed
   *
   * @param state
   * @param commit
   * @param dispatch
   * @param getters
   * @param data
   * @returns {number}
   */
  startNewRound: function startNewRound(_ref21, data) {
    var state = _ref21.state,
        commit = _ref21.commit,
        dispatch = _ref21.dispatch,
        getters = _ref21.getters;
    if (state.animation) return setTimeout(function () {
      return dispatch('startNewRound', data);
    }, 1000);
    if (!getters.isSelectionRound) _api_score__WEBPACK_IMPORTED_MODULE_5__["default"].showLastRoundResults();
    setTimeout(function () {
      commit('updateAll', data);
      commit('updateOtherPlayerHand', _api_player__WEBPACK_IMPORTED_MODULE_4__["default"].fakeHand(data.game.other_players[0].nb_cards));
    }, 1000);
  },

  /**
   * Initialize listeners
   *
   * @param state
   * @param commit
   * @param dispatch
   * @param getters
   * @param data
   * @private
   */
  _initListeners: function _initListeners(_ref22, data) {
    var state = _ref22.state,
        commit = _ref22.commit,
        dispatch = _ref22.dispatch,
        getters = _ref22.getters;
    _api_game__WEBPACK_IMPORTED_MODULE_3__["default"].waitForOthers("games.".concat(data.game.id), function (player) {
      commit('addPlayer', player);
    });
    _api_game__WEBPACK_IMPORTED_MODULE_3__["default"].waitForCardDrawnFromDeck("games.".concat(data.game.id), function (response) {
      dispatch('otherDrewFromDeck', response);
    });
    _api_game__WEBPACK_IMPORTED_MODULE_3__["default"].waitForSameRankDrawnFromDeck("player.".concat(data.player.id), function (response) {
      commit('updatePlayer', response.player);
      dispatch('clearCarpet', response);
    });
    _api_game__WEBPACK_IMPORTED_MODULE_3__["default"].waitForSelectionRoundCompleted("player.".concat(data.player.id), function (response) {
      commit('updatePlayer', response.player);
      dispatch('clearCarpet', response);
    });
    _api_game__WEBPACK_IMPORTED_MODULE_3__["default"].waitForCardDrawnFromHand("games.".concat(data.game.id), function (response) {
      dispatch('otherDrewFromHand', {
        card_id: getters.opponent.hand[_.random(response.player.nb_cards)].id,
        response: response
      });
    });
    _api_game__WEBPACK_IMPORTED_MODULE_3__["default"].waitForPlayerWonMelds("player.".concat(data.player.id), function (response) {
      commit('updateOtherPlayerMelds', response.game.other_players[0].melds);
    });
    _api_game__WEBPACK_IMPORTED_MODULE_3__["default"].waitForWhoIsNext("games.".concat(data.game.id), function (response) {
      if (state.round.round !== response.round.round) commit('updatePreviousRound', state.round);
      commit('updateTrick', response.trick);
      commit('updateWhoIsNext', response.game.who_is_next);
      commit('updateScores', response);
      dispatch('clearCarpet', response);
    });
    _api_game__WEBPACK_IMPORTED_MODULE_3__["default"].waitForNormalRoundCompleted("player.".concat(data.player.id), function (response) {
      commit('updatePreviousRound', response.round);
      dispatch('clearCarpet', response);
    });
    _api_game__WEBPACK_IMPORTED_MODULE_3__["default"].waitForNewRound("player.".concat(data.player.id), function (response) {
      dispatch('startNewRound', response);
    });
    _api_game__WEBPACK_IMPORTED_MODULE_3__["default"].waitForBackupTaken("player.".concat(data.player.id), function (response) {
      commit('updatePlayerHand', response.player.hand);
      commit('updatePlayerDiscarded', response.player.discarded);
      commit('updatePlayerBackup', response.player.backup);
      commit('updatePlayerMelds', response.player.melds);
    });
    _api_game__WEBPACK_IMPORTED_MODULE_3__["default"].waitForTookTrump("games.".concat(data.game.id), function (response) {
      commit('drawSixOfTrump', response.six_of_trump);
    });
    _api_game__WEBPACK_IMPORTED_MODULE_3__["default"].waitForTrumpTaken("player.".concat(data.player.id), function (response) {
      commit('updatePlayerMelds', response.player.melds);
    });
  }
});

/***/ }),

/***/ "./resources/js/store/constants.js":
/*!*****************************************!*\
  !*** ./resources/js/store/constants.js ***!
  \*****************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ({
  GAME_TYPES: {
    POMME: 'pomme',
    JASS: 'jass'
  },
  SUITS: [{
    code: 'H',
    name: 'Hearts'
  }, {
    code: 'S',
    name: 'Spades'
  }, {
    code: 'D',
    name: 'Diamonds'
  }, {
    code: 'C',
    name: 'Clubs'
  }],
  ROUNDS_TO_WIN: 7,
  POMMES_TO_LOSE: 3,
  POMME_THRESHOLD: 21,
  CARDS_IN_DECK: 36,
  CARDS_IN_HAND: 9,
  CARDS_IN_BACKUP: 3,
  MELDS: [{
    'code': 'four_jacks',
    'rank': 'J',
    'points': 200
  }, {
    'code': 'four_nines',
    'rank': '9',
    'points': 150
  }, {
    'code': 'four_aces',
    'rank': 'A',
    'points': 100
  }, {
    'code': 'four_kings',
    'rank': 'K',
    'points': 100
  }, {
    'code': 'four_queens',
    'rank': 'Q',
    'points': 100
  }, {
    'code': 'four_tens',
    'rank': '10',
    'points': 100
  }, {
    'code': 'royal_couple',
    'rank': '',
    'points': 20
  }]
});

/***/ }),

/***/ "./resources/js/store/getters.js":
/*!***************************************!*\
  !*** ./resources/js/store/getters.js ***!
  \***************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _constants__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./constants */ "./resources/js/store/constants.js");
/* harmony import */ var _api_player__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../api/player */ "./resources/js/api/player.js");
/* harmony import */ var _api_score__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../api/score */ "./resources/js/api/score.js");



/* harmony default export */ __webpack_exports__["default"] = ({
  /**
   * Get the opponent
   *
   * @param state
   * @returns {null}
   */
  opponent: function opponent(state) {
    return state.game.other_players[0];
  },

  /**
   * Check if it is the player's turn
   *
   * @param state
   * @returns {boolean}
   */
  canPlay: function canPlay(state) {
    return state.player.id === state.game.who_is_next;
  },

  /**
   * Check if a player can draw a card
   *
   * @param state
   * @returns {function(*=, *=): (*|boolean)}
   */
  canDrawCard: function canDrawCard(state) {
    return function (myCard, otherCard) {
      return _api_player__WEBPACK_IMPORTED_MODULE_1__["default"].canDrawCard(state.player.hand, myCard, otherCard);
    };
  },

  /**
   * Check if a player has melds
   *
   * @returns {*}
   */
  hasMelds: function hasMelds() {
    return function (player) {
      if (player == null || player.melds == null) return false;
      return player.melds.length > 0;
    };
  },

  /**
   * Check that enough cards have been discarded before taking backup cards
   *
   * @param state
   * @returns {boolean}
   */
  discardedEnoughCards: function discardedEnoughCards(state) {
    return state.discarded.length === _constants__WEBPACK_IMPORTED_MODULE_0__["default"].CARDS_IN_BACKUP;
  },

  /**
   * Retrieve the index of a discarded card
   *
   * @param state
   * @returns {function(*): number}
   */
  discardedIndex: function discardedIndex(state) {
    return function (someCard) {
      return state.discarded.findIndex(function (card) {
        return someCard.id === card.id;
      });
    };
  },

  /**
   * Check if the game can start
   *
   * @param state
   * @returns {boolean}
   */
  canStart: function canStart(state) {
    return state.game.nb_players === state.game.other_players.length + 1;
  },

  /**
   * Determine if we are in the selection round
   *
   * @param state
   * @returns {boolean}
   */
  isSelectionRound: function isSelectionRound(state) {
    return state.round.round == null || state.round.round === 0;
  },

  /**
   * Determine if we are in the first trick of the round
   *
   * @param state
   * @returns {boolean}
   */
  isFirstTrick: function isFirstTrick(state) {
    return state.trick == null || state.trick.trick === 1 && !state.trick.completed;
  },

  /**
   * Determine if the carpet is full, e.g. if all cards have been drawn
   *
   * @param state
   * @returns {null|*|boolean}
   */
  isCarpetFull: function isCarpetFull(state) {
    return state.player.card && state.game.other_players.every(function (player) {
      return player.card;
    });
  },

  /**
   * Determine the CSS identifier for a player's card
   *
   * @returns {function(*, *): string}
   */
  cardCssId: function cardCssId() {
    return function (playerId, cardId) {
      return _api_player__WEBPACK_IMPORTED_MODULE_1__["default"].cardCssId(playerId, cardId);
    };
  },

  /**
   * Determine if the player received backup cards
   *
   * @param state
   * @returns {boolean}
   */
  gotBackupCards: function gotBackupCards(state) {
    return state.backup.length === _constants__WEBPACK_IMPORTED_MODULE_0__["default"].CARDS_IN_BACKUP;
  },

  /**
   * Fetch the Six of Trump from the player hand
   *
   * @param state
   * @returns {*}
   */
  sixOfTrump: function sixOfTrump(state) {
    return _api_player__WEBPACK_IMPORTED_MODULE_1__["default"].sixOfTrump(state.player.hand, state.round.trump);
  },

  /**
   * Get the score for a given player
   *
   * @param state
   * @returns {function(*=, *=): (*|{rounds_won, pommes_count})}
   */
  score: function score(state) {
    return function (playerId) {
      return _api_score__WEBPACK_IMPORTED_MODULE_2__["default"].score(state.game.scores, playerId);
    };
  },

  /**
   * Get round points for a given player and round scores
   *
   * @returns {function(*=): *}
   */
  points: function points() {
    return function (scores, playerId) {
      return _api_score__WEBPACK_IMPORTED_MODULE_2__["default"].points(scores, playerId);
    };
  },

  /**
   * Get melds points for a given player and round scores
   *
   * @returns {function(*=, *=): *}
   */
  melds: function melds() {
    return function (scores, playerId) {
      return _api_score__WEBPACK_IMPORTED_MODULE_2__["default"].melds(scores, playerId);
    };
  },

  /**
   * Get total points for a given player and round scores
   *
   * @returns {function(*=, *=): *}
   */
  total: function total() {
    return function (scores, playerId) {
      return _api_score__WEBPACK_IMPORTED_MODULE_2__["default"].total(scores, playerId);
    };
  }
});

/***/ }),

/***/ "./resources/js/store/index.js":
/*!*************************************!*\
  !*** ./resources/js/store/index.js ***!
  \*************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var vue__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! vue */ "./node_modules/vue/dist/vue.common.js");
/* harmony import */ var vue__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(vue__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var vuex__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! vuex */ "./node_modules/vuex/dist/vuex.esm.js");
/* harmony import */ var _getters__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./getters */ "./resources/js/store/getters.js");
/* harmony import */ var _mutations__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./mutations */ "./resources/js/store/mutations.js");
/* harmony import */ var _actions__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./actions */ "./resources/js/store/actions.js");


vue__WEBPACK_IMPORTED_MODULE_0___default.a.use(vuex__WEBPACK_IMPORTED_MODULE_1__["default"]);



var debug = "development" !== 'production';
/* harmony default export */ __webpack_exports__["default"] = (new vuex__WEBPACK_IMPORTED_MODULE_1__["default"].Store({
  state: {
    player: {
      hand: [],
      melds: []
    },
    backup: [],
    discarded: [],
    game: {
      other_players: [],
      scores: []
    },
    round: {},
    previous_round: {},
    trick: {},
    deck: {
      cards: []
    },
    animation: false
  },
  getters: _getters__WEBPACK_IMPORTED_MODULE_2__["default"],
  mutations: _mutations__WEBPACK_IMPORTED_MODULE_3__["default"],
  actions: _actions__WEBPACK_IMPORTED_MODULE_4__["default"],
  strict: debug
}));

/***/ }),

/***/ "./resources/js/store/mutations.js":
/*!*****************************************!*\
  !*** ./resources/js/store/mutations.js ***!
  \*****************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ({
  /**
   * Update all
   *
   * @param state
   * @param data
   */
  updateAll: function updateAll(state, data) {
    state.player = data.player;
    state.player.melds = data.player.melds;
    state.game = data.game;
    state.round = data.round;
    state.trick = data.trick;
  },

  /**
   * Update the game
   *
   * @param state
   * @param game
   */
  updateGame: function updateGame(state, game) {
    state.game = game;
  },

  /**
   * Update the round
   *
   * @param state
   * @param round
   */
  updateRound: function updateRound(state, round) {
    state.round = round;
  },

  /**
   * Update the previous round - used for showing round results
   *
   * @param state
   * @param previousRound
   */
  updatePreviousRound: function updatePreviousRound(state, previousRound) {
    state.previous_round = _.cloneDeep(previousRound);
  },

  /**
   * Update the trick
   *
   * @param state
   * @param trick
   */
  updateTrick: function updateTrick(state, trick) {
    state.trick = trick;
  },

  /**
   * Update the player
   *
   * @param state
   * @param player
   */
  updatePlayer: function updatePlayer(state, player) {
    state.player = player;
  },

  /**
   * Update the player's melds
   *
   * @param state
   * @param melds
   */
  updatePlayerMelds: function updatePlayerMelds(state, melds) {
    state.player.melds = melds;
  },

  /**
   * Update the player's hand
   *
   * @param state
   * @param hand
   */
  updatePlayerHand: function updatePlayerHand(state, hand) {
    state.player.hand = hand;
  },

  /**
   * Update the player's backup
   *
   * @param state
   * @param backup
   */
  updatePlayerBackup: function updatePlayerBackup(state, backup) {
    state.backup = backup;
  },

  /**
   * Update the player's discarded cards
   *
   * @param state
   * @param discarded
   */
  updatePlayerDiscarded: function updatePlayerDiscarded(state, discarded) {
    state.player.discarded = discarded;
  },

  /**
   * Update the player's card
   *
   * @param state
   * @param card
   */
  updatePlayerCard: function updatePlayerCard(state, card) {
    state.player.card = card;
  },

  /**
   * Update other player's card
   *
   * @param state
   * @param card
   */
  updateOtherPlayerCard: function updateOtherPlayerCard(state, card) {
    state.game.other_players[0].card = card;
  },

  /**
   * Update other player's hand
   *
   * @param state
   * @param hand
   */
  updateOtherPlayerHand: function updateOtherPlayerHand(state, hand) {
    state.game.other_players[0].hand = hand;
  },

  /**
   * Update other player's melds
   *
   * @param state
   * @param melds
   */
  updateOtherPlayerMelds: function updateOtherPlayerMelds(state, melds) {
    state.game.other_players[0].melds = melds;
  },

  /**
   * Update who is next
   *
   * @param state
   * @param whoIsNext
   */
  updateWhoIsNext: function updateWhoIsNext(state, whoIsNext) {
    state.game.who_is_next = whoIsNext;
  },

  /**
   * Update scores
   *
   * @param state
   * @param data
   */
  updateScores: function updateScores(state, data) {
    state.game.scores = data.game.scores;
    state.round.scores = data.round.scores;
  },

  /**
   * Update the deck of cards
   *
   * @param state
   * @param cards
   */
  updateDeck: function updateDeck(state, cards) {
    state.deck.cards = cards;
  },

  /**
   * Clear the carpet of any cards
   *
   * @param state
   */
  clearCarpet: function clearCarpet(state) {
    state.player.card = null;
    state.game.other_players.forEach(function (player) {
      return player.card = null;
    });
  },

  /**
   * Add another player
   *
   * @param state
   * @param player
   */
  addPlayer: function addPlayer(state, player) {
    state.game.other_players.push(player);
  },

  /**
   * Start the animation for drawing a deck card
   *
   * @param state
   * @param cardPos
   */
  startDrawFromDeck: function startDrawFromDeck(state, cardPos) {
    state.deck.cards[cardPos].animated = true;
  },

  /**
   * Stop the animation for drawing a deck card
   *
   * @param state
   * @param cardPos
   */
  stopDrawFromDeck: function stopDrawFromDeck(state, cardPos) {
    state.deck.cards[cardPos].disabled = true;
    state.deck.cards[cardPos].animated = false;
  },

  /**
   * Reset the deck of cards
   *
   * @param state
   */
  resetDeck: function resetDeck(state) {
    state.deck.cards.forEach(function (value, index, cards) {
      cards[index].disabled = true;
      cards[index].animated = false;
    });
  },

  /**
   * Draw the Six of Trump
   *
   * @param state
   * @param sixOfTrump
   */
  drawSixOfTrump: function drawSixOfTrump(state, sixOfTrump) {
    state.round.trump = sixOfTrump;
  },

  /**
   * Add a card to discarded list
   *
   * @param state
   * @param card
   */
  discardCard: function discardCard(state, card) {
    state.discarded.push(card);
  },

  /**
   * Remove a card from discarded list
   *
   * @param state
   * @param cardIndex
   */
  rollbackDiscarded: function rollbackDiscarded(state, cardIndex) {
    state.discarded.splice(cardIndex, 1);
  },

  /**
   * Remove all cards from discarded list
   *
   * @param state
   */
  emptyDiscarded: function emptyDiscarded(state) {
    state.discarded = [];
  },

  /**
   * Remove all cards from backup cards list
   *
   * @param state
   */
  emptyBackup: function emptyBackup(state) {
    state.backup = [];
  },

  /**
   * Start an animation
   *
   * @param state
   */
  startAnimation: function startAnimation(state) {
    state.animation = true;
  },

  /**
   * Stop an animation
   *
   * @param state
   */
  stopAnimation: function stopAnimation(state) {
    state.animation = false;
  }
});

/***/ }),

/***/ "./resources/sass/pomme.scss":
/*!***********************************!*\
  !*** ./resources/sass/pomme.scss ***!
  \***********************************/
/*! no static exports found */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),

/***/ 0:
/*!*****************************************************************!*\
  !*** multi ./resources/js/pomme.js ./resources/sass/pomme.scss ***!
  \*****************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

__webpack_require__(/*! /Users/jmariller/Documents/Sites/pomme/resources/js/pomme.js */"./resources/js/pomme.js");
module.exports = __webpack_require__(/*! /Users/jmariller/Documents/Sites/pomme/resources/sass/pomme.scss */"./resources/sass/pomme.scss");


/***/ })

},[[0,"/js/manifest","/js/vendor"]]]);
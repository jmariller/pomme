<?php

return [


    /*
    |--------------------------------------------------------------------------
    | Constants
    |--------------------------------------------------------------------------
    |
    | General constants used in the game
    |
    */
    'constants' => [
        'type_pomme' => 'pomme',
        'type_jass' => 'jass',
        'cards_per_player' => 9,
        'points_for_cinq_de_der' => 5,
        'jass_nb_players' => 4,
        'pomme_nb_players' => 2,
        'pomme_threshold' => 21,
        'pomme_nb_rounds_to_win' => 7,
        'pomme_nb_pommes_to_loose' => 3,
    ],

    /*
    |--------------------------------------------------------------------------
    | Type of games
    |--------------------------------------------------------------------------
    |
    | The type of games available with their corresponding number of
    | players (e.g. pomme is 2, jass is 4).
    |
    */
    'game_types' => [
        'pomme' => ['nb_players' => 2],
        'jass' => ['nb_players' => 4],
    ],

    /*
    |--------------------------------------------------------------------------
    | List of Suits
    |--------------------------------------------------------------------------
    |
    | The main list of suits with their description and sort order
    | Default sort order: Hearts, Spades, Diamonds and Clubs
    | If no Spades: Hearts, Clubs, Diamonds
    | If no Diamonds: Spades, Hearts, Clubs
    |
    */
    'suits' => [
        ['code' => 'H', 'name' => 'Hearts',   'sort' => 1, 'color' => 'red'],
        ['code' => 'S', 'name' => 'Spades',   'sort' => 2, 'color' => 'black'],
        ['code' => 'D', 'name' => 'Diamonds', 'sort' => 3, 'color' => 'red'],
        ['code' => 'C', 'name' => 'Clubs',    'sort' => 4, 'color' => 'black'],
    ],

    /*
    |--------------------------------------------------------------------------
    | List of Cards in a Plain Suit
    |--------------------------------------------------------------------------
    |
    | List of cards in a plain suit, with their index, points and sort order
    |
    */
    'plain_suit' => [
        ['symbol' => 'A',  'index' => 9, 'points' => 11, 'sort' => 1],
        ['symbol' => 'K',  'index' => 8, 'points' => 4,  'sort' => 2],
        ['symbol' => 'Q',  'index' => 7, 'points' => 3,  'sort' => 3],
        ['symbol' => 'J',  'index' => 6, 'points' => 2,  'sort' => 4],
        ['symbol' => '10', 'index' => 5, 'points' => 10, 'sort' => 5],
        ['symbol' => '9',  'index' => 4, 'points' => 0,  'sort' => 6],
        ['symbol' => '8',  'index' => 3, 'points' => 0,  'sort' => 7],
        ['symbol' => '7',  'index' => 2, 'points' => 0,  'sort' => 8],
        ['symbol' => '6',  'index' => 1, 'points' => 0,  'sort' => 9],
    ],

    /*
    |--------------------------------------------------------------------------
    | List of Cards in a Trump Suit
    |--------------------------------------------------------------------------
    |
    | List of cards in a trump suit, with their index, points and sort order
    |
    */
    'trump_suit' => [
        ['symbol' => 'J',  'index' => 18, 'points' => 20, 'sort' => 4],
        ['symbol' => '9',  'index' => 17, 'points' => 14, 'sort' => 6],
        ['symbol' => 'A',  'index' => 16, 'points' => 11, 'sort' => 1],
        ['symbol' => 'K',  'index' => 15, 'points' => 4,  'sort' => 2],
        ['symbol' => 'Q',  'index' => 14, 'points' => 3,  'sort' => 3],
        ['symbol' => '10', 'index' => 13, 'points' => 10, 'sort' => 5],
        ['symbol' => '8',  'index' => 12, 'points' => 0,  'sort' => 7],
        ['symbol' => '7',  'index' => 11, 'points' => 0,  'sort' => 8],
        ['symbol' => '6',  'index' => 10, 'points' => 0,  'sort' => 9],
    ],

    /*
    |--------------------------------------------------------------------------
    | Melds
    |--------------------------------------------------------------------------
    |
    | List of melds with their points and index
    |
    */
    'melds' => [
        ['code' => 'four_jacks', 'type' => 'quartet', 'nb_cards' => 4, 'points' => 200, 'index' => 13, 'symbol' => 'J'],
        ['code' => 'four_nines', 'type' => 'quartet', 'nb_cards' => 4, 'points' => 150, 'index' => 12, 'symbol' => '9'],
        ['code' => 'sequence_9', 'type' => 'sequence', 'nb_cards' => 9, 'points' => 100, 'index' => 11, 'symbol' => ''],
        ['code' => 'sequence_8', 'type' => 'sequence', 'nb_cards' => 8, 'points' => 100, 'index' => 10, 'symbol' => ''],
        ['code' => 'sequence_7', 'type' => 'sequence', 'nb_cards' => 7, 'points' => 100, 'index' => 9, 'symbol' => ''],
        ['code' => 'sequence_6', 'type' => 'sequence', 'nb_cards' => 6, 'points' => 100, 'index' => 8, 'symbol' => ''],
        ['code' => 'sequence_5', 'type' => 'sequence', 'nb_cards' => 5, 'points' => 100, 'index' => 7, 'symbol' => ''],
        ['code' => 'four_aces', 'type' => 'quartet', 'nb_cards' => 4, 'points' => 100, 'index' => 6, 'symbol' => 'A'],
        ['code' => 'four_kings', 'type' => 'quartet', 'nb_cards' => 4, 'points' => 100, 'index' => 5, 'symbol' => 'K'],
        ['code' => 'four_queens', 'type' => 'quartet', 'nb_cards' => 4, 'points' => 100, 'index' => 4, 'symbol' => 'Q'],
        ['code' => 'four_tens', 'type' => 'quartet', 'nb_cards' => 4, 'points' => 100, 'index' => 3, 'symbol' => '10'],
        ['code' => 'sequence_4', 'type' => 'sequence', 'nb_cards' => 4, 'points' => 50, 'index' => 2, 'symbol' => ''],
        ['code' => 'sequence_3', 'type' => 'sequence', 'nb_cards' => 3, 'points' => 20, 'index' => 1, 'symbol' => ''],
        ['code' => 'royal_couple', 'type' => 'sequence', 'nb_cards' => 2, 'points' => 20, 'index' => null, 'symbol' => '']
    ],

];

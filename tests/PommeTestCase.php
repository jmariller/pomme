<?php

namespace Tests;

use App\Jass\Entities\Trick;
use App\Jass\Entities\Round;

abstract class PommeTestCase extends GameTestCase
{

    /**
     * Start a new Pomme
     */
    protected function startPomme()
    {
        $this->user1 = factory('App\User')->create();

        $this->actingAs($this->user1)->joinGame('pomme');

        $this->user2 = factory('App\User')->create();

        $this->actingAs($this->user2)->joinGame('pomme');

        $this->round = Round::all()->last();

        $this->game = $this->round->game;
    }

    /**
     * Start a normal round for the Pomme
     */
    protected function startNormalRoundForPomme()
    {
        $this->game = factory('App\Jass\Entities\Game')->state('two_players')->create([
            'rounds_count' => 1,
            'who_is_next' => 1
        ]);

        $this->user1 = factory('App\User')->create();
        $this->user1->player->update(['game_id' => $this->game->id, 'sort_order' => 1]);

        $this->user2 = factory('App\User')->create();
        $this->user2->player->update(['game_id' => $this->game->id, 'sort_order' => 2]);

        $this->round = $this->game->addRound($this->user1->id);

        $this->game->players->each->addRoundScores($this->round->id);

        $game = $this->game;
        $this->game->players()->each(function ($player) use ($game) {
            $game->scores()->create(['player_id' => $player->id]);
        });

        $this->round->distributeCards();

        $this->player1 = $this->user1->fresh()->player;
        $this->player2 = $this->user2->fresh()->player;
    }

    /**
     * Run the selection round to get "user1" as winner
     *
     * @return \Illuminate\Foundation\Testing\TestResponse
     */
    protected function runSelectionWithWinner()
    {
        $kings = $this->cardsByRank('K');
        $aces = $this->cardsByRank('A');

        $this->actingAs($this->user1)
            ->drawCardFromDeck($this->game->id, $aces->keys()->first());

        return $this->actingAs($this->user2)
            ->drawCardFromDeck($this->game->id, $kings->keys()->first());
    }

    /**
     * Take backup cards (specific for Pomme - normal round)
     *
     * @param int   $playerId
     * @param string $discarded
     * @return \Illuminate\Foundation\Testing\TestResponse
     */
    protected function takeBackup(int $playerId, string $discarded)
    {
        return $this->postJson("/players/$playerId/backup", [
            'discarded' => $discarded
        ]);
    }

    /**
     * Exchange the Six of Trump against the actual Trump (specific for Pomme - normal round)
     *
     * @param int $playerId
     * @return \Illuminate\Foundation\Testing\TestResponse
     */
    protected function exchangeSixOfTrump(int $playerId)
    {
        return $this->postJson("/players/$playerId/trump", [
            'dummy' => 'dummy'
        ]);
    }

    /**
     * Simulate the moment before the end of current round (by default, first player wins the round)
     *
     * @param int $player1Points
     * @param int $player2Points
     */
    protected function beforeEndOfRound($player1Points = 60, $player2Points = 40)
    {
        $this->round->update(['tricks_count' => 8]);

        $this->round->scores()->wherePlayerId($this->player1->id)->update([
            'points' => $player1Points,
            'melds' => 0,
            'total' => $player1Points
        ]);

        $this->round->scores()->wherePlayerId($this->player2->id)->update([
            'points' => $player2Points,
            'melds' => 0,
            'total' => $player2Points
        ]);

        factory(Trick::class, 8)->create([
            'round_id' => $this->round->id,
            'trick' => 8,
            'completed' => true,
            'winner_id' => $this->player1->id
        ]);

        $this->player1->update(['hand' => 'SA']);
        $this->player2->update(['hand' => 'SQ']);
    }

    /**
     * Simulate a game won for player 1
     */
    protected function gameWonForPlayer1()
    {
        $this->player1->gameScores()->whereGameId($this->game->id)->update(['rounds_won' => 6]);

        $this->beforeEndOfRound(50, 20);

        $this->actingAs($this->user1)
            ->drawCardFromHand($this->game->id, $this->round->round, $this->player1->fresh()->hand->first()->id);

        $this->actingAs($this->user2)
            ->drawCardFromHand($this->game->id, $this->round->round, $this->player2->fresh()->hand->first()->id);
    }

    /**
     * Simulate a game over for player 1 with 3 pommes
     */
    protected function gameOverWithPommeForPlayer1()
    {
        $this->player1->gameScores()->whereGameId($this->game->id)->update(['pommes_count' => 2]);

        $this->beforeEndOfRound(0, 21);

        $this->actingAs($this->user1)
            ->drawCardFromHand($this->game->id, $this->round->round, $this->player1->fresh()->hand->first()->id);

        $this->actingAs($this->user2)
            ->drawCardFromHand($this->game->id, $this->round->round, $this->player2->fresh()->hand->first()->id);
    }

}

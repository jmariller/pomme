import { shallowMount } from "@vue/test-utils";
import expect from "expect";
import Card from "../../resources/js/components/Card";

describe('Card', () => {

    let wrapper;

    beforeEach(() => {
        let card = {
            suit: {name: 'Spades'},
            rank: {symbol: 'A'}
        };

        wrapper = shallowMount(Card, {
            propsData: { card }
        });
    });

    it ('presents the rank symbol', () => {
        expect(wrapper.find('.card-rank').html()).toContain('A');
    });

    it ('presents the image of the suit', () => {
        expect(wrapper.find('.card-suit').html()).toContain('images/suits/spades-small.png');
    });

});
<?php

namespace Tests;

use App\User;
use App\Jass\Entities\Game;
use App\Jass\Entities\Round;
use App\Jass\Entities\Player;
use App\Jass\Cards\PlayingCard;
use Illuminate\Support\Collection;

abstract class GameTestCase extends TestCase
{

    /**
     * @var User
     */
    protected $user1, $user2;

    /**
     * @var Player
     */
    protected $player1, $player2;

    /**
     * @var Game
     */
    protected $game;

    /**
     * @var Round
     */
    protected $round;

    /**
     * Join a game
     *
     * @param $type
     * @return \Illuminate\Foundation\Testing\TestResponse
     */
    protected function joinGame($type = 'pomme')
    {
        $data = $type == null ? [] : ['type' => $type];

        return $this->postJson('/games', $data);
    }

    /**
     * Draw a card from the deck (selection round)
     *
     * @param int    $gameId
     * @param int    $cardPos
     * @return \Illuminate\Foundation\Testing\TestResponse
     */
    protected function drawCardFromDeck(int $gameId, int $cardPos)
    {
        return $this->postJson("/games/$gameId/rounds/0", [
            'card_pos' => $cardPos
        ]);
    }

    /**
     * Draw a card from hand (normal round)
     *
     * @param int    $gameId
     * @param int    $roundNb
     * @param string $cardId
     * @return \Illuminate\Foundation\Testing\TestResponse
     */
    protected function drawCardFromHand(int $gameId, int $roundNb, string $cardId)
    {
        return $this->postJson("/games/$gameId/rounds/$roundNb", [
            'card_id' => $cardId
        ]);
    }

    /**
     * Draw all given ranks from the cards deck
     *
     * @param string $rank
     * @return Collection
     */
    protected function cardsByRank($rank = 'K')
    {
        return $this->round->cards_deck
            ->filter(function (PlayingCard $card) use ($rank) {
                return $card->rank->symbol === $rank;
            });
    }

    /**
     * Draw card from a hand
     *
     * @param $hand
     * @param $id
     * @return mixed
     */
    protected function getCardFromHand($hand, $id)
    {
        return $hand->first(function ($card) use ($id) {
            return $card->id === $id;
        });
    }

}

<?php

namespace Tests\Feature\Pomme;

use Tests\PommeTestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class MeldsTest extends PommeTestCase
{

    use RefreshDatabase;

    /**
     * @inheritdoc
     */
    public function setUp()
    {
        parent::setUp();

        $this->startNormalRoundForPomme();

        $this->player1->melds()->delete();
        $this->player2->melds()->delete();

        $this->player1->update([
            'round_id' => $this->round->id,
            'hand' => 'DQ;C9;D6;HA;D9;H10;DK;CJ;C8',
            'trump' => 'DJ'
        ]);

        $this->player1->melds()->create([
            'round_id' => $this->round->id,
            'best_index' => 1,
            'sequence_3' => true,
            'royal_couple' => true,
            'points' => 20
        ]);
    }

    /** @test */
    public function the_player_with_highest_index_wins_the_melds()
    {
        $this->assertEquals(1, $this->player1->best_melds_index);
        $this->assertEquals('Three Cards', $this->player1->best_melds_title);
    }

    /** @test */
    public function melds_are_only_made_visible_for_the_winner_after_the_1st_trick()
    {
        $this->assertArrayNotHasKey('melds', $this->player1->withMelds($won = false, $firstTrick = false)->toArray());
        $this->assertArrayNotHasKey('melds', $this->player1->withMelds($won = true, $firstTrick = false)->toArray());
        $this->assertArrayHasKey('melds', $this->player1->withMelds($won = true, $firstTrick = true)->toArray());
    }

    /** @test */
    public function the_royal_couple_within_a_sequence_is_announced_together_with_that_sequence()
    {
        $melds = $this->player1->withMelds($won = true, $firstTrick = true)->current_melds->toArray();

        $this->assertTrue($melds['royal_couple']);
    }

    /** @test */
    public function the_royal_couple_alone_is_announced_only_after_the_2nd_card_is_drawn()
    {
        $this->player1->melds()->whereRoundId($this->round->id)->update([
            'best_index' => -1,
            'sequence_3' => false,
            'royal_couple_alone' => true,
            'points' => 20
        ]);

        $this->actingAs($this->user1);

        $melds = $this->player1->current_melds->toArray();

        $this->assertTrue($melds['royal_couple']);
        $this->assertTrue($melds['royal_couple_alone']);

        $this->actingAs($this->user2);

        $melds = $this->game->other_players[0]->withMelds($won = true, $firstTrick = true)->current_melds->toArray();

        $this->assertFalse($melds['royal_couple']);
        $this->assertFalse($melds['royal_couple_alone']);

        // Play the Queen of Trump (DQ) => 1 out of Royal Couple left
        $this->player1->update(['hand' => 'C9;D6;HA;D9;H10;DK;CJ;C8']);

        $melds = $this->game->other_players[0]->withMelds($won = true, $firstTrick = true)->current_melds->toArray();

        $this->assertFalse($melds['royal_couple']);
        $this->assertFalse($melds['royal_couple_alone']);

        // Play the King of Trump (DK) => 0 out of Royal Couple left
        $this->player1->update(['hand' => 'C9;D6;HA;D9;H10;CJ;C8']);

        $melds = $this->game->other_players[0]->withMelds($won = true, $firstTrick = true)->current_melds->toArray();

        $this->assertTrue($melds['royal_couple']);
        $this->assertTrue($melds['royal_couple_alone']);
    }

}

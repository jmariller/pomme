<?php

namespace Tests\Feature\Pomme;

use Tests\PommeTestCase;
use Illuminate\Support\Facades\Event;
use Illuminate\Foundation\Testing\RefreshDatabase;

class SelectionRoundTest extends PommeTestCase
{

    use RefreshDatabase;

    /**
     * @inheritdoc
     */
    public function setUp()
    {
        parent::setUp();

        $this->startPomme();
    }

    /** @test */
    public function the_selection_round_is_created_when_all_players_joined()
    {
        $this->assertNotNull($this->round);

        $this->assertEquals(0, $this->round->round);
    }

    /** @test */
    public function when_the_selection_round_is_created_it_has_a_deck_of_36_cards()
    {
        $this->assertDatabaseHas('rounds', ['round' => 0])
            ->assertEquals(36, $this->round->cards_deck->count());
    }

    /** @test */
    public function only_cards_within_the_deck_can_be_selected()
    {
        Event::fake();

        $this->actingAs($this->user1)
            ->drawCardFromDeck($this->game->id, 40)
            ->assertStatus(422);

        $this->drawCardFromDeck($this->game->id, -2)
            ->assertStatus(422);

        $this->drawCardFromDeck($this->game->id, 0)
            ->assertOk();

        $this->drawCardFromDeck($this->game->id, 13)
            ->assertOk();

        $this->drawCardFromDeck($this->game->id, 35)
            ->assertOk();
    }

    /** @test */
    public function a_player_can_only_draw_a_card_that_is_still_on_the_carpet()
    {
        $this->actingAs($this->user1);

        $this->drawCardFromDeck($this->game->id, 0)
            ->assertOk();

        $this->drawCardFromDeck($this->game->id, 0)
            ->assertStatus(422);
    }

    /** @test */
    public function when_the_first_player_draws_a_card_from_the_deck_both_players_see_that_card()
    {
        $card_pos = rand(0, 35);

        $card_id = $this->round->cards_deck->get($card_pos)->id;

        $this->actingAs($this->user1)
            ->drawCardFromDeck($this->game->id, $card_pos)
            ->assertJsonFragment(['id' => $card_id]);

        $this->assertDatabaseHas('cards', [
            'card_id' => $card_id
        ])->assertDatabaseHas('tricks', [
            'round' => 0,
            'trick' => 1,
            'cards_count' => 1
        ])->assertDatabaseHas('rounds', [
            'tricks_count' => 1,
            'cards_drawn' => $card_id
        ]);
    }

    /** @test */
    public function after_a_player_has_drawn_a_card_from_the_deck_they_cannot_draw_another_one_right_away()
    {
        $this->actingAs($this->user1)
            ->drawCardFromDeck($this->game->id, 0)
            ->assertOk();

        $this->drawCardFromDeck($this->game->id, 10)
            ->assertStatus(422);
    }

    /** @test */
    public function when_both_players_draw_the_same_rank_there_is_a_tie()
    {
        $kings = $this->cardsByRank('K');

        $this->actingAs($this->user1)
            ->drawCardFromDeck($this->game->id, $kings->keys()->first());

        $this->actingAs($this->user2)
            ->drawCardFromDeck($this->game->id, $kings->keys()->last())
            ->assertJsonFragment([
                'winner_id' => null,
                'tricks_count' => 1,
                'who_is_next' => $this->user1->id
            ]);

        $this->assertDatabaseHas('tricks', [
            'trick' => 1,
            'cards_count' => 2
        ]);

        $this->assertDatabaseHas('rounds', [
            'tricks_count' => 1,
            'cards_drawn' => implode(";", [$kings->first()->id, $kings->last()->id])
        ]);
    }

    /** @test */
    public function the_player_with_highest_rank_wins_the_round()
    {
        $this->runSelectionWithWinner()
            ->assertJsonFragment([
                'who_is_next' => $this->user1->id,
                'round' => 1
            ]);

        $this->assertEquals(1, $this->game->fresh()->rounds_count);
    }

    /** @test */
    public function drawing_a_card_is_forbidden_when_the_selection_round_is_completed()
    {
        $this->runSelectionWithWinner();

        $this->actingAs($this->user1)
            ->drawCardFromDeck($this->game->id, 0)
            ->assertStatus(422);
    }
    
}

<?php

namespace Tests\Feature\Pomme;

use Tests\PommeTestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ScoresTest extends PommeTestCase
{

    use RefreshDatabase;

    /**
     * @inheritdoc
     */
    public function setUp()
    {
        parent::setUp();

        $this->startNormalRoundForPomme();
    }

    /** @test */
    public function both_players_get_scores_after_selection_round()
    {
        $this->assertCount(1, $this->player1->roundScores);
        $this->assertCount(1, $this->player1->gameScores);

        $this->assertCount(1, $this->player2->roundScores);
        $this->assertCount(1, $this->player2->gameScores);
    }

    /** @test */
    public function when_a_trick_is_completed_the_round_scores_are_updated_accordingly()
    {
        $this->player1->update(['hand' => 'S8;SA']);
        $this->player2->update(['hand' => 'S10;SQ']);

        $this->actingAs($this->user1)
            ->drawCardFromHand($this->game->id, $this->round->round, $this->player1->fresh()->hand->first()->id);

        $this->actingAs($this->user2)
            ->drawCardFromHand($this->game->id, $this->round->round, $this->player2->fresh()->hand->first()->id);

        $this->assertEquals(10, $this->round->scores()->wherePlayerId($this->player2->id)->first()->points);

        $this->actingAs($this->user2)
            ->drawCardFromHand($this->game->id, $this->round->round, $this->player2->fresh()->hand->first()->id);

        $this->actingAs($this->user1)
            ->drawCardFromHand($this->game->id, $this->round->round, $this->player1->fresh()->hand->first()->id);

        $this->assertEquals(14, $this->round->scores()->wherePlayerId($this->player1->id)->first()->points);
    }

    /** @test */
    public function the_player_who_wins_the_last_tricks_gets_5_points_more_on_their_total_points()
    {
        $this->beforeEndOfRound(60, 78);

        $this->actingAs($this->user1)
            ->drawCardFromHand($this->game->id, $this->round->round, $this->player1->fresh()->hand->first()->id);

        $this->actingAs($this->user2)
            ->drawCardFromHand($this->game->id, $this->round->round, $this->player2->fresh()->hand->first()->id);

        // 79 points => 60 points before the last trick + 14 points for last trick + 5 points "der"
        $this->assertEquals(79, $this->game->round(1)->scores()->wherePlayerId($this->player1->id)->first()->points);
    }

    /** @test  */
    public function the_player_with_highest_score_wins_the_round()
    {
        $this->beforeEndOfRound(60, 78);

        $this->actingAs($this->user1)
            ->drawCardFromHand($this->game->id, $this->round->round, $this->player1->fresh()->hand->first()->id);

        $this->actingAs($this->user2)
            ->drawCardFromHand($this->game->id, $this->round->round, $this->player2->fresh()->hand->first()->id);

        $this->assertEquals($this->player1->id, $this->round->fresh()->winner_id);
    }

    /** @test */
    public function the_player_with_7_rounds_won_wins_the_game()
    {
        $this->gameWonForPlayer1();

        $this->assertEquals(7, $this->player1->gameScores()->whereGameId($this->game->id)->first()->rounds_won);

        $this->assertTrue($this->game->fresh()->completed);
        $this->assertEquals($this->player1->id, $this->game->fresh()->winner_id);
    }

    /** @test */
    public function the_player_with_3_pommes_loses_the_game()
    {
        $this->gameOverWithPommeForPlayer1();

        $this->assertEquals(3, $this->player1->gameScores()->whereGameId($this->game->id)->first()->pommes_count);
        $this->assertEquals(1, $this->player2->gameScores()->whereGameId($this->game->id)->first()->rounds_won);

        $this->assertTrue($this->game->fresh()->completed);
        $this->assertEquals($this->player2->id, $this->game->fresh()->winner_id);
    }

    /** @test */
    public function a_new_round_is_not_created_after_the_game_is_over()
    {
        $this->assertEquals(1, $this->game->fresh()->rounds_count);

        $this->gameOverWithPommeForPlayer1();

        $this->assertEquals(1, $this->game->fresh()->rounds_count);
    }

}

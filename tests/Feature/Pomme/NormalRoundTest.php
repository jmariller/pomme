<?php

namespace Tests\Feature\Pomme;

use Tests\PommeTestCase;
use Illuminate\Support\Facades\Event;
use Illuminate\Foundation\Testing\RefreshDatabase;

class NormalRoundTest extends PommeTestCase
{

    use RefreshDatabase;

    /**
     * @inheritdoc
     */
    public function setUp()
    {
        parent::setUp();

        $this->startNormalRoundForPomme();
    }

    /** @test */
    public function both_players_receive_a_hand_of_9_cards_and_a_backup_of_3_cards()
    {
        $this->assertEquals(9, $this->player1->hand->count());
        $this->assertEquals(3, $this->player1->backup->count());

        $this->assertEquals(9, $this->player2->hand->count());
        $this->assertEquals(3, $this->player2->backup->count());
    }

    /** @test */
    public function a_player_can_only_draw_a_card_from_his_hand()
    {
        Event::fake();

        $this->actingAs($this->user1)
            ->drawCardFromHand($this->game->id, $this->round->round, $this->player1->hand->first()->id)
            ->assertOk();

        $this->drawCardFromHand($this->game->id, $this->round->round, $this->round->cards_deck->last()->id)
            ->assertStatus(422);
    }

    /** @test */
    public function a_player_can_ony_draw_a_valid_card_after_their_opponent_played_their_card()
    {
        $this->round->update(['trump' => 'D6']);
        $this->player1->update(['trump' => 'D6', 'hand' => 'SQ;HJ;S10;D9;CJ']);
        $this->player2->update(['trump' => 'D6', 'hand' => 'SA;S6;H10;DJ;H6']);

        // The player must draw cards of same color
        $this->actingAs($this->user1)
            ->drawCardFromHand($this->game->id, $this->round->round, $this->getCardFromHand($this->player1->fresh()->hand, 'HJ')->id);

        $this->actingAs($this->user2)
            ->drawCardFromHand($this->game->id, $this->round->round, $this->getCardFromHand($this->player2->fresh()->hand, 'S6')->id)
            ->assertStatus(422);

        $this->actingAs($this->user2)
            ->drawCardFromHand($this->game->id, $this->round->round, $this->getCardFromHand($this->player2->fresh()->hand, 'H10')->id)
            ->assertOk();

        // If the player doesn't have any card in that suit, they can play any card
        $this->actingAs($this->user1)
            ->drawCardFromHand($this->game->id, $this->round->round, $this->getCardFromHand($this->player1->fresh()->hand, 'CJ')->id);

        $this->actingAs($this->user2)
            ->drawCardFromHand($this->game->id, $this->round->round, $this->getCardFromHand($this->player2->fresh()->hand, 'H6')->id)
            ->assertOk();

        // Special case when the Jass (Jack of trump) is the last trump in hand:
        // - Can be held till the end, so any other card can be drawn
        $this->actingAs($this->user1)
            ->drawCardFromHand($this->game->id, $this->round->round, $this->getCardFromHand($this->player1->fresh()->hand, 'D9')->id);

        $this->actingAs($this->user2)
            ->drawCardFromHand($this->game->id, $this->round->round, $this->getCardFromHand($this->player2->fresh()->hand, 'S6')->id)
            ->assertOk();

        // The trump is almighty and can be drawn anytime
        $this->actingAs($this->user1)
            ->drawCardFromHand($this->game->id, $this->round->round, $this->getCardFromHand($this->player1->fresh()->hand, 'S10')->id);

        $this->actingAs($this->user2)
            ->drawCardFromHand($this->game->id, $this->round->round, $this->getCardFromHand($this->player2->fresh()->hand, 'DJ')->id)
            ->assertOk();
    }

    /** @test */
    public function after_a_player_has_drawn_a_card_from_their_hand_they_must_wait_their_turn_to_draw_again()
    {
        $this->actingAs($this->user1)
            ->drawCardFromHand($this->game->id, $this->round->round, $this->player1->hand->first()->id)
            ->assertOk();

        $this->drawCardFromHand($this->game->id, $this->round->round, $this->player1->hand->last()->id)
            ->assertStatus(422);
    }

    /** @test */
    public function the_player_with_best_card_wins_the_trick()
    {
        $this->player1->update(['hand' => 'SA']);
        $this->player2->update(['hand' => 'SQ']);

        $this->actingAs($this->user1)
            ->drawCardFromHand($this->game->id, $this->round->round, $this->player1->fresh()->hand->first()->id);

        $this->actingAs($this->user2)
            ->drawCardFromHand($this->game->id, $this->round->round, $this->player2->fresh()->hand->first()->id);

        $this->assertEquals($this->player1->id, $this->game->who_is_next);

        $this->assertEquals($this->player1->id, $this->round->tricks->first()->winner_id);
    }

    /** @test */
    public function a_new_round_is_properly_started_after_the_current_round_is_completed()
    {
        $this->beforeEndOfRound(40, 0);

        $this->assertEquals(8, $this->game->currentRound()->tricks_count);

        $this->assertCount(1, $this->player1->hand);
        $this->assertCount(1, $this->player2->hand);

        $this->actingAs($this->user1)
            ->drawCardFromHand($this->game->id, $this->round->round, $this->player1->fresh()->hand->first()->id);

        $this->actingAs($this->user2)
            ->drawCardFromHand($this->game->id, $this->round->round, $this->player2->fresh()->hand->first()->id);

        $this->round = $this->game->currentRound();

        $this->assertTrue($this->game->round(1)->completed);
        $this->assertEquals(2, $this->round->round);

        $this->assertCount(9, $this->player1->fresh()->hand);
        $this->assertCount(9, $this->player2->fresh()->hand);

        $this->assertEquals(2, $this->game->fresh()->rounds_count);

        $this->assertEquals($this->player2->id, $this->round->first_player_id);
        $this->assertEquals($this->player2->id, $this->game->fresh()->who_is_next);

        $this->assertCount(2, $this->player1->roundScores);
        $this->assertCount(2, $this->player2->roundScores);

        $this->assertEquals(1, $this->game->scores()->wherePlayerId($this->player1->id)->first()->rounds_won);
        $this->assertEquals(1, $this->game->scores()->wherePlayerId($this->player2->id)->first()->pommes_count);
    }

    /** @test */
    public function drawing_a_card_from_the_hand_is_only_allowed_in_current_round()
    {
        $this->actingAs($this->user1)
            ->drawCardFromHand($this->game->id, $round_nb = 99, $this->player1->fresh()->hand->first()->id)
            ->assertStatus(422);
    }

}

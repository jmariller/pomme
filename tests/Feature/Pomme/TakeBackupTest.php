<?php

namespace Tests\Feature\Pomme;

use Tests\PommeTestCase;
use Illuminate\Support\Facades\Event;
use Illuminate\Foundation\Testing\RefreshDatabase;

class TakeBackupTest extends PommeTestCase
{

    use RefreshDatabase;

    /**
     * @inheritdoc
     */
    public function setUp()
    {
        parent::setUp();

        $this->startNormalRoundForPomme();

        $this->actingAs($this->user1);
    }

    /** @test */
    public function a_player_can_only_discard_3_cards_from_their_hand()
    {
        Event::fake();

        $this->round = $this->game->currentRound();

        $discarded = implode(';', [
            $this->round->cards_deck->get(33)->id,
            $this->round->cards_deck->get(34)->id,
            $this->round->cards_deck->get(35)->id,
        ]);

        $this->takeBackup($this->player1->id, $discarded)
            ->assertStatus(422);

        $discarded = implode(';', [
            $this->player1->hand->get(0)->id,
            $this->player1->hand->get(1)->id,
        ]);

        $this->takeBackup($this->player1->id, $discarded)
            ->assertStatus(422);

        $discarded = implode(';', [
            $this->player1->hand->get(0)->id,
            $this->player1->hand->get(1)->id,
            $this->player1->hand->get(2)->id,
        ]);

        $this->takeBackup($this->player1->id, $discarded)
            ->assertOk();
    }

    /** @test */
    public function a_player_gets_3_backup_cards_after_having_discarded_3_cards_from_their_hand()
    {
        $discarded = implode(';', [
            $this->player1->hand->get(0)->id,
            $this->player1->hand->get(1)->id,
            $this->player1->hand->get(2)->id,
        ]);

        $this->takeBackup($this->player1->id, $discarded);

        $this->assertEquals($discarded, $this->player1->fresh()->discarded->implode('id', ';'));

        $this->assertTrue(
            $this->player1->fresh()->backup->pluck('id')->every(function ($cardId) {
                return $this->player1->fresh()->hand->pluck('id')->contains($cardId);
            })
        );
    }
    
}

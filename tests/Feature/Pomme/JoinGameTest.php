<?php

namespace Tests\Feature\Pomme;

use Tests\GameTestCase;
use App\Jass\Entities\Game;
use Illuminate\Foundation\Testing\RefreshDatabase;

class JoinGameTest extends GameTestCase
{

    use RefreshDatabase;

    /** @test */
    public function a_new_game_is_created_if_a_guest_does_not_find_a_place_in_an_existing_game()
    {
        $this->joinGame('pomme');

        $this->assertDatabaseHas('games', [
            'type' => 'pomme',
            'nb_players' => 2
        ]);
    }

    /** @test */
    public function a_new_game_is_created_if_a_connected_player_does_not_find_a_place_in_an_existing_game()
    {
        $user = factory('App\User')->create();

        $this->actingAs($user);

        $this->joinGame('pomme');

        $this->assertDatabaseHas('games', [
            'type' => 'pomme',
            'nb_players' => 2
        ]);
    }

    /** @test */
    public function the_first_player_to_connect_is_next()
    {
        $user1 = factory('App\User')->create();

        $this->actingAs($user1);

        $this->joinGame('pomme');

        $user2 = factory('App\User')->create();

        $this->actingAs($user2);

        $this->joinGame('pomme');

        $this->assertEquals($user1->id, Game::first()->who_is_next);
    }
    
}

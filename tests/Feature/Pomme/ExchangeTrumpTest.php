<?php

namespace Tests\Feature\Pomme;

use Tests\PommeTestCase;
use Illuminate\Support\Facades\Event;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ExchangeTrumpTest extends PommeTestCase
{

    use RefreshDatabase;

    /**
     * @inheritdoc
     */
    public function setUp()
    {
        parent::setUp();

        $this->startNormalRoundForPomme();

        $this->actingAs($this->user1);

        $this->game->currentRound()->update(['trump' => 'SJ']);
    }

    /** @test */
    public function a_player_can_take_the_trump_only_if_they_have_the_6_of_trump()
    {
        Event::fake();

        $this->player1->saveCards([
            'hand' => 'C6;C7;C8;C9;C10;CJ;CQ;CK;CA'
        ]);

        $this->exchangeSixOfTrump($this->player1->id)
            ->assertStatus(422);

        $this->player1->saveCards([
            'hand' => 'C6;C7;C8;C9;C10;CJ;CQ;CK;S6'
        ]);

        $this->exchangeSixOfTrump($this->player1->id)
            ->assertOk();
    }

    /** @test */
    public function the_player_gets_the_trump_against_the_6_of_trump()
    {
        $this->player1->saveCards([
            'hand' => 'C6;C7;C8;C9;C10;CJ;CQ;CK;S6'
        ]);

        $this->assertEquals('SJ', $this->game->currentRound()->trump->id);

        $this->exchangeSixOfTrump($this->player1->id);

        $this->assertTrue(
            $this->player1->fresh()->hand->first(function ($card) {
                return $card->id == 'SJ';
            }) !== null
        );

        $this->assertEquals('S6', $this->game->currentRound()->trump->id);
    }
    
}

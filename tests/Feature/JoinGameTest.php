<?php

namespace Tests\Feature;

use App\User;
use Tests\GameTestCase;
use App\Jass\Entities\Game;
use Illuminate\Foundation\Testing\RefreshDatabase;

class JoinGameTest extends GameTestCase
{

    use RefreshDatabase;

    /** @test */
    public function a_game_must_have_a_valid_type()
    {
        $this->joinGame(null)->assertStatus(422);

        $this->joinGame('invalid_game_type')->assertStatus(422);

        $this->joinGame('pomme')->assertOk();

        $this->joinGame('jass')->assertOk();
    }
    
    /** @test */
    public function a_new_guest_is_automatically_registered_and_signed_in()
    {
        $this->joinGame();

        $this->assertDatabaseHas('users', ['name' => 'Guest 1'])
            ->assertTrue(auth()->check());
    }

    /** @test */
    public function after_a_page_reload_the_connected_player_is_still_in_the_same_game()
    {
        $user = factory(User::class)->create();
        $this->actingAs($user);

        $this->assertNull($user->player->game_id);

        $this->joinGame();

        $this->assertNotNull($game_id = $user->player->game_id);
        $this->assertDatabaseHas('games', [
            'id' => $game_id,
            'players_count' => 1
        ]);

        $this->joinGame();

        $this->assertEquals($user->player->game_id, $game_id);
        $this->assertDatabaseHas('games', [
            'id' => $game_id,
            'players_count' => 1
        ]);
    }

    /** @test */
    public function a_guest_can_join_an_existing_game_that_has_another_player_waiting()
    {
        $game = factory(Game::class)->state('one_player')->create();
        $player = factory(User::class)->create()->player;
        $player->update(['game_id' => $game->id]);

        $guest = factory(User::class)->create();
        $this->actingAs($guest);

        $this->assertEquals(1, $game->players_count);
        $this->assertEquals(null, $guest->player->game_id);

        $this->joinGame();

        $this->assertEquals(2, $game->fresh()->players_count);
        $this->assertEquals($game->id, $guest->player->game_id);
    }
    
}

<?php

namespace Tests\Unit;

use Tests\TestCase;
use App\Jass\Cards\PlayingCard;
use App\Jass\Melds\MeldsDetector;
use Illuminate\Foundation\Testing\RefreshDatabase;

class MeldsDetectorTest extends TestCase
{
    
    use RefreshDatabase;

    /** @test */
    public function it_finds_the_royal_couple()
    {
        $hand = app('deck')->fromString('SA;C6;C7;H6;D9;D10;CJ;S8;HQ')->applyTrump(PlayingCard::byId('HJ'))->cards();

        $this->assertFalse(MeldsDetector::hasRoyalCouple($hand));

        $hand = app('deck')->fromString('SA;C6;C7;HK;D9;D10;CJ;S8;HQ')->applyTrump(PlayingCard::byId('HJ'))->cards();

        $this->assertTrue(MeldsDetector::hasRoyalCouple($hand));
    }

    /** @test */
    public function it_knows_when_the_royal_couple_is_alone()
    {
        $hand = app('deck')->fromString('SA;C6;C7;HK;D9;D10;HJ;S8;HQ')
            ->applyTrump(PlayingCard::byId('HJ'))
            ->sort()
            ->cards();

        $this->assertFalse(MeldsDetector::hasRoyalCoupleAlone($hand));

        $hand = app('deck')->fromString('SA;C6;C7;HK;D9;D10;CJ;S8;HQ')
            ->applyTrump(PlayingCard::byId('HJ'))
            ->sort()
            ->cards();

        $this->assertTrue(MeldsDetector::hasRoyalCoupleAlone($hand));
    }

    /** @test */
    public function it_finds_quartets()
    {
        // Four Aces
        $hand = app('deck')->fromString('SA;CA;C7;HA;D9;DA;CJ;S8;HQ')->cards();

        $this->assertTrue(MeldsDetector::hasAnyQuartet($hand));

        $this->assertTrue(MeldsDetector::hasQuartet($hand, 'A'));

        // Four Kings
        $hand = app('deck')->fromString('SK;CK;C7;HK;D9;DK;CJ;S8;HQ')->cards();

        $this->assertTrue(MeldsDetector::hasQuartet($hand, 'K'));
    }

    /** @test */
    public function it_finds_sequences()
    {
        // Three cards: S10, SJ, SQ and D8, D9, D10
        $hand = app('deck')->fromString('S10;D10;SQ;D8;D9;DA;CJ;S8;SJ')->sort()->cards();

        $sequences = MeldsDetector::sequences($hand);

        $this->assertEquals(2, MeldsDetector::countSequencesOf($sequences, 3));

        $this->assertEquals('S10;SJ;SQ', $sequences->first()->implode('id', ';'));
        $this->assertEquals('D8;D9;D10', $sequences->last()->implode('id', ';'));
    }

    /** @test */
    public function it_finds_conflicts_in_sequences()
    {
        // Four Aces and 3 Cards (SQ, SK, SA) => conflict
        $hand = app('deck')->fromString('SA;CA;SQ;HA;D9;DA;CJ;S8;SK')->cards();

        $sequences = MeldsDetector::sequences($hand);

        $this->assertEquals(0, MeldsDetector::countSequencesOf($sequences, 3));
    }

    /** @test */
    public function it_counts_points_for_sequences()
    {
        // Three cards: S10, SJ, SQ. Four cards: D7, D8, D9, D10
        $hand = app('deck')->fromString('S10;D10;SQ;D8;D9;DA;CJ;D7;SJ')->sort()->cards();

        $sequences = MeldsDetector::sequences($hand);

        $this->assertEquals(20, MeldsDetector::sequencePoints($sequences->first()));
        $this->assertEquals(50, MeldsDetector::sequencePoints($sequences->last()));
    }
    
}

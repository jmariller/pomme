<?php

namespace Tests\Unit;

use Tests\TestCase;
use App\Jass\Cards\Rank;
use App\Jass\JassConfiguration;
use Illuminate\Foundation\Testing\RefreshDatabase;

class RankTest extends TestCase
{
    
    use RefreshDatabase;
    
    /** @test */
    public function it_builds_correctly_a_plain_jack_from_config()
    {
        $jack = new Rank(...JassConfiguration::rank('J'));

        $this->assertEquals(2, $jack->points);
    }

    /** @test */
    public function it_builds_correctly_a_jass_from_config()
    {
        $jack = new Rank(...JassConfiguration::rank('J', 'trump_suit'));

        $this->assertEquals(20, $jack->points);
    }
    
}

<?php

namespace Tests\Unit;

use App\User;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class UserTest extends TestCase
{
    
    use RefreshDatabase;

    /**
     * @var User
     */
    protected $user;

    /**
     * @inheritdoc
     */
    public function setUp()
    {
        parent::setUp();

        $this->user = factory(User::class)->create();
    }
    
    /** @test */
    public function it_generates_a_corresponding_player_after_it_was_created()
    {
        $this->assertDatabaseHas('players', ['pseudo' => $this->user->pseudo]);
    }

    /** @test */
    public function it_adds_guests()
    {
        $guest = $this->user->addGuest();

        $this->assertDatabaseHas('users', [
            'pseudo' => $guest->pseudo,
            'guest' => true
        ]);
    }
    
}

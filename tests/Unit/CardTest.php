<?php

namespace Tests\Unit;

use App\User;
use Tests\TestCase;
use App\Jass\Cards\Rank;
use App\Jass\Cards\Suit;
use App\Jass\Entities\Card;
use App\Jass\Entities\Trick;
use App\Jass\Entities\Player;
use App\Jass\Cards\PlayingCard;
use Illuminate\Foundation\Testing\RefreshDatabase;

class CardTest extends TestCase
{
    
    use RefreshDatabase;

    /**
     * @var Card
     */
    protected $card;

    /**
     * @inheritdoc
     */
    public function setUp()
    {
        parent::setUp();

        $this->card = factory(Card::class)->create();
    }

    /** @test */
    public function it_belongs_to_a_trick()
    {
        $this->assertInstanceOf(Trick::class, $this->card->trick);
    }

    /** @test */
    public function it_belongs_to_a_player()
    {
        $this->card->update(['player_id' => factory(User::class)->create()->id]);

        $this->assertInstanceOf(Player::class, $this->card->player);
    }

    /** @test */
    public function it_has_a_playing_card()
    {
        $this->assertInstanceOf(PlayingCard::class, $this->card->card);

        $this->assertInstanceOf(Suit::class, $this->card->card->suit);

        $this->assertEquals('S', $this->card->card->suit->code);

        $this->assertInstanceOf(Rank::class, $this->card->card->rank);

        $this->assertEquals('A', $this->card->card->rank->symbol);
    }

}

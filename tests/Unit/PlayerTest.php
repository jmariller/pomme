<?php

namespace Tests\Unit;

use App\User;
use Tests\GameTestCase;
use App\Jass\Entities\Game;
use App\Jass\Entities\Player;
use App\Jass\Cards\PlayingCard;
use Illuminate\Support\Collection;
use Illuminate\Foundation\Testing\RefreshDatabase;

class PlayerTest extends GameTestCase
{
    
    use RefreshDatabase;
    
    /** @test */
    public function it_is_bound_to_a_user()
    {
        $player = factory(User::class)->create()->player;

        $this->assertInstanceOf(User::class, $player->user);
    }

    /** @test */
    public function it_has_a_game()
    {
        $player = factory(User::class)->create()->player;

        $player->update(['game_id' => factory(Game::class)->create()->id]);

        $this->assertInstanceOf(Game::class, $player->game);
    }

    /** @test */
    public function it_has_a_hand_of_deck_cards()
    {
        $player = factory(Player::class)->make();

        $this->assertInstanceOf(Collection::class, $player->hand);

        $this->assertEquals(0, $player->hand->count());

        $player = factory(Player::class)->state('with_hand')->make();

        $this->assertEquals(9, $player->hand->count());

        $this->assertInstanceOf(PlayingCard::class, $player->hand->get(0));
    }

    /** @test */
    public function it_has_a_card()
    {
        $player = factory(Player::class)->state('with_card')->make();

        $this->assertInstanceOf(PlayingCard::class, $player->card);
    }

    /** @test */
    public function it_has_melds()
    {
        $player = factory(Player::class)->make();

        $this->assertInstanceOf(Collection::class, $player->melds);
    }

    /** @test */
    public function it_has_scores()
    {
        $player = factory(Player::class)->make();

        $this->assertInstanceOf(Collection::class, $player->roundScores);

        $this->assertInstanceOf(Collection::class, $player->gameScores);
    }

    /** @test */
    public function it_has_a_sorted_hand_of_cards()
    {
        $player = factory(Player::class)->make(['hand' => 'D8;C9;D6;HA;D9;H10;DK;SJ;C8']);

        $this->assertEquals('H10;HA;SJ;D6;D8;D9;DK;C8;C9', $player->hand->implode('id', ';'));
    }

    /** @test */
    public function it_alternates_red_and_black_suits_when_they_have_only_three_suits()
    {
        // No Spades...
        $player = factory(Player::class)->make(['hand' => 'D8;C9;D6;HA;D9;H10;DK;CJ;C8']);

        // .. expect Hearts, Clubs, Diamonds.
        $this->assertEquals('H10;HA;C8;C9;CJ;D6;D8;D9;DK', $player->hand->implode('id', ';'));

        // No Diamonds...
        $player->hand = 'H8;C9;H6;HA;H9;H10;HK;SJ;C8';

        // .. expect Spades, Hearts, Clubs.
        $this->assertEquals('SJ;H6;H8;H9;H10;HK;HA;C8;C9', $player->hand->implode('id', ';'));
    }

    /** @test */
    public function it_can_only_draw_valid_cards()
    {
        $player = factory(Player::class)->state('with_hand')->make(['trump' => 'C6']);

        // The player must draw cards of same color
        $my_card = $this->getCardFromHand($player->hand, 'D8');

        $this->assertTrue($player->canDrawCard($my_card, PlayingCard::byId('DA')));
        $this->assertFalse($player->canDrawCard($my_card, PlayingCard::byId('HA')));

        // The trump is almighty and can be drawn anytime
        $my_trump_card = $this->getCardFromHand($player->hand, 'C8');

        $this->assertTrue($player->canDrawCard($my_trump_card, PlayingCard::byId('C7')->toTrump()));
        $this->assertTrue($player->canDrawCard($my_trump_card, PlayingCard::byId('DA')));

        // If the player doesn't have any card in that suit, they can play any card
        $my_card = $this->getCardFromHand($player->hand, 'D8');

        $this->assertTrue($player->canDrawCard($my_card, PlayingCard::byId('SA')));

        // Special case when the Jass (Jack of trump) is the last trump in hand:
        // - Can be held till the end, so any other card can be drawn
        $player->hand = $this->leaveOnlyJassInHand($player->hand);

        // Sanity check: the hand is updated with only the Jass
        $this->assertTrue(
            $player->hand->filter->isTrump()->count() === 1 &&
            $player->hand->filter->isJass()->count() === 1
        );

        $my_card = $this->getCardFromHand($player->hand, 'D8');

        $this->assertTrue($player->canDrawCard($my_card, PlayingCard::byId('C7')->toTrump()));
    }

    /**
     * Leave only the Jass in given hand
     *
     * @param $hand
     * @return mixed
     */
    private function leaveOnlyJassInHand(Collection $hand)
    {
        return $hand->filter(function (PlayingCard $card) {
            return $card->isJass() || !$card->isTrump();
        });
    }

}

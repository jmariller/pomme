<?php

namespace Tests\Unit;

use App\User;
use Tests\TestCase;
use App\Jass\Entities\Game;
use App\Jass\Entities\Round;
use Illuminate\Support\Collection;
use Illuminate\Foundation\Testing\RefreshDatabase;

class GameTest extends TestCase
{
    
    use RefreshDatabase;

    /**
     * @var Game
     */
    protected $game;

    /**
     * @inheritdoc
     */
    public function setUp()
    {
        parent::setUp();

        $this->game = factory(Game::class)->create();
    }

    /** @test */
    public function it_has_players()
    {
        $this->assertInstanceOf(Collection::class, $this->game->players);
    }

    /** @test */
    public function it_has_sorted_players()
    {
        $user3 = factory(User::class)->create();
        $user3->player()->update(['game_id' => $this->game->id, 'sort_order' => 3]);

        $user1 = factory(User::class)->create();
        $user1->player()->update(['game_id' => $this->game->id, 'sort_order' => 1]);

        $user2 = factory(User::class)->create();
        $user2->player()->update(['game_id' => $this->game->id, 'sort_order' => 2]);

        $this->assertEquals($user3->player->sort_order, $this->game->sortedPlayers->last()->sort_order);
    }

    /** @test */
    public function it_knows_other_players()
    {
        $user1 = factory(User::class)->create();
        $user1->player()->update(['game_id' => $this->game->id]);

        $user2 = factory(User::class)->create();
        $user2->player()->update(['game_id' => $this->game->id]);

        $user3 = factory(User::class)->create();
        $user3->player()->update(['game_id' => $this->game->id]);

        $this->actingAs($user1);

        $this->assertInstanceOf(Collection::class, $this->game->other_players);

        $this->assertCount(2, $this->game->other_players);

        $this->assertCount(0, $this->game->other_players->filter(function ($player) use ($user1) {
            return $player->id == $user1->id;
        }));
    }

    /** @test */
    public function it_has_rounds()
    {
        $this->assertInstanceOf(Collection::class, $this->game->rounds);
    }

    /** @test */
    public function it_has_scores()
    {
        $this->assertInstanceOf(Collection::class, $this->game->scores);
    }

    /** @test */
    public function it_finds_rounds_by_their_number()
    {
        $first_round = factory(Round::class)->create(['game_id' => $this->game->id, 'round' => 0]);
        $second_round = factory(Round::class)->create(['game_id' => $this->game->id, 'round' => 1]);
        $last_round = factory(Round::class)->create(['game_id' => $this->game->id, 'round' => 2]);

        $this->assertEquals($first_round->round, $this->game->round(0)->round);
        $this->assertEquals($second_round->round, $this->game->round(1)->round);
        $this->assertEquals($last_round->round, $this->game->round(2)->round);
    }

    /** @test */
    public function it_knows_the_last_card_drawn()
    {
        $this->assertNull($this->game->last_card);

        $user = factory(User::class)->create();
        $user->player()->update([
            'game_id' => $this->game->id,
            'card' => 'DK'
        ]);

        $this->assertEquals('DK', $this->game->last_card->id);
    }

    /** @test */
    public function it_knows_the_current_round()
    {
        $this->assertNull($this->game->currentRound()->id);

        factory(Round::class)->create(['game_id' => $this->game->id, 'round' => 0]);

        $current_round = $this->game->currentRound();

        $this->assertInstanceOf(Round::class, $current_round);

        $this->assertEquals(0, $current_round->round);
    }

    /** @test */
    public function it_knows_the_previous_round()
    {
        factory(Round::class)->create(['game_id' => $this->game->id, 'round' => 0]);

        $previous_round = $this->game->previousRound();

        $this->assertNull($previous_round);

        factory(Round::class)->create(['game_id' => $this->game->id, 'round' => 1]);

        $previous_round = $this->game->previousRound();

        $this->assertInstanceOf(Round::class, $previous_round);

        $this->assertEquals(0, $previous_round->round);
    }
    
}

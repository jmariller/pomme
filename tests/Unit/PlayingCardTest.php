<?php

namespace Tests\Unit;

use Tests\TestCase;
use App\Jass\Cards\Rank;
use App\Jass\Cards\Suit;
use App\Jass\Cards\PlayingCard;
use App\Jass\JassConfiguration;
use Illuminate\Foundation\Testing\RefreshDatabase;

class PlayingCardTest extends TestCase
{
    
    use RefreshDatabase;
    
    /** @test */
    public function it_gets_an_identifier_and_a_name_based_on_its_suit_and_rank()
    {
        $suit = new Suit(...JassConfiguration::suit('S'));
        $rank = new Rank(...JassConfiguration::rank('A'));

        $card = new PlayingCard($suit, $rank);

        $this->assertEquals('SA', $card->id);
        $this->assertEquals('Ace of Spades', $card->name);
    }

    /** @test */
    public function it_has_helpers_for_various_values()
    {
        $card = PlayingCard::byId('SA');

        $this->assertEquals($card->color, $card->suit->color);
        $this->assertEquals($card->rank_index, $card->rank->index);
        $this->assertEquals($card->points, $card->rank->points);
    }

    /** @test */
    public function it_is_found_by_id()
    {
        $this->assertEquals('SA', PlayingCard::byId('SA')->id);

        $this->assertNull(PlayingCard::byId('XX'));
    }

    /** @test */
    public function it_converts_to_trump()
    {
        $jass = PlayingCard::byId('SJ');

        $this->assertEquals(2, $jass->rank->points);

        $jass->toTrump();

        $this->assertEquals(20, $jass->rank->points);

        $this->assertTrue($jass->isJass());
    }

    /** @test */
    public function it_converts_to_plain()
    {
        $jass = PlayingCard::byId('SJ')->toTrump();

        $this->assertEquals(20, $jass->rank->points);

        $jass->toPlain();

        $this->assertEquals(2, $jass->rank->points);
    }

    /** @test */
    public function it_converts_to_fake()
    {
        $card = PlayingCard::byId('SJ');

        $this->assertEquals('J', $card->rank->symbol);

        $card->toFake();

        $this->assertEquals('F', $card->rank->symbol);
    }

    /** @test */
    public function it_detects_the_king_or_queen_of_trump()
    {
        $six = PlayingCard::byId('S6')->toTrump();
        $king = PlayingCard::byId('SQ')->toTrump();
        $queen = PlayingCard::byId('SQ')->toTrump();

        $this->assertFalse($six->isKingOrQueenOfTrump());
        $this->assertTrue($king->isKingOrQueenOfTrump());
        $this->assertTrue($queen->isKingOrQueenOfTrump());
    }
    
}

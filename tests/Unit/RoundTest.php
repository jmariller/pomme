<?php

namespace Tests\Unit;

use App\User;
use Tests\TestCase;
use App\Jass\Entities\Game;
use App\Jass\Entities\Round;
use App\Jass\Entities\Player;
use App\Jass\Cards\PlayingCard;
use Illuminate\Support\Collection;

use Illuminate\Foundation\Testing\RefreshDatabase;

class RoundTest extends TestCase
{
    
    use RefreshDatabase;

    /**
     * @var Round
     */
    protected $round;

    /**
     * @inheritdoc
     */
    public function setUp()
    {
        parent::setUp();

        $user_id = factory(User::class)->create()->id;

        $this->round = factory(Round::class)->create([
            'cards_deck' => 'SA;C8;H10;CJ;C6;CK',
            'cards_drawn' => 'C8;C6',
            'trump' => 'SJ',
            'first_player_id' => 1,
            'winner_id' => $user_id,
        ]);
    }

    /** @test */
    public function it_belongs_to_a_game()
    {
        $this->assertInstanceOf(Game::class, $this->round->game);
    }

    /** @test */
    public function it_has_tricks()
    {
        $this->assertInstanceOf(Collection::class, $this->round->tricks);
    }

    /** @test */
    public function it_has_scores()
    {
        $this->assertInstanceOf(Collection::class, $this->round->scores);
    }

    /** @test */
    public function it_has_melds()
    {
        $this->assertInstanceOf(Collection::class, $this->round->melds);
    }

    /** @test */
    public function it_has_a_first_player()
    {
        $this->assertInstanceOf(Player::class, $this->round->firstPlayer);
    }

    /** @test */
    public function it_has_a_winner()
    {
        $this->assertInstanceOf(Player::class, $this->round->winner);
    }

    /** @test */
    public function it_has_a_trump()
    {
        $this->assertInstanceOf(PlayingCard::class, $this->round->trump);

        $this->assertEquals('SJ', $this->round->trump->id);
    }

    /** @test */
    public function it_finds_the_next_player()
    {
        $user1 = factory(User::class)->create();
        $user1->player()->update(['game_id' => $this->round->game->id, 'sort_order' => 1]);

        $user2 = factory(User::class)->create();
        $user2->player()->update(['game_id' => $this->round->game->id, 'sort_order' => 2]);

        $this->round->update(['first_player_id' => $user2->id]);

        $this->assertEquals($user1->id, $this->round->nextPlayer()->user_id);
    }

    /** @test */
    public function it_has_a_deck_of_cards()
    {
        $this->assertInstanceOf(Collection::class, $this->round->cards_deck);

        $this->assertCount(6, $this->round->cards_deck);

        $this->assertInstanceOf(PlayingCard::class, $this->round->cards_deck->get(0));
    }

    /** @test */
    public function it_knows_its_cards_drawn_indexes()
    {
        $this->assertInstanceOf(Collection::class, $this->round->cards_drawn_indexes);

        $this->assertCount(2, $this->round->cards_drawn_indexes);

        $this->assertEquals(1, $this->round->cards_drawn_indexes->first());
        $this->assertEquals(4, $this->round->cards_drawn_indexes->last());
    }
    
}

<?php

namespace Tests\Unit;

use Tests\TestCase;
use App\Jass\Cards\Deck;
use App\Jass\Cards\PlayingCard;
use Illuminate\Support\Collection;
use Illuminate\Foundation\Testing\RefreshDatabase;

class DeckTest extends TestCase
{
    
    use RefreshDatabase;

    /**
     * @var Deck
     */
    protected $cards_deck;

    /**
     * @inheritdoc
     */
    public function setUp()
    {
        parent::setUp();

        $this->cards_deck = app('deck')->fromConfig();
    }

    /** @test */
    public function it_returns_a_collection_of_cards()
    {
        $this->assertInstanceOf(Collection::class, $this->cards_deck->cards());
    }

    /** @test */
    public function it_creates_a_deck_of_36_cards()
    {
        $this->assertCount(36, $this->cards_deck->cards());
    }
    
    /** @test */
    public function it_shuffles_a_deck_of_cards()
    {
        $this->assertNotEquals($this->cards_deck->cards(), $this->cards_deck->shuffle()->cards());
    }

    /** @test */
    public function it_builds_cards_from_a_string()
    {
        $cards = app('deck')->fromString('SA;CK;H7')->cards();

        $this->assertCount(3, $cards);

        $this->assertEquals('C', $cards->get(1)->suit->code);

        $this->assertEquals('7', $cards->get(2)->rank->symbol);
    }

    /** @test */
    public function it_applies_the_trump()
    {
        $deck = app('deck')->fromString('SA;CK;H7;HJ');

        $this->assertEquals(2, $deck->cards()->last()->rank->points);

        $deck->applyTrump(PlayingCard::byId('H6'));

        $this->assertEquals(20, $deck->cards()->last()->rank->points);
    }
    
}

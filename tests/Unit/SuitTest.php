<?php

namespace Tests\Unit;

use Tests\TestCase;
use App\Jass\Cards\Suit;
use App\Jass\JassConfiguration;
use Illuminate\Foundation\Testing\RefreshDatabase;

class SuitTest extends TestCase
{
    
    use RefreshDatabase;
    
    /** @test */
    public function it_builds_correctly_a_suit_from_config()
    {
        $spades = new Suit(...JassConfiguration::suit('S'));

        $this->assertEquals('Spades', $spades->name);
    }
    
}

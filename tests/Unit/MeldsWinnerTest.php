<?php

namespace Tests\Unit;

use App\User;
use Tests\TestCase;
use App\Jass\Entities\Game;
use App\Jass\Entities\Round;
use App\Jass\Entities\Player;
use App\Jass\Melds\PlayerMelds;
use App\Jass\Melds\MeldsWinner;
use App\Jass\Melds\PlayerMeldSequence;
use Illuminate\Foundation\Testing\RefreshDatabase;

class MeldsWinnerTest extends TestCase
{
    
    use RefreshDatabase;

    /**
     * @var Round
     */
    protected $round;

    /**
     * @var Player
     */
    protected $player1, $player2;

    /**
     * @inheritdoc
     */
    public function setUp()
    {
        parent::setUp();

        $game = factory(Game::class)->create();

        $this->player1 = factory(User::class)->create()->player;

        $this->player2 = factory(User::class)->create()->player;

        $this->round = factory(Round::class)->create([
            'game_id' => $game->id,
            'first_player_id' => $this->player1->id
        ]);

        $this->player1->update([
            'game_id' => $game->id,
            'round_id' => $this->round->id
        ]);

        $this->player2->update([
            'game_id' => $game->id,
            'round_id' => $this->round->id
        ]);
    }

    /** @test */
    public function it_finds_the_melds_winner_for_different_melds_levels()
    {
        $this->createMelds($this->round->id, $this->player1->id, 'four_nines', 12);
        $this->createMelds($this->round->id, $this->player2->id, 'four_jacks', 13);

        $this->assertEquals($this->player2->id, MeldsWinner::winner($this->round));
    }

    /** @test */
    public function it_finds_the_melds_winner_for_same_sequences()
    {
        $this->round->update(['trump' => 'SJ']);

        $this->createSequence($this->createMelds($this->round->id, $this->player1->id)->id, 'C6;C7;C8', 3, 3);
        $this->createSequence($this->createMelds($this->round->id, $this->player2->id)->id, 'S6;S7;S8', 3, 3);

        $this->assertEquals($this->player2->id, MeldsWinner::winner($this->round));

        $this->round->melds()->delete();

        $this->createSequence($this->createMelds($this->round->id, $this->player1->id)->id, 'C6;C7;C8', 3, 3);
        $this->createSequence($this->createMelds($this->round->id, $this->player2->id)->id, 'D6;D7;D8', 3, 3);

        $this->assertEquals($this->player1->id, MeldsWinner::winner($this->round));
    }

    /** @test */
    public function it_finds_the_best_index_for_a_meld()
    {
        $this->assertEquals(13, MeldsWinner::bestIndex( $this->makeMelds('four_jacks') ));

        $this->assertEquals(6, MeldsWinner::bestIndex( $this->makeMelds('four_aces') ));

        $this->assertEquals(11, MeldsWinner::bestIndex( $this->makeMelds('sequence_9') ));
    }

    /** @test */
    public function it_calculates_the_total_points_for_a_meld()
    {
        $melds = factory(PlayerMelds::class)->make(['four_nines' => true]);     // 150

        $this->assertEquals(150, MeldsWinner::totalPoints($melds));

        $melds = factory(PlayerMelds::class)->make([
            'four_jacks' => true,   // 200
            'sequence_4' => true,   // 50
            'royal_couple' => true, // 20
        ]);

        $this->assertEquals(270, MeldsWinner::totalPoints($melds));
    }

    /**
     * Make melds by code
     *
     * @param $code
     * @return mixed
     */
    private function makeMelds($code)
    {
        return factory(PlayerMelds::class)->make([$code => true]);
    }

    /**
     * Create a melds
     *
     * @param        $roundId
     * @param        $player
     * @param string $meld
     * @param int    $bestIndex
     * @return mixed
     */
    private function createMelds($roundId, $player, $meld = 'sequence_3', $bestIndex = 1)
    {
        return factory(PlayerMelds::class)->create([
            'round_id' => $roundId,
            'player_id' => $player,
            $meld => true,
            'best_index' => $bestIndex,
        ]);
    }

    /**
     * Create a sequence
     *
     * @param $playerMeld
     * @param $sequence
     * @param $length
     * @param $highestRankIndex
     * @return mixed
     */
    private function createSequence($playerMeld, $sequence, $length, $highestRankIndex)
    {
        return factory(PlayerMeldSequence::class)->create([
            'player_meld_id' => $playerMeld,
            'sequence' => $sequence,
            'length' => $length,
            'highest_rank_index' => $highestRankIndex,
        ]);
    }
    
}

<?php

namespace Tests\Unit;

use App\User;
use Tests\TestCase;
use App\Jass\Entities\Card;
use App\Jass\Entities\Trick;
use App\Jass\Entities\Round;
use App\Jass\Entities\Player;
use App\Jass\Cards\PlayingCard;
use Illuminate\Support\Collection;
use Illuminate\Foundation\Testing\RefreshDatabase;

class TrickTest extends TestCase
{
    
    use RefreshDatabase;

    /**
     * @var Trick
     */
    protected $trick;

    /**
     * @inheritdoc
     */
    public function setUp()
    {
        parent::setUp();

        $this->trick = factory(Trick::class)->create();
    }

    /** @test */
    public function it_belongs_to_a_round()
    {
        $this->assertInstanceOf(Round::class, $this->trick->round);
    }

    /** @test */
    public function it_has_cards()
    {
        $this->assertInstanceOf(Collection::class, $this->trick->cards);
    }

    /** @test */
    public function it_has_a_first_player()
    {
        $this->trick->update(['first_player_id' => factory(User::class)->create()->id]);

        $this->assertInstanceOf(Player::class, $this->trick->firstPlayer);
    }

    /** @test */
    public function it_has_a_winner()
    {
        $this->trick->update(['winner_id' => factory(User::class)->create()->id]);

        $this->assertInstanceOf(Player::class, $this->trick->winner);
    }

    /** @test */
    public function it_has_a_trump()
    {
        $this->trick->update(['trump' => 'SJ']);

        $this->assertInstanceOf(PlayingCard::class, $this->trick->trump);

        $this->assertEquals('SJ', $this->trick->trump->id);
    }

    /** @test */
    public function it_has_playing_cards()
    {
        $this->assertInstanceOf(Collection::class, $this->trick->playing_cards);

        $this->assertCount(0, $this->trick->playing_cards);

        $this->addCards();

        $this->assertCount(4, $this->trick->playing_cards);

        $this->assertInstanceOf(PlayingCard::class, $this->trick->playing_cards->get(0));

        $this->assertEquals(2, $this->trick->playing_cards->get(0)->rank->points);
        $this->assertEquals(0, $this->trick->playing_cards->get(1)->rank->points);
        $this->assertEquals(11, $this->trick->playing_cards->get(2)->rank->points);
        $this->assertEquals(0, $this->trick->playing_cards->get(3)->rank->points);

        $this->trick->update(['trump' => 'S6']);

        $this->assertEquals(20, $this->trick->playing_cards->get(0)->rank->points);
        $this->assertEquals(14, $this->trick->playing_cards->get(1)->rank->points);
        $this->assertEquals(11, $this->trick->playing_cards->get(2)->rank->points);
        $this->assertEquals(0, $this->trick->playing_cards->get(3)->rank->points);
    }

    /** @test */
    public function it_calculates_points()
    {
        $this->addCards();

        $this->assertEquals(13, $this->trick->points());

        $this->trick->update(['trump' => 'S6']);

        $this->assertEquals(45, $this->trick->points());
    }

    /**
     * Add some cards to the trick
     */
    private function addCards()
    {
        factory(Card::class)->create([
            'trick_id' => $this->trick->id,
            'card_id' => 'SJ' // plain: 2 points, trump: 20 points
        ]);

        factory(Card::class)->create([
            'trick_id' => $this->trick->id,
            'card_id' => 'S9' // plain: 0 points, trump: 14 points
        ]);

        factory(Card::class)->create([
            'trick_id' => $this->trick->id,
            'card_id' => 'CA' // plain & trump: 11 points
        ]);

        factory(Card::class)->create([
            'trick_id' => $this->trick->id,
            'card_id' => 'H6' // plain & trump: 0 points
        ]);
    }
    
}

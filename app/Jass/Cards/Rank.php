<?php

namespace App\Jass\Cards;

use App\Jsonable;

class Rank
{

    use Jsonable;

    /**
     * @var string
     */
    public $symbol;

    /**
     * @var int
     */
    public $index;

    /**
     * @var int
     */
    public $points;

    /**
     * @var int
     */
    public $sort;

    /**
     * Rank constructor.
     *
     * @param $symbol
     * @param $index
     * @param $points
     * @param $sort
     */
    public function __construct($symbol, $index, $points, $sort)
    {
        $this->symbol = $symbol;    // A, K, Q, J, 10, 9, 8, 7, 6
        $this->index = $index;      // 9, 8, 7, 6, 5, 4, 3, 2, 1 (higher is stronger)
        $this->points = $points;    // Depends on whether it is trump suit or not
        $this->sort = $sort;        // Sort order
    }

}

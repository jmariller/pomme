<?php

namespace App\Jass\Cards;

use App\Jass\JassConfiguration;

class PlayingCard
{

    /**
     * @var string
     */
    public $id;

    /**
     * @var string
     */
    public $name;

    /**
     * @var Suit
     */
    public $suit;

    /**
     * @var string
     */
    public $color;

    /**
     * @var Rank
     */
    public $rank;

    /**
     * @var int
     */
    public $rank_index;

    /**
     * @var int
     */
    public $points;

    /**
     * @var bool
     */
    public $six_of_trump;

    /**
     * PlayingCard constructor.
     *
     * @param Suit $suit
     * @param Rank $rank
     */
    public function __construct(Suit $suit, Rank $rank)
    {
        $this->suit = $suit;
        $this->rank = $rank;

        $this->setHelpers();
    }

    /**
     * Get a card by its unique identifier
     *
     * @param $id
     * @return PlayingCard|null
     */
    public static function byId($id)
    {
        if (!static::isValid($id)) {
            return null;
        }

        $card_decoded = JassConfiguration::decode($id);

        return new static(
            new Suit(...$card_decoded['suit']),
            new Rank(...$card_decoded['rank'])
        );
    }

    /**
     * Convert current card to trump
     *
     * @return PlayingCard
     */
    public function toTrump()
    {
        $this->suit->trump = true;
        $this->rank = new Rank(
            ...JassConfiguration::rank($this->rank->symbol, 'trump_suit')
        );

        $this->setHelpers();

        return $this;
    }

    /**
     * Convert current card to plain
     *
     * @return PlayingCard
     */
    public function toPlain()
    {
        $this->suit->trump = false;
        $this->rank = new Rank(
            ...JassConfiguration::rank($this->rank->symbol, 'plain_suit')
        );

        $this->setHelpers();

        return $this;
    }

    /**
     * Convert current card to fake
     *
     * @return PlayingCard
     */
    public function toFake()
    {
        $this->suit = new Suit('F', 'Fake', 0, 'fake');
        $this->rank = new Rank('F', 0, 0 ,0);

        $this->name = 'Fake Card';

        $this->setHelpers();

        return $this;
    }

    /**
     * Is the card in given suit?
     *
     * @param Suit $suit
     * @return bool
     */
    public function isInSuit(Suit $suit)
    {
        return $this->suit->code === $suit->code;
    }

    /**
     * Is the card the King or Queen of trump?
     *
     * @return bool
     */
    public function isKingOrQueenOfTrump()
    {
        return $this->isTrump() && ($this->rank->symbol === 'K' || $this->rank->symbol === 'Q');
    }

    /**
     * Check if a card is trump
     *
     * @return bool
     */
    public function isTrump()
    {
        return $this->suit->trump;
    }

    /**
     * Check if a card is the Jack of trump
     *
     * @return bool
     */
    public function isJass()
    {
        return $this->isTrump() && $this->rank->symbol === 'J';
    }

    /**
     * Set helpers (id, rank_index, points, six_of_trump)
     */
    private function setHelpers()
    {
        $this->id = $this->suit->code . $this->rank->symbol;
        $this->name = __('jass.ranks.' . $this->rank->symbol) . ' ' . __('jass.misc.of') . ' ' . $this->suit->name;
        $this->color = $this->suit->color;
        $this->rank_index = $this->rank->index;
        $this->points = $this->rank->points;
        $this->six_of_trump = $this->suit->trump && $this->rank->symbol === '6';
    }

    /**
     * Give card identifier should have proper format (see example)
     *
     * @example <Suit Code><Rank Symbol> (e.g. SA for Ace of Spades)
     * @param $id
     * @return bool
     */
    private static function isValid($id)
    {
        return preg_match('/[SCHD]([6789JQKA]|10)/', $id) === 1;
    }

}

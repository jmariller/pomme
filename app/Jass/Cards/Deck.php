<?php

namespace App\Jass\Cards;

use App\Jass\JassConfiguration;
use Illuminate\Support\Collection;

class Deck
{

    /**
     * @var array list of cards
     */
    protected $cards = [];

    /**
     * Set list of cards from a given string list (e.g. "SA;C9;HJ...")
     *
     * @param $joinedCards
     * @return Deck
     */
    public function fromString($joinedCards)
    {
        if ($joinedCards != null) {
            $cards_ids = explode(';', $joinedCards);

            foreach ($cards_ids as $card_id) {
                $this->cards[] = $this->getCard($card_id);
            }
        }

        return $this;
    }

    /**
     * Set list of cards from a given string list (e.g. "SA;C9;HJ...")
     *
     * @return Deck
     */
    public function fromConfig()
    {
        foreach (config('jass.suits') as $suit) {
            foreach (config('jass.plain_suit') as $rank) {
                $this->cards[] = new PlayingCard(
                    new Suit(...array_values($suit)),
                    new Rank(...array_values($rank))
                );
            }
        }

        return $this;
    }

    /**
     * Apply the trump to the current deck
     *
     * @param PlayingCard $trump
     * @return Deck
     */
    public function applyTrump($trump)
    {
        if ($trump !== null) {
            foreach ($this->cards as $card) {
                if ($card->suit->code === $trump->suit->code) {
                    $card->toTrump();
                }
            }
        }

        return $this;
    }

    /**
     * Fetch the trump (specific for Pomme: 25th card)
     *
     * @return PlayingCard
     */
    public function trump()
    {
        return $this->cards[24];
    }

    /**
     * Draw a card located in a given position
     *
     * @param int $position
     * @return PlayingCard
     */
    public function draw(int $position)
    {
        return $this->cards[$position];
    }

    /**
     * Get the list of cards
     *
     * @return Collection
     */
    public function cards()
    {
        return collect($this->cards);
    }

    /**
     * Implode the list of cards
     *
     * @return string
     */
    public function implode()
    {
        return $this->cards()->implode('id', ';');
    }

    /**
     * Shuffle the list of cards
     *
     * @param int $iterations
     * @return Deck
     */
    public function shuffle(int $iterations = 100)
    {
        for ($i = 1 ; $i <= $iterations ; $i++) shuffle($this->cards);

        return $this;
    }

    /**
     * Sort cards by suit and rank
     *
     * @return $this
     */
    public function sort()
    {
        $this->adjustHandOrderWith3colors();

        usort($this->cards, $this->buildSortClosure());

        return $this;
    }

    /**
     * Adjust order of suits for hands with 3 colors (e.g. alternate red/black)
     */
    private function adjustHandOrderWith3colors()
    {
        $unique_suits = $this->cards()->pluck('suit.code')->unique();

        if ($this->cards()->count() <= 9 && $unique_suits->count() === 3) {
            $missing_spades = !$unique_suits->contains('S');
            $missing_diamonds = !$unique_suits->contains('D');

            $this->cards = array_map(function (PlayingCard $card) use ($missing_spades, $missing_diamonds) {
                if ($card->suit->code == 'C' && $missing_spades) {
                    $card->suit->sort = 2;
                } elseif ($card->suit->code == 'H' && $missing_diamonds) {
                    $card->suit->sort = 3;
                }

                return $card;
            }, $this->cards);
        }
    }

    /**
     * Build a sort function using suits sort order
     *
     * @return \Closure
     */
    private function buildSortClosure()
    {
        return function ($card1, $card2) {
            if (($suit_sort = $card1->suit->sort - $card2->suit->sort) !== 0) {
                return $suit_sort;
            }

            return $card2->rank->sort - $card1->rank->sort;
        };
    }

    /**
     * Get a card by ID from the configuration
     *
     * @param $id
     * @return PlayingCard
     */
    private function getCard($id)
    {
        if ($id == 'FF') {
            return PlayingCard::byId('SA')->toFake();
        }

        $card_decoded = JassConfiguration::decode($id);

        return new PlayingCard(
            new Suit(...$card_decoded['suit']),
            new Rank(...$card_decoded['rank'])
        );
    }

}

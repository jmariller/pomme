<?php

namespace App\Jass\Cards;

use App\Jsonable;

class Suit
{

    use Jsonable;

    /**
     * @var string
     */
    public $code;

    /**
     * @var string
     */
    public $name;

    /**
     * @var int
     */
    public $sort;

    /**
     * @var string
     */
    public $color;

    /**
     * @var bool
     */
    public $trump;

    /**
     * Suit constructor.
     *
     * @param      $code
     * @param      $name
     * @param      $sort
     * @param      $color
     * @param bool $trump
     */
    public function __construct($code, $name, $sort, $color, $trump = false)
    {
        $this->code = $code;    // D, H, S, C
        $this->name = $name;    // Diamond, Hearts, Spades, Clubs
        $this->sort = $sort;    // Sort order
        $this->color = $color;    // Sort order
        $this->trump = $trump;  // Is this suit the trump?
    }

}

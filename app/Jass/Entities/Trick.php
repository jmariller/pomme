<?php

namespace App\Jass\Entities;

use App\Jass\Cards\PlayingCard;
use Illuminate\Support\Collection;
use Illuminate\Database\Eloquent\Model;

class Trick extends Model
{

    use TrickKnowsTheWinner;

    /**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
    protected $guarded = [
        'id', 'cards_count', 'created_at', 'updated_at',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'trick' => 'integer',
        'winner_id' => 'integer',
        'completed' => 'boolean',
        'cards_count' => 'integer'
    ];

    /**
     * The attributes that should be visible for arrays.
     *
     * @var array
     */
    protected $visible = ['trick', 'completed', 'winner_id'];

    /**
     * Get the list of playing cards
     *
     * @return Collection
     */
    public function getPlayingCardsAttribute()
    {
        return app('deck')->fromString($this->fresh()->cards->implode('card_id', ';'))
            ->applyTrump($this->trump)->cards();
    }

    /**
     * Get the actual trump
     *
     * @param $value
     * @return PlayingCard
     */
    public function getTrumpAttribute($value)
    {
        return PlayingCard::byId($value);
    }

    /**
     * The round where this trick belongs to
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function round()
    {
        return $this->belongsTo(Round::class);
    }

    /**
     * List of cards played in that trick
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function cards()
    {
        return $this->hasMany(Card::class);
    }

    /**
     * The player that starts the trick
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function firstPlayer()
    {
        return $this->hasOne(Player::class, 'user_id', 'first_player_id');
    }

    /**
     * The winner of the trick
     * 
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function winner()
    {
        return $this->hasOne(Player::class, 'user_id', 'winner_id');
    }

    /**
     * Add a card to the current trick
     *
     * @param int    $playerId
     * @param string $cardId
     * @return Model
     */
    public function addCard(int $playerId, string $cardId)
    {
        return $this->cards()->create([
            'player_id' => $playerId,
            'card_id' => $cardId
        ]);
    }

    /**
     * Finish the trick
     */
    public function finish()
    {
        $this->update([
            'winner_id' => $this->findWinner(),
            'completed' => true,
            'points' => $this->points()
        ]);
    }

}

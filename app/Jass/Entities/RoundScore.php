<?php

namespace App\Jass\Entities;

use Illuminate\Database\Eloquent\Model;

class RoundScore extends Model
{

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'round_id', 'player_id', 'points', 'melds', 'total'
    ];

    /**
     * The attributes that should be visible for arrays.
     *
     * @var array
     */
    protected $visible = ['player_id', 'points', 'melds', 'total'];

    /**
     * The corresponding round
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function round()
    {
        return $this->belongsTo(Round::class);
    }
}

<?php

namespace App\Jass\Entities;

use App\Jass\Cards\PlayingCard;

trait PlayerCanOnlyDrawValidCards
{

    /**
     * Check if a card can be played against another one
     *
     * @param PlayingCard $myCard
     * @param PlayingCard $otherCard
     * @return bool
     */
    public function canDrawCard(PlayingCard $myCard, PlayingCard $otherCard)
    {
        // The trump is almighty and can be drawn anytime
        if ($myCard->suit->trump) {
            return true;
        }

        // If the player doesn't have any card in that suit, they can play any card
        if (!$this->hasSuitFromCard($otherCard)) {
            return true;
        }

        // Special case when the Jass (Jack of trump) is the last trump in hand:
        // - Can be held till the end, so any other card can be drawn
        if ($otherCard->suit->trump && $this->nbTrumps() == 1 && $this->hasJass()) {
            return true;
        }

        // The player must draw cards of same color
        return $myCard->isInSuit($otherCard->suit);
    }

    /**
     * Does the player have the Jass (Jack of Trump)?
     *
     * @return bool
     */
    private function hasJass()
    {
        return $this->hand->contains->isJass();
    }

    /**
     * Does the player have a given suit?
     *
     * @param PlayingCard $otherCard
     * @return bool
     */
    private function hasSuitFromCard(PlayingCard $otherCard)
    {
        return $this->hand->contains->isInSuit($otherCard->suit);
    }

    /**
     * How many trumps does the player have?
     *
     * @return int
     */
    private function nbTrumps()
    {
        return $this->hand->filter->isTrump()->count();
    }

}

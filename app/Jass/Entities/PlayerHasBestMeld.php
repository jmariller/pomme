<?php

namespace App\Jass\Entities;

use App\Jass\JassConfiguration;

trait PlayerHasBestMeld
{

    /**
     * Load and show player's melds after first trick, provided they won them
     *
     * @param bool $wonMelds
     * @param bool $after1stTrick
     * @return PlayerHasBestMeld
     */
    public function withMelds(bool $wonMelds, bool $after1stTrick)
    {
        return !($wonMelds && $after1stTrick) ? $this : $this->load(['melds' => function ($query) {
            $query->whereRoundId($this->round_id);
        }])->makeVisible('melds');
    }

    /**
     * Get the best melds index
     *
     * @return mixed
     */
    public function getBestMeldsIndexAttribute()
    {
        if (($melds = $this->current_melds) == null) {
            return 0;
        }

        return $melds->best_index;
    }

    /**
     * Get the best melds title
     *
     * @return string
     */
    public function getBestMeldsTitleAttribute()
    {
        if ($this->best_melds_index == 0) {
            return '';
        }

        return __('jass.melds')[
            JassConfiguration::meldByIndex($this->best_melds_index)['code']
        ];
    }

    /**
     * Get the melds for the current round
     *
     * @return mixed
     */
    public function getCurrentMeldsAttribute()
    {
        return $this->melds()->whereRoundId($this->round_id)->first();
    }

}

<?php

namespace App\Jass\Entities;

use App\Jass\Cards\PlayingCard;
use Illuminate\Support\Collection;

trait RoundHasCards
{

    /**
     * Get the deck of cards as a list of playing cards
     *
     * @param $value
     * @return Collection
     */
    public function getCardsDeckAttribute($value)
    {
        return app('deck')->fromString($value)->applyTrump($this->trump)->cards();
    }

    /**
     * Set the deck of cards optionally from a list of playing cards
     *
     * @param $value
     */
    public function setCardsDeckAttribute($value)
    {
        $this->setAttributeValue('cards_deck', $value);
    }

    /**
     * Get the cards drawn as a list of playing cards
     *
     * @param $value
     * @return Collection
     */
    public function getCardsDrawnAttribute($value)
    {
        return app('deck')->fromString($value)->cards();
    }

    /**
     * Set the cards drawn optionally from a list of playing cards
     *
     * @param $value
     */
    public function setCardsDrawnAttribute($value)
    {
        $this->setAttributeValue('cards_drawn', $value);
    }

    /**
     * Get the indexes of the cards drawn from the deck
     *
     * @return Collection
     */
    public function getCardsDrawnIndexesAttribute()
    {
        return $this->cards_deck
            ->filter(function (PlayingCard $card) {
                return $this->cards_drawn->contains($card);
            })
            ->keys();
    }

    /**
     * Get the actual trump
     *
     * @param $value
     * @return PlayingCard
     */
    public function getTrumpAttribute($value)
    {
        $card = PlayingCard::byId($value);

        return $card != null ? $card->toTrump() : null;
    }

    /**
     * Set an attribute value
     *
     * @param $attribute
     * @param $value
     */
    private function setAttributeValue($attribute, $value)
    {
        $this->attributes[$attribute] = $value instanceof Collection ? $value->implode('id', ';') : $value;
    }

    /**
     * Get a hand within the deck for a given player key
     *
     * @param int $playerKey
     * @return Collection
     */
    private function handPerPlayer(int $playerKey)
    {
        return $this->cards_deck
            ->chunk(3)
            ->filter(function ($chunk, $key) use ($playerKey) {
                return $key == $playerKey || $key == $playerKey + $this->nb_players || $key == $playerKey + $this->nb_players * 2;
            })
            ->flatten();
    }

    /**
     * Get backup cards within the deck for a given player key
     *
     * @param int $playerKey
     * @return Collection
     */
    private function backupPerPlayer(int $playerKey)
    {
        return $this->cards_deck
            ->chunk(3)
            ->filter(function ($chunk, $key) use ($playerKey) {
                return $key == $playerKey + 6;
            })
            ->flatten();
    }

}

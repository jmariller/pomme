<?php

namespace App\Jass\Entities;

use Illuminate\Database\Eloquent\Model;

class GameScore extends Model
{

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'game_id', 'player_id', 'rounds_won', 'pommes_count'
    ];

    /**
     * The attributes that should be visible for arrays.
     *
     * @var array
     */
    protected $visible = ['player_id', 'pommes_count', 'rounds_won'];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'rounds_won' => 'integer',
        'pommes_count' => 'integer'
    ];

    /**
     * The corresponding game
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function game()
    {
        return $this->belongsTo(Game::class);
    }

    /**
     * Minimal information
     *
     * @return array
     */
    public function minimalInfo()
    {
        return [
            'player_id' => $this->player_id,
            'rounds_won' => $this->rounds_won,
            'pommes_count' => $this->pommes_count
        ];
    }

}

<?php

namespace App\Jass\Entities;

use App\Jass\JassConfiguration;

trait RoundKnowsTheWinner
{

    /**
     * Find the round winner
     *
     * @return int|null
     */
    public function findWinner()
    {
        if ($this->bothPlayersBelow21points()) {
            return null;
        }

        if ($this->onePlayerBelow21points()) {
            return $this->firstPlayerAbove21points();
        }

        if ($this->hasTie()) {
            return null;
        }

        return $this->playerWithTopScore();
    }

    /**
     * Both players have less than 21 points?
     *
     * @return bool
     */
    private function bothPlayersBelow21points()
    {
        return $this->game_type == JassConfiguration::constant('type_pomme') &&
            $this->scores()->where('points', '<', JassConfiguration::constant('pomme_threshold'))->count() == JassConfiguration::constant('pomme_nb_players');
    }

    /**
     * One player has less than 21 points?
     *
     * @return bool
     */
    private function onePlayerBelow21points()
    {
        return $this->game_type == JassConfiguration::constant('type_pomme') &&
            $this->scores()->where('points', '<', JassConfiguration::constant('pomme_threshold'))->count() == 1;
    }

    /**
     * First player who has at least 21 points
     *
     * @return int
     */
    private function firstPlayerAbove21points()
    {
        return $this->scores()->where('points', '>=', JassConfiguration::constant('pomme_threshold'))->first()->player_id;
    }

    /**
     * Is there a tie in the scores?
     *
     * @return bool
     */
    private function hasTie()
    {
        return $this->scores->unique->total->count() !== $this->scores()->count();
    }

    /**
     * Player with best score
     *
     * @return int
     */
    private function playerWithTopScore()
    {
        return $this->scores->sortBy('total')->last()->player_id;
    }

}

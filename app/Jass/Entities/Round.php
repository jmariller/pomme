<?php

namespace App\Jass\Entities;

use App\Jass\JassConfiguration;
use App\Jass\Melds\PlayerMelds;
use Illuminate\Support\Collection;
use Illuminate\Database\Eloquent\Model;

class Round extends Model
{

    use RoundHasCards, RoundKnowsTheWinner;

    /**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
    protected $guarded = ['id', 'created_at', 'updated_at'];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'round' => 'integer',
        'first_player_id' => 'integer',
        'winner_id' => 'integer',
        'completed' => 'boolean',
        'tricks_count' => 'integer'
    ];

    /**
     * The attributes that should be visible for arrays.
     *
     * @var array
     */
    protected $visible = ['round', 'trump', 'scores', 'cards_drawn_indexes', 'tricks_count', 'winner_id'];

    /**
     * The accessors to append to the model's array form.
     *
     * @var array
     */
    protected $appends = ['cards_drawn_indexes'];

    /**
     * Determine if a normal round is finished (e.g. 9 tricks played)
     *
     * @return bool
     */
    public function getIsFinishedAttribute()
    {
        return $this->round > 0 && $this->tricks_count == JassConfiguration::constant('cards_per_player');
    }

    /**
     * Get the corresponding game
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function game()
    {
        return $this->belongsTo(Game::class);
    }

    /**
     * Get the corresponding tricks
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function tricks()
    {
        return $this->hasMany(Trick::class);
    }

    /**
     * Get the scores
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function scores()
    {
        return $this->hasMany(RoundScore::class);
    }

    /**
     * Get the melds
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function melds()
    {
        return $this->hasMany(PlayerMelds::class);
    }

    /**
     * The player that starts the round
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function firstPlayer()
    {
        return $this->hasOne(Player::class, 'user_id', 'first_player_id');
    }

    /**
     * The player after the one starting the round
     *
     * @return mixed
     */
    public function secondPlayer()
    {
        return $this->sortedPlayers()->get(1);
    }

    /**
     * Get the list of players in the round sorted by first player
     *
     * @return Collection
     */
    public function sortedPlayers()
    {
        $players = $this->game->sortedPlayers;

        return $players->map(function ($player, $key) use ($players) {
            return $players[
                ( $key + $players->pluck('id')->search($this->first_player_id) ) % $players->count()
            ];
        });
    }

    /**
     * The player for next round
     *
     * @return mixed
     */
    public function nextPlayer()
    {
        $players = $this->game->sortedPlayers;

        return $players[
            ( $players->pluck('id')->search($this->first_player_id) + 1 ) % $players->count()
        ];
    }

    /**
     * Get the winner for that round
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function winner()
    {
        return $this->hasOne(Player::class, 'user_id', 'winner_id');
    }

    /**
     * Get the current trick
     *
     * @return Model|\Illuminate\Database\Eloquent\Relations\HasMany|null|object
     */
    public function currentTrick()
    {
        return $this->tricks()->latest('trick')->first();
    }

    /**
     * Add a trick to the current round
     *
     * @param int $firstPlayerId
     * @return Model
     */
    public function addTrick(int $firstPlayerId)
    {
        return $this->tricks()->create([
            'round' => $this->round,
            'trump' => $this->trump ? $this->trump->id : null,
            'trick' => $this->tricks_count + 1,
            'first_player_id' => $firstPlayerId
        ]);
    }

    /**
     * Distribute cards to players
     */
    public function distributeCards()
    {
        $this->sortedPlayers()->each(function (Player $player, int $key) {
            $player->saveCards([
                'card' => null,
                'discarded' => null,
                'played' => null,
                'hand' => $this->handPerPlayer($key),
                'backup' => $this->backupPerPlayer($key),
                'trump' => $this->trump->id
            ]);
        });
    }

    /**
     * Finish the round
     *
     * @param int|null $winnerId
     */
    public function finish(int $winnerId = null)
    {
        $this->update([
            'winner_id' => $winnerId ?: $this->findWinner(),
            'completed' => true
        ]);
    }

}

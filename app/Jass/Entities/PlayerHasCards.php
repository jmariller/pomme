<?php

namespace App\Jass\Entities;

use App\Jass\Cards\PlayingCard;
use Illuminate\Support\Collection;

trait PlayerHasCards
{

    /**
     * Get the current player with a fake hand
     *
     * @return PlayerHasCards
     */
    public function withFakeHand()
    {
        $this->hand = $this->hand->toFake();

        return $this;
    }

    /**
     * Determine if the player has the king or queen of trump in hand
     *
     * @return bool
     */
    public function getHasKingOrQueenOfTrumpAttribute()
    {
        return $this->hand->filter->isKingOrQueenOfTrump()->count() >= 1;
    }

    /**
     * Get the actual card
     *
     * @param $value
     * @return PlayingCard
     */
    public function getCardAttribute($value)
    {
        return PlayingCard::byId($value);
    }

    /**
     * Get the hand as an actual list of playing cards
     *
     * @param $value
     * @return Collection
     */
    public function getHandAttribute($value)
    {
        return app('deck')->fromString($value)->applyTrump($this->trump)->sort()->cards();
    }

    /**
     * Set the hand optionally from an actual list of playing cards
     *
     * @param $value
     */
    public function setHandAttribute($value)
    {
        $this->setAttributeValue('hand', $value);
    }

    /**
     * Get the number of cards in hand
     *
     * @return mixed
     */
    public function getNbCardsAttribute()
    {
        return $this->hand->count();
    }

    /**
     * Get the backup as an actual list of playing cards
     *
     * @param $value
     * @return Collection
     */
    public function getBackupAttribute($value)
    {
        return app('deck')->fromString($value)->cards();
    }

    /**
     * Set the backup optionally from an actual list of playing cards
     *
     * @param $value
     */
    public function setBackupAttribute($value)
    {
        $this->setAttributeValue('backup', $value);
    }

    /**
     * Get the discarded cards as an actual list of playing cards
     *
     * @param $value
     * @return Collection
     */
    public function getDiscardedAttribute($value)
    {
        return app('deck')->fromString($value)->cards();
    }

    /**
     * Set the discarded cards optionally from an actual list of playing cards
     *
     * @param $value
     */
    public function setDiscardedAttribute($value)
    {
        $this->setAttributeValue('discarded', $value);
    }

    /**
     * Get the played cards as an actual list of playing cards
     *
     * @param $value
     * @return Collection
     */
    public function getPlayedAttribute($value)
    {
        return app('deck')->fromString($value)->cards();
    }

    /**
     * Set the played cards optionally from an actual list of playing cards
     *
     * @param $value
     */
    public function setPlayedAttribute($value)
    {
        $this->setAttributeValue('played', $value);
    }

    /**
     * Get the current trump
     *
     * @param $value
     * @return PlayingCard|null
     */
    public function getTrumpAttribute($value)
    {
        return PlayingCard::byId($value);
    }

    /**
     * Set an attribute value
     *
     * @param $attribute
     * @param $value
     */
    private function setAttributeValue($attribute, $value)
    {
        $this->attributes[$attribute] = $value instanceof Collection ? $value->implode('id', ';') : $value;
    }

}

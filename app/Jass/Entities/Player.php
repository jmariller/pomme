<?php

namespace App\Jass\Entities;

use App\User;
use App\Jass\Melds\PlayerMelds;
use App\Jass\Cards\PlayingCard;
use App\Events\PlayerTookTrump;
use App\Events\PlayerTookBackup;
use App\Events\PlayerJoinedGame;
use Illuminate\Support\Collection;
use App\Events\PlayerDrewCardFromDeck;
use App\Events\PlayerDrewCardFromHand;
use Illuminate\Database\Eloquent\Model;

class Player extends Model
{

    use PlayerHasCards, PlayerCanOnlyDrawValidCards, PlayerHasBestMeld;

    /**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
    protected $guarded = ['created_at', 'updated_at'];

    /**
     * The attributes that should be visible for arrays.
     *
     * @var array
     */
    protected $visible = ['id', 'pseudo', 'hand', 'discarded', 'card', 'nb_cards', 'melds'];

    /**
     * The accessors to append to the model's array form.
     *
     * @var array
     */
    protected $appends = ['id', 'nb_cards'];

    /**
     * Primary key is the user id
     *
     * @var string
     */
    protected $primaryKey = 'user_id';

    /**
     * Get the player identifier (based on its user ID)
     *
     * @return integer
     */
    public function getIdAttribute()
    {
        return $this->user_id;
    }

    /**
     * Determine if player is already in a game
     *
     * @return bool
     */
    public function getInGameAttribute()
    {
        return $this->game_id != null;
    }

    /**
     * Determine if player is authenticated
     *
     * @return bool
     */
    public function getIsAuthenticatedAttribute()
    {
        return $this->id === auth()->id();
    }

    /**
     * The corresponding user
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function user()
    {
        return $this->hasOne(User::class, 'id', 'user_id');
    }

    /**
     * The game where the player is presently
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function game()
    {
        return $this->belongsTo(Game::class);
    }

    /**
     * Get the game scores
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function gameScores()
    {
        return $this->hasMany(GameScore::class, 'player_id', 'user_id');
    }

    /**
     * Get the round scores
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function roundScores()
    {
        return $this->hasMany(RoundScore::class, 'player_id', 'user_id');
    }

    /**
     * Get the melds
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function melds()
    {
        return $this->hasMany(PlayerMelds::class, 'player_id', 'user_id');
    }

    /**
     * Join a game
     *
     * @broadcasts PlayerJoinedGame
     *
     * @param string $type
     * @return Game
     */
    public function joinGame(string $type)
    {
        if ($this->in_game) return $this->game;

        $game = app('game')->firstNotFullOrNew($type);

        $this->update([
            'game_id' => $game->id,
            'sort_order' => $game->players_count + 1,
        ]);

        broadcast(new PlayerJoinedGame($this, $game))->toOthers();

        return $game;
    }

    /**
     * Draw a card from the deck
     *
     * @broadcasts PlayerDrewCardFromDeck
     * @param Collection $deck
     * @param int        $cardPos
     */
    public function drawCardFromDeck(Collection $deck, int $cardPos)
    {
        $this->abortIfNotMyTurn();

        $card = $deck->get($cardPos);

        $this->saveCards([
            'card' => $card->id,
            'hand' => $card->id,
            'played' => $card->id
        ]);

        broadcast(new PlayerDrewCardFromDeck($this, $card))->toOthers();
    }

    /**
     * Draw a card from hand
     *
     * @broadcasts PlayerDrewCardFromHand
     * @param string $cardId
     */
    public function drawCardFromHand(string $cardId)
    {
        $this->abortIfNotMyTurn();

        $card = PlayingCard::byId($cardId);
        
        $this->abortIfCardCannotBeDrawn($card, $this->game->last_card);

        $this->saveCards([
            'card' => $card->id,
            'hand' => $this->hand->pluck('id')->diff($card->id)->implode(';'),
            'played' => $card->id
        ]);

        broadcast(new PlayerDrewCardFromHand($this, $card))->toOthers();
    }

    /**
     * Save a set of cards (card, hand, backup, discarded, played)
     *
     * @param array $cards
     * @return Player
     */
    public function saveCards(array $cards)
    {
        $this->update($cards);

        return $this;
    }

    /**
     * Add a new round score
     *
     * @param int $roundId
     * @return Model
     */
    public function addRoundScores(int $roundId)
    {
        return $this->roundScores()->create(['round_id' => $roundId]);
    }

    /**
     * Take the backup cards, and exchange them with the discarded ones
     *
     * @broadcasts PlayerTookBackup
     * @param Collection $discarded
     */
    public function takeBackup(Collection $discarded)
    {
        $this->saveCards([
            'hand' => $this->hand->pluck('id')->diff($discarded)->concat($this->backup->pluck('id'))->implode(';'),
            'discarded' => $discarded->implode(';')
        ]);

        event(new PlayerTookBackup($this));
    }

    /**
     * Exchange 6 of Trump with actual Trump
     *
     * @broadcasts PlayerTookTrump
     * @param PlayingCard $trump
     */
    public function exchangeTrump(PlayingCard $trump)
    {
        $six_of_trump = $this->hand->toTrump($trump)->first->six_of_trump;

        $this->saveCards([
            'hand' => $this->hand->toTrump($trump)->reject->six_of_trump->push($trump)
        ]);

        broadcast(new PlayerTookTrump($this, $six_of_trump))->toOthers();
    }

    /**
     * Abort if not my turn
     */
    private function abortIfNotMyTurn()
    {
        abort_if($this->id != $this->game->who_is_next, 422, 'Not your turn...');
    }

    /**
     * Abort if my card cannot be drawn against last card
     *
     * @param $myCard
     * @param $lastCard
     */
    private function abortIfCardCannotBeDrawn($myCard, $lastCard)
    {
        $my_card = $myCard->isInSuit($this->trump->suit) ? $myCard->toTrump() : $myCard;
        $last_card = $lastCard !== null && $lastCard->isInSuit($this->trump->suit) ? $lastCard->toTrump() : $lastCard;

        abort_if($last_card != null && !$this->canDrawCard($my_card, $last_card), 422, 'Cannot draw this card!');
    }

}

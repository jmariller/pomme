<?php

namespace App\Jass\Entities;

use App\Jass\Cards\PlayingCard;
use Illuminate\Database\Eloquent\Model;

class Card extends Model
{

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'trick_id', 'player_id', 'card_id'
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'player_id' => 'integer'
    ];

    /**
     * The accessors to append to the model's array form.
     *
     * @var array
     */
    protected $appends = ['card'];

    /**
     * Fetch the corresponding deck card
     *
     * @return PlayingCard
     */
    public function getCardAttribute()
    {
        return PlayingCard::byId($this->card_id);
    }

    /**
     * The trick where this card belongs to
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function trick()
    {
        return $this->belongsTo(Trick::class);
    }

    /**
     * The player to whom this card belongs to
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function player()
    {
        return $this->hasOne(Player::class, 'user_id', 'player_id');
    }

}

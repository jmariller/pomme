<?php

namespace App\Jass\Entities;

use App\Jass\JassConfiguration;
use Illuminate\Database\Eloquent\Model;

class Game extends Model
{

    /**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
    protected $guarded = ['id', 'created_at', 'updated_at'];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'nb_players' => 'integer',
        'who_is_next' => 'integer',
        'completed' => 'boolean'
    ];

    /**
     * The attributes that should be visible for arrays.
     *
     * @var array
     */
    protected $visible = ['id', 'nb_players', 'players_count', 'other_players', 'scores', 'who_is_next', 'last_card'];

    /**
     * The accessors to append to the model's array form.
     *
     * @var array
     */
    protected $appends = ['other_players', 'last_card'];

    /**
     * Determine if we are in the selection round
     *
     * @return bool
     */
    public function getIsSelectionRoundAttribute()
    {
        return $this->rounds_count == 0;
    }

    /**
     * Get list of other players
     *
     * @return mixed
     */
    public function getOtherPlayersAttribute()
    {
        $current_round = $this->currentRound();

        return $this->players()->where('user_id', '!=', auth()->user()->player->id)->get()
            ->each(function (Player $player) use ($current_round) {
                return $player->withFakeHand()->makeHidden(['backup', 'discarded', 'melds'])
                    ->withMelds(
                        $current_round->melds_winner_id == $player->id,
                        $current_round->tricks_count >= 1
                    );
            });
    }

    /**
     * Get the last card drawn from the current round
     *
     * @return mixed
     */
    public function getLastCardAttribute()
    {
        $player_with_last_card = $this->players()->whereNotNull('card')->latest('updated_at')->first();

        return $player_with_last_card != null ? $player_with_last_card->card : null;
    }

    /**
     * Get the list of players in the game
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function players()
    {
        return $this->hasMany(Player::class);
    }

    /**
     * Get the list of player, in the order they should play
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function sortedPlayers()
    {
        return $this->players()->oldest('sort_order');
    }

    /**
     * Get the list of rounds
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function rounds()
    {
        return $this->hasMany(Round::class);
    }

    /**
     * Get the list of scores
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function scores()
    {
        return $this->hasMany(GameScore::class);
    }

    /**
     * Get the current round
     *
     * @return Model|\Illuminate\Database\Eloquent\Relations\HasMany|null|object
     */
    public function currentRound()
    {
        return $this->rounds()->latest('round')->first() ?? new Round;
    }

    /**
     * Get the previous round
     *
     * @return Round
     */
    public function previousRound()
    {
        return $this->round($this->rounds()->count() - 2);
    }

    /**
     * Fetch a round by its number
     *
     * @param $nbRound
     * @return Round
     */
    public function round($nbRound)
    {
        return $this->rounds()->whereRound($nbRound)->first();
    }

    /**
     * Create a new round
     *
     * @param int|null $firstPlayerId
     * @return Model
     */
    public function addRound(int $firstPlayerId = null)
    {
        $deck = app('deck')->fromConfig()->shuffle();

        $round = $this->rounds()->create([
            'game_type' => $this->type,
            'nb_players' => $this->nb_players,
            'round' => $this->rounds_count,
            'cards_deck' => $deck->implode(),
            'trump' => $this->is_selection_round ? null : $deck->trump()->id,
            'first_player_id' => $this->is_selection_round ? $this->sortedPlayers()->first()->id : $firstPlayerId,
        ]);

        $this->players()->update(['round_id' => $round->id]);

        return $round;
    }

    /**
     * Find the first game available with one player waiting, or create a new one
     *
     * @param string $type
     * @return Game
     */
    public function firstNotFullOrNew(string $type)
    {
        $nb_players = JassConfiguration::nbPlayers($type);

        $game = $this->firstNotFull($nb_players);

        if ($game === null) {
            $game = $this->create([
                'type' => $type,
                'nb_players' => $nb_players
            ]);
        }

        return $game;
    }

    /**
     * Find out who is next in line based on a given player identifier
     *
     * @param int $playerId
     * @return mixed
     */
    public function nextInLine(int $playerId)
    {
        $players = $this->sortedPlayers;
        $last_player_index = $players->pluck('id')->search($playerId);

        return $players->get( ($last_player_index + 1) % $players->count() )->id;
    }

    /**
     * Find the first game not full (e.g. waiting for opponents)
     *
     * @param int $nbPlayers
     * @return Game
     */
    private function firstNotFull(int $nbPlayers)
    {
        return $this->whereNotIn('players_count', [0, $nbPlayers])->oldest()->first();
    }

}

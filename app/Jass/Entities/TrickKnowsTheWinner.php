<?php

namespace App\Jass\Entities;

trait TrickKnowsTheWinner
{

    /**
     * Find the trick winner
     *
     * @return null|int
     */
    public function findWinner()
    {
        if ($this->round == 0 && $this->cardsWithSameRank()) {
            return null;
        }

        if ($this->round == 0) {
            $best_card = $this->playing_cards->sortByDesc('rank_index')->first();
        } else {
            $best_card = $this->bestCard($this->playing_cards->toArray());
        }

        return $this->cards()->whereCardId($best_card->id)->first()->player_id;
    }

    /**
     * Calculate total points for the trick winner
     *
     * @return int
     */
    public function points()
    {
        return $this->playing_cards->sum->points;
    }

    /**
     * Find the best card in a trick
     *
     * @param array $cards
     * @return mixed
     */
    private function bestCard(array $cards)
    {
        $previous_card = $cards[0];
        $winner = $cards[0];

        foreach (array_slice($cards, 1) as $card) {

            // First card played in plain suits wins
            if (!$previous_card->isTrump() && !$card->isTrump() && $previous_card->suit->code !== $card->suit->code) {
                $winner = $previous_card;
                continue;
            }

            // Otherwise card with higher rank wins
            $winner = ($previous_card->rank_index > $card->rank_index) ? $previous_card : $card;

            $previous_card = $card;
        }

        return $winner;
    }

    /**
     * Are there cards with same rank?
     *
     * @return bool
     */
    private function cardsWithSameRank()
    {
        return $this->playing_cards->unique->rank_index->count() !== $this->playing_cards->count();
    }

}

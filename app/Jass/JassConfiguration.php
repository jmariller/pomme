<?php

namespace App\Jass;

class JassConfiguration
{

    /**
     * Extract a meld by its code
     *
     * @param $code
     * @return array
     */
    public static function meldByCode($code)
    {
        return self::extract('melds', 'code', $code, false);
    }

    /**
     * Extract a meld by its index
     *
     * @param $index
     * @return array
     */
    public static function meldByIndex($index)
    {
        return self::extract('melds', 'index', $index, false);
    }

    /**
     * Extract a suit by its code
     *
     * @param $code
     * @return array
     */
    public static function suit($code)
    {
        return self::extract('suits', 'code', $code);
    }

    /**
     * Extract a rank by its symbol
     *
     * @param        $symbol
     * @param string $suit
     * @return array
     */
    public static function rank($symbol, $suit = 'plain_suit')
    {
        return self::extract($suit, 'symbol', $symbol);
    }

    /**
     * Get a given constant from configuration
     *
     * @param $constant
     * @return int
     */
    public static function constant($constant)
    {
        return config("jass.constants.$constant");
    }

    /**
     * Number of players for a given game type
     *
     * @param $gameType
     * @return int
     */
    public static function nbPlayers($gameType)
    {
        return config("jass.game_types.$gameType.nb_players");
    }

    /**
     * Extract a specific value from the configuration
     *
     * @param string $section
     * @param string $key
     * @param string $value
     * @param bool   $valuesOnly
     * @return array
     */
    public static function extract(string $section, string $key, string $value, bool $valuesOnly = true)
    {
        $filter = self::filter($section, $key, $value);

        if (count($filter) == 0) {
            return [];
        }

        return $valuesOnly ? array_values($filter[0]) : $filter[0];
    }

    /**
     * Decode a card identifier to its suit and rank
     *
     * @param string $cardId
     * @return array
     */
    public static function decode(string $cardId)
    {
        $suit = substr($cardId, 0, 1);
        $rank = substr($cardId, 1, strlen($cardId));

        return [
            'suit' => static::suit($suit),
            'rank' => static::rank($rank)
        ];
    }

    /**
     * Filter a specific section of the configuration
     *
     * @param $section
     * @param $key
     * @param $value
     * @return \Illuminate\Config\Repository|mixed
     */
    private static function filter($section, $key, $value)
    {
        return array_values(
            array_filter(config("jass.$section"), function ($element) use ($key, $value) {
                return $element[$key] == $value;
            })
        );
    }
}

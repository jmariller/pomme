<?php

namespace App\Jass\Melds;

use App\Jass\Entities\Round;
use App\Jass\JassConfiguration;

class MeldsWinner
{

    /**
     * Find the meld's winner for a given round
     *
     * @param Round $round
     * @return int
     */
    public static function winner(Round $round)
    {
        $melds = $round->melds;

        if (static::isPomme($melds) && static::hasTie($melds)) {
            return static::bestPlayerWhenTie($round, $melds);
        }

        if (static::isJass($melds) && static::hasTie($melds)) {
            // TODO implement winner melds for 4 players
            return null;
        }

        return static::playerWithBestIndex($melds);
    }

    /**
     * Find the best index within given player melds
     *
     * @param PlayerMelds $melds
     * @return mixed
     */
    public static function bestIndex(PlayerMelds $melds)
    {
        $melds_config = collect(config('jass.melds'));

        $code = $melds_config->pluck('code')->first(function ($code) use ($melds) {
            return $melds[$code];
        });

        return $code != null ? $melds_config->where('code', $code)->first()['index'] : null;
    }

    /**
     * Calculate the total points for given player melds
     *
     * @param PlayerMelds $melds
     * @return int
     */
    public static function totalPoints(PlayerMelds $melds)
    {
        return collect(config('jass.melds'))->sum(function ($meld) use ($melds) {
            return $meld['points'] * $melds[ $meld['code'] ];
        });
    }

    /**
     * Who is the best player in case of tie?
     *
     * @param $round
     * @param $melds
     * @return mixed
     */
    private static function bestPlayerWhenTie($round, $melds)
    {
        $first_player_melds = $round->firstPlayer->current_melds;
        $second_player_melds = $round->secondPlayer()->current_melds;

        $first_player_rank = $first_player_melds->bestSequence( static::nbCardsForBestIndex($melds) )->highest_rank_index;
        $second_player_best_sequence = $second_player_melds->bestSequence( static::nbCardsForBestIndex($melds) );
        $second_player_rank = $second_player_best_sequence->highest_rank_index;

        if ($first_player_rank == $second_player_rank) {
            $second_player_best_card = $second_player_best_sequence->sequence->last();

            return $second_player_best_card->suit->code == $round->trump->suit->code ? $second_player_melds->player_id : $first_player_melds->player_id;
        }

        return $first_player_rank > $second_player_rank ? $first_player_melds->player_id : $second_player_melds->player_id;
    }

    /**
     * Find the player with best index
     *
     * @param $melds
     * @return int|null
     */
    private static function playerWithBestIndex($melds)
    {
        return $melds->count() > 0 ? $melds->sortBy('best_index')->last()->player_id : null;
    }

    /**
     * Find the number of cards for a given melds's best index
     *
     * @param $melds
     * @return mixed
     */
    private static function nbCardsForBestIndex($melds)
    {
        return collect(config('jass.melds'))->where('index', $melds->first()->best_index)->first()['nb_cards'];
    }

    /**
     * Is there a tie in the melds?
     *
     * @param $melds
     * @return bool
     */
    private static function hasTie($melds)
    {
        return $melds->unique->best_index->count() != $melds->count();
    }

    /**
     * Are we in a Pomme melds?
     *
     * @param $melds
     * @return bool
     */
    private static function isPomme($melds)
    {
        return $melds->count() == JassConfiguration::constant('pomme_nb_players');
    }

    /**
     * Are we in a Jass melds?
     *
     * @param $melds
     * @return bool
     */
    private static function isJass($melds)
    {
        return $melds->count() == JassConfiguration::constant('jass_nb_players');
    }
    
}

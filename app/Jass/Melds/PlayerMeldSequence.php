<?php

namespace App\Jass\Melds;

use App\Jass\Cards\PlayingCard;
use Illuminate\Support\Collection;
use Illuminate\Database\Eloquent\Model;

class PlayerMeldSequence extends Model
{
    /**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
    protected $guarded = ['id', 'created_at', 'updated_at'];

    /**
     * The attributes that should be visible for arrays.
     *
     * @var array
     */
    protected $visible = ['sequence', 'points', 'code'];

    /**
     * The accessors to append to the model's array form.
     *
     * @var array
     */
    protected $appends = ['code'];

    /**
     * Get the sequence as a list of playing cards
     *
     * @param $value
     * @return Collection
     */
    public function getSequenceAttribute($value)
    {
        return app('deck')->fromString($value)->applyTrump($this->trump)->cards();
    }

    /**
     * Get the sequence code based on its length
     *
     * @return string
     */
    public function getCodeAttribute()
    {
        return "sequence_$this->length";
    }

    /**
     * Get the current trump
     *
     * @param $value
     * @return PlayingCard|null
     */
    public function getTrumpAttribute($value)
    {
        return PlayingCard::byId($value);
    }

    /**
     * Get the corresponding meld
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function meld()
    {
        return $this->belongsTo(PlayerMelds::class, 'player_meld_id', 'id');
    }

}

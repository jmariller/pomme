<?php

namespace App\Jass\Melds;

use App\Jass\Cards\PlayingCard;
use Illuminate\Support\Collection;

class MeldsDetector
{

    /**
     * Determine if the hand contains the royal couple (King and Queen of Trump)
     *
     * @param Collection $hand
     * @return bool
     */
    public static function hasRoyalCouple(Collection $hand)
    {
        return $hand->filter->isKingOrQueenOfTrump()->count() == 2;
    }

    /**
     * Determine if the hand contains the royal couple alone (not within a sequence)
     *
     * @param Collection $hand
     * @return bool
     */
    public static function hasRoyalCoupleAlone(Collection $hand)
    {
        return !self::sequences($hand)->contains(function ($sequence) {
            return self::hasRoyalCouple($sequence);
        });
    }

    /**
     * Determine if the hand has at least one quartet
     *
     * @param Collection $hand
     * @return bool
     */
    public static function hasAnyQuartet(Collection $hand)
    {
        return collect(config('jass.melds'))->pluck('symbol')->reject(function ($symbol) {
            return empty($symbol);
        })->first(function ($symbol) use ($hand) {
            return self::hasQuartet($hand, $symbol);
        }) != null;
    }

    /**
     * Determine if the hand contains a quartet (4 same cards, e.g. 4 aces)
     *
     * @param Collection $hand
     * @param string     $rankSymbol
     * @return bool
     */
    public static function hasQuartet(Collection $hand, string $rankSymbol)
    {
        return $hand->filter(function (PlayingCard $card) use ($rankSymbol) {
            return $card->rank->symbol == $rankSymbol;
        })->count() == 4;
    }

    /**
     * Get the number of points for a sequence
     *
     * @param Collection $sequence
     * @return int
     */
    public static function sequencePoints(Collection $sequence)
    {
        return collect(config('jass.melds'))->first(function($config) use ($sequence) {
            return $config['type'] == 'sequence' && $config['nb_cards'] == $sequence->count();
        })['points'];
    }

    /**
     * Count sequences of given length
     *
     * @param Collection $sequences
     * @param int        $length
     * @return int
     */
    public static function countSequencesOf(Collection $sequences, int $length)
    {
        return $sequences->filter(function ($sequence) use ($length) {
            return count($sequence) == $length;
        })->count();
    }

    /**
     * Find sequences within a sorted hand
     * TODO: Refactor?
     *
     * @param Collection $hand
     * @return Collection
     */
    public static function sequences(Collection $hand)
    {
        $OUT_OF_RANGE = -1;
        $SEQUENCE_MIN_LENGTH = 3;

        $sequences = [];
        $sequence = [];

        $hand->each(function (PlayingCard $card, $key) use ($hand, &$sequences, &$sequence, $OUT_OF_RANGE, $SEQUENCE_MIN_LENGTH) {

            // Store the sort order and suit code for previous, current and next card
            $prev_sort = $key - 1 > 0 ? $hand[$key - 1]->rank->sort : $OUT_OF_RANGE;
            $prev_suit = $key - 1 > 0 ? $hand[$key - 1]->suit->code : '';
            $curr_sort = $card->rank->sort;
            $curr_suit = $card->suit->code;
            $next_sort = $key + 1 < count($hand) ? $hand[$key + 1]->rank->sort : $OUT_OF_RANGE;
            $next_suit = $key + 1 < count($hand) ? $hand[$key + 1]->suit->code : '';

            // Check if previous and next are in sequence
            $prev_seq = abs($curr_sort - $prev_sort) == 1 && $curr_suit == $prev_suit;
            $next_seq = abs($curr_sort - $next_sort) == 1 && $curr_suit == $next_suit;

            // Add cards in current sequence
            if (($prev_sort == $OUT_OF_RANGE && !$prev_seq && $next_seq) || ($prev_seq || $next_seq)) {
                if (!static::hasQuartet($hand, $card->rank->symbol)) {
                    $sequence[] = $card;
                }
            }

            // Is this the end of a sequence ?
            if (!$prev_seq && !$next_seq || $prev_seq && !$next_seq || $prev_seq && $next_sort === $OUT_OF_RANGE) {

                // Record current sequence if it is long enough
                if (count($sequence) >= $SEQUENCE_MIN_LENGTH) {
                    $sequences[] = collect($sequence);
                }

                // Reset current sequence
                $sequence = [];
            }
        });

        return collect($sequences);
    }
    
}

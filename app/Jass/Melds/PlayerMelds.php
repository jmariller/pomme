<?php

namespace App\Jass\Melds;

use App\Jass\Entities\Round;
use App\Jass\Entities\Player;
use Illuminate\Database\Eloquent\Model;

class PlayerMelds extends Model
{

    /**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
    protected $guarded = ['id', 'created_at', 'updated_at'];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'four_jacks' => 'boolean',
        'four_nines' => 'boolean',
        'sequence_9' => 'boolean',
        'sequence_8' => 'boolean',
        'sequence_7' => 'boolean',
        'sequence_6' => 'boolean',
        'sequence_5' => 'boolean',
        'four_aces' => 'boolean',
        'four_kings' => 'boolean',
        'four_queens' => 'boolean',
        'four_tens' => 'boolean',
        'sequence_4' => 'boolean',
        'sequence_3' => 'boolean',
        'royal_couple' => 'boolean',
        'royal_couple_alone' => 'boolean'
    ];

    /**
     * The relations to eager load on every query.
     *
     * @var array
     */
    protected $with = ['sequences'];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = ['id', 'round_id', 'player', 'created_at', 'updated_at'];

    /**
     * The accessors to append to the model's array form.
     *
     * @var array
     */
    protected $appends = ['titles'];

    /**
     * Get the titles translations for melds
     *
     * @return array|null|string
     */
    public function getTitlesAttribute()
    {
        return __('jass.melds');
    }

    /**
     * Get the royal couple translated based on the status of the player hand
     *
     * @param $value
     * @return bool
     */
    public function getRoyalCoupleAttribute($value)
    {
        if ($value && !$this->attributes['royal_couple_alone']) {
            return true;
        }

        return $value && ($this->player->is_authenticated || !$this->player->has_king_or_queen_of_trump);
    }

    /**
     * Get the royal couple alone translated based on the status of the player hand
     *
     * @param $value
     * @return bool
     */
    public function getRoyalCoupleAloneAttribute($value)
    {
        return $value && ($this->player->is_authenticated || !$this->player->has_king_or_queen_of_trump);
    }

    /**
     * Get the corresponding player
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function player()
    {
        return $this->belongsTo(Player::class, 'player_id', 'user_id');
    }

    /**
     * Get the corresponding round
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function round()
    {
        return $this->belongsTo(Round::class);
    }

    /**
     * Get the corresponding sequences
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function sequences()
    {
        return $this->hasMany(PlayerMeldSequence::class, 'player_meld_id', 'id');
    }

    /**
     * Find the best sequence for a given length
     *
     * @param $length
     * @return mixed
     */
    public function bestSequence($length)
    {
        return $this->sequences()->whereLength($length)->orderBy('highest_rank_index', 'desc')->first();
    }

}

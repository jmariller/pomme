<?php

namespace App;

use App\Jass\Entities\Player;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'pseudo', 'name', 'email', 'password', 'guest',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * Boot the model.
     */
    protected static function boot()
    {
        parent::boot();

        static::created(function (User $user) {
            Player::create([
                'user_id' => $user->id,
                'pseudo' => $user->pseudo
            ]);
        });
    }

    /**
     * The corresponding player
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function player()
    {
        return $this->hasOne(Player::class);
    }

    /**
     * Create and return a new guest user
     *
     * @return User
     */
    public function addGuest()
    {
        $nb_new = $this->where('guest', true)->count() + 1;

        return $this->create([
            'pseudo' => "Guest {$nb_new}",
            'name' => "Guest {$nb_new}",
            'password' => '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm', // secret
            'guest' => true,
        ]);
    }
}

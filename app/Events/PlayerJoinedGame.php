<?php

namespace App\Events;

use App\Jass\Entities\Game;
use App\Jass\Entities\Player;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class PlayerJoinedGame implements ShouldBroadcast
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    /**
     * The player who joined the game
     *
     * @var Player
     */
    public $player;

    /**
     * The associated game
     *
     * @var Game
     */
    public $game;

    /**
     * Create a new event instance.
     *
     * @param Player $player
     * @param Game   $game
     */
    public function __construct(Player $player, Game $game)
    {
        $this->player = $player;

        $this->game = $game;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel("games.{$this->game->id}");
    }
}

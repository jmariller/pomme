<?php

namespace App\Events;

use App\Jass\Entities\Game;
use App\Jass\Entities\Round;
use App\Jass\Entities\Player;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class NewRoundStarted implements ShouldBroadcast
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    /**
     * @var Game
     */
    public $game;

    /**
     * @var Round
     */
    public $round;

    /**
     * @var Player
     */
    public $player;

    /**
     * Create a new event instance.
     *
     * @param Game   $game
     * @param Player $player
     */
    public function __construct(Game $game, Player $player)
    {
        $this->game = $game->load('scores');

        $this->round = $game->currentRound()->load('scores');

        $this->player = $player;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel("player.{$this->player->id}");
    }

    /**
     * Get the data to broadcast.
     *
     * @return array
     */
    public function broadcastWith()
    {
        return [
            'game' => $this->game,
            'round' => $this->round,
            'player' => $this->player->load(['melds' => function ($query) {
                $query->whereRoundId($this->round->id);
            }])
        ];
    }

}

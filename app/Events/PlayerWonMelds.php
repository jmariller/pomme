<?php

namespace App\Events;

use App\Jass\Entities\Game;
use App\Jass\Entities\Player;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class PlayerWonMelds implements ShouldBroadcast
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    /**
     * @var Game
     */
    public $game;

    /**
     * @var Player
     */
    public $player;

    /**
     * Create a new event instance.
     *
     * @param Game|null $game
     * @param Player    $player
     */
    public function __construct(Game $game, Player $player)
    {
        $this->game = $game;

        $this->player = $player;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel("player.{$this->player->id}");
    }

    /**
     * Get the data to broadcast.
     *
     * @return array
     */
    public function broadcastWith()
    {
        return [
            'game' => $this->game
        ];
    }

}

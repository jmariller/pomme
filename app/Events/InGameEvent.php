<?php

namespace App\Events;

use App\Jass\Entities\Game;
use App\Jass\Entities\Round;
use App\Jass\Entities\Player;
use App\Jass\Cards\PlayingCard;

interface InGameEvent
{

    /**
     * Create a new event instance.
     *
     * @param Player      $player
     * @param PlayingCard $card
     * @param Game        $game
     * @param Round       $round
     */
    public function __construct(Player $player, PlayingCard $card, Game $game, Round $round);

}

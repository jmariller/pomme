<?php

namespace App\Events;

use App\Jass\Entities\Game;
use App\Jass\Entities\Round;
use App\Jass\Entities\Trick;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class WhoIsNext implements ShouldBroadcast
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    /**
     * @var Game
     */
    public $game;

    /**
     * @var Round
     */
    public $round;

    /**
     * @var Trick
     */
    public $trick;

    /**
     * Create a new event instance.
     *
     * @param Game   $game
     */
    public function __construct(Game $game)
    {
        $this->game = $game->load('scores');

        $this->round = $game->currentRound()->load('scores');

        $this->trick = $this->round->currentTrick();
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel("games.{$this->game->id}");
    }
}

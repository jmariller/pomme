<?php

namespace App\Events;

use App\Jass\Entities\Player;
use App\Jass\Cards\PlayingCard;

interface PlayerDrewCard
{

    /**
     * Create a new event instance.
     *
     * @param Player      $player
     * @param PlayingCard $card
     */
    public function __construct(Player $player, PlayingCard $card);

}

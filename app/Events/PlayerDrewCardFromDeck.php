<?php

namespace App\Events;

use App\Jass\Entities\Game;
use App\Jass\Entities\Round;
use App\Jass\Entities\Player;
use App\Jass\Cards\PlayingCard;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class PlayerDrewCardFromDeck implements ShouldBroadcast, PlayerDrewCard
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    /**
     * @var Game
     */
    public $game;

    /**
     * @var Round
     */
    public $round;

    /**
     * @var Player
     */
    public $player;

    /**
     * @var PlayingCard
     */
    public $card;

    /**
     * @var int
     */
    public $card_pos;

    /**
     * Create a new event instance.
     *
     * @param Player      $player
     * @param PlayingCard $card
     */
    public function __construct(Player $player, PlayingCard $card)
    {
        $this->player = $player;

        $this->card = $card;

        $this->game = $this->player->game;

        $this->round = $this->game->round(0);

        $this->card_pos = $this->cardPosition($this->card);
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel("games.{$this->game->id}");
    }

    /**
     * Get the selected card's position in the deck
     *
     * @param PlayingCard $cardDrawn
     * @return int
     */
    private function cardPosition(PlayingCard $cardDrawn)
    {
        return $this->round->cards_deck
            ->filter(function (PlayingCard $card) use ($cardDrawn) {
                return $cardDrawn->id == $card->id;
            })
            ->keys()
            ->first();
    }
}

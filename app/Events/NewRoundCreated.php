<?php

namespace App\Events;

use App\Jass\Entities\Game;
use App\Jass\Entities\Round;
use App\Jass\Entities\Player;
use App\Jass\Cards\PlayingCard;
use Illuminate\Queue\SerializesModels;

class NewRoundCreated implements InGameEvent
{
    use  SerializesModels;

    /**
     * @var Game
     */
    public $game;

    /**
     * @var Round
     */
    public $round;

    /**
     * @var Player
     */
    public $player;

    /**
     * Create a new event instance.
     *
     * @param Game             $game
     * @param Round            $round
     * @param Player           $player
     * @param PlayingCard|null $card
     */
    public function __construct(Player $player, PlayingCard $card = null, Game $game = null, Round $round = null)
    {
        $this->game = $game;

        $this->round = $round;

        $this->player = $player;
    }

}

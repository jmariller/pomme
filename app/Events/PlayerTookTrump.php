<?php

namespace App\Events;

use App\Jass\Entities\Game;
use App\Jass\Entities\Round;
use App\Jass\Entities\Player;
use App\Jass\Cards\PlayingCard;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class PlayerTookTrump implements ShouldBroadcast, InGameEvent
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    /**
     * @var Game
     */
    public $game;

    /**
     * @var Round
     */
    public $round;

    /**
     * @var Player
     */
    public $player;

    /**
     * @var PlayingCard
     */
    public $six_of_trump;

    /**
     * Create a new event instance.
     *
     * @param Player      $player
     * @param PlayingCard $card
     * @param Game|null   $game
     * @param Round|null  $round
     */
    public function __construct(Player $player, PlayingCard $card, Game $game = null, Round $round = null)
    {
        $this->six_of_trump = $card;

        $this->game = $player->game;

        $this->round = $this->game->currentRound();

        $this->player = $player;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel("games.{$this->game->id}");
    }
}

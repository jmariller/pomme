<?php

namespace App\Events;

use App\Jass\Entities\Game;
use App\Jass\Entities\Round;
use App\Jass\Entities\Player;
use App\Jass\Cards\PlayingCard;
use Illuminate\Queue\SerializesModels;

class PlayerTookBackup implements InGameEvent
{
    use SerializesModels;

    /**
     * @var Game
     */
    public $game;

    /**
     * @var Round
     */
    public $round;

    /**
     * @var Player
     */
    public $player;

    /**
     * Create a new event instance.
     *
     * @param Player      $player
     * @param PlayingCard $card
     * @param Game|null   $game
     * @param Round|null  $round
     */
    public function __construct(Player $player, PlayingCard $card = null, Game $game = null, Round $round = null)
    {
        $this->game = $player->game;

        $this->round = $this->game->currentRound();

        $this->player = $player;
    }

}

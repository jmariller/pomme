<?php

namespace App\Events;

use App\Jass\Entities\Game;
use App\Jass\Entities\Round;
use App\Jass\Entities\Trick;
use App\Jass\Entities\Player;

interface RoundCompleted
{

    /**
     * Create a new event instance.
     *
     * @param Game   $game
     * @param Round  $round
     * @param Trick  $trick
     * @param Player $player
     */
    public function __construct(Game $game, Round $round, Trick $trick, Player $player);

}

<?php

namespace App\Events;

use App\Jass\Entities\Game;
use App\Jass\Entities\Player;
use App\Jass\Entities\Round;
use App\Jass\Entities\Trick;
use Illuminate\Queue\SerializesModels;

class NormalTrickCompleted
{
    use SerializesModels;

    /**
     * @var Game
     */
    public $game;

    /**
     * @var Round
     */
    public $round;

    /**
     * @var Trick
     */
    public $trick;

    /**
     * @var Player
     */
    public $player;

    /**
     * Create a new event instance.
     *
     * @param Game   $game
     * @param Round  $round
     * @param Trick  $trick
     * @param Player $player
     */
    public function __construct(Game $game, Round $round, Trick $trick, Player $player)
    {
        $this->game = $game;

        $this->round = $round;

        $this->trick = $trick;

        $this->player = $player;
    }

}

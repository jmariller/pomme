<?php

namespace App\Events;

use App\Jass\Entities\Game;
use App\Jass\Entities\Round;
use App\Jass\Entities\Player;
use App\Jass\Cards\PlayingCard;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class PlayerDrewCardFromHand implements ShouldBroadcast, PlayerDrewCard
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    /**
     * @var Game
     */
    public $game;

    /**
     * @var Round
     */
    public $round;

    /**
     * @var Player
     */
    public $player;

    /**
     * @var PlayingCard
     */
    public $card;

    /**
     * Create a new event instance.
     *
     * @param Player      $player
     * @param PlayingCard $card
     */
    public function __construct(Player $player, PlayingCard $card)
    {
        $this->card = $card;

        $this->game = $player->game;

        $this->round = $this->game->currentRound()->load('scores');

        $this->player = $player;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel("games.{$this->game->id}");
    }

    /**
     * Get the data to broadcast.
     *
     * @return array
     */
    public function broadcastWith()
    {
        return [
            'card' => $this->card,
            'game' => $this->game,
            'round' => $this->round,
            'player' => $this->player()
        ];
    }

    /**
     * Get the player with only necessary information
     *
     * @return Player
     */
    private function player()
    {
        return $this->player
            ->withFakeHand()
            ->makeHidden(['backup', 'discarded', 'melds'])
            ->append(['best_melds_index', 'best_melds_title'])
            ->makeVisible(['best_melds_index', 'best_melds_title']);
    }

}

<?php

namespace App\Events;

use App\Jass\Entities\Game;
use App\Jass\Entities\Round;
use App\Jass\Entities\Trick;
use App\Jass\Entities\Player;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class SelectionRoundCompleted implements ShouldBroadcast, RoundCompleted
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    /**
     * @var Game
     */
    public $game;

    /**
     * @var Round
     */
    public $round;

    /**
     * @var Trick
     */
    public $trick;

    /**
     * @var Player
     */
    public $player;

    /**
     * Create a new event instance.
     *
     * @param Game   $game
     * @param Round  $round
     * @param Trick  $trick
     * @param Player $player
     */
    public function __construct(Game $game, Round $round, Trick $trick, Player $player)
    {
        $this->game = $game;

        $this->round = $round;

        $this->trick = $trick;

        $this->player = $player;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel("player.{$this->player->id}");
    }

}

<?php

namespace App\Providers;

use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        'App\Events\PlayerJoinedGame' => [
            'App\Listeners\PlayerJoinedGame\UpdateGamePlayersCount',
            'App\Listeners\PlayerJoinedGame\StartSelectionRound',
        ],
        'App\Events\PlayerDrewCardFromDeck' => [
            'App\Listeners\PlayerDrewCard\SaveCard',
            'App\Listeners\PlayerDrewCard\FinishTrickForSelectionRound',
            'App\Listeners\PlayerDrewCard\LetPlayersKnowWhoIsNext',
        ],
        'App\Events\PlayersDrewSameRankFromDeck' => [
            'App\Listeners\ClearPlayersCard',
        ],
        'App\Events\SelectionRoundCompleted' => [
            'App\Listeners\RoundCompleted\LetPlayersKnowWhoIsNext',
            'App\Listeners\SelectionRoundCompleted\CreateGameScores',
            'App\Listeners\SelectionRoundCompleted\PrepareFirstRound',
        ],
        'App\Events\NewRoundCreated' => [
            'App\Listeners\NewRoundCreated\CreateRoundScores',
            'App\Listeners\NewRoundCreated\DistributeCards',
            'App\Listeners\Melds\DetectAndSaveMelds',
            'App\Listeners\Melds\FindMeldsWinner',
            'App\Listeners\Melds\SaveScoreMeldsPoints',
            'App\Listeners\NewRoundCreated\LetPlayersKnowThatRoundIsReady',
        ],
        'App\Events\PlayerTookBackup' => [
            'App\Listeners\Melds\DetectAndSaveMelds',
            'App\Listeners\Melds\FindMeldsWinner',
            'App\Listeners\Melds\SaveScoreMeldsPoints',
            'App\Listeners\LetPlayerKnowAboutTheirBackup',
        ],
        'App\Events\PlayerTookTrump' => [
            'App\Listeners\ExchangeSixOfTrump',
            'App\Listeners\Melds\DetectAndSaveMelds',
            'App\Listeners\Melds\FindMeldsWinner',
            'App\Listeners\Melds\SaveScoreMeldsPoints',
            'App\Listeners\LetPlayerKnowAboutTrumpTaken',
        ],
        'App\Events\PlayerDrewCardFromHand' => [
            'App\Listeners\PlayerDrewCard\SaveCard',
            'App\Listeners\PlayerDrewCard\FinishTrickForNormalRound',
            'App\Listeners\PlayerDrewCard\LetPlayersKnowWhoIsNext',
        ],
        'App\Events\NormalTrickCompleted' => [
            'App\Listeners\NormalTrickCompleted\LetPlayersKnowAboutMeldsWinner',
            'App\Listeners\NormalTrickCompleted\UpdateRoundScores',
            'App\Listeners\NormalTrickCompleted\FinishNormalRound',
        ],
        'App\Events\NormalRoundCompleted' => [
            'App\Listeners\RoundCompleted\LetPlayersKnowWhoIsNext',
            'App\Listeners\NormalRoundCompleted\UpdateGameScores',
            'App\Listeners\NormalRoundCompleted\FindGameWinner',
            'App\Listeners\NormalRoundCompleted\PrepareNextRound',
        ],
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();
    }
}

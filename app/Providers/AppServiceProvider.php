<?php

namespace App\Providers;

use App\Jass\Cards\PlayingCard;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultStringLength(191);

        Collection::macro('toTrump', function (PlayingCard $trump) {
            return app('deck')->fromString($this->implode('id', ';'))->applyTrump($trump)->cards();
        });

        Collection::macro('toPlain', function () {
            return app('deck')->fromString($this->implode('id', ';'))->cards()->each->toPlain();
        });

        Collection::macro('toFake', function () {
            return app('deck')->fromString($this->implode('id', ';'))->cards()->each->toFake();
        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind('user', 'App\User');
        $this->app->bind('game', 'App\Jass\Entities\Game');
        $this->app->bind('deck', 'App\Jass\Cards\Deck');
    }
}

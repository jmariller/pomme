<?php

namespace App;

trait Jsonable
{

    /**
     * Convert to array
     *
     * @return array
     */
    public function toArray()
    {
        return get_object_vars($this);
    }

    /**
     * Convert to string
     *
     * @return string
     */
    public function __toString()
    {
        return json_encode($this->toArray());
    }
}

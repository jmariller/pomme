<?php

namespace App\Http\Requests;

use App\Jass\Entities\Game;
use App\Jass\Entities\Player;
use Illuminate\Foundation\Http\FormRequest;

class JoinGameRequest extends FormRequest
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'type' => 'required|in:pomme,jass'
        ];
    }

    /**
     * Sign in and join the game
     *
     * @return Game
     */
    public function boot()
    {
        return $this->signIn()->joinGame($this->type);
    }

    /**
     * Find or create a guest user
     *
     * @return Player
     */
    private function signIn()
    {
        if (!auth()->check()) {
            auth()->login(app('user')->addGuest(), true);
        }

        return auth()->user()->player;
    }

}

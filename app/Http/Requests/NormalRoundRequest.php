<?php

namespace App\Http\Requests;

use App\Jass\Entities\Game;
use App\Jass\Entities\Player;
use Illuminate\Foundation\Http\FormRequest;

class NormalRoundRequest extends FormRequest
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'card_id' => 'required|string|in:' . $this->player()->hand->implode('id', ',')
        ];
    }

    /**
     * Draw a card from the hand
     *
     * @param Game $game
     * @param int  $roundNb
     */
    public function boot(Game $game, int $roundNb)
    {
        $this->abortIfNotInCurrentRound($game, $roundNb);

        $this->player()->drawCardFromHand($this->card_id);
    }

    /**
     * Get the player
     *
     * @return Player
     */
    private function player()
    {
        return auth()->user()->player->fresh();
    }

    /**
     * Abort if not in current round
     *
     * @param Game $game
     * @param int  $roundNb
     */
    private function abortIfNotInCurrentRound(Game $game, int $roundNb)
    {
        abort_if($game->rounds_count != $roundNb, 422, "Round #$roundNb is not the current round!");
    }

}

<?php

namespace App\Http\Requests;

use App\Jass\Entities\Game;
use App\Jass\Entities\Player;
use Illuminate\Foundation\Http\FormRequest;

class SelectionRoundRequest extends FormRequest
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'card_pos' => 'required|integer|between:0,35|not_in:' . $this->cardsDrawnImploded()
        ];
    }

    /**
     * Draw a card from the deck
     *
     * @param Game $game
     */
    public function boot(Game $game)
    {
        $this->abortIfNotSelectionRound($game);

        $deck = $game->round(0)->cards_deck;

        $this->player()->drawCardFromDeck($deck, $this->card_pos);
    }

    /**
     * Get the player
     *
     * @return Player
     */
    private function player()
    {
        return auth()->user()->player;
    }

    /**
     * Get the cards drawn as a comma separated list
     *
     * @return mixed
     */
    private function cardsDrawnImploded()
    {
        return $this->player()->game->round(0)->cards_drawn_indexes->implode(',');
    }

    /**
     * Abort if the selection round is already completed
     *
     * @param Game $game
     */
    private function abortIfNotSelectionRound(Game $game)
    {
        abort_if($game->rounds_count > 0, 422, 'The selection round is already completed!');
    }

}

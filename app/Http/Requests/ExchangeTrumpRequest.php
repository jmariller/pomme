<?php

namespace App\Http\Requests;

use App\Jass\Entities\Player;
use Illuminate\Foundation\Http\FormRequest;

class ExchangeTrumpRequest extends FormRequest
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'dummy' => [
                'required',
                function ($attribute, $value, $fail) {
                    if (!$this->hasSixOfTrump()) {
                        $fail('Player does not have Six of Trump');
                    }
                },
            ]
        ];
    }

    /**
     * Take trump card by discarding 6th of trump
     *
     * @param Player $player
     */
    public function boot(Player $player)
    {
        $player->exchangeTrump($this->trump());
    }

    /**
     * Check if the player has the six of trump
     *
     * @return bool
     */
    private function hasSixOfTrump()
    {
        return $this->player()->hand->toTrump($this->trump())->filter->six_of_trump->count() == 1;
    }

    /**
     * Get the current trump
     *
     * @return mixed
     */
    private function trump()
    {
        return $this->player()->game->currentRound()->trump;
    }

    /**
     * Get the player
     *
     * @return Player
     */
    private function player()
    {
        return auth()->user()->player->fresh();
    }

}

<?php

namespace App\Http\Requests;

use App\Jass\Entities\Player;
use Illuminate\Support\Collection;
use Illuminate\Foundation\Http\FormRequest;

class TakeBackupRequest extends FormRequest
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'discarded' => [
                'required',
                'string',
                function ($attribute, $value, $fail) {
                    if (!$this->isValid($value)) {
                        $fail('Invalid discarded cards');
                    }
                },
            ]
        ];
    }

    /**
     * Take backup cards by exchanging them with 3 cards from player's hand
     *
     * @param Player $player
     */
    public function boot(Player $player)
    {
        $player->takeBackup($this->discarded($this->discarded));
    }

    /**
     * Check that the given discarded cards are valid
     *
     * @param $value
     * @return bool
     */
    private function isValid($value)
    {
        return $this->noneDiscardedYet() && $this->gave3cards($value) && $this->hasCards($value);
    }

    /**
     * Check that player has not discarded any card yet
     *
     * @return bool
     */
    private function noneDiscardedYet()
    {
        return $this->player()->discarded->count() == 0;
    }

    /**
     * Check that player gave exactly 3 cards
     *
     * @param $value
     * @return bool
     */
    private function gave3cards($value)
    {
        return $this->discarded($value)->count() == 3;
    }

    /**
     * Check that player has the cards he wants to discard
     *
     * @param $cards
     * @return bool
     */
    private function hasCards($cards)
    {
        $hand = $this->player()->hand->pluck('id');

        return $this->discarded($cards)->every(function ($cardId) use ($hand) {
            return $hand->contains($cardId);
        });
    }

    /**
     * Get discarded cards as collection
     *
     * @param $cards
     * @return Collection
     */
    private function discarded($cards)
    {
        return collect(explode(';', $cards));
    }

    /**
     * Get the player
     *
     * @return Player
     */
    private function player()
    {
        return auth()->user()->player->fresh();
    }

}

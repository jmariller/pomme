<?php

namespace App\Http\Controllers;

use App\Jass\Entities\Player;
use App\Http\Responses\GameResponse;
use App\Http\Requests\TakeBackupRequest;

class TakeBackupController extends Controller
{

    /**
     * Selection round
     *
     * @param Player               $player
     * @param TakeBackupRequest $request
     * @return GameResponse
     */
    public function store(Player $player, TakeBackupRequest $request)
    {
        $request->boot($player);

        return new GameResponse($player->game);
    }
}

<?php

namespace App\Http\Controllers;

use App\Jass\Entities\Game;
use App\Http\Responses\GameResponse;
use App\Http\Requests\NormalRoundRequest;

class NormalRoundController extends Controller
{

    /**
     * Selection round
     *
     * @param Game               $game
     * @param int                $roundNb
     * @param NormalRoundRequest $request
     * @return GameResponse
     */
    public function store(Game $game, int $roundNb, NormalRoundRequest $request)
    {
        $request->boot($game, $roundNb);

        return new GameResponse($game);
    }

}

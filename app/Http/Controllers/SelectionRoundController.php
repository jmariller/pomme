<?php

namespace App\Http\Controllers;

use App\Jass\Entities\Game;
use App\Http\Responses\GameResponse;
use App\Http\Requests\SelectionRoundRequest;

class SelectionRoundController extends Controller
{

    /**
     * Selection round
     *
     * @param Game                  $game
     * @param SelectionRoundRequest $request
     * @return GameResponse
     */
    public function store(Game $game, SelectionRoundRequest $request)
    {
        $request->boot($game);

        return new GameResponse($game);
    }

}

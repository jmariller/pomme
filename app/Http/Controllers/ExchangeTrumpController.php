<?php

namespace App\Http\Controllers;

use App\Jass\Entities\Player;
use App\Http\Responses\GameResponse;
use App\Http\Requests\ExchangeTrumpRequest;

class ExchangeTrumpController extends Controller
{

    /**
     * Selection round
     *
     * @param Player               $player
     * @param ExchangeTrumpRequest $request
     * @return GameResponse
     */
    public function store(Player $player, ExchangeTrumpRequest $request)
    {
        $request->boot($player);

        return new GameResponse($player->game);
    }
}

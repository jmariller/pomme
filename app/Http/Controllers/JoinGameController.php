<?php

namespace App\Http\Controllers;

use App\Http\Responses\GameResponse;
use App\Http\Requests\JoinGameRequest;

class JoinGameController extends Controller
{

    /**
     * Join a game
     *
     * @param JoinGameRequest $request
     * @return GameResponse
     */
    public function store(JoinGameRequest $request)
    {
        return new GameResponse($request->boot());
    }

}

<?php

namespace App\Http\Responses;

use App\Jsonable;
use App\Jass\Entities\Game;
use App\Jass\Entities\Round;
use App\Jass\Entities\Trick;
use App\Jass\Entities\Player;

class GameResponse
{

    use Jsonable;

    /**
     * @var Game
     */
    public $game;

    /**
     * @var Round
     */
    public $round;

    /**
     * @var Trick
     */
    public $trick;

    /**
     * @var Player
     */
    public $player;

    /**
     * GameResponse constructor.
     *
     * @param Game $game
     */
    public function __construct(Game $game)
    {
        $this->game = $game->fresh()->load('scores');

        $this->round = $this->game->currentRound()->load('scores');

        $this->trick = $this->round->currentTrick();

        $this->player = auth()->user()->player->fresh()->load(['melds' => function($query) {
            $query->whereRoundId($this->round->id);
        }]);
    }

}

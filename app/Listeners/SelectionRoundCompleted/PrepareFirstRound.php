<?php

namespace App\Listeners\SelectionRoundCompleted;

use App\Listeners\BaseListener;
use App\Events\NewRoundCreated;
use App\Events\SelectionRoundCompleted;

class PrepareFirstRound extends BaseListener
{

    /**
     * Handle the event.
     *
     * @param SelectionRoundCompleted $event
     * @return void
     */
    public function handle(SelectionRoundCompleted $event)
    {
        $event->round->finish($event->trick->winner_id);

        $event->game->increment('rounds_count');

        event(
            new NewRoundCreated(
                $event->player, null,
                $event->game,
                $event->game->addRound($event->trick->winner_id)
            )
        );
    }

}

<?php

namespace App\Listeners\SelectionRoundCompleted;

use App\Listeners\BaseListener;
use App\Events\SelectionRoundCompleted;

class CreateGameScores extends BaseListener
{

    /**
     * Handle the event.
     *
     * @param SelectionRoundCompleted $event
     * @return void
     */
    public function handle(SelectionRoundCompleted $event)
    {
        $event->game->players()->each(function ($player) use ($event) {
            $event->game->scores()->create(['player_id' => $player->id]);
        });
    }

}

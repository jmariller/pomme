<?php

namespace App\Listeners;

use App\Events\BackupWasTakenByPlayer;
use App\Events\PlayerTookBackup;

class LetPlayerKnowAboutTheirBackup extends BaseListener
{

    /**
     * Handle the event.
     *
     * @param PlayerTookBackup $event
     * @return void
     */
    public function handle(PlayerTookBackup $event)
    {
        broadcast(new BackupWasTakenByPlayer($event->player));
    }

}

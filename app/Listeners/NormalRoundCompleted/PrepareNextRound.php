<?php

namespace App\Listeners\NormalRoundCompleted;

use App\Listeners\BaseListener;
use App\Events\NewRoundCreated;
use App\Events\NormalRoundCompleted;

class PrepareNextRound extends BaseListener
{

    /**
     * Handle the event.
     *
     * @param NormalRoundCompleted $event
     * @return void
     */
    public function handle(NormalRoundCompleted $event)
    {
        if (!$event->game->fresh()->completed) {
            $event->game->increment('rounds_count');

            event(
                new NewRoundCreated(
                    $event->player, null,
                    $event->game,
                    $event->game->addRound($event->round->nextPlayer()->id)
                )
            );
        }
    }


}

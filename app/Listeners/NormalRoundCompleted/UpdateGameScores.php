<?php

namespace App\Listeners\NormalRoundCompleted;

use App\Listeners\BaseListener;
use App\Jass\JassConfiguration;
use App\Events\NormalRoundCompleted;

class UpdateGameScores extends BaseListener
{

    /**
     * Handle the event.
     *
     * @param NormalRoundCompleted $event
     * @return void
     */
    public function handle(NormalRoundCompleted $event)
    {
        $event->game->scores()->wherePlayerId($event->round->winner_id)->increment('rounds_won');

        if ($this->otherPlayerIsPomme($event->round)) {
            $event->game->scores()->where('player_id', '!=', $event->round->winner_id)->increment('pommes_count');
        }
    }

    /**
     * Check if the other player is pomme
     *
     * @param $round
     * @return bool
     */
    private function otherPlayerIsPomme($round)
    {
        return $round->game_type == JassConfiguration::constant('type_pomme') &&
            $round->scores->where('player_id', '!=', $round->winner_id)->first()->total < JassConfiguration::constant('pomme_threshold');
    }
}

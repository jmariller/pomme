<?php

namespace App\Listeners\NormalRoundCompleted;

use App\Jass\Entities\Game;
use App\Listeners\BaseListener;
use App\Jass\JassConfiguration;
use App\Jass\Entities\GameScore;
use App\Events\NormalRoundCompleted;


class FindGameWinner extends BaseListener
{

    /**
     * Handle the event.
     *
     * @param NormalRoundCompleted $event
     * @return void
     */
    public function handle(NormalRoundCompleted $event)
    {
        if ($event->round->is_finished) {
            $this->findAndSaveWinner($event->game->fresh());
        }
    }

    /**
     * Update the winner
     *
     * @param Game $game
     */
    private function findAndSaveWinner(Game $game)
    {
        $rounds_won = $this->roundsWon($game);
        $too_many_pommes = $this->tooManyPommes($game);

        if ($rounds_won->count() > 0 || $too_many_pommes->count() > 0) {

            $winner_id = $too_many_pommes->count() > 0
                ? $this->playerNotPomme($game)
                : $rounds_won->first()->player_id;

            $game->update([
                'completed' => true,
                'winner_id' => $winner_id
            ]);
        }
    }

    /**
     * The player who is not pomme
     *
     * @param $game
     * @return mixed
     */
    private function playerNotPomme(Game $game)
    {
        return $game->scores()
            ->where('pommes_count', '<', JassConfiguration::constant('pomme_nb_pommes_to_loose'))
            ->first()
            ->player_id;
    }

    /**
     * Find the score with enough rounds won
     *
     * @param Game $game
     * @return mixed
     */
    private function roundsWon(Game $game)
    {
        return $game->scores->filter(function (GameScore $score) {
            return $score->rounds_won == JassConfiguration::constant('pomme_nb_rounds_to_win');
        });
    }

    /**
     * Find score with too many pommes
     *
     * @param Game $game
     * @return mixed
     */
    private function tooManyPommes(Game $game)
    {
        return $game->scores->filter(function (GameScore $score) {
            return $score->pommes_count == JassConfiguration::constant('pomme_nb_pommes_to_loose');
        });
    }

}

<?php

namespace App\Listeners;

use App\Events\PlayerTookTrump;

class ExchangeSixOfTrump extends BaseListener
{

    /**
     * Handle the event.
     *
     * @param PlayerTookTrump $event
     * @return void
     */
    public function handle(PlayerTookTrump $event)
    {
        $event->round->update(['trump' => $event->six_of_trump->id]);
    }

}

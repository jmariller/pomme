<?php

namespace App\Listeners\NewRoundCreated;

use App\Events\NewRoundCreated;
use App\Listeners\BaseListener;

class CreateRoundScores extends BaseListener
{

    /**
     * Handle the event.
     *
     * @param NewRoundCreated $event
     * @return void
     */
    public function handle(NewRoundCreated $event)
    {
        $event->game->players->each->addRoundScores($event->round->id);
    }

}

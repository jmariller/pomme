<?php

namespace App\Listeners\NewRoundCreated;

use App\Jass\Entities\Player;
use App\Events\NewRoundCreated;
use App\Events\NewRoundStarted;
use App\Listeners\BaseListener;

class LetPlayersKnowThatRoundIsReady extends BaseListener
{

    /**
     * Handle the event.
     *
     * @param NewRoundCreated $event
     * @return void
     */
    public function handle(NewRoundCreated $event)
    {
        $current_player_id = auth()->id();

        $event->game->players()->each(function (Player $player) use ($event) {
            auth()->loginUsingId($player->id);

            broadcast(new NewRoundStarted($event->game, $player));
        });

        auth()->loginUsingId($current_player_id);
    }

}

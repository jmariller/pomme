<?php

namespace App\Listeners\PlayerDrewCard;

use App\Events\WhoIsNext;
use App\Events\PlayerDrewCard;
use App\Listeners\BaseListener;

class LetPlayersKnowWhoIsNext extends BaseListener
{

    /**
     * Handle the event.
     *
     * @param PlayerDrewCard $event
     * @return void
     */
    public function handle(PlayerDrewCard $event)
    {
        broadcast(new WhoIsNext($event->game))->toOthers();
    }

}

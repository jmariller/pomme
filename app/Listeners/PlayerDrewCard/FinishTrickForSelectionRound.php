<?php

namespace App\Listeners\PlayerDrewCard;

use App\Listeners\BaseListener;
use App\Events\PlayerDrewCardFromDeck;
use App\Events\SelectionRoundCompleted;
use App\Events\PlayersDrewSameRankFromDeck;

class FinishTrickForSelectionRound extends BaseListener
{

    /**
     * Handle the event.
     *
     * @param  PlayerDrewCardFromDeck  $event
     * @return void
     */
    public function handle(PlayerDrewCardFromDeck $event)
    {
        $trick = $event->round->currentTrick();

        if ($trick->cards_count == $event->game->nb_players) {
            $trick->finish();

            if ($trick->winner_id != null) {
                $event->game->update(['who_is_next' => $trick->winner_id]);

                broadcast(new SelectionRoundCompleted($event->game, $event->round, $trick, $event->player));
            } else {
                broadcast(new PlayersDrewSameRankFromDeck($event->game, $event->round, $trick, $event->player));
            }
        }
    }

}

<?php

namespace App\Listeners\PlayerDrewCard;

use App\Listeners\BaseListener;
use App\Events\NormalTrickCompleted;
use App\Events\PlayerDrewCardFromHand;

class FinishTrickForNormalRound extends BaseListener
{

    /**
     * Handle the event.
     *
     * @param  PlayerDrewCardFromHand  $event
     * @return void
     */
    public function handle(PlayerDrewCardFromHand $event)
    {
        $trick = $event->round->currentTrick();

        if ($trick->cards_count == $event->game->nb_players) {
            $trick->finish();

            $event->game->update(['who_is_next' => $trick->winner_id]);

            event(new NormalTrickCompleted($event->game->fresh(), $event->round, $trick, $event->player));

            $event->game->players()->update(['card' => null]);
        }
    }

}

<?php

namespace App\Listeners\PlayerDrewCard;

use App\Events\PlayerDrewCard;
use App\Listeners\BaseListener;

class SaveCard extends BaseListener
{

    /**
     * Handle the event.
     *
     * @param  PlayerDrewCard $event
     * @return void
     */
    public function handle(PlayerDrewCard $event)
    {
        $round = $event->round;
        $trick = $round->currentTrick();
        $tricks_count = $round->tricks_count;

        if ($trick === null || $trick->completed) {
            $trick = $round->addTrick($event->player->id);
            $tricks_count++;
        }

        $trick->addCard($event->player->id, $event->card->id);
        $trick->increment('cards_count');

        $round->update([
            'tricks_count' => $tricks_count,
            'cards_drawn' => $round->cards_drawn->push($event->card)
        ]);

        $event->game->update([
            'who_is_next' => $event->game->nextInLine($event->player->id)
        ]);
    }

}

<?php

namespace App\Listeners;

use App\Jass\JassConfiguration;

abstract class BaseListener
{

    /**
     * Is the current round part of the Pomme?
     *
     * @param $round
     * @return bool
     */
    protected function isPomme($round)
    {
        return $round->game_type == JassConfiguration::constant('type_pomme');
    }

}

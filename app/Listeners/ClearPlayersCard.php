<?php

namespace App\Listeners;

use App\Events\PlayersDrewSameRankFromDeck;

class ClearPlayersCard extends BaseListener
{

    /**
     * Handle the event.
     *
     * @param PlayersDrewSameRankFromDeck $event
     * @return void
     */
    public function handle(PlayersDrewSameRankFromDeck $event)
    {
        $event->game->players()->update(['card' => null]);
    }

}

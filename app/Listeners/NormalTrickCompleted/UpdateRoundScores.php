<?php

namespace App\Listeners\NormalTrickCompleted;

use App\Listeners\BaseListener;
use App\Jass\JassConfiguration;
use App\Events\NormalTrickCompleted;

class UpdateRoundScores extends BaseListener
{

    /**
     * Handle the event.
     *
     * @param NormalTrickCompleted $event
     * @return void
     */
    public function handle(NormalTrickCompleted $event)
    {
        $this->updateScore($event->round, $event->trick);
    }

    /**
     * Update scores
     *
     * @param $round
     * @param $trick
     */
    private function updateScore($round, $trick)
    {
        $score = $round->scores()->wherePlayerId($trick->winner_id)->first();

        if ($round->is_finished) {
            $trick->points += JassConfiguration::constant('points_for_cinq_de_der');
        }

        $points = $score->points + $trick->points;

        $total = $points + $score->melds;

        if ($this->isPomme($round) && $this->isBelowThreshold($points)) {
            $total -= $score->melds;
        }

        $score->update([
            'points' => $points,
            'total' => $total
        ]);
    }

    /**
     * Are given points below Pomme's threshold?
     *
     * @param $points
     * @return bool
     */
    private function isBelowThreshold($points)
    {
        return $points < JassConfiguration::constant('pomme_threshold');
    }

}

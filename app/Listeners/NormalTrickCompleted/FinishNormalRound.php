<?php

namespace App\Listeners\NormalTrickCompleted;

use App\Listeners\BaseListener;
use App\Events\NormalRoundCompleted;
use App\Events\NormalTrickCompleted;

class FinishNormalRound extends BaseListener
{

    /**
     * Handle the event.
     *
     * @param NormalTrickCompleted $event
     * @return void
     */
    public function handle(NormalTrickCompleted $event)
    {
        if ($event->round->is_finished && !$event->game->completed) {
            $this->updateWhoIsNext($event);

            $round = $event->round->fresh();

            $round->finish();

            broadcast(new NormalRoundCompleted($event->game->fresh(), $round, $event->trick, $event->player));
        }
    }

    /**
     * Next player in line is next
     *
     * @param NormalTrickCompleted $event
     */
    private function updateWhoIsNext(NormalTrickCompleted $event)
    {
        $event->game->update([
            'who_is_next' => $event->game->nextInLine($event->round->first_player_id)
        ]);
    }


}

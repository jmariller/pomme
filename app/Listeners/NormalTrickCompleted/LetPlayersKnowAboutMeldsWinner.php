<?php

namespace App\Listeners\NormalTrickCompleted;

use App\Events\PlayerWonMelds;
use App\Jass\Entities\Player;
use App\Listeners\BaseListener;
use App\Events\NormalTrickCompleted;

class LetPlayersKnowAboutMeldsWinner extends BaseListener
{

    /**
     * Handle the event.
     *
     * @param NormalTrickCompleted $event
     * @return void
     */
    public function handle(NormalTrickCompleted $event)
    {
        if ($this->isFirstTrick($event)) {
            $current_player_id = auth()->id();

            $event->game->players()->each(function (Player $player) use ($event) {
                if ($this->isMeldsWinner($event->round->melds_winner_id, $player->id)) {
                    auth()->loginUsingId($player->id);

                    broadcast(new PlayerWonMelds($event->game, $player));
                }
            });

            auth()->loginUsingId($current_player_id);
        }

    }

    /**
     * Determine whether we are in the first trick
     *
     * @param $event
     * @return bool
     */
    private function isFirstTrick(NormalTrickCompleted $event)
    {
        return $event->trick->trick == 1;
    }

    /**
     * Determine whether the given player is the melds winner
     *
     * @param $meldWinnerId
     * @param $playerId
     * @return bool
     */
    private function isMeldsWinner($meldWinnerId, $playerId)
    {
        return $meldWinnerId != $playerId;
    }

}

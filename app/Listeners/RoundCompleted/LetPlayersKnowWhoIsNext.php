<?php

namespace App\Listeners\RoundCompleted;

use App\Events\WhoIsNext;
use App\Events\RoundCompleted;
use App\Listeners\BaseListener;

class LetPlayersKnowWhoIsNext extends BaseListener
{

    /**
     * Handle the event.
     *
     * @param RoundCompleted $event
     * @return void
     */
    public function handle(RoundCompleted $event)
    {
        broadcast(new WhoIsNext($event->game))->toOthers();
    }

}

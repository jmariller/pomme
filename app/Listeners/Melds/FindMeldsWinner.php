<?php

namespace App\Listeners\Melds;

use App\Events\InGameEvent;
use App\Jass\Melds\MeldsWinner;
use App\Listeners\BaseListener;

class FindMeldsWinner extends BaseListener
{

    /**
     * Handle the event.
     *
     * @param InGameEvent $event
     * @return void
     */
    public function handle(InGameEvent $event)
    {
        $event->round->update(['melds_winner_id' => MeldsWinner::winner($event->round)]);
    }
}

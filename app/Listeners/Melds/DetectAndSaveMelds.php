<?php

namespace App\Listeners\Melds;

use App\Events\InGameEvent;
use App\Jass\Entities\Round;
use App\Jass\Entities\Player;
use App\Listeners\BaseListener;
use App\Jass\Melds\MeldsWinner;
use App\Jass\Melds\MeldsDetector;
use Illuminate\Support\Collection;

class DetectAndSaveMelds extends BaseListener
{

    /**
     * Handle the event.
     *
     * @param InGameEvent $event
     * @return void
     */
    public function handle(InGameEvent $event)
    {
        $round = $event->round;

        $event->game->players()->each(function (Player $player) use ($round) {
            $player->melds()->whereRoundId($round->id)->delete();

            $this->saveMelds($player, $round);
        });
    }

    /**
     * Create melds for a given player on current round
     *
     * @param Player $player
     * @param Round  $round
     */
    private function saveMelds(Player $player, Round $round)
    {
        $hand = $player->hand;

        $melds = [];

        if (MeldsDetector::hasRoyalCouple($hand)) {
            $melds['royal_couple'] = true;
            $melds['royal_couple_alone'] = MeldsDetector::hasRoyalCoupleAlone($hand);
        }

        if (MeldsDetector::hasAnyQuartet($hand)) {
            $melds = array_merge($melds, $this->quartets($hand));
        }

        $sequences = MeldsDetector::sequences($hand->toPlain());
        if ($sequences->count() > 0) {
            $melds = array_merge($melds, $this->sequences($sequences));
        }

        if (count($melds)) {
            $meld = $player->melds()->create(
                array_merge($melds, ['round_id' => $round->id])
            );

            $this->saveSequences($sequences, $meld, $round->trump);

            $meld->update([
                'best_index' => MeldsWinner::bestIndex($meld),
                'points' => MeldsWinner::totalPoints($meld)
            ]);
        }
    }

    /**
     * Find quartets
     *
     * @param Collection $hand
     * @return array
     */
    private function quartets(Collection $hand)
    {
        return [
            'four_jacks' => MeldsDetector::hasQuartet($hand, 'J'),
            'four_nines' => MeldsDetector::hasQuartet($hand, '9'),
            'four_aces' => MeldsDetector::hasQuartet($hand, 'A'),
            'four_kings' => MeldsDetector::hasQuartet($hand, 'K'),
            'four_queens' => MeldsDetector::hasQuartet($hand, 'Q'),
            'four_tens' => MeldsDetector::hasQuartet($hand, '10'),
        ];
    }

    /**
     * Find list of sequences
     *
     * @param Collection $sequences
     * @return array
     */
    private function sequences(Collection $sequences)
    {
        return [
            'sequence_9' => MeldsDetector::countSequencesOf($sequences, 9),
            'sequence_8' => MeldsDetector::countSequencesOf($sequences, 8),
            'sequence_7' => MeldsDetector::countSequencesOf($sequences, 7),
            'sequence_6' => MeldsDetector::countSequencesOf($sequences, 6),
            'sequence_5' => MeldsDetector::countSequencesOf($sequences, 5),
            'sequence_4' => MeldsDetector::countSequencesOf($sequences, 4),
            'sequence_3' => MeldsDetector::countSequencesOf($sequences, 3),
        ];
    }

    /**
     * Save all sequences found
     *
     * @param Collection $sequences
     * @param            $meld
     * @param            $trump
     */
    private function saveSequences(Collection $sequences, $meld, $trump)
    {
        $sequences->each(function (Collection $sequence) use ($meld, $trump) {
            $meld->sequences()->create([
                'sequence' => $sequence->implode('id', ';'),
                'length' => $sequence->count(),
                'highest_rank_index' => $sequence->last()->rank_index,
                'points' => MeldsDetector::sequencePoints($sequence),
                'trump' => $trump->id,
                'has_royal_couple' => MeldsDetector::hasRoyalCouple($sequence),
            ]);
        });
    }

}

<?php

namespace App\Listeners\Melds;

use App\Events\InGameEvent;
use App\Listeners\BaseListener;

class SaveScoreMeldsPoints extends BaseListener
{

    /**
     * Handle the event.
     *
     * @param InGameEvent $event
     * @return void
     */
    public function handle(InGameEvent $event)
    {
        $event->round->scores()->update(['melds' => 0]);

        if ($this->hasMeldWinner($event->round)) {
            $event->round->scores()->update(['melds' => 0]);

            $this->scoreForMeldWinner($event->round)->update([
                'melds' => $this->meldWinner($event->round)->points
            ]);
        }
    }

    /**
     * Get the score for the meld winner
     *
     * @param $round
     * @return mixed
     */
    private function scoreForMeldWinner($round)
    {
        return $round->scores()->wherePlayerId($round->melds_winner_id)->first();
    }

    /**
     * Get the meld winner
     *
     * @param $round
     * @return mixed
     */
    private function meldWinner($round)
    {
        return $round->melds()->wherePlayerId($round->melds_winner_id)->first();
    }

    /**
     * Is there a meld winner?
     *
     * @param $round
     * @return bool
     */
    private function hasMeldWinner($round)
    {
        return $round->melds_winner_id != null;
    }
}

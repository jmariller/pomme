<?php

namespace App\Listeners\PlayerJoinedGame;

use App\Listeners\BaseListener;
use App\Events\PlayerJoinedGame;

class StartSelectionRound extends BaseListener
{

    /**
     * Handle the event.
     *
     * @param PlayerJoinedGame $event
     * @return void
     */
    public function handle(PlayerJoinedGame $event)
    {
        if ($event->game->players_count === $event->game->nb_players) {
            $event->game->addRound();
        }
    }

}

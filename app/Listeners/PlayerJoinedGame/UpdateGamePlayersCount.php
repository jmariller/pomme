<?php

namespace App\Listeners\PlayerJoinedGame;

use App\Listeners\BaseListener;
use App\Events\PlayerJoinedGame;

class UpdateGamePlayersCount extends BaseListener
{

    /**
     * Handle the event.
     *
     * @param PlayerJoinedGame $event
     * @return void
     */
    public function handle(PlayerJoinedGame $event)
    {
        $attributes = ['players_count' => $event->game->players()->count()];

        if ($attributes['players_count'] == 1) {
            $attributes['who_is_next'] = $event->player->id;
        }

        $event->game->update($attributes);
    }

}

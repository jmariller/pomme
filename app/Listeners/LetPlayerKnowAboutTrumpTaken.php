<?php

namespace App\Listeners;

use App\Events\PlayerTookTrump;
use App\Events\TrumpWasTakenByPlayer;

class LetPlayerKnowAboutTrumpTaken extends BaseListener
{

    /**
     * Handle the event.
     *
     * @param PlayerTookTrump $event
     * @return void
     */
    public function handle(PlayerTookTrump $event)
    {
        broadcast(new TrumpWasTakenByPlayer($event->player));
    }

}

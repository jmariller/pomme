let mix = require('laravel-mix');

mix.js('resources/js/pomme.js', 'public/js/pomme.js')

    .sass('resources/sass/pomme.scss', 'public/css')
    .options({
        processCssUrls: false
    });

if (process.env.NODE_ENV !== 'test') {
    mix.extract();
}

if (mix.inProduction()) {
    mix.version();
}
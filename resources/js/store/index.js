import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);

import getters from "./getters";
import mutations from './mutations';
import actions from './actions';

const debug = process.env.NODE_ENV !== 'production';

export default new Vuex.Store({
    state: {
        player: {
            hand: [],
            melds: []
        },
        backup: [],
        discarded: [],
        game: {
            other_players: [],
            scores: []
        },
        round: {},
        previous_round: {},
        trick: {},
        deck: {
            cards: []
        },
        animation: false
    },

    getters,

    mutations,

    actions,

    strict: debug
});
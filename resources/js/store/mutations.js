export default {

    /**
     * Update all
     *
     * @param state
     * @param data
     */
    updateAll(state, data) {
        state.player = data.player;
        state.player.melds = data.player.melds;
        state.game = data.game;
        state.round = data.round;
        state.trick = data.trick;
    },

    /**
     * Update the game
     *
     * @param state
     * @param game
     */
    updateGame(state, game) {
        state.game = game;
    },

    /**
     * Update the round
     *
     * @param state
     * @param round
     */
    updateRound(state, round) {
        state.round = round;
    },

    /**
     * Update the previous round - used for showing round results
     *
     * @param state
     * @param previousRound
     */
    updatePreviousRound(state, previousRound) {
        state.previous_round = _.cloneDeep(previousRound);
    },

    /**
     * Update the trick
     *
     * @param state
     * @param trick
     */
    updateTrick(state, trick) {
        state.trick = trick;
    },

    /**
     * Update the player
     *
     * @param state
     * @param player
     */
    updatePlayer(state, player) {
        state.player = player;
    },

    /**
     * Update the player's melds
     *
     * @param state
     * @param melds
     */
    updatePlayerMelds(state, melds) {
        state.player.melds = melds;
    },

    /**
     * Update the player's hand
     *
     * @param state
     * @param hand
     */
    updatePlayerHand(state, hand) {
        state.player.hand = hand;
    },

    /**
     * Update the player's backup
     *
     * @param state
     * @param backup
     */
    updatePlayerBackup(state, backup) {
        state.backup = backup;
    },

    /**
     * Update the player's discarded cards
     *
     * @param state
     * @param discarded
     */
    updatePlayerDiscarded(state, discarded) {
        state.player.discarded = discarded;
    },

    /**
     * Update the player's card
     *
     * @param state
     * @param card
     */
    updatePlayerCard(state, card) {
        state.player.card = card;
    },

    /**
     * Update other player's card
     *
     * @param state
     * @param card
     */
    updateOtherPlayerCard(state, card) {
        state.game.other_players[0].card = card;
    },

    /**
     * Update other player's hand
     *
     * @param state
     * @param hand
     */
    updateOtherPlayerHand(state, hand) {
        state.game.other_players[0].hand = hand;
    },

    /**
     * Update other player's melds
     *
     * @param state
     * @param melds
     */
    updateOtherPlayerMelds(state, melds) {
        state.game.other_players[0].melds = melds;
    },

    /**
     * Update who is next
     *
     * @param state
     * @param whoIsNext
     */
    updateWhoIsNext(state, whoIsNext) {
        state.game.who_is_next = whoIsNext;
    },

    /**
     * Update scores
     *
     * @param state
     * @param data
     */
    updateScores(state, data) {
        state.game.scores = data.game.scores;
        state.round.scores = data.round.scores;
    },

    /**
     * Update the deck of cards
     *
     * @param state
     * @param cards
     */
    updateDeck(state, cards) {
        state.deck.cards = cards;
    },

    /**
     * Clear the carpet of any cards
     *
     * @param state
     */
    clearCarpet(state) {
        state.player.card = null;
        state.game.other_players.forEach(player => player.card = null);
    },

    /**
     * Add another player
     *
     * @param state
     * @param player
     */
    addPlayer(state, player) {
        state.game.other_players.push(player);
    },

    /**
     * Start the animation for drawing a deck card
     *
     * @param state
     * @param cardPos
     */
    startDrawFromDeck(state, cardPos) {
        state.deck.cards[cardPos].animated = true;
    },

    /**
     * Stop the animation for drawing a deck card
     *
     * @param state
     * @param cardPos
     */
    stopDrawFromDeck(state, cardPos) {
        state.deck.cards[cardPos].disabled = true;
        state.deck.cards[cardPos].animated = false;
    },

    /**
     * Reset the deck of cards
     *
     * @param state
     */
    resetDeck(state) {
        state.deck.cards.forEach((value, index, cards) => {
            cards[index].disabled = true;
            cards[index].animated = false;
        });
    },

    /**
     * Draw the Six of Trump
     *
     * @param state
     * @param sixOfTrump
     */
    drawSixOfTrump(state, sixOfTrump) {
        state.round.trump = sixOfTrump;
    },

    /**
     * Add a card to discarded list
     *
     * @param state
     * @param card
     */
    discardCard(state, card) {
        state.discarded.push(card);
    },

    /**
     * Remove a card from discarded list
     *
     * @param state
     * @param cardIndex
     */
    rollbackDiscarded(state, cardIndex) {
        state.discarded.splice(cardIndex, 1);
    },

    /**
     * Remove all cards from discarded list
     *
     * @param state
     */
    emptyDiscarded(state) {
        state.discarded = [];
    },

    /**
     * Remove all cards from backup cards list
     *
     * @param state
     */
    emptyBackup(state) {
        state.backup = [];
    },

    /**
     * Start an animation
     *
     * @param state
     */
    startAnimation(state) {
        state.animation = true;
    },

    /**
     * Stop an animation
     *
     * @param state
     */
    stopAnimation(state) {
        state.animation = false;
    }

};

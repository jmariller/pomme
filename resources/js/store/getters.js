import CONSTANTS from "./constants";

import player from "../api/player";
import score from "../api/score"

export default {

    /**
     * Get the opponent
     *
     * @param state
     * @returns {null}
     */
    opponent: state => {
        return state.game.other_players[0];
    },

    /**
     * Check if it is the player's turn
     *
     * @param state
     * @returns {boolean}
     */
    canPlay: state => {
        return state.player.id === state.game.who_is_next;
    },

    /**
     * Check if a player can draw a card
     *
     * @param state
     * @returns {function(*=, *=): (*|boolean)}
     */
    canDrawCard: state => (myCard, otherCard) => {
        return player.canDrawCard(state.player.hand, myCard, otherCard);
    },

    /**
     * Check if a player has melds
     *
     * @returns {*}
     */
    hasMelds: () => (player) => {
        if (player == null || player.melds == null) return false;

        return player.melds.length > 0;
    },

    /**
     * Check that enough cards have been discarded before taking backup cards
     *
     * @param state
     * @returns {boolean}
     */
    discardedEnoughCards: state => {
        return state.discarded.length === CONSTANTS.CARDS_IN_BACKUP;
    },

    /**
     * Retrieve the index of a discarded card
     *
     * @param state
     * @returns {function(*): number}
     */
    discardedIndex: state => (someCard) => {
        return state.discarded.findIndex(card => someCard.id === card.id);
    },

    /**
     * Check if the game can start
     *
     * @param state
     * @returns {boolean}
     */
    canStart: state => {
        return state.game.nb_players === state.game.other_players.length + 1;
    },

    /**
     * Determine if we are in the selection round
     *
     * @param state
     * @returns {boolean}
     */
    isSelectionRound: state => {
        return state.round.round == null || state.round.round === 0;
    },

    /**
     * Determine if we are in the first trick of the round
     *
     * @param state
     * @returns {boolean}
     */
    isFirstTrick: state => {
        return state.trick == null || (state.trick.trick === 1 && !state.trick.completed);
    },

    /**
     * Determine if the carpet is full, e.g. if all cards have been drawn
     *
     * @param state
     * @returns {null|*|boolean}
     */
    isCarpetFull: state => {
        return state.player.card && state.game.other_players.every(player => player.card);
    },

    /**
     * Determine the CSS identifier for a player's card
     *
     * @returns {function(*, *): string}
     */
    cardCssId: () => (playerId, cardId) => {
        return player.cardCssId(playerId, cardId);
    },

    /**
     * Determine if the player received backup cards
     *
     * @param state
     * @returns {boolean}
     */
    gotBackupCards: state => {
        return state.backup.length === CONSTANTS.CARDS_IN_BACKUP;
    },

    /**
     * Fetch the Six of Trump from the player hand
     *
     * @param state
     * @returns {*}
     */
    sixOfTrump: state => {
        return player.sixOfTrump(state.player.hand, state.round.trump);
    },

    /**
     * Get the score for a given player
     *
     * @param state
     * @returns {function(*=, *=): (*|{rounds_won, pommes_count})}
     */
    score: state => (playerId) => {
        return score.score(state.game.scores, playerId);
    },

    /**
     * Get round points for a given player and round scores
     *
     * @returns {function(*=): *}
     */
    points: () => (scores, playerId) => {
        return score.points(scores, playerId);
    },

    /**
     * Get melds points for a given player and round scores
     *
     * @returns {function(*=, *=): *}
     */
    melds: () => (scores, playerId) => {
        return score.melds(scores, playerId);
    },

    /**
     * Get total points for a given player and round scores
     *
     * @returns {function(*=, *=): *}
     */
    total: () => (scores, playerId) => {
        return score.total(scores, playerId);
    }

};

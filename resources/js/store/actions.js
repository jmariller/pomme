import animationApi from "../api/animation";
import deckApi from "../api/deck";
import gameApi from "../api/game";
import playerApi from "../api/player";
import scoreApi from "../api/score";

export default {

    /**
     * Join a game
     *
     * @param commit
     * @param dispatch
     * @param getters
     * @param type
     */
    async join({commit, dispatch, getters}, type) {
        let {data} = await gameApi.join(type);

        commit('updateAll', data);

        if (!getters.isSelectionRound) {
            commit('updateOtherPlayerHand', playerApi.fakeHand(data.game.other_players[0].nb_cards));
        }

        if (getters.isSelectionRound) commit('updateDeck', deckApi.init(data.round.cards_drawn_indexes));

        dispatch('_initListeners', data);
    },

    /**
     * Update the deck of cards
     *
     * @param commit
     * @param deck
     */
    updateDeck({commit}, deck) {
        commit('updateDeck', deck);
    },

    /**
     * Draw a card from the deck
     *
     * @param state
     * @param commit
     * @param cardPos
     */
    async drawFromDeck({state, commit}, cardPos) {
        commit('updateWhoIsNext', -1);
        commit('startDrawFromDeck', cardPos);

        await animationApi.drawFromDeck(cardPos, state.player.id);

        let {data} = await playerApi.drawCardFromDeck(state.game.id, cardPos);

        if (!state.player.card) commit('updatePlayerCard', data.player.card);

        commit('stopDrawFromDeck', cardPos);
        commit('updateWhoIsNext', data.game.who_is_next);
    },

    /**
     * Another player drew a card from the deck
     *
     * @param commit
     * @param data
     */
    async otherDrewFromDeck({commit}, data) {
        commit('startDrawFromDeck', data.card_pos);
        commit('startAnimation');

        await animationApi.drawFromDeck(data.card_pos, data.player.id);

        commit('stopDrawFromDeck', data.card_pos);
        commit('updateOtherPlayerCard', data.player.card);
        commit('updateRound', data.round);
        commit('updateTrick', data.trick);
        commit('stopAnimation');
    },

    /**
     * Draw the Six of Trump
     *
     * @param state
     * @param getters
     * @param commit
     */
    drawSixOfTrump({state, getters, commit}) {
        commit('drawSixOfTrump', getters.sixOfTrump);
    },

    /**
     * Exchange the six of trump with the current trump
     *
     * @param state
     * @param commit
     */
    async takeTrump({state, commit}) {
        let {data} = await playerApi.exchangeSixOfTrump(state.player.id);

        commit('updatePlayerHand', data.player.hand);
    },

    /**
     * Add card to discarded list
     *
     * @param commit
     * @param card
     */
    discardCard({commit}, card) {
        commit('discardCard', card);
    },

    /**
     * Add card to discarded list
     *
     * @param commit
     * @param cardIndex
     */
    rollbackDiscarded({commit}, cardIndex) {
        commit('rollbackDiscarded', cardIndex);
    },

    /**
     * Empty discarded list
     *
     * @param commit
     */
    emptyDiscarded({commit}) {
        commit('emptyDiscarded');
    },

    /**
     * Exchange the six of trump with the current trump
     *
     * @param state
     * @param commit
     */
    takeBackup({state, commit}) {
        playerApi.takeBackup(state.player.id, state.discarded.map((card) => card.id).join(';'));
    },

    /**
     * Empty backup cards list
     *
     * @param commit
     */
    emptyBackup({commit}) {
        commit('emptyBackup');
    },

    /**
     * Draw a card from hand
     *
     * @param state
     * @param commit
     * @param dispatch
     * @param cardId
     */
    async drawFromHand({state, commit, dispatch}, cardId) {
        commit('updateWhoIsNext', -1);

        await animationApi.drawFromHand(cardId, state.player.id);

        commit('updatePlayerCard', playerApi.cardFromHandById(state.player.hand, cardId));
        commit('updatePlayerHand', playerApi.handWithoutCard(state.player.hand, cardId));

        let {data} = await playerApi.drawCardFromHand(state.game.id, state.round.round, cardId);

        commit('updateWhoIsNext', data.game.who_is_next);
        commit('updateRound', data.round);
        commit('updateTrick', data.trick);
        commit('updateScores', data);

        dispatch('clearCarpet', data);
    },

    /**
     * Another player drew a card from their hand
     *
     * @param state
     * @param commit
     * @param getters
     * @param data
     */
    async otherDrewFromHand({state, commit, getters}, data) {
        if ((!state.trick || state.trick.trick === 1) && data.response.player.best_melds_title) {
            playerApi.showBestMeldTitle(getters.opponent.id, data.response.player.best_melds_title);
        }

        commit('startAnimation');

        await animationApi.drawFromHand(data.card_id, data.response.player.id);

        commit('updateOtherPlayerCard', data.response.card);
        commit('updateOtherPlayerHand', playerApi.handWithoutCard(getters.opponent.hand, data.card_id));
        commit('stopAnimation');
    },

    /**
     * Clear the carpet
     *
     * @param state
     * @param commit
     * @param dispatch
     * @param data
     */
    clearCarpet({state, commit, dispatch}, data) {
        let trick_completed = data.trick && data.trick.completed;

        if (trick_completed && state.animation) {
            setTimeout(() => dispatch('clearCarpet', data), 500);
        } else if (trick_completed) {
            if (!data.trick.winner_id) return setTimeout(() => commit('clearCarpet'), 500);

            if (data.round.round === 0) commit('resetDeck');

            dispatch('giveCardsToWinner', data);
        }
    },

    /**
     * Give cards to winner
     *
     * @param commit
     * @param getters
     * @param data
     */
    giveCardsToWinner({commit, getters}, data) {
        setTimeout(async () => {
            await animationApi.giveCardsToWinner(data.trick.winner_id);

            setTimeout(() => {
                if (data.round.round > 0) playerApi.hideBestMeldTitle(getters.opponent.id);

                commit('clearCarpet');

                animationApi.reset();
            }, 500);
        }, 500);
    },

    /**
     * Start a new round after any on-going animation is completed
     *
     * @param state
     * @param commit
     * @param dispatch
     * @param getters
     * @param data
     * @returns {number}
     */
    startNewRound({state, commit, dispatch, getters}, data) {
        if (state.animation) return setTimeout(() => dispatch('startNewRound', data), 1000);

        if (!getters.isSelectionRound) scoreApi.showLastRoundResults();

        setTimeout(() => {
            commit('updateAll', data);
            commit('updateOtherPlayerHand', playerApi.fakeHand(data.game.other_players[0].nb_cards));
        }, 1000);
    },

    /**
     * Initialize listeners
     *
     * @param state
     * @param commit
     * @param dispatch
     * @param getters
     * @param data
     * @private
     */
    _initListeners({state, commit, dispatch, getters}, data) {
        gameApi.waitForOthers(`games.${data.game.id}`, player => {
            commit('addPlayer', player)
        });

        gameApi.waitForCardDrawnFromDeck(`games.${data.game.id}`, response => {
            dispatch('otherDrewFromDeck', response);
        });

        gameApi.waitForSameRankDrawnFromDeck(`player.${data.player.id}`, response => {
            commit('updatePlayer', response.player);

            dispatch('clearCarpet', response);
        });

        gameApi.waitForSelectionRoundCompleted(`player.${data.player.id}`, response => {
            commit('updatePlayer', response.player);

            dispatch('clearCarpet', response);
        });

        gameApi.waitForCardDrawnFromHand(`games.${data.game.id}`, response => {
            dispatch('otherDrewFromHand', {
                card_id: getters.opponent.hand[_.random(response.player.nb_cards)].id,
                response: response
            });
        });

        gameApi.waitForPlayerWonMelds(`player.${data.player.id}`, response => {
            commit('updateOtherPlayerMelds', response.game.other_players[0].melds);
        });

        gameApi.waitForWhoIsNext(`games.${data.game.id}`, response => {
            if (state.round.round !== response.round.round) commit('updatePreviousRound', state.round);

            commit('updateTrick', response.trick);
            commit('updateWhoIsNext', response.game.who_is_next);
            commit('updateScores', response);

            dispatch('clearCarpet', response);
        });

        gameApi.waitForNormalRoundCompleted(`player.${data.player.id}`, response => {
            commit('updatePreviousRound', response.round);

            dispatch('clearCarpet', response);
        });

        gameApi.waitForNewRound(`player.${data.player.id}`, response => {
            dispatch('startNewRound', response);
        });

        gameApi.waitForBackupTaken(`player.${data.player.id}`, response => {
            commit('updatePlayerHand', response.player.hand);
            commit('updatePlayerDiscarded', response.player.discarded);
            commit('updatePlayerBackup', response.player.backup);
            commit('updatePlayerMelds', response.player.melds);
        });

        gameApi.waitForTookTrump(`games.${data.game.id}`, response => {
            commit('drawSixOfTrump', response.six_of_trump);
        });

        gameApi.waitForTrumpTaken(`player.${data.player.id}`, response => {
            commit('updatePlayerMelds', response.player.melds);
        });
    }

};

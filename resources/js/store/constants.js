export default {

    GAME_TYPES: {
        POMME: 'pomme',
        JASS: 'jass'
    },

    SUITS: [
        {code: 'H', name: 'Hearts'},
        {code: 'S', name: 'Spades'},
        {code: 'D', name: 'Diamonds'},
        {code: 'C', name: 'Clubs'},
    ],

    ROUNDS_TO_WIN: 7,
    POMMES_TO_LOSE: 3,
    POMME_THRESHOLD: 21,

    CARDS_IN_DECK: 36,
    CARDS_IN_HAND: 9,
    CARDS_IN_BACKUP: 3,

    MELDS: [
        {'code': 'four_jacks', 'rank': 'J', 'points': 200},
        {'code': 'four_nines', 'rank': '9', 'points': 150},
        {'code': 'four_aces', 'rank': 'A', 'points': 100},
        {'code': 'four_kings', 'rank': 'K', 'points': 100},
        {'code': 'four_queens', 'rank': 'Q', 'points': 100},
        {'code': 'four_tens', 'rank': '10', 'points': 100},
        {'code': 'royal_couple', 'rank': '', 'points': 20}
    ],

};
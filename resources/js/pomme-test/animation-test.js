require('../bootstrap');

import anime from 'animejs';
import Vue from 'vue';

new Vue({
    el: '#test-animation',
    data: {
        animate: false,
        target: null
    },
    methods: {
        enter: function (el, done) {
            let delta = {
                x: this.$target.offset().left - $(el).offset().left,
                y: this.$target.offset().top - $(el).offset().top,
            };

            anime({
                targets: el,
                translateX: delta.x,
                translateY: delta.y,
                easing: 'easeOutCubic',
                complete: done
            });
        },
        leave: function (el, done) {
            anime({
                targets: el,
                translateX: 0,
                translateY: 0,
                easing: 'easeOutCubic',
                complete: done
            });
        }
    },

    mounted() {
        this.$target = $('#target');
    }
});

// import anime from 'animejs';
//
// let $card = $('#animation');
//
// anime({
//     targets: '#animation',
//     translateY: [
//         { value: $(window).height() - $card.height() - $card.offset().top - $card.css("border-bottom-width").replace(/[^-\d\.]/g, '') * 2, duration: 1500 }
//     ],
//     opacity: [
//         { value: 0, delay: 500 }
//     ],
//     elasticity: 0,
//     easing: 'easeOutCubic',
//     loop: false
// });
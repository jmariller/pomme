require('../bootstrap');

window.Vue = require('vue');

import Game from "./core/Game";
import Player from "./core/Player";
import {CARDS_DISTRIBUTED, CARDS_IN_BACKUP, CARDS_IN_HAND} from "./core/Constants";

// Only for testing
import Card from "./core/Card";
import {MELDS, SPADES, HEARTS, CLUBS, DIAMONDS} from "./core/Constants";
import {SPADES_TRUMP, HEARTS_TRUMP, CLUBS_TRUMP, DIAMONDS_TRUMP} from "./core/Constants";
import {PLAIN_A, PLAIN_K, PLAIN_Q, PLAIN_J, PLAIN_10, PLAIN_9, PLAIN_8, PLAIN_7, PLAIN_6} from "./core/Constants";
import {TRUMP_A, TRUMP_K, TRUMP_Q, TRUMP_J, TRUMP_10, TRUMP_9, TRUMP_8, TRUMP_7, TRUMP_6} from "./core/Constants";

window.app = new Vue({
    el: '#root',
    data() {
        return {
            CARDS_IN_HAND: CARDS_IN_HAND,
            CARDS_IN_BACKUP: CARDS_IN_BACKUP,

            game: null,
            current_round: null,

            card1: undefined,
            card2: undefined,
            winner: ''
        }
    },
    methods: {

        _initGame() {
            this.game = new Game([
                new Player(1, 'Julien'),
                new Player(2, 'Rossella')
            ]);

            this.current_round = this.game.currentRound();

            this.current_round.current_player = this.current_round.players[0];
            this.current_round.first_player = this.current_round.current_player;
        },

        startGame() {
            // Define first player & winner of the current round
            this.current_round.winner_player = this.current_round.currentTrick().highestCard().player;
            this.current_round.first_player = this.current_round.winner_player;

            // Put cards back into the deck
            this.game.players.forEach(player => {
                player.throwHand(this.current_round.cards_deck);
            });

            // Close the selection round and start the first round
            this.newRound(this.current_round.first_player);
        },

        takeBackup(player) {
            if (player.discarded.length <= CARDS_IN_BACKUP) {
                player.useBackup();
                player.melds = player.findMelds();
            }
        },

        exchangeSixOfTrump(player) {
            player.exchangeSixOfTrump(this.current_round.cards_deck.trump, this.current_round.cards_deck);
            player.melds = player.findMelds();
        },

        play(card) {
            // Find the melds winner during the first trick, when the first card is drawn
            if (this.current_round.tricks.length === 1 && this.current_round.currentTrick().cards.length === 0) {
                this.current_round.meldsWinner();
            }

            // Add cards to trick while there are still players to draw
            if (this.current_round.currentTrick().cards.length < this.game.players.length) {
                this.current_round.currentTrick().cards.push(card);
                this.current_round.current_player.throwCard(card);
                this.current_round.current_player = this.current_round.nextPlayer();
            }

            // Determine winner for the current trick...
            if (this.current_round.currentTrick().completed(this.game.players)) {
                this.current_round.currentTrick().winner_player = this.current_round.currentTrick().winner();
                this.current_round.current_player = this.current_round.currentTrick().winner_player;
                this.current_round.current_player.winsTrick(this.current_round.currentTrick());

                // ... and prepare the next trick if the round is not finished
                if (!this.current_round.completed()) {
                    this.current_round.newTrick();
                }

                this.card1 = undefined;
                this.card2 = undefined;
            }

            // Determine winner for current round
            if (this.current_round.completed()) {
                this.current_round.winner_player = this.current_round.winner();
            }
        },

        newRound(next_player) {
            // Create a new round
            this.game.newRound();

            // Prepare the new round
            this.current_round = this.game.currentRound();
            this.current_round.first_player = next_player;
            this.current_round.current_player = next_player;

            // Shuffle the deck, distribute cards and set the trump
            this.current_round.cards_deck.shuffle();
            this.current_round.cards_deck.distribute(this.game.players, CARDS_DISTRIBUTED, CARDS_IN_HAND, CARDS_IN_BACKUP);

            // Check for melds
            this.game.players.forEach(player => player.melds = player.findMelds());
        },

        grabCardForSelection(card) {
            if (!this.current_round.currentTrick().hasDraws() && this.current_round.current_player.hand.length !== CARDS_IN_HAND) {
                if (this.current_round.current_player.hand.length === 0) {
                    this.current_round.current_player.pullCardFromDeck(card, this.current_round.cards_deck);
                    this.current_round.currentTrick().cards.push(card);
                    this.current_round.current_player = this.current_round.nextPlayer();
                }

                // Selection round is completed, start the game
                if (!this.current_round.currentTrick().hasDraws() && this.current_round.currentTrickCompleted()) {
                    this.startGame();
                }

            }
        },

        emptyHands() {
            this.game.players.forEach(player => {
                player.throwHand();
            });
            this.current_round.currentTrick().reset();
        },
    },

    created() {
        this._initGame();

        ///////////////////////
        // Some tests here...
        ///////////////////////


        // let player1 = new Player(1, 'Julien');
        // let player2 = new Player(2, 'Rossella');
        //
        // player1.hand = [
        //     new Card( HEARTS, PLAIN_6 ),
        //     new Card( HEARTS, PLAIN_7 ),
        //     new Card( HEARTS, PLAIN_8 ),
        // ];
        //
        // player2.hand = [
        //     new Card( HEARTS, PLAIN_10 ),
        //     new Card( HEARTS, PLAIN_J ),
        //     new Card( HEARTS, PLAIN_Q ),
        // ];
        //
        // player1.melds = player1.findMelds();
        // player2.melds = player2.findMelds();
        //
        // console.log('[ Julien ]');
        // console.log(player1.melds.bestMeld());
        // console.log(player1.melds);
        // console.log(`Points: ${player1.melds.points()}`);
        // console.log('\n');
        //
        // console.log('[ Rossella ]');
        // console.log(player2.melds.bestMeld());
        // console.log(player2.melds);
        // console.log(`Points: ${player2.melds.points()}`);
        // console.log('\n');
        //
        // if (player1.melds.winnerMeld(player2)) {
        //     console.log('Julien wins the meld!');
        // } else {
        //     console.log('Rossella wins the meld!');
        // }
        // console.log('\n');
    }
});
export default class Rank {

    constructor(symbol, index, points, sort) {
        this.symbol = symbol;   // A, K, Q, J, 10, 9, 8, 7, 6
        this.index = index;     // 9, 8, 7, 6, 5, 4, 3, 2, 1 (higher is stronger)
        this.points = points;   // Depends on whether it is trump suit or not
        this.sort = sort;       // Sort order
    }

}
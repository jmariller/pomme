import Card from "./Card";
import {CARDS_DISTRIBUTED, CARDS_IN_HAND, PLAIN_SUITE, SHUFFLE_ITERATIONS} from "./Constants";

export default class CardsDeck {

    constructor(suites)  {
        this.suites = suites;
        this.cards = this._prepareCards();
        this.trump = undefined;
    }

    /**
     * Pull a card from the deck
     *
     * @param someCard
     */
    pullCard(someCard) {
        let card_index_to_pull = this.cards.findIndex(card => {
            return someCard.id === card.id;
        });

        this.cards.splice(card_index_to_pull, 1);
    }

    /**
     * Shuffle a few times the cards
     *
     * @param iterations
     */
    shuffle(iterations = SHUFFLE_ITERATIONS) {
        for (let i = 1 ; i <= iterations ; i++) {
            this.cards = _.shuffle(this.cards)
        }
    }

    /**
     * Distribute cards evenly to players
     *
     * @param players
     * @param nbCards
     * @param limit
     * @param backup
     */
    distribute(players, nbCards = CARDS_DISTRIBUTED, limit = CARDS_IN_HAND, backup = 0) {
        // In some cases (e.g. Pomme) the last card after the backup will define the trump
        // /!\ IMPORTANT: This must be done before actually giving cards to player !! /!\
        this._applyTrump(players.length * limit + players.length * backup);

        // Alternate {nbCards} for each player until ${TOTAL_LIMIT} is reached
        this._cardsToPlayers(players, nbCards, limit, 'hand');

        // In some cases (e.g. Pomme) give backup cards
        if (backup > 0) {
            this._cardsToPlayers(players, nbCards, backup, 'backup');
        }
    }

    /**
     * Reset suites:
     * - No trump selected
     */
    resetSuites() {
        this.suites.forEach(suit => suit.trump = false);
    }

    /**
     * Give cards to players in hand or backup
     *
     * @param players
     * @param nbCards
     * @param limit
     * @param cards
     * @private
     */
    _cardsToPlayers(players, nbCards, limit, cards) {
        const LIMIT = limit <= nbCards ? (limit * players.length) - 1 : limit * players.length;
        let nb_cards = 0;

        while (nb_cards <= LIMIT) {
            players.forEach(player => {
                this.cards.splice(0, nbCards).forEach(card => {
                    card.player = player;
                    player[cards].push(card);
                });
                nb_cards += player[cards].length;
            });
        }
    }

    /**
     * Prepare the cards based on the suites, and apply trump if set
     *
     * @returns {Array}
     * @private
     */
    _prepareCards() {
        let cards = [];

        this.suites.forEach(suit => PLAIN_SUITE.forEach(rank => cards.push(new Card(suit, rank))));

        return cards;
    }

    /**
     * Apply the trump suit
     *
     * @param index
     * @private
     */
    _applyTrump(index) {
        this.trump = this.cards[index];

        this.suites.map(suit => suit.trump = suit.code === this.trump.suit.code);

        this.cards.forEach((card, index, cards) => {
            if (card.suit.code === this.trump.suit.code) {
                cards[index] = card.toTrump();
            }
        });
    }

}
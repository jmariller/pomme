import {TRUMP_SUITE} from "./Constants";

export default class Card {

    constructor(suit, rank, player = undefined)  {
        this.suit = suit;		// Diamond, Hearts, Spades, Clubs
        this.rank = rank;		// See Rank.js
        this.player = player;   // Player who owns that card

        // Unique ID for a card is composed of its suit code and rank symbol
        // Some example:
        // - 'DK' => King of Diamonds
        // - 'H7' => 7 of Hearts
        this.id = this.suit.code + this.rank.symbol;
    }

    /**
     * Check if the current card is in a given suit
     *
     * @param suit
     * @returns {boolean}
     */
    inSuite(suit) {
        return this.suit.code === suit.code;
    }

    /**
     * Check if the card is a trump
     *
     * @returns {*|boolean}
     */
    isTrump() {
        return this.suit.trump;
    }

    /**
     * Convert the current card to its trump equivalent
     *
     * @return {Card}
     */
    toTrump() {
        this.rank = TRUMP_SUITE.find(rank => this.rank.symbol === rank.symbol);
        return this;
    }

    /**
     * Retrieve the card in a readable way ({rank} of {suit})
     *
     * @returns {string}
     */
    toString() {
        return `${this.rank.symbol} of ${this.suit.name}`;
    }

    /**
     * Sort function to differentiate two cards by suit and then rank
     *
     * @param card1
     * @param card2
     * @returns {number}
     */
    static sortFunction(card1, card2) {
        let suite_sort = card1.suit.sort - card2.suit.sort;

        if (suite_sort !== 0) {
            return suite_sort;
        }

        return card2.rank.sort - card1.rank.sort;
    }

}
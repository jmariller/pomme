import Card from "./Card";
import {MELDS} from "./Constants";

export default class PlayerMelds {

    constructor(player) {
        this.player = player;

        // King and Queen of trumps (!! This meld is always valid !!)
        this.royal_couple = this._royalCouple();

        this.four_aces = this._hasQuartet('A');         // Has all Aces
        this.four_kings = this._hasQuartet('K');        // Has all Kings
        this.four_queens = this._hasQuartet('Q');       // Has all Queens
        this.four_jacks = this._hasQuartet('J');        // Has all Jacks
        this.four_tens = this._hasQuartet('10');        // Has all Tens
        this.four_nines = this._hasQuartet('9');        // Has all Nines

        this.sequences = this._sequences();            // Retrieve existing sequences

        // Cleanup any conflicts in the sequences
        this._removeConflicts();

        // Check if the given player has at least one meld (royal couple counts in any case, so it is not included here)
        this.has_melds = this._hasAnyQuartet() || this.sequences.length > 0;
    }

    /**
     * Sum up points for all valid melds
     *
     * @return {number}
     */
    points() {
        let points = 0;

        // Sum up points for any quartet
        points += this.four_aces ? MELDS.FOUR_A.points : 0;
        points += this.four_kings ? MELDS.FOUR_K.points : 0;
        points += this.four_queens ? MELDS.FOUR_Q.points : 0;
        points += this.four_jacks ? MELDS.FOUR_J.points : 0;
        points += this.four_tens ? MELDS.FOUR_10.points : 0;
        points += this.four_nines ? MELDS.FOUR_9.points : 0;

        // Sum up points for any sequence
        this.sequences.forEach(sequence => {
            points += MELDS['SEQUENCE_' + sequence.length].points;
        });

        return points;
    }

    /**
     * Determine if the current player wins the meld
     *
     * @param otherPlayer
     * @return {boolean}
     */
    winnerMeld(otherPlayer) {
        // Get the best meld for each player
        let my_best_meld = this.bestMeld();
        let other_best_meld = otherPlayer.melds.bestMeld();

        // Special case for same level sequences:
        if (my_best_meld.index === other_best_meld.index) {
            let my_best_sequence = this.bestSequence();
            let my_highest_rank = my_best_sequence.slice().pop().rank.index;

            let other_best_sequence = otherPlayer.melds.bestSequence();
            let other_highest_rank = other_best_sequence.slice().pop().rank.index;

            // - Same highest rank, the trump or first announcer wins
            if (my_highest_rank === other_highest_rank) {
                if (other_best_sequence.slice().pop().suit.trump) {
                    return false;
                }

                // We assume the current player is the first announcer
                return true;
            }

            // - Different rank, the highest one wins
            return my_highest_rank > other_highest_rank;
        }

        // Otherwise meld with higher level wins
        return my_best_meld.index > other_best_meld.index;
    }

    /**
     * Get the best meld of the player
     *
     * @return {Meld}
     */
    bestMeld() {
        if (this.four_jacks) {
            return MELDS.FOUR_J;
        }
        if (this.four_nines) {
            return MELDS.FOUR_9;
        }
        if (this._findSequence(9).length > 0) {
            return MELDS.SEQUENCE_9;
        }
        if (this._findSequence(8).length > 0) {
            return MELDS.SEQUENCE_8;
        }
        if (this._findSequence(7).length > 0) {
            return MELDS.SEQUENCE_7;
        }
        if (this._findSequence(6).length > 0) {
            return MELDS.SEQUENCE_6;
        }
        if (this._findSequence(5).length > 0) {
            return MELDS.SEQUENCE_5;
        }
        if (this.four_aces) {
            return MELDS.FOUR_A;
        }
        if (this.four_kings) {
            return MELDS.FOUR_K;
        }
        if (this.four_queens) {
            return MELDS.FOUR_Q;
        }
        if (this.four_tens) {
            return MELDS.FOUR_10;
        }
        if (this._findSequence(4).length > 0) {
            return MELDS.SEQUENCE_4;
        }
        if (this._findSequence(3).length > 0) {
            return MELDS.SEQUENCE_3;
        }
    }

    /**
     * Find the best sequence
     *
     * @return {T}
     */
    bestSequence() {
        return this.sequences.slice().sort((seq1, seq2) => seq1.length - seq2.length).pop();
    }

    /**
     * Build a list of melds
     *
     * @return {Array}
     */
    toList() {
        let list = [];

        // First add the quartets in the order of points
        if (this.four_jacks) {
            list.push({name: MELDS.FOUR_J.name, points: MELDS.FOUR_J.points, top: ''});
        }
        if (this.four_nines) {
            list.push({name: MELDS.FOUR_9.name, points: MELDS.FOUR_9.points, top: ''});
        }
        if (this.four_aces) {
            list.push({name: MELDS.FOUR_A.name, points: MELDS.FOUR_A.points, top: ''});
        }
        if (this.four_kings) {
            list.push({name: MELDS.FOUR_K.name, points: MELDS.FOUR_K.points, top: ''});
        }
        if (this.four_queens) {
            list.push({name: MELDS.FOUR_Q.name, points: MELDS.FOUR_Q.points, top: ''});
        }
        if (this.four_tens) {
            list.push({name: MELDS.FOUR_10.name, points: MELDS.FOUR_10.points, top: ''});
        }

        // Then add any sequence
        if (this.sequences.length > 0) {
            this.sequences.forEach(sequence => {
                list.push({
                    name: MELDS['SEQUENCE_' + sequence.length].name,
                    points: MELDS['SEQUENCE_' + sequence.length].points,
                    top: sequence.slice().pop()
                })
            });
        }

        // Finally add the royal couple
        if (this.royal_couple !== undefined) {
            list.push({name: MELDS.ROYAL_COUPLE.name, points: MELDS.ROYAL_COUPLE.points, top: ''});
        }

        return list;
    }

    /**
     * If available, fetch the royal couple:
     * - King and Queen of trumps
     *
     * @return {*}
     * @private
     */
    _royalCouple() {
        let trumps = this.player.hand.filter(card => card.suit.trump);

        if (trumps.find(card => card.rank.symbol === 'K') !== undefined && trumps.find(card => card.rank.symbol === 'Q') !== undefined) {
            return MELDS.ROYAL_COUPLE;
        }

        return undefined;
    }

    /**
     * Determine if the player has a quartet
     *
     * @param kind
     * @returns {boolean}
     * @private
     */
    _hasQuartet(kind) {
        return this.player.hand.filter(card => card.rank.symbol === kind).length === 4;
    }

    /**
     * Check if has any quartet
     *
     * @return {boolean|*}
     * @private
     */
    _hasAnyQuartet() {
        return this.four_aces || this.four_kings || this.four_queens ||
               this.four_jacks || this.four_tens || this.four_nines;
    }

    /**
     * Find a sequence of given length
     *
     * @param length
     * @return {T[]}
     * @private
     */
    _findSequence(length) {
        return this.sequences.filter(sequence => sequence.length === length);
    }

    /**
     * Retrieve sequences of at least 3 cards in player's hand
     *
     * @return {Array}
     * @private
     */
    _sequences() {
        const OUT_OF_RANGE = -1;
        const SEQUENCE_MIN_LENGTH = 3;

        let sequence = [];
        let sequences = [];

        // Create a sorted copy of the hand (from left to right, e.g. Q > K > A)
        let sorted_hand = this.player.hand.slice().sort(Card.sortFunction);

        // Look for sequences
        sorted_hand.forEach((card, index, cards) => {

            // Store the sort order and suit code for previous, current and next card
            let prev_sort = cards[index - 1] !== undefined ? cards[index - 1].rank.sort : OUT_OF_RANGE;
            let prev_suit = cards[index - 1] !== undefined ? cards[index - 1].suit.code : '';
            let curr_sort = cards[index].rank.sort;
            let curr_suit = cards[index].suit.code;
            let next_sort = cards[index + 1] !== undefined ? cards[index + 1].rank.sort : OUT_OF_RANGE;
            let next_suit = cards[index + 1] !== undefined ? cards[index + 1].suit.code : '';

            // Check if previous and next are in sequence
            let prev_seq = Math.abs(curr_sort - prev_sort) === 1 && curr_suit === prev_suit;
            let next_seq = Math.abs(curr_sort - next_sort) === 1 && curr_suit === next_suit;

            // Add cards in current sequence
            if ((prev_sort === OUT_OF_RANGE && !prev_seq && next_seq) || (prev_seq || next_seq)) {
                sequence.push(card);
            }

            // Is this the end of a sequence ?
            if (!prev_seq && !next_seq || prev_seq && !next_seq || prev_seq && next_sort === OUT_OF_RANGE) {

                // Record current sequence if it is long enough
                if (sequence.length >= SEQUENCE_MIN_LENGTH) {
                    sequences.push(sequence);
                }

                // Reset current sequence
                sequence = [];
            }
        });

        return sequences;
    }

    /**
     * Remove conflicting melds within sequences
     * (e.g. three cards from an Ace cannot be used with a quartet of Queens)
     *
     * @private
     */
    _removeConflicts() {
        // If current sequence contains any of the quartet figure, remove that sequence from the list
        this.sequences.forEach(sequence => {
            let ace_index = sequence.findIndex(card => card.rank.symbol === 'A');
            let king_index = sequence.findIndex(card => card.rank.symbol === 'K');
            let queen_index = sequence.findIndex(card => card.rank.symbol === 'Q');
            let jack_index = sequence.findIndex(card => card.rank.symbol === 'J');
            let ten_index = sequence.findIndex(card => card.rank.symbol === '10');
            let nine_index = sequence.findIndex(card => card.rank.symbol === '9');

            if (this.four_aces && ace_index !== -1) {
                sequence.splice(ace_index, 1);
            }
            if (this.four_kings && king_index !== -1) {
                sequence.splice(king_index, 1);
            }
            if (this.four_queens && queen_index !== -1) {
                sequence.splice(queen_index, 1);
            }
            if (this.four_jacks && jack_index !== -1) {
                sequence.splice(jack_index, 1);
            }
            if (this.four_tens && ten_index !== -1) {
                sequence.splice(ten_index, 1);
            }
            if (this.four_nines && nine_index !== -1) {
                sequence.splice(nine_index, 1);
            }
        });
    }

}
import Card from "./Card";
import PlayerMelds from "./PlayerMelds";
import {SIX_OF_TRUMP, POMME_LIMIT, CINQ_DE_DER} from "./Constants";

export default class Player {

    constructor(id, name)  {
        // General properties of the player
        this.id = id;
        this.name = name;

        // Player melds, and whether he won the last meld
        this.melds = undefined;
        this.melds_won = false;

        // The various set of cards for the player:
        this.hand = [];         // Currently in hand
        this.backup = [];       // Backup cards: only applicable for the Pomme game
        this.discarded = [];    // Discarded cards: only applicable for the Pomme game
        this.won = [];          // Cards won: the cards which were won during a round
        
        // Specifically for the Pomme: count rounds won and Pommes
        this.rounds_won = 0;
        this.pommes = 0;

        // Has won the last trick (La Der)
        this.has_der = false;

        // Status of the player
        this.ready = false;
    }

    /**
     * Reset the player attributes relevant for a new round
     */
    reset() {
        this.melds_won = false;
        this.has_der = false;
        this.ready = false;
        this.backup = [];
        this.won = [];
    }

    /**
     * Count points won so far and add 5 points if the last trick was won
     *
     * TODO: make it work for more than two players
     *
     * @return {*}
     */
    points() {
        let tricks_points = this.won.reduce((accumulator, card) => accumulator + card.rank.points, 0);
        let cinq_de_der = this.has_der ? CINQ_DE_DER : 0;

        let total = tricks_points + cinq_de_der;

        // Add the royal couple (Stöck) which always counts
        total += this.melds !== undefined && this.melds.royal_couple !== undefined ? this.melds.royal_couple.points : 0;

        // Specifically for the Pomme:
        // - Only add melds points if the tricks points are above the Pomme limit
        total += (total >= POMME_LIMIT) ? (this.melds_won ? this.melds.points() : 0) : 0;

        return total;
    }

    /**
     * * Specifically for the Pomme:
     * - Does player have less than 21 points?
     *
     * @return {boolean}
     */
    isPomme() {
        return this.points() < POMME_LIMIT;
    }

    /**
     * Win a trick and update points
     *
     * @param trick
     */
    winsTrick(trick) {
        trick.cards.forEach(card => this.won.push(card));
    }

    /**
     * Find the melds
     *
     * @return {PlayerMelds}
     */
    findMelds() {
        return new PlayerMelds(this);
    }

    /**
     * Specifically for the Pomme:
     * - Exchange the six of trump against the trump card
     *
     * @param trump
     * @param deck
     */
    exchangeSixOfTrump(trump, deck) {
        if (this.hasSixOfTrump(trump)) {
            this.pullCardFromDeck(trump, deck);
            this.throwCard(this._sixOfTrump(trump), deck);
        }
    }

    /**
     * Specifically for the Pomme:
     * - Check if player has the six of trump
     *
     * @param trump
     * @returns {boolean}
     */
    hasSixOfTrump(trump) {
        return this._sixOfTrump(trump) !== undefined;
    }

    /**
     * Does the player have the Jass (Jack of Trump)?
     *
     * @return {boolean}
     */
    hasJass() {
        return this.hand.find(card => card.suit.trump && card.rank.symbol === 'J') !== undefined;
    }

    /**
     * Specifically for the Pomme:
     * - Use the backup cards
     * - Move backup cards into hand
     */
    useBackup() {
        // Remove discarded cards from hand
        this.discarded.forEach(discarded => {
            this.throwCard(discarded);
        });

        // Move backup cards into hand
        this.backup.forEach(card => this.hand.push(card));
        this.backup = [];
    }

    /**
     * Pull a card from a deck
     *
     * @param card
     * @param deck
     */
    pullCardFromDeck(card, deck) {
        card.player = this;
        this.hand.push(card);
        deck.pullCard(card);
    }

    /**
     * Throw a card from hand optionally into deck
     *
     * @param someCard
     * @param deck
     */
    throwCard(someCard, deck = undefined) {
        this.hand.forEach((card, index) => {
            if (card.id === someCard.id) {
                this.hand.splice(index, 1);
            }
        });

        if (deck !== undefined && deck.cards.length > 0) {
            deck.cards.push(someCard);
        }
    }

    /**
     * Throw hand and optionally put back into the deck
     *
     * @param deck
     */
    throwHand(deck = undefined) {
        if (deck !== undefined && deck.cards.length > 0) {
            this.hand.forEach(card => {
                card.player = undefined;
                deck.cards.push(card)
            });
        }
        this.hand = [];
    }

    /**
     * Can the player draw a given card considering the opponent's card?
     *
     * @param myCard
     * @param otherCard
     * @returns {boolean}
     */
    canDrawCard(myCard, otherCard) {
        // The trump is almighty and can be drawn anytime
        if (myCard.suit.trump) {
            return true;
        } else {
            // If the player doesn't have any card in that suit, they can play any card
            if (!this._hasSuite(otherCard)) {
                return true;
            }

            // Special case when the Jass (Jack of trump) is the last trump in hand:
            // - Can be held till the end, so any other card can be drawn
            if (otherCard.suit.trump && this.nbTrumps() === 1 && this.hasJass()) {
                return true;
            }

            // The player must draw cards of same color
            return myCard.inSuite(otherCard.suit);
        }
    }

    /**
     * Sort the cards in player's hand by suit and rank
     */
    sortCards() {
        this.hand.sort(Card.sortFunction);
    }

    /**
     * How many trumps does the player have?
     *
     * @returns {number}
     */
    nbTrumps() {
        return this.hand.filter(card => card.suit.trump).length;
    }

    /**
     * Does the player have a given suit?
     *
     * @param someCard
     * @returns {boolean}
     * @private
     */
    _hasSuite(someCard) {
        return this.hand.find(card => card.suit.code === someCard.suit.code) !== undefined;
    }

    /**
     * Specifically for the Pomme:
     * - Get the six of trump in the hand
     *
     * @param trump
     * @returns {*}
     * @private
     */
    _sixOfTrump(trump) {
        return this.hand.find(card => card.suit.code === trump.suit.code && card.rank.symbol === SIX_OF_TRUMP);
    }

}
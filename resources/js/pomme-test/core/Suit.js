export default class Suit {

    constructor(name, trump, sort, code)  {
        this.name = name;       // Diamond, Hearts, Spades, Clubs
        this.trump = trump;     // True if trump, False if plain
        this.sort = sort;       // Sort order
        this.code = code;       // D, H, S, C
    }

}
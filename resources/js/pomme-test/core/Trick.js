export default class Trick {

    constructor(cards = [])  {
        // By default no card is added to the trick.
        this.cards = cards;

        this.winner_player = undefined;    // No winner of the trick at the beginning, obviously...
    }

    /**
     * Specifically for Pomme:
     * - Remove cards from trick
     */
    reset() {
        this.cards = [];
    }

    /**
     * Specifically for Pomme:
     * - Does the trick have draws?
     *
     * @returns {boolean}
     */
    hasDraws() {
        let indexes = this._indexes();
        return indexes.length !== new Set(indexes).size;
    }

    /**
     * Retrieve highest card based on its index
     *
     * @returns {*}
     */
    highestCard() {
        let highest_index = this._highestIndex();
        return this.cards.find(card => card.rank.index === highest_index);
    }

    /**
     * Determine if the trick is completed
     *
     * @param players
     * @return {boolean}
     */
    completed(players) {
        return this.cards.length === players.length;
    }

    /**
     * Determine which player wins the trick with following rules:
     * - First card played in plain suites wins
     * - Otherwise card with higher rank wins
     *
     * @returns {*}
     */
    winner() {
        let previous_card = this.cards[0];
        let winner = this.cards[0];

        this.cards.slice(1, this.cards.length).forEach(card => {
            // First card played in plain suites wins
            if (!previous_card.isTrump() && !card.isTrump() && previous_card.suit.code !== card.suit.code) {
                winner = previous_card;
                return;
            }

            // Otherwise card with higher rank wins
            winner = (previous_card.rank.index > card.rank.index) ? previous_card : card;

            previous_card = card;
        });

        return winner.player;
    }

    /**
     * Get the card with highest index
     *
     * @returns {any}
     * @private
     */
    _highestIndex() {
        let indexes = this._indexes();
        return indexes.slice(indexes.length - 1, indexes.length)[0];
    }


    /**
     * Retrieve the sorted card's indexes
     *
     * @returns {any[]}
     * @private
     */
    _indexes() {
        return this.cards.map(card => card.rank.index).sort((a, b) => a - b);
    }

}
import Meld from "./Meld";
import Rank from "./Rank";
import Suit from "./Suit";

// Standard parameters for the various games
export const CINQ_DE_DER = 5;
export const CARDS_IN_HAND = 9;
export const CARDS_DISTRIBUTED = 3;

// Specific parameters for the Pomme
export const CARDS_IN_BACKUP = 3;
export const POMME_LIMIT = 21;
export const POMME_MAX = 3;
export const ROUNDS_TO_WIN = 7;
export const SIX_OF_TRUMP = '6';

// How many iterations to apply when shuffling the cards
export const SHUFFLE_ITERATIONS = 100;

// List of normal & trump suites
export const SPADES = new Suit('Spades', false, 1, 'S');
export const HEARTS = new Suit('Hearts', false, 2, 'H');
export const DIAMONDS = new Suit('Diamonds', false, 3, 'D');
export const CLUBS = new Suit('Clubs', false, 4, 'C');

export const SPADES_TRUMP = new Suit('Spades', true, 1, 'S');
export const HEARTS_TRUMP = new Suit('Hearts', true, 2, 'H');
export const DIAMONDS_TRUMP = new Suit('Diamonds', true, 3, 'D');
export const CLUBS_TRUMP = new Suit('Clubs', true, 4, 'C');

export const SUITES = [SPADES, HEARTS, DIAMONDS, CLUBS];

// List of plain cards, shown here in the order of importance
export const PLAIN_A = new Rank('A', 9, 11, 1);
export const PLAIN_K = new Rank('K', 8, 4, 2);
export const PLAIN_Q = new Rank('Q', 7, 3, 3);
export const PLAIN_J = new Rank('J', 6, 2, 4);
export const PLAIN_10 = new Rank('10', 5, 10, 5);
export const PLAIN_9 = new Rank('9', 4, 0, 6);
export const PLAIN_8 = new Rank('8', 3, 0, 7);
export const PLAIN_7 = new Rank('7', 2, 0, 8);
export const PLAIN_6 = new Rank('6', 1, 0, 9);

export const PLAIN_SUITE = [PLAIN_A, PLAIN_K, PLAIN_Q, PLAIN_J, PLAIN_10, PLAIN_9, PLAIN_8, PLAIN_7, PLAIN_6];

// List of trump cards, show here in the order of importance
export const TRUMP_J = new Rank('J', 18, 20, 4);
export const TRUMP_9 = new Rank('9', 17, 14, 6);
export const TRUMP_A = new Rank('A', 16, 11, 1);
export const TRUMP_K = new Rank('K', 15, 4, 2);
export const TRUMP_Q = new Rank('Q', 14, 3, 3);
export const TRUMP_10 = new Rank('10', 13, 10, 5);
export const TRUMP_8 = new Rank('8', 12, 0, 7);
export const TRUMP_7 = new Rank('7', 11, 0, 8);
export const TRUMP_6 = new Rank('6', 10, 0, 9);

export const TRUMP_SUITE = [TRUMP_A, TRUMP_K, TRUMP_Q, TRUMP_J, TRUMP_10, TRUMP_9, TRUMP_8, TRUMP_7, TRUMP_6];

// The list of possible melds in the descending order of power
export const MELDS = {
    FOUR_J: new Meld('all_jacks', 'Deux cents !', 'quartet', 4, 200, 13),
    FOUR_9: new Meld('all_nines', 'Cent cinquante !', 'quartet', 4, 150, 12),
    SEQUENCE_9: new Meld('hundred_in_nine', 'Cent !', 'sequence', 9, 100, 11),
    SEQUENCE_8: new Meld('hundred_in_eight', 'Cent !', 'sequence', 8, 100, 10),
    SEQUENCE_7: new Meld('hundred_in_seven', 'Cent !', 'sequence', 7, 100, 9),
    SEQUENCE_6: new Meld('hundred_in_six', 'Cent !', 'sequence', 6, 100, 8),
    SEQUENCE_5: new Meld('hundred_in_five', 'Cent !', 'sequence', 5, 100, 7),
    FOUR_A: new Meld('all_aces', 'Cent !', 'quartet', 4, 100, 6),
    FOUR_K: new Meld('all_kings', 'Cent !', 'quartet', 4, 100, 5),
    FOUR_Q: new Meld('all_queens', 'Cent !', 'quartet', 4, 100, 4),
    FOUR_10: new Meld('all_tens', 'Cent !', 'quartet', 4, 100, 3),
    SEQUENCE_4: new Meld('fifty', 'Cinquante !', 'sequence', 4, 50, 2),
    SEQUENCE_3: new Meld('twenty', 'Trois cartes !', 'sequence', 3, 20, 1),
    ROYAL_COUPLE: new Meld('royal_couple', 'Stöck !', 'duo', 2, 20, -1)
};
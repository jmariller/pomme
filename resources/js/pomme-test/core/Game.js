import Round from "./Round";
import {POMME_MAX, ROUNDS_TO_WIN} from "./Constants";

export default class Game {

    constructor(players)  {
        // Set the players
        this.players = players;

        // Start the first round
        this.rounds = [
            new Round(this.players)
        ];
    }

    /**
     * Get the current round
     *
     * @returns {Round}
     */
    currentRound() {
        return this.rounds[this.currentRoundIndex()];
    }

    /**
     * Get the current round index
     *
     * @returns {number}
     */
    currentRoundIndex() {
        return this.rounds.length - 1;
    }

    /**
     * Determine if a game is completed
     * - For the Pomme it happens when one of the players
     * - has 3 pomme or if one player has won 7 rounds
     *
     * @return {boolean}
     */
    completed() {
        return this._pommePlayer !== undefined || this._winnerPlayer() !== undefined;
    }

    /**
     * Determine who won the game
     *
     * @return {*}
     */
    winner() {
        // Specifically for the Pomme:
        // - If a player is the pomme, the other one wins
        // - Otherwise get player who won 7 rounds
        if (this.players.length === 2) {
            if (this._pommePlayer() !== undefined) {
                return this.players.find(player => player.pommes < POMME_MAX);
            }

            return this._winnerPlayer();

        // TODO: handle more than 2 players (e.g. Le Roi / Jass with 3 and 4 players)
        } else {
            return true;
        }
    }

    /**
     * Reset players and create a new round
     */
    newRound() {
        this.players.forEach(player => player.reset());

        this.rounds.push(new Round(this.players));
    }

    /**
     * Specifically for the Pomme:
     * - Find the player who has one the 7 rounds
     *
     * @return {*}
     * @private
     */
    _winnerPlayer() {
        return this.players.find(player => player.rounds_won === ROUNDS_TO_WIN);
    }

    /**
     * Specifically for the Pomme:
     * - Find the player who is pomme
     *
     * @return {*}
     * @private
     */
    _pommePlayer() {
        return this.players.find(player => player.pommes === POMME_MAX);
    }

}
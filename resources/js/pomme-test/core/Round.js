import Trick from "./Trick";
import CardsDeck from "./CardsDeck";
import {CARDS_IN_HAND, SUITES} from "./Constants";

export default class Round {

    constructor(players)  {
        // Prepare the players
        this.players = players;
        this.first_player = undefined;
        this.current_player = undefined;
        this.winner_player = undefined;

        // Prepare the deck
        this.cards_deck = new CardsDeck(SUITES);
        this.cards_deck.resetSuites();
        this.cards_deck.shuffle();

        // Prepare the list of tricks and the current trick
        this.tricks = [
            new Trick()
        ];

        // Record scores per round
        this.scores = {
            player1: 0,
            player2: 0,
            team1: 0,
            team2: 0,
        }
    }

    /**
     * Get previous player in the list (modular list)
     *
     * @returns {*}
     */
    previousPlayer() {
        let index = this._currentPlayerIndex();
        return index === 0 ? this.players[this.players.length - 1] : this.players[index - 1];
    }

    /**
     * Get next player in the list (modular list)
     *
     * @returns {*}
     */
    nextPlayer() {
        let index = this._currentPlayerIndex();
        return index === this.players.length - 1 ? this.players[0] : this.players[index + 1];
    }

    /**
     * Create a new trick and return it
     *
     * @returns {*}
     */
    newTrick() {
        this.tricks.push(new Trick());
    }

    /**
     * Get the current trick
     *
     * @returns {*}
     */
    currentTrick() {
        return this.tricks[this._currentTrickIndex()];
    }

    /**
     * Determine if a trick is completed: all players have drawn a card
     *
     * @returns {boolean}
     */
    currentTrickCompleted() {
        return this.currentTrick().cards.length === this.players.length;
    }

    /**
     * Determine if the round is completed:
     * - All cards in one hand have been played
     *
     * @returns {boolean}
     */
    completed() {
        return this.currentTrickCompleted() && this.tricks.length === CARDS_IN_HAND;
    }

    /**
     * Set the player who wins the melds
     *
     * TODO: make it work for more than two players
     */
    meldsWinner() {
        // Only current player has a meld
        if (this.current_player.melds.has_melds && !this.nextPlayer().melds.has_melds) {
            this.current_player.melds_won = true;

        // Only next player has a meld
        } else if (!this.current_player.melds.has_melds && this.nextPlayer().melds.has_melds) {
            this.nextPlayer().melds_won = true;

        // Both players have a meld, find which one is the highest
        } else if (this.current_player.melds.has_melds && this.nextPlayer().melds.has_melds) {
            this.current_player.melds_won = this.current_player.melds.winnerMeld(this.nextPlayer());
            this.nextPlayer().melds_won = !this.current_player.melds_won;
        }
    }

    /**
     * Determine the winner of a round:
     * - Player/Team who has most points
     * - Specifically for the Pomme:
     *    > one player has more points OR
     *    > the other player has less than 21 points
     *
     * @returns {boolean}
     */
    winner() {
        if (this.players.length === 2) {
            return this._winnerPomme();

        // TODO: handle more than 2 players (e.g. Le Roi / Jass with 3 and 4 players)
        } else {
            return true;
        }
    }

    /**
     * Specifically for the Pomme:
     * - One player has more points => update rounds
     * - Other player has less than 21 points => update counts for Pommes
     * @return {*}
     * @private
     */
    _winnerPomme() {
        // Winner for last trick gets the Der (+ 5 points)
        this.currentTrick().winner_player.has_der = true;

        // Save players' scores
        this._saveScores();

        // First player wins
        if (this.players[1].isPomme() || this.players[0].points() > this.players[1].points()) {
            this.players[0].rounds_won++;
            this.players[1].pommes += this.players[1].isPomme();
            return this.players[0];

            // Second player wins
        } else if (this.players[0].isPomme() || this.players[1].points() > this.players[0].points()) {
            this.players[1].rounds_won++;
            this.players[0].pommes += this.players[0].isPomme();
            return this.players[1];
        }

        // Rare case, nobody wins?
        // TODO: check if this can really happen
        return undefined;
    }

    /**
     * Save the scores
     *
     * @private
     */
    _saveScores() {
        // Specific for the Pomme: save each player' score separately
        if (this.players.length === 2) {
            this.scores.player1 = this.players[0].points();
            this.scores.player2 = this.players[1].points();

        // TODO: save scores for teams
        } else {

        }
    }

    /**
     * Get the current trick index
     *
     * @returns {number}
     * @private
     */
    _currentTrickIndex() {
        return this.tricks.length - 1;
    }

    /**
     * Get the current player's index in the list
     *
     * @returns {*}
     * @private
     */
    _currentPlayerIndex() {
        return this.players.findIndex(player => player.id === this.current_player.id);
    }

}
export default class Meld {

    constructor(code, name, type, nbCards, points, index) {
        this.code = code;           // The code for the meld (e.g. all_jacks for 4 jacks)

        // TODO: this is for testing purpose only, remove once done (name will be retrieved via config file)
        this.name = name;

        this.type = type;           // Quartet, sequence, duo
        this.nb_cards = nbCards;    // How many cards compose this meld
        this.points = points;       // How many points for this meld

        // An index to identify the importance of the meld
        // (higher is stronger, and -1 means it counts in any case)
        this.index = index;
    }

}
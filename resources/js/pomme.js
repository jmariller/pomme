require('./bootstrap');

import Vue from 'vue';
import store from "./store";

import Scores from "./components/pomme/Scores";
import RoundResults from "./components/pomme/RoundResults";
import Carpet from "./components/pomme/Carpet";
import CardsDeck from "./components/CardsDeck";
import OtherHand from "./components/OtherHand";
import PlayerHand from "./components/PlayerHand";

new Vue({
    el: '#root',

    components: {Scores, RoundResults, Carpet, CardsDeck, PlayerHand, OtherHand},

    store,

    created() {
        this.$store.dispatch('join', 'pomme');
    }
});

import animation from "./api/animation";
window.animation = animation;
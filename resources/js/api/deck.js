import CONSTANTS from "../store/constants";

export default {

    /**
     * Initialize the deck
     *
     * @return {Array}
     */
    init(cardsDrawn) {
        return Array.from(Array(CONSTANTS.CARDS_IN_DECK).keys())
            .map(key => this._emptyCard(key, cardsDrawn.includes(key)));
    },

    /**
     * Build an empty card
     *
     * @param pos
     * @param disabled
     * @returns {{id: null, pos: *, animated: boolean, disabled: boolean}}
     * @private
     */
    _emptyCard(pos, disabled) {
        return {
            id: null,
            pos: pos,
            animated: false,
            disabled: disabled
        };
    },

}
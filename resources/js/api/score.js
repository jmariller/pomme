export default {

    /**
     * Show the results of last round
     */
    showLastRoundResults() {
        $('#round-results-modal').modal('show');
    },

    /**
     * Get game scores for a player
     *
     * @param scores
     * @param playerId
     * @returns {*}
     */
    score(scores, playerId) {
        return scores.find(score => score.player_id === playerId);
    },

    /**
     * Find round points for a player
     *
     * @param scores
     * @param playerId
     * @returns {*}
     */
    points(scores, playerId) {
        return this._roundScore(scores, playerId, 'points');
    },

    /**
     * Find round melds for a player
     *
     * @param scores
     * @param playerId
     * @returns {*}
     */
    melds(scores, playerId) {
        return this._roundScore(scores, playerId, 'melds');
    },


    /**
     * Find round total for a player
     *
     * @param scores
     * @param playerId
     * @returns {*}
     */
    total(scores, playerId) {
        return this._roundScore(scores, playerId, 'total');
    },

    /**
     * Get a round score by type for a given scores and player
     *
     * @param scores
     * @param playerId
     * @param scoreType
     * @returns {number}
     * @private
     */
    _roundScore(scores, playerId, scoreType) {
        if (!scores || !playerId) return 0;

        let player_score = this.score(scores, playerId);

        return player_score ? player_score[scoreType] : 0;
    },

}
import anime from "animejs";

export default {

    animation: null,

    /**
     * Animate the giving of cards to trick winner
     *
     * @param winnerId
     * @returns {*}
     */
    async giveCardsToWinner(winnerId) {
        let $target = $(`#hidden-pocket-${winnerId}`);

        this.animation = anime({
            targets: `.card-game-played`,
            translateY: card => $target.offset().top - $(card).offset().top,
            scaleX: 0,
            scaleY: 0,
            easing: 'easeOutCubic',
            duration: 750
        });

        return this.animation.finished;
    },

    /**
     * Reset the current animation
     */
    reset() {
        this.animation.seek(0);
    },

    /**
     * Animate the drawing of a card from a player hand
     *
     * @param cardId
     * @param playerId
     * @return {*}
     */
    async drawFromHand(cardId, playerId) {
        return this._drawCard(
            $(`#player-${playerId}-hand-card-${cardId}`),
            this._buildTargetFromCard($(`#player-card-${playerId}`))
        );
    },

    /**
     * Animate the drawing of a card from the deck to a player
     *
     * @param cardPos
     * @param playerId
     * @return {*}
     */
    async drawFromDeck(cardPos, playerId) {
        return this._drawCard(
            $(`#card-deck-${cardPos}`),
            this._buildTargetFromCard($(`#player-card-${playerId}`))
        );
    },

    /**
     * Build a target based on a given card
     *
     * @param $card
     * @returns {{top: *, left: *, width: (*|never), height: (*|never)}}
     * @private
     */
    _buildTargetFromCard($card) {
        return {
            left: $card.offset().left,
            top: $card.offset().top,
            width: $card.outerWidth(),
            height: $card.outerHeight()
        };
    },

    /**
     * Animate the drawing of a card
     *
     * @param $card
     * @param target
     * @returns {*}
     * @private
     */
    _drawCard($card, target) {
        let scale = {
            x: target.width / $card.outerWidth(),
            y: target.height / $card.outerHeight()
        };
        
        let translate = {
            x: target.left - $card.offset().left + (scale.x === 1 ? 0 : $card.outerWidth() / scale.x),
            y: target.top - $card.offset().top + (scale.y === 1 ? 0 : $card.outerHeight() / scale.y)
        };

        this.animation = anime({
            targets: `#${$card.attr('id')}`,
            translateX: translate.x,
            translateY: translate.y,
            scaleX: scale.x,
            scaleY: scale.y,
            easing: 'easeOutCubic',
            duration: 750
        });

        return this.animation.finished;
    },

}
export default {

    /**
     * Draw a card from the deck
     *
     * @param gameId
     * @param cardPos
     */
    async drawCardFromDeck(gameId, cardPos) {
        return axios.post(`/games/${gameId}/rounds/0`, {card_pos: cardPos});
    },

    /**
     * Draw a card from the hand
     *
     * @param gameId
     * @param roundNb
     * @param cardId
     * @return {*}
     */
    async drawCardFromHand(gameId, roundNb, cardId) {
        return axios.post(`/games/${gameId}/rounds/${roundNb}`, {card_id: cardId});
    },

    /**
     * Take the backup card
     *
     * @param playerId
     * @param discarded
     * @param callback
     */
    takeBackup(playerId, discarded, callback) {
        axios.post(`/players/${playerId}/backup`, {discarded: discarded});
    },

    /**
     * Exchange the six of trump with the trump card
     *
     * @returns {*}
     */
    exchangeSixOfTrump(playerId) {
        return axios.post(`/players/${playerId}/trump`, {'dummy': 'dummy'});
    },

    /**
     * Find the Six of Trump in a hand
     *
     * @param hand
     * @param trump
     * @returns {*}
     */
    sixOfTrump(hand, trump) {
        if (!trump) return null;

        return hand.find(card => card.suit.code === trump.suit.code && card.rank.symbol === '6');
    },

    /**
     * Get a card by its identifier from the given hand
     *
     * @param hand
     * @param cardId
     * @returns {*}
     */
    cardFromHandById(hand, cardId) {
        return hand.find(card => card.id === cardId);
    },

    /**
     * Get the hand without a given card
     *
     * @param hand
     * @param cardId
     * @returns {*}
     */
    handWithoutCard(hand, cardId) {
        let new_hand = hand.slice();

        new_hand.splice(hand.findIndex(card => card.id === cardId), 1);

        return new_hand;
    },

    /**
     * Get a fake hand for other players
     *
     * @param nbCards
     * @returns {*}
     */
    fakeHand(nbCards) {
        return Array.from(Array(nbCards).keys()).map(key => {
            return {'id': key + 1}
        });
    },

    /**
     * Determine the CSS identifier for a player's card
     *
     * @param playerId
     * @param cardId
     * @returns {string}
     */
    cardCssId(playerId, cardId) {
        return `player-${playerId}-hand-card-${cardId}`;
    },

    /**
     * Show the best meld title for a given player
     *
     * @param playerId
     * @param title
     */
    showBestMeldTitle(playerId, title) {
        $(`#player-hand-${playerId}`)
            .popover('dispose')
            .popover({
                content: `<strong class="px-2">${title}</strong>`,
                html: true,
                trigger: 'manual',
                placement: 'bottom'
            })
            .popover('show');
    },

    /**
     * Hide the best meld title for a given player
     *
     * @param playerId
     */
    hideBestMeldTitle(playerId) {
        $(`#player-hand-${playerId}`)
            .popover('hide')
            .popover('dispose');
    },

    /**
     * Show the list of melds for a given player
     *
     * @param playerId
     */
    showMelds(playerId) {
        $(`#melds-modal-${playerId}`).modal('show');
    },

    /**
     * Check for a given hand if a card can be played against another one
     *
     * @param hand
     * @param myCard
     * @param otherCard
     * @returns {boolean}
     */
    canDrawCard(hand, myCard, otherCard) {
        // The trump is almighty and can be drawn anytime
        if (myCard.suit.trump) {
            return true;
        }

        // If the player doesn't have any card in that suit, they can play any card
        if (!this._hasSuit(hand, otherCard)) {
            return true;
        }

        // Special case when the Jass (Jack of trump) is the last trump in hand:
        // - Can be held till the end, so any other card can be drawn
        if (otherCard.suit.trump && this._nbTrumps(hand) === 1 && this._hasJass(hand)) {
            return true;
        }

        // The player must draw cards of same color
        return this._cardInSuit(myCard, otherCard.suit);
    },

    /**
     * Check if a card is in a suit
     *
     * @param card
     * @param suit
     * @returns {boolean}
     * @private
     */
    _cardInSuit(card, suit) {
        return card.suit.code === suit.code;
    },

    /**
     * Does the player have the Jass (Jack of Trump)?
     *
     * @param hand
     * @returns {boolean}
     * @private
     */
    _hasJass(hand) {
        return hand.find(card => card.suit.trump && card.rank.symbol === 'J') !== undefined;
    },

    /**
     * Does the player have a given suit?
     *
     * @param hand
     * @param someCard
     * @returns {boolean}
     * @private
     */
    _hasSuit(hand, someCard) {
        if (!someCard) return false;

        return hand.find(card => card.suit.code === someCard.suit.code) !== undefined;
    },

    /**
     * How many trumps does the player have?
     *
     * @param hand
     * @returns {number}
     * @private
     */
    _nbTrumps(hand) {
        return hand.filter(card => card.suit.trump).length;
    },

}
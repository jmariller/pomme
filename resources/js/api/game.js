export default {

    /**
     * Join a game
     *
     * @param type
     * @param callback
     * @return {*}
     */
    async join(type, callback) {
        return axios.post('/games', {type: type});
    },

    /**
     * Wait for other players to join
     *
     * @param channel
     * @param callback
     */
    waitForOthers(channel, callback) {
        window.Echo.private(channel).listen('PlayerJoinedGame', ({player}) => callback(player));
    },

    /**
     * Wait for a card drawn from deck
     *
     * @param channel
     * @param callback
     */
    waitForCardDrawnFromDeck(channel, callback) {
        window.Echo.private(channel).listen('PlayerDrewCardFromDeck', data => callback(data));
    },

    /**
     * Wait for same rank drawn from deck
     *
     * @param channel
     * @param callback
     */
    waitForSameRankDrawnFromDeck(channel, callback) {
        window.Echo.private(channel).listen('PlayersDrewSameRankFromDeck', data => callback(data));
    },

    /**
     * Wait for the selection round to be completed
     *
     * @param channel
     * @param callback
     */
    waitForSelectionRoundCompleted(channel, callback) {
        window.Echo.private(channel).listen('SelectionRoundCompleted', data => callback(data));
    },

    /**
     * Wait for a card drawn by others
     *
     * @param channel
     * @param callback
     */
    waitForCardDrawnFromHand(channel, callback) {
        window.Echo.private(channel).listen('PlayerDrewCardFromHand', data => callback(data));
    },

    /**
     * Wait for a player who won melds
     *
     * @param channel
     * @param callback
     */
    waitForPlayerWonMelds(channel, callback) {
        window.Echo.private(channel).listen('PlayerWonMelds', data => callback(data));
    },

    /**
     * Wait to know who is next
     *
     * @param channel
     * @param callback
     */
    waitForWhoIsNext(channel, callback) {
        window.Echo.private(channel).listen('WhoIsNext', data => callback(data));
    },

    /**
     * Wait for a normal round to be completed
     *
     * @param channel
     * @param callback
     */
    waitForNormalRoundCompleted(channel, callback) {
        window.Echo.private(channel).listen('NormalRoundCompleted', data => callback(data));
    },

    /**
     * Wait for a new round to be kicked off
     *
     * @param channel
     * @param callback
     */
    waitForNewRound(channel, callback) {
        window.Echo.private(channel).listen('NewRoundStarted', data => callback(data));
    },

    /**
     * Wait for backup cards
     *
     * @param channel
     * @param callback
     */
    waitForBackupTaken(channel, callback) {
        window.Echo.private(channel).listen('BackupWasTakenByPlayer', data => callback(data));
    },

    /**
     * Wait for trump taken by other player
     *
     * @param channel
     * @param callback
     */
    waitForTookTrump(channel, callback) {
        window.Echo.private(channel).listen('PlayerTookTrump', data => callback(data));
    },

    /**
     * Wait for trump taken by me
     *
     * @param channel
     * @param callback
     */
    waitForTrumpTaken(channel, callback) {
        window.Echo.private(channel).listen('TrumpWasTakenByPlayer', data => callback(data));
    }

}
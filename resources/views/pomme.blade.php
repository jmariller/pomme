<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width,user-scalable=no" />
    <meta name="csrf-token" content="{{ csrf_token() }}" />

    <title>Pomme</title>

    <link rel="stylesheet" href="{{ asset('css/pomme.css') }}">

    <link rel="icon" type="image/png" href="{{ asset('favicon.png') }}" />

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.7.0/animate.min.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.4.1/css/all.css" integrity="sha384-5sAR7xN1Nv6T6+dT2mhtzEpVJvfS3NScPQTrOxhwjIuvcA67KV2R5Jz6kr4abQsz" crossorigin="anonymous">

    <style>
        [v-cloak] {
            display: none;
        }
    </style>
</head>
<body>

    @verbatim
        <div id="root" class="container-fluid" v-cloak>

            <!-- General information on the game: current round, score, etc. -->
            <scores></scores>

            <!-- The results for the last round -->
            <round-results></round-results>

            <!-- 1st row: the opponent for Pomme, or the partner for Jass -->
            <div class="row">
                <div class="col">
                    <other-hand :other_player="$store.getters.opponent"></other-hand>
                </div>
            </div>

            <!-- 2nd row: show the carpet & deck of cards -->
            <div class="row my-4">
                <div class="col">
                    <carpet>
                        <cards-deck></cards-deck>
                    </carpet>
                </div>
            </div>

            <!-- 3rd row: show the current player -->
            <div class="row">
                <div class="col">
                    <player-hand></player-hand>
                </div>
            </div>

        </div>
    @endverbatim

    <!-- Preload images -->
    <div class="d-none">
        <img src="{{ asset('images/backup-icon.png') }}" alt="backup-preload" />
        <img src="{{ asset('images/melds-icon.png') }}" alt="melds-preload" />

        <img src="{{ asset('images/suits/diamonds-small.png') }}" alt="diamonds-preload" />
        <img src="{{ asset('images/suits/hearts-small.png') }}" alt="hearts-preload" />
        <img src="{{ asset('images/suits/clubs-small.png') }}" alt="clubs-preload" />
        <img src="{{ asset('images/suits/spades-small.png') }}" alt="spades-preload" />
    </div>

    <script src="{{ asset('js/manifest.js') }}"></script>
    <script src="{{ asset('js/vendor.js') }}"></script>
    <script src="{{ asset('js/pomme.js') }}"></script>
</body>
</html>
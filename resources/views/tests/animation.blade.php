<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width,user-scalable=no" />
    <meta name="csrf-token" content="{{ csrf_token() }}" />

    <title>Animation - Tests</title>

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">

    <link rel="icon" type="image/png" href="{{ asset('favicon.png') }}" />

    <style>
        [v-cloak] {
            display: none;
        }
    </style>
</head>
<body>

    <div class="container" id="test-animation">
        <h1>Animation - Tests</h1>

        <p>
            <button type="button" class="btn btn-primary" @click="animate = ! animate">Animate</button>
        </p>

        <transition @enter="enter"
                    @leave="leave"
                    :css="false">
            <div id="animation" class="card" style="background-color: #3495e3; width: 120px; height: 150px; z-index: 2" v-show="animate">
            </div>
        </transition>

        <div id="target" style="background-color: #64c8e3; width: 120px; height: 150px; position: absolute; top: 300px; left: 300px; z-index: 1;">Target</div>
    </div>

    <script src="{{ asset('js/manifest.js') }}"></script>
    <script src="{{ asset('js/vendor.js') }}"></script>
    <script src="{{ asset('js/animation-test.js') }}"></script>
</body>
</html>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width,user-scalable=no" />
    <meta name="csrf-token" content="{{ csrf_token() }}" />

    <title>Pomme - 1 VS 1</title>

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">

    <link rel="icon" type="image/png" href="{{ asset('favicon.png') }}" />

    <style>
        [v-cloak] {
            display: none;
        }
    </style>
</head>
<body>

    @verbatim
    <div id="root" class="container pt-3" v-cloak>
        <h4 class="mt-2 mb-4">Simulation Pomme - 1 VS 1</h4>

        <!--=====================-->
        <!-- General information -->
        <!--=====================-->

        <!-- Reset game -->
        <div class="form-group form-inline">
            <button type="button" class="btn btn-primary mr-2" @click="_initGame()">Reset game</button>
        </div>

        <!-- General information on the game -->
        <div>
            <p>
                Next player: {{ current_round.current_player.name }}
                <span v-if="game !== null && game.currentRoundIndex() === 0"> (Starter selection, pick a card in the deck below)</span>
            </p>
        </div>

        <div>
            <p>
                Round: {{ game === null ? 0 : game.currentRoundIndex() }} [Started with {{ current_round.first_player.name }}]
            </p>
            <button type="button" class="btn btn-primary" @click="emptyHands()" v-if="game !== null && game.currentRoundIndex() === 0 && current_round.currentTrick().hasDraws()">
                Draw! Click here to take another card
            </button>
        </div>

        <div v-if="current_round.cards_deck.trump !== undefined">
            <label>Trump: {{ `${current_round.cards_deck.trump.toString()}` }}</label>
        </div>

        <hr />

        <!--=================-->
        <!-- Player 1's hand -->
        <!--=================-->
        <div class="form-group">
            <label for="player-1-hand">
                {{ `${game.players[0].name}'s hand (trumps: ${game.players[0].nbTrumps()}, points: ${game.players[0].points()}, rounds won: ${game.players[0].rounds_won}, pommes: ${game.players[0].pommes})` }}
            </label>

            <!-- This is to select final cards -->
            <select multiple size="9" class="form-control" id="player-1-hand" v-model="game.players[0].discarded" v-if="!game.players[0].ready">
                <option v-for="card1 in game.players[0].hand" :value="card1" :class="card1.isTrump() ? 'text-success' : ''">
                    {{ card1.toString() }}
                </option>
            </select>

            <!-- This is the actual hand -->
            <div class="form-inline" v-if="game.players[0].ready">
                <select class="form-control" id="player-2-hand" v-model="card1" :disabled="current_round.current_player !== game.players[0]">
                    <option v-for="card1 in game.players[0].hand" :value="card1" :disabled="card2 !== undefined ? !game.players[0].canDrawCard(card1, card2) : false" :class="card1.isTrump() ? 'text-success' : ''">
                        {{ `${card1.toString()}` }}
                    </option>
                </select>
                <button type="button" class="btn btn-primary ml-2" @click="play(card1)" v-if="game.players[1].ready" :disabled="current_round.current_player !== game.players[0] || card1 === undefined">Play!</button>
            </div>

            <div v-if="game.players[0].melds === undefined ? '': game.players[0].melds.has_melds">
                <h6 class="mt-3 mb-2">Melds:</h6>
                <ul class="list-group mb-3 mt-2 w-50">
                    <li class="list-group-item list-group-item-info d-flex justify-content-between align-items-center" v-for="meld in game.players[0].melds.toList()">
                        {{ `${meld.name} ${meld.top !== '' ? ('(' + meld.top.toString() + ')') : ''}` }}
                        <span class="badge badge-primary badge-pill">{{ meld.points }} points</span>
                    </li>
                </ul>
            </div>

            <button type="button" class="btn btn-primary mt-2" @click="game.players[0].sortCards()" :disabled="game.players[0].hand.length <= 1">Sort cards</button>

            <!-- Backup hand -->
            <div class="form-inline mt-2" v-if="!game.players[0].ready">
                <label for="player-1-backup">Backup ({{ game.players[0].backup.length }}):</label>
                <select id="player-1-backup" class="form-control ml-2">
                    <option v-for="card in game.players[0].backup" :class="card.isTrump() ? 'text-success' : ''">
                        {{ card.toString() }}
                    </option>
                </select>
            </div>

            <!-- Ready to start the round -->
            <div class="form-inline mt-2" v-if="game.currentRoundIndex() > 0 && !game.players[0].ready">
                <button type="button" class="btn btn-primary" @click="game.players[0].ready = true">Start!</button>
                <button type="button" class="btn btn-secondary ml-2" @click="takeBackup(game.players[0])" v-if="game.players[0].backup.length !== 0" :disabled="game.players[0].discarded.length !== CARDS_IN_BACKUP">Take backup</button>
                <button type="button" class="btn btn-secondary ml-2" @click="exchangeSixOfTrump(game.players[0])" v-if="current_round.cards_deck.trump !== undefined && game.players[0].hasSixOfTrump(current_round.cards_deck.trump)">
                    Grab the {{ `${current_round.cards_deck.trump.toString()}` }}
                </button>
            </div>
        </div>

        <hr />

        <!--=================-->
        <!-- Player 2's hand -->
        <!--=================-->
        <div class="form-group">
            <label for="player-2-hand" class="mr-2">
                {{ `${game.players[1].name}'s hand (trumps: ${game.players[1].nbTrumps()}, points: ${game.players[1].points()}, rounds won: ${game.players[1].rounds_won}, pommes: ${game.players[1].pommes})` }}
            </label>

            <!-- This is to select final cards -->
            <select multiple size="9" class="form-control" id="player-2-hand" v-model="game.players[1].discarded" v-if="!game.players[1].ready">
                <option v-for="card2 in game.players[1].hand" :value="card2" :class="card2.isTrump() ? 'text-success' : ''">
                    {{ card2.toString() }}
                </option>
            </select>

            <!-- This is the actual hand -->
            <div class="form-inline" v-if="game.players[1].ready">
                <select class="form-control" id="player-2-hand" v-model="card2" :disabled="current_round.current_player !== game.players[1]">
                    <option v-for="card2 in game.players[1].hand" :value="card2" :disabled="card1 !== undefined ? !game.players[1].canDrawCard(card2, card1) : false" :class="card2.isTrump() ? 'text-success' : ''">
                        {{ card2.toString() }}
                    </option>
                </select>
                <button type="button" class="btn btn-primary ml-2" @click="play(card2)" v-if="game.players[0].ready" :disabled="current_round.current_player !== game.players[1] || card2 === undefined">Play!</button>
            </div>

            <div v-if="game.players[1].melds === undefined ? '': game.players[1].melds.has_melds">
                <h6 class="mt-3 mb-2">Melds:</h6>
                <ul class="list-group mb-3 mt-2 w-50">
                    <li class="list-group-item list-group-item-info d-flex justify-content-between align-items-center" v-for="meld in game.players[1].melds.toList()">
                        {{ `${meld.name} ${meld.top !== '' ? ('(' + meld.top.toString() + ')') : ''}` }}
                        <span class="badge badge-primary badge-pill">{{ meld.points }} points</span>
                    </li>
                </ul>
            </div>

            <button type="button" class="btn btn-primary mt-2" @click="game.players[1].sortCards()" :disabled="game.players[1].hand.length <= 1">Sort cards</button>

            <!-- Backup hand -->
            <div class="form-inline mt-2" v-if="!game.players[1].ready">
                <label for="player-2-backup">Backup ({{ game.players[1].backup.length }}):</label>
                <select id="player-2-backup" class="form-control ml-2">
                    <option v-for="card in game.players[1].backup" :class="card.isTrump() ? 'text-success' : ''">
                        {{ `${card.toString()}` }}
                    </option>
                </select>
            </div>

            <!-- Ready to start the round -->
            <div class="form-inline mt-2" v-if="game.currentRoundIndex() > 0 && !game.players[1].ready">
                <button type="button" class="btn btn-primary" @click="game.players[1].ready = true">Start!</button>
                <button type="button" class="btn btn-secondary ml-2" @click="takeBackup(game.players[1])" v-if="game.players[1].backup.length !== 0" :disabled="game.players[1].discarded.length !== CARDS_IN_BACKUP">Take backup</button>
                <button type="button" class="btn btn-secondary ml-2" @click="exchangeSixOfTrump(game.players[1])" v-if="current_round.cards_deck.trump !== undefined && game.players[1].hasSixOfTrump(current_round.cards_deck.trump)">
                    Grab the {{ `${current_round.cards_deck.trump.toString()}` }}
                </button>
            </div>
        </div>

        <!--================-->
        <!-- List of tricks -->
        <!--================-->
        <div class="container" v-if="game.players[0].ready && game.players[1].ready">
            <hr class="mt-4 mb-4" />

            <h5>Current round:</h5>

            <div class="p-2 text-white" style="background-color: #2a9055; min-height: 40px">
                <ul>
                    <li v-for="trick in current_round.tricks">
                        {{ `${trick.cards[0] !== undefined ? trick.cards[0].toString() + ' (' + trick.cards[0].player.name + ')'  : '...'} - ${trick.cards[1] !== undefined ? trick.cards[1].toString() + ' (' + trick.cards[1].player.name + ')' : '...'} => ${trick.winner_player !== undefined ? trick.winner_player.name : ''}` }}
                    </li>
                </ul>
            </div>

            <!-- Ready to start next round! -->
            <div class="form-group mt-3" v-if="current_round.completed()">
                <button type="button" class="btn btn-primary" @click="newRound(current_round.nextPlayer())">Next round!</button>
            </div>
        </div>

        <hr class="mt-5 mb-5" />

        <!--=======================-->
        <!-- Current state of deck -->
        <!--=======================-->
        <div class="container">
            <h5 class="mb-3">Current deck of cards ({{ current_round.cards_deck.cards.length }})</h5>
            <table class="table table-hover">
                <thead class="thead-light">
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Suit</th>
                    <th scope="col">Rank</th>
                    <th scope="col">Points</th>
                </tr>
                </thead>
                <tbody>
                <tr v-for="(card, index) in current_round.cards_deck.cards" :class="card.suit.trump ? 'table-success' : ''" @click="grabCardForSelection(card)">
                    <td>{{ index + 1 }}</td>
                    <td>{{ card.suit.name }}</td>
                    <td>{{ card.rank.symbol }}</td>
                    <td>{{ card.rank.points }}</td>
                </tr>
                </tbody>
            </table>
        </div>

    </div>

    @endverbatim

    <script src="{{ asset('js/manifest.js') }}"></script>
    <script src="{{ asset('js/vendor.js') }}"></script>
    <script src="{{ asset('js/pomme-test.js') }}"></script>
</body>
</html>
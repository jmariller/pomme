<?php

return [

    'misc' => [
        'of' => 'of',
    ],

    'ranks' => [
        'A' => 'Ace',
        'K' => 'King',
        'Q' => 'Queen',
        'JT' => 'Jass',
        'J' => 'Jack',
        '10' => 'Ten',
        '9T' => 'Nell',
        '9' => 'Nine',
        '8' => 'Eight',
        '7' => 'Seven',
        '6' => 'Six',
    ],

    'melds' => [
        'four_jacks' => 'Two Hundred',
        'four_nines' => 'One Hundred and Fifty',
        'sequence_9' => 'Hundred',
        'sequence_8' => 'Hundred',
        'sequence_7' => 'Hundred',
        'sequence_6' => 'Hundred',
        'sequence_5' => 'Hundred',
        'four_aces' => 'Hundred',
        'four_kings' => 'Hundred',
        'four_queens' => 'Hundred',
        'four_tens' => 'Hundred',
        'sequence_4' => 'Fifty',
        'sequence_3' => 'Three Cards',
        'royal_couple' => 'Stöck'
    ]
];
